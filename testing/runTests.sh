#!${BASH}
runTests() {
  echo -e "Loading... Runner 'runTests', version 0.1"
}


saveConditionsToRunTests() {
  local theAction="${1:-false}"
  local theContent="${2:-false}"

  exceptTheGroupTests=false
  onlyTheGroupTests=false
  considerLaunchAllTests=false

  if [ "${theAction[@]}" = false ] || [ "${theContent[@]}" = false ]
    then
      considerLaunchAllTests=true
  fi 

  case ${theAction} in
    "--exceptTheGroupTests" )
      exceptTheGroupTests="${theContent[@]}"
    ;;
    "--onlyTheGroupTests" )
      onlyTheGroupTests="${theContent[@]}"
    ;;
  esac
}


launchTests() {
  local theAction="${1:-false}"
  local theTest="${2:-false}"
  local validationInitialize

  saveConditionsToRunTests "${theAction[@]}" "${theTest[@]}"

  local title="Coded As Human"
  identifyTheTag=$(date +%s%N)
  header --describe "${title[@]}" --alias "LaunchTests" --labels "CAH" --momentum "${identifyTheTag[@]}" --parent "launchTests" --explanation ""
  initializeViewDocument
  validationInitialize=$?
  if [ "${validationInitialize}" == ${validatedTheCodeWithFALSE} ]
    then
      echo "${messageReturnedByFunction[ErrorInInitializeViewDocument]}"
      return ${validatedTheCodeWithFALSE}
  fi

  local savedCurrentVisibilityTitle=$showTitleMessages
  local savedCurrentVisibilityAbstract=$showAbstractMessages
  local savedCurrentVisibilityResult=$showResultMessages
  local savedCurrentVisibilityExplanation=$showExplanationMessages
  local savedCurrentVisibilityTask=$showSpecificationsSummary
  local savedCurrentVisibilityExpectSUCCESS=$showExpectSUCCESS
  local savedCurrentVisibilityExpectERROR=$showExpectERROR

  local savedhideDivisionsInUsersInvolved=("${hideDivisionsInUsersInvolved[@]}")
  local savedhideDivisionsInSpecification=("${hideDivisionsInSpecification[@]}")
  local savedhideDivisionsInSelectLanguage=("${hideDivisionsInSelectLanguage[@]}")
  local savedHideDivisionsInEnsureTool=("${hideDivisionsInEnsureTool[@]}")
  local savedHideDivisionsInUninstallTool=("${hideDivisionsInUninstallTool[@]}")
  local savedHideDivisionsInRefreshRepository=("${hideDivisionsInRefreshRepository[@]}")
  local savedHideDivisionsInUpdateAllPackages=("${hideDivisionsInUpdateAllPackages[@]}")
  local savedHideDivisionsInEnsureRepository=("${hideDivisionsInEnsureRepository[@]}")
  local savedHideDivisionsInEraseRepository=("${hideDivisionsInEraseRepository[@]}")
  local savedHideDivisionsInSetUpDistro=("${hideDivisionsInSetUpDistro[@]}")
  local savedHideDivisionsInEnsureManagementPanel=("${hideDivisionsInEnsureManagementPanel[@]}")
  local savedHideDivisionsInEraseManagementPanel=("${hideDivisionsInEraseManagementPanel[@]}")
  local savedHideDivisionsInCopyManagementPanel=("${hideDivisionsInCopyManagementPanel[@]}")
  local savedHideDivisionsInMoveManagementPanel=("${hideDivisionsInMoveManagementPanel[@]}")
  local savedHideDivisionsInEnsureBriefingTask=("${hideDivisionsInEnsureBriefingTask[@]}")
  local savedHideDivisionsInEraseBriefingTask=("${hideDivisionsInEraseBriefingTask[@]}")
  local savedHideDivisionsInEnsureRecipeIngredient=("${hideDivisionsInEnsureRecipeIngredient[@]}")
  hideDivisionsInUsersInvolved=(false)
  hideDivisionsInSpecification=("result" "abstract" "explanation")
  hideDivisionsInSelectLanguage=(false)
  hideDivisionsInEnsureTool=(false)
  hideDivisionsInUninstallTool=(false)
  hideDivisionsInRefreshRepository=(false)
  hideDivisionsInUpdateAllPackages=(false)
  hideDivisionsInEnsureRepository=(false)
  hideDivisionsInEraseRepository=(false)
  hideDivisionsInSetUpDistro=(false)
  hideDivisionsInEnsureManagementPanel=(false)
  hideDivisionsInEraseManagementPanel=(false)
  hideDivisionsInCopyManagementPanel=(false)
  hideDivisionsInMoveManagementPanel=(false)
  hideDivisionsInEnsureBriefingTask=(false)
  hideDivisionsInEraseBriefingTask=(false)
  hideDivisionsInEnsureRecipeIngredient=(false)

  showTitleMessages=true
  showAbstractMessages=true
  showResultMessages=true
  showExplanationMessages=true
  showSpecificationsSummary=true
  showExpectSUCCESS=true
  showExpectERROR=true


  local savedRunbookStatus=${theRunbookPauseIs}
  theRunbookPauseIs=true

  local unloadedPath="unloaded"

  local fixturePath="fixture"
  loadAddOn "${testingPath}" "${fixturePath}"

  loadAddOn "${testingPath}" "infrastructure"

  loadAddOn "${testingPath}" "integration"

  loadAddOn "${testingPath}" "endToEnd"

  loadAddOn "${testingPath}" "unit"

  showTitleMessages=$savedCurrentVisibilityTitle
  showAbstractMessages=$savedCurrentVisibilityAbstract
  showResultMessages=$savedCurrentVisibilityResult
  showExplanationMessages=$savedCurrentVisibilityExplanation
  showSpecificationsSummary=$savedCurrentVisibilityTask
  showExpectSUCCESS=$savedCurrentVisibilityExpectSUCCESS
  showExpectERROR=$savedCurrentVisibilityExpectERROR

  hideDivisionsInUsersInvolved=("${savedhideDivisionsInUsersInvolved[@]}")
  hideDivisionsInSpecification=("${savedhideDivisionsInSpecification[@]}")
  hideDivisionsInSelectLanguage=("${savedhideDivisionsInSelectLanguage[@]}")
  hideDivisionsInEnsureTool=("${savedHideDivisionsInEnsureTool[@]}")
  hideDivisionsInUninstallTool=("${savedHideDivisionsInUninstallTool[@]}")
  hideDivisionsInRefreshRepository=("${savedHideDivisionsInRefreshRepository[@]}")
  hideDivisionsInUpdateAllPackages=("${savedHideDivisionsInUpdateAllPackages[@]}")
  hideDivisionsInEnsureRepository=("${savedHideDivisionsInEnsureRepository[@]}")
  hideDivisionsInEraseRepository=("${savedHideDivisionsInEraseRepository[@]}")
  hideDivisionsInSetUpDistro=("${savedHideDivisionsInSetUpDistro[@]}")
  hideDivisionsInEnsureManagementPanel=("${savedHideDivisionsInEnsureManagementPanel[@]}")
  hideDivisionsInEraseManagementPanel=("${savedHideDivisionsInEraseManagementPanel[@]}")
  hideDivisionsInCopyManagementPanel=("${savedHideDivisionsInCopyManagementPanel[@]}")
  hideDivisionsInMoveManagementPanel=("${savedHideDivisionsInMoveManagementPanel[@]}")
  hideDivisionsInEnsureBriefingTask=("${savedHideDivisionsInEnsureBriefingTask[@]}")
  hideDivisionsInEraseBriefingTask=("${savedHideDivisionsInEraseBriefingTask[@]}")
  hideDivisionsInEnsureRecipeIngredient=("${savedHideDivisionsInEnsureRecipeIngredient[@]}")

  theRunbookPauseIs=${savedRunbookStatus}
}

#!${BASH}
managementPanelTests() {
  echo -e "Loading... Testing 'managementPanelTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="InfrastructureManagementPanelTests"

managementPanelTestingTheFolder() {
  local getMessageFolder
  local checkFolder
  local getMessageDestinationFolder
  local checkDestinationFolder

  header --context "Testing management panel (folders)" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local theLocation="$PWD"
    local theFolder="$theLocation/managementPanel"
    local destinationFolder="$theLocation/managementPanelCopy"

    header --section "Check folder creation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      getMessageFolder=$(checkIfFolderExist "${theFolder}")
      checkFolder=$?
      if [ "${checkFolder}" = ${validatedTheCodeWithFALSE} ]
        then
          local result=$(createFolder "${theFolder}")
      fi
      getMessageFolder=$(checkIfFolderExist "${theFolder}")
      checkFolder=$?

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkFolder}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Check folder deletion" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      getMessageFolder=$(checkIfFolderExist "${theFolder}")
      checkFolder=$?
      if [ "${checkFolder}" = ${validatedTheCodeWithTRUE} ]
        then
          result=$(removeFolder "${theFolder}")
      fi
      getMessageFolder=$(checkIfFolderExist "${theFolder}")
      checkFolder=$?

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkFolder}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Check folder copy" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "Create origin folder" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        getMessageFolder=$(checkIfFolderExist "${theFolder}")
        checkFolder=$?
        if [ "${checkFolder}" = ${validatedTheCodeWithFALSE} ]
          then
            result=$(createFolder "${theFolder}")
        fi
        getMessageFolder=$(checkIfFolderExist "${theFolder}")
        checkFolder=$?

        weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkFolder}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "Create destination folder" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        getMessageDestinationFolder=$(checkIfFolderExist "${destinationFolder}")
        checkDestinationFolder=$?
        if [ "${checkDestinationFolder}" = ${validatedTheCodeWithFALSE} ]
          then
            result=$(createFolder "${destinationFolder}")
        fi
        getMessageDestinationFolder=$(checkIfFolderExist "${destinationFolder}")
        checkDestinationFolder=$?

        weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkDestinationFolder}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --subsection "Copy folder" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        getMessageFolder=$(checkIfFolderExist "${theFolder}")
        checkFolder=$?
        getMessageDestinationFolder=$(checkIfFolderExist "${destinationFolder}")
        checkDestinationFolder=$?

        if [ "$checkFolder" = ${validatedTheCodeWithTRUE} ] && [ "$checkDestinationFolder" = ${validatedTheCodeWithTRUE} ]
          then
            result=$(copyFolder "${theFolder}" "${destinationFolder}")
        fi

        getMessageDestinationFolder=$(checkIfFolderExist "${destinationFolder}")
        checkDestinationFolder=$?

        weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkDestinationFolder}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --subsection "Clean environment: delete origin folder" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        getMessageFolder=$(checkIfFolderExist "${theFolder}")
        checkFolder=$?
        if [ "${checkFolder}" = ${validatedTheCodeWithTRUE} ]
          then
            result=$(removeFolder "${theFolder}")
        fi
        getMessageFolder=$(checkIfFolderExist "${theFolder}")
        checkFolder=$?

        weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkFolder}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --subsection "Clean environment: delete destination folder" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        getMessageDestinationFolder=$(checkIfFolderExist "${destinationFolder}")
        checkDestinationFolder=$?
        if [ "${checkDestinationFolder}" = ${validatedTheCodeWithTRUE} ]
          then
            result=$(removeFolder "${destinationFolder}")
        fi
        checkDestinationFolder=$(checkIfFolderExist ${destinationFolder})
        getMessageDestinationFolder=$(checkIfFolderExist "${destinationFolder}")
        checkDestinationFolder=$?

        weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkDestinationFolder}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests managementPanelTestingTheFolder


managementPanelTestingTheFile() {
  local getMessageFileExist
  local checkFile

  header --context "Testing management panel (files)" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local theLocation="$PWD"
    local theFile="oneDocument.txt"

    header --section "Check creation file" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local thePath="${theLocation}/${theFile}"
      getMessageFileExist=$(checkIfFileExist "${thePath}")
      checkFile=$?
      if [ "${checkFile}" = ${validatedTheCodeWithFALSE} ]
        then
          local result=$(createFile "${thePath}")
      fi
      getMessageFileExist=$(checkIfFileExist "${thePath}")
      checkFile=$?

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkFile}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Check 'soft deletion' file" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      thePath="${theLocation}/${theFile}"
      getMessageFileExist=$(checkIfFileExist "${thePath}")
      checkFile=$?
      if [ "${checkFile}" = ${validatedTheCodeWithTRUE} ]
        then
          result=$(softDeleteFile "${thePath}")
      fi
      getMessageFileExist=$(checkIfFileExist "${thePath}")
      checkFile=$?

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkFile}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests managementPanelTestingTheFile



managementPanelTestingContentOfFile() {
  local getMessageFileExist
  local checkFile

  header --context "Testing management panel (files content)" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local theLocation="$PWD"
    local theFile="oneDocument.txt"
    local thePath="${theLocation}/${theFile}"
    getMessageFileExist=$(checkIfFileExist "${thePath}")
    checkFile=$?
    if [ "${checkFile}" = ${validatedTheCodeWithFALSE} ]
      then
        local result=$(createFile "${thePath}")
    fi
    getMessageFileExist=$(checkIfFileExist "${thePath}")
    checkFile=$?

    header --section "Add content to file" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      if [ "${checkFile}" = ${validatedTheCodeWithTRUE} ]
        then
          local theLine="one line to add"
          result=$(addLineToFile "${theLine[@]}" "${thePath}")
      fi
      local getContent=$(cat "${thePath}")

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${getContent[@]}" --sentence "contain" --secondOperator "${theLine[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    header --section "Fixture: remove '${thePath}' file" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    result=$(softDeleteFile "${thePath}")

} ; launchGroupTests managementPanelTestingContentOfFile


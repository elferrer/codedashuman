#!${BASH}
repositoryTests() {
  echo -e "Loading... Testing 'repositoryTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="InfrastructureRepositoryTests"

repositoryTesting() {
  header --context "Check for pending application updates" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local dateNow=$(currentDate)

    refreshRepository --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}"
    
    local file="/var/cache/apt/pkgcache.bin"

    waitForTheFileToBeCreated "${file}"

    local dateOfLastUpdate=$(lastDateChangeInTheFile "${file}")

    if [[ "${dateNow}" < "${dateOfLastUpdate}" ]]
      then
        local cacheIsUpdated=true
      else
        local cacheIsUpdated=false
    fi

  weWishes --messageInterpretation "theCheckIs" --firstOperator "${cacheIsUpdated}" --sentence "=" --secondOperator "true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "Update the packages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    updateRepositoryPackages --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}"
    noUpdatesPending=$?

  weWishes --messageInterpretation "theCheckIs" --firstOperator "${noUpdatesPending}" --sentence "=" --secondOperator "0" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests repositoryTesting


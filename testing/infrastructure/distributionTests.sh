#!${BASH}
distributionTests() {
  echo -e "Loading... Testing 'distributionTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="InfrastructureDistributionTests"

distributionTesting() {

  header --context "Testing distributions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --section "Load accepted distributions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local catchDistroList=$(catchDistributionList)

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${#catchDistroList}" --sentence ">" --secondOperator "0" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Check if my distribution isn't in the list" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local myDistro="AnInexistentDistribution"
      local withDistroList=("debian" "ubuntu" "alpine")

      local validation=$(checkDistribution ${myDistro} ${withDistroList[@]})

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${validation}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Check if my distribution is the primary" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local myDistro="debian"
      local withDistroList=("debian" "ubuntu" "alpine")

      local validation=$(checkPrimaryDistribution ${myDistro} ${withDistroList[@]})

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${validation}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Load the first available distribution" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... and this isn't 'my distro'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local actualDistro=${distribution}
        local myDistro="AnInexistentDistribution"

        local select=$(useDistro --name ${myDistro} --alias "${aliasTests}")

        weWishes --messageInterpretation "theCheckIs" --firstOperator "${select}" --sentence "not contain" --secondOperator "${myDistro}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... and this is an alternate distribution" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local actualDistro=${distribution}
        local myDistro="AnInexistentDistribution"

        local select=$(useDistro --name ${myDistro} --alias "${aliasTests}")

        weWishes --messageInterpretation "theCheckIs" --firstOperator "${select}" --sentence "contain" --secondOperator "${actualDistro}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  header --context "Testing repository" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --section "Load list of repositories" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local catchDistroList=$(listAptEntries)

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${#catchDistroList}" --sentence ">" --secondOperator "0" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Check if one repository isn't in the list" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local oneRepository="AnInexistentRepository"
      local check
      check=$(isTheRepositoryInTheList ${oneRepository})
      local validation=$?

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${validation}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Check if one repository is within the list" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      case "$distribution" in
        "ubuntu" ) 
          local oneRepository="archive.ubuntu.com/ubuntu" ;;
        "neon" ) 
          local oneRepository="archive.neon.kde.org/" ;;
        "debian" ) 
          local oneRepository="deb.debian.org/debian" ;;
      esac

      local check
      check=$(isTheRepositoryInTheList ${oneRepository})
      local validation=$?

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${validation}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests distributionTesting


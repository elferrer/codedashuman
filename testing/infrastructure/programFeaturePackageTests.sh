#!${BASH}
programFeaturePackageTests() {
  echo -e "Loading... Testing 'programFeaturePackageTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="InfrastructureProgramPackageTests"

programFeaturePackageTesting() {
  local getMessagePackage
  local existPackage
  local existThePackage

  header --context "Testing package features" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --section "Probe to install and uninstall a package" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      local tool="Package"
      local theProgramName="debian-faq"
      local thePackageName="debian-faq_9.0_all.deb"
      local thePackageUrl="http://ftp.es.debian.org/debian/pool/main/d/debian-faq/"
      local result=""
      getMessagePackage=$(isTheToolInstalled "${tool}" "${theProgramName[@]}")
      existPackage=$?
      case ${existPackage} in
        ${validatedTheCodeWithTRUE} )
            header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        ;;

        ${validatedTheCodeWithFALSE} )

          header --subsection "Download ${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          wget "${thePackageUrl[@]}/${thePackageName[@]}" "${thePackageName[@]}"

          header --subsection "Installing ${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
            local resultOfAction=$(installTool "${tool}" "${thePackageName[@]}")
            getMessagePackage=$(isTheToolInstalled "${tool}" "${theProgramName[@]}")
            existThePackage=$?

            weWishes --messageInterpretation "theCheckIs" --firstOperator "${existThePackage}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Uninstalling ${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

            resultOfAction=$(removeTool "Program" "${theProgramName[@]}")
            getMessagePackage=$(isTheToolInstalled "${tool}" "${theProgramName[@]}")
            existThePackage=$?

            weWishes --messageInterpretation "theCheckIs" --firstOperator "${existThePackage}" --sentence "!=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Remove ${thePackageName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          rm "${thePackageName[@]}"
        ;;
      esac

} ; launchGroupTests programFeaturePackageTesting


#!${BASH}
programFeatureProgramTests() {
  echo -e "Loading... Testing 'programFeatureProgramTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="InfrastructureProgramTests"

programFeatureProgramTesting() {
  local getMessageProgram
  local existProgram
  local existTheProgram

  header --context "Testing program features" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --section "Probe to install and uninstall a program" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      local tool="Program"
      local suggestedProgramsForTest=("tkinfo" "htop" "zipcmp")
      local theToolName
        for theToolName in ${suggestedProgramsForTest[@]}
          do
            local result=""
            getMessageProgram=$(isTheToolInstalled "${tool}"  "${theToolName[@]}")
            existProgram=$?
            case ${existProgram} in
              ${validatedTheCodeWithTRUE} )
                  header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
                  continue
              ;;

              ${validatedTheCodeWithFALSE} )

                header --subsection "Installing ${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                  local resultOfAction=$(installTool "${tool}" "${theToolName[@]}")
                  getMessageProgram=$(isTheToolInstalled "${tool}" "${theToolName[@]}")
                  existTheProgram=$?

                  weWishes --messageInterpretation "theCheckIs" --firstOperator "${existTheProgram}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                header --subsection "Uninstalling ${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                  resultOfAction=$(removeTool "${tool}" "${theToolName[@]}")
                  getMessageProgram=$(isTheToolInstalled "${tool}" "${theToolName[@]}")
                  existTheProgram=$?

                  weWishes --messageInterpretation "theCheckIs" --firstOperator "${existTheProgram}" --sentence "!=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
                break
              ;;
            esac
        done

} ; launchGroupTests programFeatureProgramTesting


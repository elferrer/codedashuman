#!${BASH}
environmentTests() {
  echo -e "Loading... Testing 'environmentTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="InfrastructureEnvironmentTests"

environmentTesting() {
  local getMessage
  local check


  header --context "Check safe space" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local localPath="${userFolder[0]}"

    getMessage=$(checkIfItIsInUserFolder ${localPath})
    check=$?
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${check}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    getMessage=$(checkIfPathIsValid ${localPath})
    check=$?
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${check}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "Check temp space" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local localPath="${tempFolder}"

    getMessage=$(checkIfItIsInTempFolder ${localPath})
    check=$?
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${check}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    getMessage=$(checkIfPathIsValid ${localPath})
    check=$?
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${check}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "Check protected space" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local localPath="${protectedFolder[0]}"

    getMessage=$(checkIfItIsInProtectedFolder ${localPath})
    check=$?
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${check}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    getMessage=$(checkIfPathIsValid ${localPath})
    check=$?
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${check}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "Check unsafe space" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local localPath="/opt"

    getMessage=$(checkIfPathIsValid ${localPath})
    check=$?

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${check}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests environmentTesting


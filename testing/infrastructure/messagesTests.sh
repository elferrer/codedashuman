#!${BASH}
messagesTests() {
  echo -e "Loading... Testing 'messagesTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="InfrastructureMessageTests"

basicVariablesTesting() {

  local withoutText=""
  local readFalseMessage=$(saveOrSeeValidatedMessage "abstract" "${withoutText}" ${validatedTheCodeWithTRUE} ${systemView})
  local lenghtInOneEmptyTextString=${#readFalseMessage}

  header --context "Check the basic variables for tests" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local messagesToTest=("unloadVariable" "unloadFunction" "titleDescribe" "titleContext" "titleSection" "titleSubsection" "tagTitleComments" "tagAbstract" "tagExplanation" "tagStatus")

  for taggedMessages in ${messagesToTest[@]}
    do
      header --section "Check the message ${taggedMessages}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        header --subsection "Check if are visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          local titleOnTesting="ARE VISIBLE"

          local savedCurrentVisibility=$globalShow

          showFromHere
          local minimumLenght=$((${#titleOnTesting} + ${lenghtInOneEmptyTextString}))

          local showLabel=true
          local viewMessage=$(constructionOfMessages "${taggedMessages}" "${titleOnTesting[@]}" ${validatedTheCodeWithTRUE} ${systemView} ${showLabel})

          local check=$(saveOrSeeValidatedMessage "abstract" "${viewMessage[@]}" ${validatedTheCodeWithTRUE} ${systemView})
          local globalShow=$savedCurrentVisibility

        weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#check} --sentence ">" --secondOperator ${minimumLenght} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --subsection "Check if aren't visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local titleOnTesting="ARE VISIBLE"

          local savedCurrentVisibility=$globalShow
          hideFromHere
          local minimumLenght=$((${#titleOnTesting} + ${lenghtInOneEmptyTextString}))
          local showLabel=true
          local viewMessage=$(constructionOfMessages "${taggedMessages}" "${titleOnTesting[@]}" ${validatedTheCodeWithTRUE} ${systemView} ${showLabel})

          local check=$(saveOrSeeValidatedMessage "abstract" "${viewMessage[@]}" ${validatedTheCodeWithTRUE} ${systemView})
          local globalShow=$savedCurrentVisibility

        weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#check} --sentence "<" --secondOperator ${minimumLenght} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
  done

} ; launchGroupTests basicVariablesTesting


theTitlesTesting() {
  local withoutText=""
  local readFalseMessage=$(saveOrSeeValidatedMessage "title" "${withoutText}" ${validatedTheCodeWithTRUE} ${systemView})
  local lenghtInOneEmptyTextString=${#readFalseMessage}

  local savedCurrentVisibility=$globalShow


  header --context "The tipus 'header --describe'..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --section "... are visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local titleOnTesting="ARE VISIBLE"
        showFromHere
        local minimumLenght=$((${#titleOnTesting} + ${lenghtInOneEmptyTextString}))
        header --describe "${titleOnTesting[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local check=$(header --describe "${titleOnTesting}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#check} --sentence ">" --secondOperator ${minimumLenght} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "... aren't visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideFromHere
        local minimumLenght=$((${#titleOnTesting} + ${lenghtInOneEmptyTextString}))
        header --describe "${titleOnTesting[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local check=$(header --describe "${titleOnTesting}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#check} --sentence "<" --secondOperator ${minimumLenght} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "The tipus 'header --context'..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --section "... are visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local titleOnTesting="ARE VISIBLE"
        showFromHere
        local minimumLenght=$((${#titleOnTesting} + ${lenghtInOneEmptyTextString}))
        header --context "${titleOnTesting[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local check=$(header --context "${titleOnTesting}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#check} --sentence ">" --secondOperator ${minimumLenght} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "... aren't visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideFromHere
        local minimumLenght=$((${#titleOnTesting} + ${lenghtInOneEmptyTextString}))
        header --context "${titleOnTesting[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local check=$(header --context "${titleOnTesting}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#check} --sentence "<" --secondOperator ${minimumLenght} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "The tipus 'header --section'..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --section "... are visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local titleOnTesting="ARE VISIBLE"
        showFromHere
        local minimumLenght=$((${#titleOnTesting} + ${lenghtInOneEmptyTextString}))
        header --section "${titleOnTesting[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local check=$(header --section "${titleOnTesting}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#check} --sentence ">" --secondOperator ${minimumLenght} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "... aren't visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideFromHere
        local minimumLenght=$((${#titleOnTesting} + ${lenghtInOneEmptyTextString}))
        header --section "${titleOnTesting[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local check=$(header --section "${titleOnTesting}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#check} --sentence "<" --secondOperator ${minimumLenght} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "The tipus 'header --subsection'..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --section "... are visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local titleOnTesting="ARE VISIBLE"
        showFromHere
        local minimumLenght=$((${#titleOnTesting} + ${lenghtInOneEmptyTextString}))
        header --subsection "${titleOnTesting[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local check=$(header --subsection "${titleOnTesting}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#check} --sentence ">" --secondOperator ${minimumLenght} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "... aren't visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideFromHere
        local minimumLenght=$((${#titleOnTesting} + ${lenghtInOneEmptyTextString}))
        header --subsection "${titleOnTesting[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local check=$(header --subsection "${titleOnTesting}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#check} --sentence "<" --secondOperator ${minimumLenght} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  globalShow=$savedCurrentVisibility

} ; launchGroupTests theTitlesTesting


sendMessagesTesting() {

  header --context "Test function 'send messages'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local titleForTesting="Send messages"
  header --section "${titleForTesting}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local nameOfParentFunction="theCheckIs"
  local validation=${validatedTheCodeWithTRUE}
  local firstOperatorInSentence="one comparation"
  local sentence="="
  local secondOperatorInSentence="one comparation"
  local messageReturnedBySystem=${unexplainedError}

  local saveSummaryMessages=${summaryMessages[@]}

  local capturedMessages

  local captureSystemViewMessages=$(tellTheStory "${nameOfParentFunction[@]}" "${validation}" "${firstOperatorInSentence[@]}" "${sentence[@]}" "${secondOperatorInSentence[@]}" "${messageReturnedBySystem[@]}" "${messageReturnedByFunction[ErrorInTest]}" "${aliasTests}" "CAH" "${identifyTheTag}" "" "")
  readarray -t capturedMessages <<<"$captureSystemViewMessages"

  local capturedAbstract=${capturedMessages[0]}
  local capturedResult=${capturedMessages[1]}
  local capturedExplanation=${capturedMessages[2]}
  local capturedExplanation+=${capturedMessages[3]}
  local capturedExplanation+=${capturedMessages[4]}
  local capturedExplanation+=${capturedMessages[5]}
  local capturedExplanation+=${capturedMessages[6]}


  header --subsection "The system view messages contain the messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${capturedAbstract[@]}" --sentence "contain" --secondOperator "${firstOperatorInSentence}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${capturedResult[@]}" --sentence "contain" --secondOperator "${validation}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${capturedExplanation[@]}" --sentence "contain" --secondOperator "${messageReturnedBySystem}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  header --subsection "The system print messages contain the messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${summaryMessages[@]}" --sentence "contain" --secondOperator "${firstOperatorInSentence}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${summaryMessages[@]}" --sentence "contain" --secondOperator "${validation}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${summaryMessages[@]}" --sentence "contain" --secondOperator "${messageReturnedBySystem}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  summaryMessages=${saveSummaryMessages[@]}

} ; launchGroupTests sendMessagesTesting



errorMessagesTesting() {

  header --context "Test SUCCESS message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    capturedweWishes=$(weWishes --messageInterpretation "theCheckIs" --firstOperator "a" --sentence "=" --secondOperator "a" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation "" )

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${capturedweWishes[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  header --context "Test ERROR message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    capturedweWishes=$(weWishes --messageInterpretation "theCheckIs" --firstOperator "a" --sentence "=" --secondOperator "b" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation "" )

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${capturedweWishes[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithFALSE} --parent "" --explanation "" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}"


  local languages=("english" "spanish")
  for language in ${languages[@]}
    do
      header --section "Check variable 'error message' for '${language}' language " --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      loadAddOn "${languagesPath}" "${typeOfLanguagePath}" "${language}"

      local variableErrorsToCheck=("ErrorInTest" "ErrorInApplyLanguage" "ErrorInDistribution" "ErrorInApplyToolRemove" "ErrorInApplyToolInstallation" "ErrorInRefreshRepository" "ErrorInUpdateAllPackages" "ErrorInEnsureRepository" "ErrorInEraseRepository" "ErrorInIsTheToolInstalled" "ErrorInAddRecipeIngredientToFile" "ErrorInSoftDeleteBriefingTask" "ErrorInEraseFile" "ErrorInInitTypoCaption"  "ErrorInInitializeViewDocument" "ErrorInMovePanel" "ErrorInCopyPanel" "ErrorInErasePanel" "ErrorInCreatePanel" "ErrorInInterfaceFunctionWithoutMessage" "ErrorInCheckIfPathIsValid" "ErrorInCloseTypoCaption" "ErrorInCompareFolder" "ErrorInconstructTheTask" "ErrorInsurveyTheTask" "ErrorInUsersInvolved" "ErrorInTitleOfProject" "ErrorInValidationUserFolder" "ErrorInValidationTempFolder" "ErrorInValidationProtectedFolder")

      for oneErrorToCheck in ${variableErrorsToCheck[@]}
        do
          header --subsection "Check error message '${oneErrorToCheck}' exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          weWishes --firstOperator ${#messageReturnedByFunction[$oneErrorToCheck]} --sentence ">" --secondOperator 0 --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      done
  done

} ; launchGroupTests errorMessagesTesting


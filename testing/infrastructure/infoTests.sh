#!${BASH}
infoTests() {
  echo -e "Loading... Testing 'infoTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="InfrastructureInfoTests"

infoTesting() {

  header --context "Get info date" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local localDate=$(date +%Y)
    local getDate=$(getInfoOf "Date")

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${getDate[@]}" --sentence "contain" --secondOperator "${localDate}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  header --context "Get info user" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local localUser="$USER"
    local getUser=$(getInfoOf "User")

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${getUser[@]}" --sentence "contain" --secondOperator "${localUser}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  header --context "Get info path" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local localPath="$PWD"
    local getPath=$(getInfoOf "Path")

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${getPath[@]}" --sentence "contain" --secondOperator "${localPath}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests infoTesting


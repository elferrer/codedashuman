#!${BASH}
programFeatureGemTests() {
  echo -e "Loading... Testing 'programFeatureGemTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="InfrastructureProgramGemTests"

gemFeatureTesting() {
  local getMessageGem
  local existGem
  local existTheGem

  header --context "Prepare ruby" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  cook --recipe ruby --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}"


  header --context "Testing ruby gem features" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --section "Probe to install and uninstall a gem" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local tool="Gem"
      local theGem="gem-example"
      local result=""
      getMessageGem=$(isTheToolInstalled "${tool}" "${theGem[@]}")
      existGem=$?
      case ${existGem} in
        ${validatedTheCodeWithTRUE} )
          removeGem=$(removeTool "${tool}" "${theGem[@]}")
        ;;
      esac

      header --subsection "Installing ${theGem[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local tool="Gem"
        local resultOfAction=$(installTool "${tool}" "${theGem[@]}")
        getMessageGem=$(isTheToolInstalled "${tool}" "${theGem[@]}")
        existTheGem=$?

        weWishes --messageInterpretation "theCheckIs" --firstOperator "${existTheGem}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "Uninstalling ${theGem[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local tool="Gem"
        resultOfAction=$(removeTool "${tool}" "${theGem[@]}")
        getMessageGem=$(isTheToolInstalled "${tool}" "${theGem[@]}")
        existTheGem=$?

        weWishes --messageInterpretation "theCheckIs" --firstOperator "${existTheGem}" --sentence "!=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      break

} ; launchGroupTests gemFeatureTesting


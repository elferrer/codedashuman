#!${BASH}
actionDistributionTests() {
  echo -e "Loading... Testing 'actionDistributionTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="ActionDistributionTests"

actionDistributionTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive


  header --context "Testing 'distribution' end-to-end interface" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  liftContentFromDinamicVariable --newVariable "tagOfResult" \
    --validation ${validatedTheCodeWithTRUE} \
    --getContentFrom "tagStatus"
  local returnResultFalse="${tagOfResult[0]} ${validatedTheCodeWithFALSE}"
  local returnResultTrue="${tagOfResult[0]} ${validatedTheCodeWithTRUE}"


    header --section "The 'set up distribution' not return an error when it is badly constructed if one of possible distribution not appear in" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local capturedMessage=$(setUpDistro --name "false" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

    weWishes --firstOperator "$theRunbookIsBrokenIs" --sentence "=" --secondOperator "false" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'set up distribution' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local initialInfo="user"
    local endInfo="date"

    local capturedMessage=$(setUpDistro --name "${distribution}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
    local localUser="$USER"
    local localDate=$(date +%Y)

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'set up distribution' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    header --section "The 'set up distribution' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local savedHideDivisionsInSetUpDistro=("${hideDivisionsInSetUpDistro[@]}")

    liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "useDistro"

    liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagExplanation"


    header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local fakeTitle="Title"
    hideDivisionsInSetUpDistro=(false)

    local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; setUpDistro --name "${defaultDistribution}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide title message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInSetUpDistro=("title")
    local captureError=$( setUpDistro --name "${defaultDistribution}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide abstract and explanation messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInSetUpDistro=("abstract" "explanation")
    local captureError=$( setUpDistro --name "${defaultDistribution}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide result message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInSetUpDistro=("result")
    local captureError=$( setUpDistro --name "${defaultDistribution}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    hideDivisionsInSetUpDistro=("${savedHideDivisionsInSetUpDistro[@]}")



    header --section "Action 'set up distribution' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local storeTheRunbookStatus=${theRunbookPauseIs}
    local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; setUpDistro --name "${defaultDistribution}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    theRunbookPauseIs=${storeTheRunbookStatus}

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionDistributionTesting


actionRepositoriesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive


  header --context "Testing 'repositories' end-to-end interface" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  liftContentFromDinamicVariable --newVariable "tagOfResult" \
    --validation ${validatedTheCodeWithTRUE} \
    --getContentFrom "tagStatus"
  local returnResultFalse="${tagOfResult[0]} ${validatedTheCodeWithFALSE}"
  local returnResultTrue="${tagOfResult[0]} ${validatedTheCodeWithTRUE}"

  local theRepositoryType="ppa"
  local theRepositoryName="inkscape.dev/trunk"

    header --section "The 'ensure tool repository' returns a message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local capturedMessage=$(ensureToolRepository "${theRepositoryName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'ensure tool repository' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local initialInfo="user"
    local endInfo="date"

    local capturedMessage=$(ensureToolRepository --repositoryName "${theRepositoryName[@]}" --repositoryType "${theRepositoryType[@]}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
    local localUser="$USER"
    local localDate=$(date +%Y)

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'ensure tool repository' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    header --section "Action 'erase tool repository' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local capturedMessage=$(uninstallTool "${theRepositoryName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    header --section "Action 'erase tool repository' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local initialInfo="user"
    local endInfo="date"

    local capturedMessage=$(eraseToolRepository --repositoryName "${theRepositoryName[@]}" --repositoryType "${theRepositoryType[@]}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    local localUser="$USER"
    local localDate=$(date +%Y)

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'erase tool repository' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    header --section "Action 'ensure tool repository' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local captureRunbookError=$(ensureToolRepository --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

    weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'ensure tool repository' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local storeTheRunbookStatus=${theRunbookPauseIs}
    local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; ensureToolRepository --repositoryName "${theRepositoryName[@]}" --repositoryType "${theRepositoryType[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    theRunbookPauseIs=${storeTheRunbookStatus}

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    header --section "Action 'erase tool repository' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local storeTheRunbookStatus=${theRunbookPauseIs}
    local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; eraseToolRepository --repositoryName "${theRepositoryName[@]}" --repositoryType "${theRepositoryType[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    theRunbookPauseIs=${storeTheRunbookStatus}

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionRepositoriesTesting

actionRepositoriesToolShowMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageTool
  local existTheToolRepository

  local theRepositoryType="ppa"
  local theRepositoryName="inkscape.dev/trunk"
  local result=""

        header --section "The 'ensure repository tool' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEnsureRepository=("${hideDivisionsInEnsureRepository[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "ensureRepository"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"


          header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local fakeTitle="Title"
          hideDivisionsInEnsureRepository=(false)

          local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; ensureToolRepository --repositoryName "${theRepositoryName[@]}" --repositoryType "${theRepositoryType[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEnsureRepository=("${savedHideDivisionsInEnsureRepository[@]}")


        header --section "The 'erase repository tool' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEraseRepository=("${hideDivisionsInEraseRepository[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "eraseRepository"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"


          header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local fakeTitle="Title"
          hideDivisionsInEraseRepository=(false)

          local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; eraseRepository --repositoryName "${theRepositoryName[@]}" --repositoryType "${theRepositoryType[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEraseRepository=("${savedHideDivisionsInEraseRepository[@]}")

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionRepositoriesToolShowMessagesTesting

actionRepositoriesToolHideAbstractAndExplanationMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageTool
  local existTheToolRepository

  local theRepositoryType="ppa"
  local theRepositoryName="inkscape.dev/trunk"
  local result=""

        header --subsection "... hidding the abstract of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEnsureRepository=("${hideDivisionsInEnsureRepository[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "ensureRepository"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInEnsureRepository=("abstract" "explanation")

        local captureError=$( ensureToolRepository --repositoryName "${theRepositoryName[@]}" --repositoryType "${theRepositoryType[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEnsureRepository=("${savedHideDivisionsInEnsureRepository[@]}")


        header --subsection "... hidding the abstract of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEraseRepository=("${hideDivisionsInEraseRepository[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "eraseRepository"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInEraseRepository=("abstract" "explanation")

        local captureError=$( eraseRepository --repositoryName "${theRepositoryName[@]}" --repositoryType "${theRepositoryType[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEraseRepository=("${savedHideDivisionsInEraseRepository[@]}")

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionRepositoriesToolHideAbstractAndExplanationMessagesTesting

actionRepositoriesToolHideResultAndExplanationMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageTool
  local existTheToolRepository

  local theRepositoryType="ppa"
  local theRepositoryName="inkscape.dev/trunk"
  local result=""

        header --subsection "... hidding the result of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEnsureRepository=("${hideDivisionsInEnsureRepository[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "ensureRepository"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInEnsureRepository=("result" "explanation")

        local captureError=$( ensureToolRepository --repositoryName "${theRepositoryName[@]}" --repositoryType "${theRepositoryType[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEnsureRepository=("${savedHideDivisionsInEnsureRepository[@]}")


        header --subsection "... hidding the result of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEraseRepository=("${hideDivisionsInEraseRepository[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "eraseRepository"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInEraseRepository=("result" "explanation")

        local captureError=$( eraseRepository --repositoryName "${theRepositoryName[@]}" --repositoryType "${theRepositoryType[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEraseRepository=("${savedHideDivisionsInEraseRepository[@]}")

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionRepositoriesToolHideResultAndExplanationMessagesTesting


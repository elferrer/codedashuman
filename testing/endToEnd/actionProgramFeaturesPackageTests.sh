#!${BASH}
actionProgramFeaturesPackageTests() {
  echo -e "Loading... Testing 'actionProgramFeaturesPackageTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="ActionProgramPackageTests"

actionProgramFeaturesPackageConstructionMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageProgram
  local existTheProgram

  liftContentFromDinamicVariable --newVariable "tagOfResult" \
    --validation ${validatedTheCodeWithTRUE} \
    --getContentFrom "tagStatus"
  local returnResultFalse="${tagOfResult[0]} ${validatedTheCodeWithFALSE}"
  local returnResultTrue="${tagOfResult[0]} ${validatedTheCodeWithTRUE}"

  local tooling="Package"
  local theProgramName="debian-faq"
  local thePackageName="debian-faq_9.0_all.deb"
  local thePackageUrl="http://ftp.es.debian.org/debian/pool/main/d/debian-faq"

  sudo apt remove "${theProgramName[@]}" -y

  header --section "Download ${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
  wget "${thePackageUrl[@]}/${thePackageName[@]}"

  local result=""
  getMessageProgram=$(isTheToolInstalled "${tooling}" "${theProgramName[@]}")
  existTheProgram=$?

  case ${existTheProgram} in
    ${validatedTheCodeWithTRUE} )
        header --section "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    ;;
    ${validatedTheCodeWithFALSE} )

        header --section "Action 'ensure package' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage=$(ensureTool "${thePackageName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --section "Action 'ensure package' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local initialInfo="user"
        local endInfo="date"

        local capturedMessage=$(ensureTool --package "${thePackageName[@]}" --program "${theProgramName[@]}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
        local localUser="$USER"
        local localDate=$(date +%Y)

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --section "Action 'ensure package' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'uninstall package' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage=$(uninstallTool "${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'uninstall package' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local initialInfo="user"
        local endInfo="date"

        local capturedMessage=$(uninstallTool --program "${theProgramName[@]}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
        local localUser="$USER"
        local localDate=$(date +%Y)

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --section "Action 'uninstall package' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'ensure package' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local captureRunbookError=$(ensureTool --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

        weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --section "Action 'ensure package' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local storeTheRunbookStatus=${theRunbookPauseIs}
        local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; ensureTool --package "${thePackageName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
        theRunbookPauseIs=${storeTheRunbookStatus}

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'uninstall package' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local storeTheRunbookStatus=${theRunbookPauseIs}
        local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; uninstallTool --program "${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
        theRunbookPauseIs=${storeTheRunbookStatus}

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    ;;
  esac

  header --section "Remove ${thePackageName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  rm "${thePackageName[@]}"

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesPackageConstructionMessagesTesting


actionProgramFeaturesPackageShowMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageProgram
  local existTheProgram

  local tooling="Package"
  local theProgramName="debian-faq"
  local thePackageName="debian-faq_9.0_all.deb"
  local thePackageUrl="http://ftp.es.debian.org/debian/pool/main/d/debian-faq"

  header --section "Download ${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  wget "${thePackageUrl[@]}/${thePackageName[@]}" "${thePackageName[@]}"

  local result=""
  getMessageProgram=$(isTheToolInstalled "${tooling}" "${theProgramName[@]}")
  existTheProgram=$?

  case ${existTheProgram} in
    ${validatedTheCodeWithTRUE} )
        header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    ;;

    ${validatedTheCodeWithFALSE} )
        header --section "The 'ensure package' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEnsureTool=("${hideDivisionsInEnsureTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolInstallation"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"


          header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local fakeTitle="Title"
          hideDivisionsInEnsureTool=(false)

          local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; ensureTool --package "${thePackageName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEnsureTool=("${savedHideDivisionsInEnsureTool[@]}")


        header --section "The 'uninstall package' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInUninstallTool=("${hideDivisionsInUninstallTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolRemove"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"


          header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local fakeTitle="Title"
          hideDivisionsInUninstallTool=(false)

          local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; uninstallTool --program "${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInUninstallTool=("${savedHideDivisionsInUninstallTool[@]}")
    ;;
  esac

  header --section "Remove ${thePackageName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  rm "${thePackageName[@]}"

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesPackageShowMessagesTesting


actionProgramFeaturesPackageHideAbstractAndExplanationMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageProgram
  local existTheProgram

  local tooling="Package"
  local theProgramName="debian-faq"
  local thePackageName="debian-faq_9.0_all.deb"
  local thePackageUrl="http://ftp.es.debian.org/debian/pool/main/d/debian-faq"

  header --section "Download ${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  wget "${thePackageUrl[@]}/${thePackageName[@]}" "${thePackageName[@]}"

  local result=""
  getMessageProgram=$(isTheToolInstalled "${tooling}" "${theProgramName[@]}")
  existTheProgram=$?
  case ${existTheProgram} in
    ${validatedTheCodeWithTRUE} )
        header --section "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    ;;

    ${validatedTheCodeWithFALSE} )
        header --subsection "... hidding the abstract of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEnsureTool=("${hideDivisionsInEnsureTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolInstallation"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInEnsureTool=("abstract" "explanation")

        local captureError=$( ensureTool --package "${thePackageName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEnsureTool=("${savedHideDivisionsInEnsureTool[@]}")


        header --subsection "... hidding the abstract of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInUninstallTool=("${hideDivisionsInUninstallTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolRemove"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInUninstallTool=("abstract" "explanation")

        local captureError=$( uninstallTool --program "${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInUninstallTool=("${savedHideDivisionsInUninstallTool[@]}")
    ;;
  esac

  header --section "Remove ${thePackageName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  rm "${thePackageName[@]}"

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesPackageHideAbstractAndExplanationMessagesTesting


actionProgramFeaturesPackageHideResultAndExplanationMessagesTesting() {

  local tooling="Package"
  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageProgram
  local existTheProgram
  local theProgramName="debian-faq"
  local thePackageName="debian-faq_9.0_all.deb"
  local thePackageUrl="http://ftp.es.debian.org/debian/pool/main/d/debian-faq"

  header --section "Download ${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  wget "${thePackageUrl[@]}/${thePackageName[@]}" "${thePackageName[@]}"

  local result=""
  getMessageProgram=$(isTheToolInstalled "${tooling}" "${theProgramName[@]}")
  existTheProgram=$?
  case ${existTheProgram} in
    ${validatedTheCodeWithTRUE} )
        header --section "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    ;;

    ${validatedTheCodeWithFALSE} )
        header --subsection "... hidding the result of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEnsureTool=("${hideDivisionsInEnsureTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolInstallation"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInEnsureTool=("result" "explanation")

        local captureError=$( ensureTool --package "${thePackageName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEnsureTool=("${savedHideDivisionsInEnsureTool[@]}")


        header --subsection "... hidding the result of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInUninstallTool=("${hideDivisionsInUninstallTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolRemove"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInUninstallTool=("result" "explanation")

        local captureError=$( uninstallTool --program "${theProgramName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInUninstallTool=("${savedHideDivisionsInUninstallTool[@]}")
    ;;
  esac

  header --section "Remove ${thePackageName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
  
  rm "${thePackageName[@]}"

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesPackageHideResultAndExplanationMessagesTesting


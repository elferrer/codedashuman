#!${BASH}
actionProgramFeaturesGemTests() {
  echo -e "Loading... Testing 'actionProgramFeaturesGemTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="ActionProgramGemTests"

actionProgramFeaturesToolConstructionMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageTool
  local existTheTool

  liftContentFromDinamicVariable --newVariable "tagOfResult" \
    --validation ${validatedTheCodeWithTRUE} \
    --getContentFrom "tagStatus"
  local returnResultFalse="${tagOfResult[0]} ${validatedTheCodeWithFALSE}"
  local returnResultTrue="${tagOfResult[0]} ${validatedTheCodeWithTRUE}"

  header --context "Prepare ruby" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  cook --recipe "ruby" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}"


  local tool="Gem"
  local theToolName="gem-example"
  local result=""
  getMessageTool=$(isTheToolInstalled "${tool}" "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
  existTheTool=$?

  case ${existTheTool} in
    ${validatedTheCodeWithTRUE} )
        header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        continue
    ;;
    ${validatedTheCodeWithFALSE} )

        header --section "Action 'ensure gem tool' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage=$(ensureTool "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --section "Action 'ensure gem tool' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local initialInfo="user"
        local endInfo="date"

        local capturedMessage=$(ensureTool --gem "${theToolName[@]}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
        local localUser="$USER"
        local localDate=$(date +%Y)

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --section "Action 'ensure gem tool' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'uninstall gem tool' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage=$(uninstallTool "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'uninstall gem tool' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local initialInfo="user"
        local endInfo="date"

        local capturedMessage=$(uninstallTool --gem "${theToolName[@]}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
        local localUser="$USER"
        local localDate=$(date +%Y)

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --section "Action 'uninstall gem tool' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'ensure gem tool' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local captureRunbookError=$(ensureTool --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

        weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --section "Action 'ensure gem tool' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local storeTheRunbookStatus=${theRunbookPauseIs}
        local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; ensureTool --gem "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
        theRunbookPauseIs=${storeTheRunbookStatus}

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'uninstall gem tool' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local storeTheRunbookStatus=${theRunbookPauseIs}
        local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; uninstallTool --gem "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
        theRunbookPauseIs=${storeTheRunbookStatus}

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    ;;
  esac

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesToolConstructionMessagesTesting


actionProgramFeaturesToolShowMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageTool
  local existTheTool

  header --context "Prepare ruby" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  cook --recipe ruby --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}"

  local tool="Gem"
  local theToolName="gem-example"
  local result=""
  getMessageTool=$(isTheToolInstalled "${tool}" "${theToolName[@]}")
  existTheTool=$?

  case ${existTheTool} in
    ${validatedTheCodeWithTRUE} )
        header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        continue
    ;;

    ${validatedTheCodeWithFALSE} )
        header --section "The 'ensure gem tool' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEnsureTool=("${hideDivisionsInEnsureTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolInstallation"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"


          header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local fakeTitle="Title"
          hideDivisionsInEnsureTool=(false)

          local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; ensureTool --gem "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEnsureTool=("${savedHideDivisionsInEnsureTool[@]}")


        header --section "The 'uninstall gem tool' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInUninstallTool=("${hideDivisionsInUninstallTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolRemove"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"


          header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local fakeTitle="Title"
          hideDivisionsInUninstallTool=(false)

          local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; uninstallTool --gem "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInUninstallTool=("${savedHideDivisionsInUninstallTool[@]}")
    ;;
  esac

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesToolShowMessagesTesting


actionProgramFeaturesToolHideAbstractAndExplanationMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageTool
  local existTheTool

  header --context "Prepare ruby" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  cook --recipe ruby --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}"

  local tool="Gem"
  local theToolName="gem-example"
  local result=""
  getMessageTool=$(isTheToolInstalled "${tool}" "${theToolName[@]}")
  existTheTool=$?
  case ${existTheTool} in
    ${validatedTheCodeWithTRUE} )
        header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        continue
    ;;

    ${validatedTheCodeWithFALSE} )
        header --subsection "... hidding the abstract of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEnsureTool=("${hideDivisionsInEnsureTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolInstallation"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInEnsureTool=("abstract" "explanation")

        local captureError=$( ensureTool --gem "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEnsureTool=("${savedHideDivisionsInEnsureTool[@]}")


        header --subsection "... hidding the abstract of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInUninstallTool=("${hideDivisionsInUninstallTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolRemove"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInUninstallTool=("abstract" "explanation")

        local captureError=$( uninstallTool --gem "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInUninstallTool=("${savedHideDivisionsInUninstallTool[@]}")
    ;;
  esac

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesToolHideAbstractAndExplanationMessagesTesting


actionProgramFeaturesToolHideResultAndExplanationMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageTool
  local existTheTool

  header --context "Prepare ruby" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  cook --recipe ruby --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}"

  local tool="Gem"
  local theToolName="gem-example"
  local result=""
  getMessageTool=$(isTheToolInstalled "${tool}" "${theToolName[@]}")
  existTheTool=$?
  case ${existTheTool} in
    ${validatedTheCodeWithTRUE} )
        header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        continue
    ;;

    ${validatedTheCodeWithFALSE} )
        header --subsection "... hidding the result of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInEnsureTool=("${hideDivisionsInEnsureTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolInstallation"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInEnsureTool=("result" "explanation")

        local captureError=$( ensureTool --gem "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInEnsureTool=("${savedHideDivisionsInEnsureTool[@]}")


        header --subsection "... hidding the result of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local savedHideDivisionsInUninstallTool=("${hideDivisionsInUninstallTool[@]}")

        liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "applyToolRemove"

        liftContentFromDinamicVariable --newVariable "tagOfResult" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagStatus"

        liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
          --validation ${validatedTheCodeWithTRUE} \
          --getContentFrom "tagExplanation"

        hideDivisionsInUninstallTool=("result" "explanation")

        local captureError=$( uninstallTool --gem "${theToolName[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

        weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        hideDivisionsInUninstallTool=("${savedHideDivisionsInUninstallTool[@]}")
    ;;
  esac

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesToolHideResultAndExplanationMessagesTesting


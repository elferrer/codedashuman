#!${BASH}
actionManagementPanelsTests() {
  echo -e "Loading... Testing 'actionManagementPanelsTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="ActionManagementPanelsTests"

actionManagementPanelsTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  header --context "Testing 'management panels' end-to-end interface" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      liftContentFromDinamicVariable --newVariable "tagOfResult" \
        --validation ${validatedTheCodeWithTRUE} \
        --getContentFrom "tagStatus"
      local returnResultFalse="${tagOfResult[0]} ${validatedTheCodeWithFALSE}"
      local returnResultTrue="${tagOfResult[0]} ${validatedTheCodeWithTRUE}"

      local theFolder="${userFolder}/managementPanel"
      local destinationFolder="${userFolder}/managementPanelCopy"


      header --section "Action 'ensure management panel' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local capturedMessage=$(ensureManagementPanel "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'ensure management panel' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local initialInfo="user"
      local endInfo="date"

      local capturedMessage
      capturedMessage=$(ensureManagementPanel --name "${theFolder}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
      local localUser="$USER"
      local localDate=$(date +%Y)

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'ensure management panel' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'copy management panel' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local capturedMessage=$(copyManagementPanel "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'copy management panel' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local initialInfo="user"
      local endInfo="date"

      local capturedMessage=$(copyManagementPanel --origin "${theFolder}" --destination "${destinationFolder}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
      local localUser="$USER"
      local localDate=$(date +%Y)

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'copy management panel' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'erase management panel' when it is poorly constructed send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local capturedMessage=$(eraseManagementPanel "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")


      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'erase management panel' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local initialInfo="user"
      local endInfo="date"

      local capturedMessage=$(eraseManagementPanel --name "${theFolder}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
      local localUser="$USER"
      local localDate=$(date +%Y)

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'erase management panel' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'move management panel' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local capturedMessage=$(moveManagementPanel "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'move management panel' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local initialInfo="user"
      local endInfo="date"

      local capturedMessage=$(moveManagementPanel --origin "${destinationFolder}" --destination "${theFolder}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
      local localUser="$USER"
      local localDate=$(date +%Y)

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'move management panel' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



      header --section "Clean test with 'erase management panel'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local capturedMessage=$(eraseManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



      header --section "Action 'ensure management panel' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local captureRunbookError=$(ensureManagementPanel --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

      weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'ensure management panel' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local storeTheRunbookStatus=${theRunbookPauseIs}
      local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; ensureManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
      theRunbookPauseIs=${storeTheRunbookStatus}

      weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'erase management panel' never break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local captureRunbookError=$(eraseManagementPanel --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

      weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator false --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'erase management panel' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local storeTheRunbookStatus=${theRunbookPauseIs}
      local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; eraseManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
      theRunbookPauseIs=${storeTheRunbookStatus}

      weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'copy management panel' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local captureRunbookError=$(copyManagementPanel --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

      weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



      header --section "Action 'copy management panel' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local captureError=$(theRunbookIsBrokenIs=true ; copyManagementPanel --origin "${theFolder}" --destination "${destinationFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'move management panel' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local captureRunbookError=$(moveManagementPanel --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

      weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'move management panel' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local captureError=$(theRunbookIsBrokenIs=true ; moveManagementPanel --origin "${destinationFolder}" --destination "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

      weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionManagementPanelsTesting


actionBriefingTaskTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  header --context "Testing 'briefing tasks' end-to-end interface" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  liftContentFromDinamicVariable --newVariable "tagOfResult" \
    --validation ${validatedTheCodeWithTRUE} \
    --getContentFrom "tagStatus"

  local theLocation="${userFolder}"
  local theFile="oneDocument.txt"
  local fileName="${theLocation}/${theFile}"
  local aLineOfText="This is a example='this is a text' "
  local initialInfo="user"
  local endInfo="date"

  local cleanFile=$( eraseBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

  liftContentFromDinamicVariable --newVariable "tagOfResult" \
    --validation ${validatedTheCodeWithTRUE} \
    --getContentFrom "tagStatus"
  local returnResultFalse="${tagOfResult[0]} ${validatedTheCodeWithFALSE}"
  local returnResultTrue="${tagOfResult[0]} ${validatedTheCodeWithTRUE}"


    header --section "Action 'ensure briefing task' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local capturedMessage=$(ensureBriefingTask "${theFile}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'ensure briefing task' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local capturedMessage
    capturedMessage=$(ensureBriefingTask --name "${fileName}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
    local validation=$?
    local localUser="$USER"
    local localDate=$(date +%Y)

    weWishes --firstOperator "${validation}" --sentence "contains" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'ensure briefing task' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'ensure recipe ingredient' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local capturedMessage
    capturedMessage=$(ensureRecipeIngredient --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    local validation=$?

    weWishes --firstOperator "${validation}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    header --section "Action 'ensure recipe ingredient' only add one item..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "... when the text is not at the beginning of line" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local cleanFile=$( eraseBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    capturedMessage=$(ensureBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    getMessageContentExist=$(isTheContentInTheFile "${aLineOfText[@]}" "${fileName}" true)

    weWishes --firstOperator "${getMessageContentExist}" --sentence "contains" --secondOperator false --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local capturedMessage=""
    capturedMessage=$(ensureRecipeIngredient  --name "${fileName}" --line "# another text" --atTheBeginningOfTheLine "true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

    capturedMessage=$(ensureRecipeIngredient  --name "${fileName}" --line "another text" --atTheBeginningOfTheLine "false" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
    getMessageCounter=$(countContentInTheFile "another text" "${fileName}")

    weWishes --firstOperator "${getMessageCounter}" --sentence "<" --secondOperator 2 --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... when the text is at the beginning of line" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local cleanFile=$( eraseBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    capturedMessage=$(ensureBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    getMessageContentExist=$(isTheContentInTheFile "${aLineOfText[@]}" "${fileName}" true)

    weWishes --firstOperator "${getMessageContentExist}" --sentence "contains" --secondOperator false --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local capturedMessage=""
    capturedMessage=$(ensureRecipeIngredient  --name "${fileName}" --line "${aLineOfText[@]}" --atTheBeginningOfTheLine "true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
    capturedMessage=$(ensureRecipeIngredient  --name "${fileName}" --line "${aLineOfText[@]}" --atTheBeginningOfTheLine "true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
    getMessageCounter=$(countContentInTheFile "${aLineOfText[@]}" "${fileName}")

    weWishes --firstOperator "${getMessageCounter}" --sentence "<" --secondOperator 2 --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""




    header --section "Action 'ensure recipe ingredient' it is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local capturedMessage
    capturedMessage=$(ensureRecipeIngredient  --name "${fileName}" --line "${aLineOfText[@]}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
    local validation=$?
    local localUser="$USER"
    local localDate=$(date +%Y)

    weWishes --firstOperator "${validation}" --sentence "contains" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    header --section "Action 'ensure recipe ingredient' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'erase briefing task' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local capturedMessage=$(eraseBriefingTask "${theFile}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'erase briefing task' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local fileName="${theLocation}/${theFile}"
    local initialInfo="user"
    local endInfo="date"

    local capturedMessage=$( eraseBriefingTask --name "${fileName}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

    local localUser="$USER"
    local localDate=$(date +%Y)

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'erase briefing task' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'ensure briefing task' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local captureRunbookError=$(ensureBriefingTask --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

    weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'ensure briefing task' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local storeTheRunbookStatus=${theRunbookPauseIs}
    local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; ensureBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    theRunbookPauseIs=${storeTheRunbookStatus}

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'ensure recipe ingredient' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local captureRunbookError
    captureRunbookError=$(ensureRecipeIngredient --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )
    local validation=$?

    weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'ensure recipe ingredient' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local storeTheRunbookStatus=${theRunbookPauseIs}
    local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; ensureRecipeIngredient  --name "${fileName}" --line "${aLineOfText[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    theRunbookPauseIs=${storeTheRunbookStatus}

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'erase briefing task' never break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local captureRunbookError=$(eraseBriefingTask --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

    weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator false --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'erase briefing task' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local storeTheRunbookStatus=${theRunbookPauseIs}
    local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; eraseBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
    theRunbookPauseIs=${storeTheRunbookStatus}

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionBriefingTaskTesting


actionHideEnsureManagementPanelMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  header --context "The 'management panels' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local savedHideDivisionsInEnsureManagementPanel=("${hideDivisionsInEnsureManagementPanel[@]}")

    liftContentFromDinamicVariable --newVariable "abstractMessageOfCreatePanel" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "ensureThePanel"

    liftContentFromDinamicVariable --newVariable "tagOfResult" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagStatus"

    liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagExplanation"

    local theFolder="${userFolder}/managementPanel"


    header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local fakeTitle="Title"
    hideDivisionsInEnsureManagementPanel=(false)

    local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; ensureManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${abstractMessageOfCreatePanel[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide title message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEnsureManagementPanel=("title")
    local captureError=$( ensureManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide abstract and explanation messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEnsureManagementPanel=("abstract" "explanation")
    local captureError=$( ensureManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide result message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEnsureManagementPanel=("result")
    local captureError=$( ensureManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    hideDivisionsInEnsureManagementPanel=("${savedHideDivisionsInEnsureManagementPanel[@]}")

    theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionHideEnsureManagementPanelMessagesTesting


actionHideEraseManagementPanelMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  header --context "The 'management panels' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local savedHideDivisionsInEraseManagementPanel=("${hideDivisionsInEraseManagementPanel[@]}")

    liftContentFromDinamicVariable --newVariable "abstractMessageOfErasePanel" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "eraseTheManagementPanel"

    liftContentFromDinamicVariable --newVariable "tagOfResult" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagStatus"

    liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagExplanation"

    local theFolder="${userFolder}/managementPanel"


    header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local fakeTitle="Title"
    hideDivisionsInEraseManagementPanel=(false)

    local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; eraseManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${abstractMessageOfErasePanel[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide title message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEraseManagementPanel=("title")
    local captureError=$( eraseManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide abstract and explanation messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEraseManagementPanel=("abstract" "explanation")
    local captureError=$( eraseManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide result message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEraseManagementPanel=("result")
    local captureError=$( eraseManagementPanel --name "${theFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    hideDivisionsInEraseManagementPanel=("${savedHideDivisionsInEraseManagementPanel[@]}")

    theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionHideEraseManagementPanelMessagesTesting


actionHideCopyManagementPanelMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  header --context "The 'management panels' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local savedHideDivisionsInCopyManagementPanel=("${hideDivisionsInCopyManagementPanel[@]}")

    liftContentFromDinamicVariable --newVariable "abstractMessageOfCopyPanel" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "copyTheManagementPanel"

    liftContentFromDinamicVariable --newVariable "tagOfResult" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagStatus"

    liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagExplanation"

    local theFolder="${userFolder}/managementPanel"
    local destinationFolder="${userFolder}/managementPanelCopy"


    header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local fakeTitle="Title"
    hideDivisionsInCopyManagementPanel=(false)

    local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; copyManagementPanel --origin "${theFolder}" --destination "${destinationFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${abstractMessageOfCopyPanel[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide title message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInCopyManagementPanel=("title")
    local captureError=$( copyManagementPanel --origin "${theFolder}" --destination "${destinationFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide abstract and explanation messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInCopyManagementPanel=("abstract" "explanation")
    local captureError=$( copyManagementPanel --origin "${theFolder}" --destination "${destinationFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide result message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInCopyManagementPanel=("result")
    local captureError=$( copyManagementPanel --origin "${theFolder}" --destination "${destinationFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    hideDivisionsInCopyManagementPanel=("${savedHideDivisionsInCopyManagementPanel[@]}")

    theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionHideCopyManagementPanelMessagesTesting


actionHideMoveManagementPanelMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  header --context "The 'management panels' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local savedHideDivisionsInMoveManagementPanel=("${hideDivisionsInMoveManagementPanel[@]}")

    liftContentFromDinamicVariable --newVariable "abstractMessageOfMoveManagementPanel" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "moveThePanel"

    liftContentFromDinamicVariable --newVariable "tagOfResult" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagStatus"

    liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagExplanation"

    local theFolder="${userFolder}/managementPanel"
    local destinationFolder="${userFolder}/managementPanelCopy"


    header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local fakeTitle="Title"
    hideDivisionsInMoveManagementPanel=(false)

    local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; moveManagementPanel --origin "${theFolder}" --destination "${destinationFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${abstractMessageOfMoveManagementPanel[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide title message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInMoveManagementPanel=("title")
    local captureError=$( moveManagementPanel --origin "${theFolder}" --destination "${destinationFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide abstract and explanation messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInMoveManagementPanel=("abstract" "explanation")
    local captureError=$( moveManagementPanel --origin "${theFolder}" --destination "${destinationFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide result message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInMoveManagementPanel=("result")
    local captureError=$( moveManagementPanel --origin "${theFolder}" --destination "${destinationFolder}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    hideDivisionsInMoveManagementPanel=("${savedHideDivisionsInMoveManagementPanel[@]}")

    theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionHideMoveManagementPanelMessagesTesting


actionHideBriefingTaskMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  header --context "The 'briefing tasks' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local savedHideDivisionsInEnsureBriefingTask=("${hideDivisionsInEnsureBriefingTask[@]}")
  local savedHideDivisionsInEraseBriefingTask=("${hideDivisionsInEraseBriefingTask[@]}")

    liftContentFromDinamicVariable --newVariable "abstractMessageOfCreateBriefingTask" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "ensureTheBriefingTask"

    liftContentFromDinamicVariable --newVariable "abstractMessageOfEraseBriefingTask" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "eraseTheBriefingTask"

    liftContentFromDinamicVariable --newVariable "tagOfResult" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagStatus"

    local theLocation="${userFolder}"
    local theFile="oneDocument.txt"
    local fileName="${theLocation}/${theFile}"


    header --subsection "creating... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local fakeTitle="Title"
    hideDivisionsInEnsureBriefingTask=(false)

    local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; ensureBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${abstractMessageOfCreateBriefingTask[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fileName[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "removing... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local fakeTitle="Title"
    hideDivisionsInEraseBriefingTask=(false)

    local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; eraseBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${abstractMessageOfEraseBriefingTask[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fileName[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "creating... hide title message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEnsureBriefingTask=("title")
    local captureError=$( ensureBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "removing... hide title message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEraseBriefingTask=("title")
    local captureError=$(  eraseBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "creating... hide abstract and explanation messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEnsureBriefingTask=("abstract" "explanation")
    local captureError=$( ensureBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "removing... hide abstract and explanation messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEraseBriefingTask=("abstract" "explanation")
    local captureError=$(  eraseBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "creating... hide result message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEnsureBriefingTask=("result")
    local captureError=$( ensureBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "removing... hide result message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEraseBriefingTask=("result")
    local captureError=$( eraseBriefingTask --name "${fileName}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEnsureBriefingTask=("${savedHideDivisionsInEnsureBriefingTask[@]}")
    hideDivisionsInEraseBriefingTask=("${savedHideDivisionsInEraseBriefingTask[@]}")

    theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionHideBriefingTaskMessagesTesting


actionHideEnsureRecipeIngredientMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  header --context "The 'management panels' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local savedHideDivisionsInEnsureRecipeIngredient=("${hideDivisionsInEnsureRecipeIngredient[@]}")

    liftContentFromDinamicVariable --newVariable "abstractMessageOfEnsureRecipeIngredient" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "ensureTheRecipeIngredient"

    liftContentFromDinamicVariable --newVariable "tagOfResult" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagStatus"

    liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagExplanation"

    local theLocation="${userFolder}"
    local theFile="oneDocument.txt"
    local fileName="${theLocation}/${theFile}"
    local aLineOfText="This is a example='this is a text' "
    local initialInfo="user"
    local endInfo="date"


    header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local fakeTitle="Title"
    hideDivisionsInEnsureRecipeIngredient=(false)

    local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; ensureRecipeIngredient --name "${fileName}" --line "${aLineOfText[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${abstractMessageOfEnsureRecipeIngredient[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide title message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEnsureRecipeIngredient=("title")
    local captureError=$( ensureRecipeIngredient --name "${fileName}" --line "${aLineOfText[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide abstract and explanation messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEnsureRecipeIngredient=("abstract" "explanation")
    local captureError=$( ensureRecipeIngredient --name "${fileName}" --line "${aLineOfText[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide result message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInEnsureRecipeIngredient=("result")
    local captureError=$( ensureRecipeIngredient --name "${fileName}" --line "${aLineOfText[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    hideDivisionsInEnsureRecipeIngredient=("${savedHideDivisionsInEnsureRecipeIngredient[@]}")

    theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionHideEnsureRecipeIngredientMessagesTesting

DIR="$(dirname "${BASH_SOURCE}")" ; FILE="$(basename "${BASH_SOURCE}")"
nameOfFileTested="[${DIR}] [${FILE}]"

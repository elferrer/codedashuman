#!${BASH}
actionLanguagesTests() {
  echo -e "Loading... Testing 'actionLanguagesTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
actionLanguagesTesting() {
aliasTests="ActionLanguageTests"

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  header --context "Testing 'language' end-to-end interface" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "applyLanguage"

    liftContentFromDinamicVariable --newVariable "tagOfResult" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagStatus"
    local returnResultFalse="${tagOfResult[0]} ${validatedTheCodeWithFALSE}"
    local returnResultTrue="${tagOfResult[0]} ${validatedTheCodeWithTRUE}"

    header --section "Action 'language' set the active language" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local desiredLanguage="english"
    selectLanguage --language "${desiredLanguage}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive
    local appliedLanguage=${activeLanguage}

    weWishes --firstOperator "${appliedLanguage[@]}" --sentence "=" --secondOperator "${desiredLanguage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    selectLanguage --language "spanish" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}"; theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive
    local appliedLanguage=${activeLanguage}

    weWishes --firstOperator "${appliedLanguage[@]}" --sentence "!=" --secondOperator "${desiredLanguage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'language' set language and show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    local desiredLanguage=${activeLanguage}

    local initialInfo="user"
    local endInfo="date"

    local language=$(selectLanguage --language "${desiredLanguage[@]}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
    local localUser="$USER"
    local localDate=$(date +%Y)

    weWishes --firstOperator "${language[@]}" --sentence "contain" --secondOperator "${desiredLanguage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${language[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${language[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'language' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local captureError=$(selectLanguage --language "${desiredLanguage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Action 'language' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local captureError=$(selectLanguage --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "The 'language' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local savedhideDivisionsInSelectLanguage=("${hideDivisionsInSelectLanguage[@]}")

    liftContentFromDinamicVariable --newVariable "tagOfResult" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagStatus"

    liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagExplanation"


      hideDivisionsInSelectLanguage=(false)
      header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local fakeTitle="Title"

      local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; selectLanguage --language "${nameOfLanguage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

      weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



      header --subsection "... hidding the abstract of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      hideDivisionsInSelectLanguage=("abstract")

      local captureError=$( selectLanguage --language "${nameOfLanguage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

      weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${titleProjectMessageCaption[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --subsection "... hidding the result of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      hideDivisionsInSelectLanguage=("result")

      local captureError=$( selectLanguage --language "${nameOfLanguage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

      weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --subsection "... hidding the explanation of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      hideDivisionsInSelectLanguage=("explanation")

      local captureError=$( selectLanguage --language "${nameOfLanguage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

      weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInSelectLanguage=("${savedhideDivisionsInSelectLanguage[@]}")




  header --section "Action 'language' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local captureRunbookError=$(selectLanguage --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

  weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  header --section "Action 'language' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local storeTheRunbookStatus=${theRunbookPauseIs}
  local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; selectLanguage --language "${desiredLanguage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")
  theRunbookPauseIs=${storeTheRunbookStatus}

  weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionLanguagesTesting


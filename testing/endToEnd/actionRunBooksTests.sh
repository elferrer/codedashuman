#!${BASH}
actionRunbooksTests() {
  echo -e "Loading... Testing 'actionRunbooksTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="ActionRunBookTests"

actionRunbooksTesting() {
  header --context "Testing 'run book' end-to-end interface" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  liftContentFromDinamicVariable --newVariable "tagOfResult" \
    --validation ${validatedTheCodeWithTRUE} \
    --getContentFrom "tagStatus"
  local returnResultFalse="${tagOfResult[0]} ${validatedTheCodeWithFALSE}"

      header --section "Action 'run book' launch specification summary is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local captureError=$(launchSpecificationsSummary --save "the_name_of_your_choice" )

      weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'run book' launch specification summary show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local initialInfo="user"
      local endInfo="date"

      local runbook=$(launchSpecificationsSummary --save "the_name_of_your_choice" --initialInfo "${initialInfo}" --endInfo "${endInfo}" )
      local localUser="$USER"
      local localDate=$(date +%Y)

      weWishes --firstOperator "${runbook[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --firstOperator "${runbook[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests actionRunbooksTesting


#!${BASH}
actionProgramFeaturesProgramTests() {
  echo -e "Loading... Testing 'actionProgramFeaturesProgramTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="ActionProgramTests"

actionProgramFeaturesProgramConstructionMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageProgram
  local existTheProgram

  liftContentFromDinamicVariable --newVariable "tagOfResult" \
    --validation ${validatedTheCodeWithTRUE} \
    --getContentFrom "tagStatus"
  local returnResultFalse="${tagOfResult[0]} ${validatedTheCodeWithFALSE}"
  local returnResultTrue="${tagOfResult[0]} ${validatedTheCodeWithTRUE}"

  local tool="Program"
  local suggestedProgramsForTest=("tkinfo" "htop" "zipcmp")
  local theProgram
    for theProgram in "${suggestedProgramsForTest[@]}"
      do
        local result=""
        getMessageProgram=$(isTheToolInstalled "${tool}" "${theProgram[@]}")
        existTheProgram=$?

        case ${existTheProgram} in
          ${validatedTheCodeWithTRUE} )
              header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
              continue
          ;;
          ${validatedTheCodeWithFALSE} )

              header --section "Action 'ensure program' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local capturedMessage=$(ensureTool "${theProgram[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

              weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


              header --section "Action 'ensure program' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local initialInfo="user"
              local endInfo="date"

              local capturedMessage=$(ensureTool --program "${theProgram[@]}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
              local localUser="$USER"
              local localDate=$(date +%Y)

              weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


              header --section "Action 'ensure program' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
              weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



              header --section "Action 'uninstall program' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local capturedMessage=$(uninstallTool "${theProgram[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

              weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



              header --section "Action 'uninstall program' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local initialInfo="user"
              local endInfo="date"

              local capturedMessage=$(uninstallTool --program "${theProgram[@]}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
              local localUser="$USER"
              local localDate=$(date +%Y)

              weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


              header --section "Action 'uninstall program' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
              weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



              header --section "Action 'ensure program' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local captureRunbookError=$(ensureTool --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

              weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


              header --section "Action 'ensure program' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local storeTheRunbookStatus=${theRunbookPauseIs}
              local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; ensureTool --program "${theProgram[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
              theRunbookPauseIs=${storeTheRunbookStatus}

              weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



              header --section "Action 'uninstall program' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local storeTheRunbookStatus=${theRunbookPauseIs}
              local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; uninstallTool --program "${theProgram[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
              theRunbookPauseIs=${storeTheRunbookStatus}

              weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          break
        ;;
      esac
  done

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesProgramConstructionMessagesTesting


actionProgramFeaturesProgramShowMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageProgram
  local existTheProgram

  local tool="Program"
  local suggestedProgramsForTest=("tkinfo" "htop" "zipcmp")
  local theProgram
    for theProgram in "${suggestedProgramsForTest[@]}"
      do
        local result=""
        getMessageProgram=$(isTheToolInstalled "${tool}" "${theProgram[@]}")
        existTheProgram=$?

        case ${existTheProgram} in
          ${validatedTheCodeWithTRUE} )
              header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
              continue
          ;;

          ${validatedTheCodeWithFALSE} )
              header --section "The 'ensure program' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local savedHideDivisionsInEnsureTool=("${hideDivisionsInEnsureTool[@]}")

              liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "applyToolInstallation"

              liftContentFromDinamicVariable --newVariable "tagOfResult" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagStatus"

              liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagExplanation"


                header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                local fakeTitle="Title"
                hideDivisionsInEnsureTool=(false)

                local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; ensureTool --program "${theProgram[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

                weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
                weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
                weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
                weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              hideDivisionsInEnsureTool=("${savedHideDivisionsInEnsureTool[@]}")


              header --section "The 'uninstall program' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local savedHideDivisionsInUninstallTool=("${hideDivisionsInUninstallTool[@]}")

              liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "applyToolRemove"

              liftContentFromDinamicVariable --newVariable "tagOfResult" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagStatus"

              liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagExplanation"


                header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                local fakeTitle="Title"
                hideDivisionsInUninstallTool=(false)

                local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; uninstallTool --program "${theProgram[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

                weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
                weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
                weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

                header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
                weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              hideDivisionsInUninstallTool=("${savedHideDivisionsInUninstallTool[@]}")

          break
        ;;
      esac
  done

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesProgramShowMessagesTesting


actionProgramFeaturesProgramHideAbstractAndExplanationMessagesTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageProgram
  local existTheProgram

  local tool="Program"
  local suggestedProgramsForTest=("tkinfo" "htop" "zipcmp")
  local theProgram
    for theProgram in "${suggestedProgramsForTest[@]}"
      do
        local result=""
        getMessageProgram=$(isTheToolInstalled "${tool}" "${theProgram[@]}")
        existTheProgram=$?
        case ${existTheProgram} in
          ${validatedTheCodeWithTRUE} )
              header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
              continue
          ;;

          ${validatedTheCodeWithFALSE} )
              header --subsection "... hidding the abstract of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local savedHideDivisionsInEnsureTool=("${hideDivisionsInEnsureTool[@]}")

              liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "applyToolInstallation"

              liftContentFromDinamicVariable --newVariable "tagOfResult" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagStatus"

              liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagExplanation"

              hideDivisionsInEnsureTool=("abstract" "explanation")

              local captureError=$( ensureTool --program "${theProgram[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

              weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              hideDivisionsInEnsureTool=("${savedHideDivisionsInEnsureTool[@]}")


              header --subsection "... hidding the abstract of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local savedHideDivisionsInUninstallTool=("${hideDivisionsInUninstallTool[@]}")

              liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "applyToolRemove"

              liftContentFromDinamicVariable --newVariable "tagOfResult" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagStatus"

              liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagExplanation"

              hideDivisionsInUninstallTool=("abstract" "explanation")

              local captureError=$( uninstallTool --program "${theProgram[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

              weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${contentOfAbstractMessage[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              hideDivisionsInUninstallTool=("${savedHideDivisionsInUninstallTool[@]}")


          break
        ;;
      esac
  done

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesProgramHideAbstractAndExplanationMessagesTesting


actionProgramFeaturesProgramHideResultAndExplanationMessagesTesting() {

  local tool="Program"
  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local getMessageProgram
  local existTheProgram

  local suggestedProgramsForTest=("tkinfo" "htop" "zipcmp")
  local theProgram
    for theProgram in "${suggestedProgramsForTest[@]}"
      do
        local result=""
        getMessageProgram=$(isTheToolInstalled "${tool}" "${theProgram[@]}")
        existTheProgram=$?
        case ${existTheProgram} in
          ${validatedTheCodeWithTRUE} )
              header --context "Skipping the test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
              continue
          ;;

          ${validatedTheCodeWithFALSE} )
              header --subsection "... hidding the result of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local savedHideDivisionsInEnsureTool=("${hideDivisionsInEnsureTool[@]}")

              liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "applyToolInstallation"

              liftContentFromDinamicVariable --newVariable "tagOfResult" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagStatus"

              liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagExplanation"

              hideDivisionsInEnsureTool=("result" "explanation")

              local captureError=$( ensureTool --program "${theProgram[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

              weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              hideDivisionsInEnsureTool=("${savedHideDivisionsInEnsureTool[@]}")


              header --subsection "... hidding the result of message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              local savedHideDivisionsInUninstallTool=("${hideDivisionsInUninstallTool[@]}")

              liftContentFromDinamicVariable --newVariable "contentOfAbstractMessage" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "applyToolRemove"

              liftContentFromDinamicVariable --newVariable "tagOfResult" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagStatus"

              liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
                --validation ${validatedTheCodeWithTRUE} \
                --getContentFrom "tagExplanation"

              hideDivisionsInUninstallTool=("result" "explanation")

              local captureError=$( uninstallTool --program "${theProgram[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )

              weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

              hideDivisionsInUninstallTool=("${savedHideDivisionsInUninstallTool[@]}")

          break
        ;;
      esac
  done

  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionProgramFeaturesProgramHideResultAndExplanationMessagesTesting


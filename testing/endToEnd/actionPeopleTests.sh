#!${BASH}
actionPeopleTests() {
  echo -e "Loading... Testing 'actionPeopleTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="ActionPeopleTests"

actionPeopleTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  header --context "Testing 'people' end-to-end interface" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      liftContentFromDinamicVariable --newVariable "tagOfAbstract" \
        --validation ${validatedTheCodeWithTRUE} \
        --getContentFrom "tagAbstract"

      liftContentFromDinamicVariable --newVariable "tagOfResult" \
        --validation ${validatedTheCodeWithTRUE} \
        --getContentFrom "tagStatus"
      local returnResultFalse="${tagOfResult[0]} ${validatedTheCodeWithFALSE}"
      local returnResultTrue="${tagOfResult[0]} ${validatedTheCodeWithTRUE}"

      local theUser=$USER


      header --section "Action 'people' returns an message error when it is poorly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local capturedMessage=$(collaboratingPeople "${theUser}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}")

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultFalse[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'people' is correctly constructed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local initialInfo="user"
      local endInfo="date"

      local capturedMessage=$(collaboratingPeople --acceptedUser "${theUser}" --initialInfo "${initialInfo}" --endInfo "${endInfo}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
      local localUser="$USER"
      local localDate=$(date +%Y)

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'people' show system info" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${localDate[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""





      header --section "Action 'people' break the runbook when it send an error" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local captureRunbookError=$(collaboratingPeople --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo "$theRunbookIsBrokenIs" )

      weWishes --firstOperator "${captureRunbookError[@]}" --sentence "contains" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'people' isn't executed when the runbook is broken" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local storeTheRunbookStatus=${theRunbookPauseIs}
      local captureError=$(theRunbookPauseIs=false ; theRunbookIsBrokenIs=true ; collaboratingPeople --acceptedUser "${theUser}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" )
      theRunbookPauseIs=${storeTheRunbookStatus}

      weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${returnResultTrue[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "The 'people' you can configure your display..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local savedHideDivisionsInUsersInvolved=("${hideDivisionsInUsersInvolved[@]}")

    liftContentFromDinamicVariable --newVariable "abstractMessageOfUsersInvolved" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "usersInvolved"

    liftContentFromDinamicVariable --newVariable "tagOfResult" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagStatus"

    liftContentFromDinamicVariable --newVariable "tagOfExplanation" \
      --validation ${validatedTheCodeWithTRUE} \
      --getContentFrom "tagExplanation"

    local theUser=$USER


    header --subsection "... show all messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local fakeTitle="Title"
    hideDivisionsInUsersInvolved=(false)

    local captureError=$( header --subsection "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; collaboratingPeople --acceptedUser "${theUser}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Abstract" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${abstractMessageOfUsersInvolved[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Result" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --subsection "Explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --firstOperator "${captureError[@]}" --sentence "contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide title message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInUsersInvolved=("title")
    local captureError=$( collaboratingPeople --acceptedUser "${theUser}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${fakeTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide abstract and explanation messages" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInUsersInvolved=("abstract" "explanation")
    local captureError=$( collaboratingPeople --acceptedUser "${theUser}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfAbstract[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --subsection "... hide result message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    hideDivisionsInUsersInvolved=("result")
    local captureError=$( collaboratingPeople --acceptedUser "${theUser}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" ; echo $? )

    weWishes --firstOperator "${captureError[@]}" --sentence "not contains" --secondOperator "${tagOfResult[0]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    hideDivisionsInUsersInvolved=("${savedHideDivisionsInUsersInvolved[@]}")

    theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests actionPeopleTesting


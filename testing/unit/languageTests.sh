#!${BASH}
languageTests() {
  echo -e "Loading... Testing 'languageTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="UnitLanguageTests"

languageTesting() {

  header --context "Set language" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local validation=${validatedTheCodeWithTRUE}
      local language="english"

      loadAddOn "${languagesPath}" "${typeOfLanguagePath}" "${language}"
      liftContentFromDinamicVariable --newVariable "englishMessage" \
        --validation ${validation} \
        --getContentFrom "unexplained"

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${englishMessage[0]}" --sentence "contain" --secondOperator "${genericPassMessage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local language="spanish"

      loadAddOn "${languagesPath}" "${typeOfLanguagePath}" "${language}"
      liftContentFromDinamicVariable --newVariable "spanishMessage" \
        --validation ${validation} \
        --getContentFrom "unexplained"

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${spanishMessage[0]}" --sentence "contain" --secondOperator "${genericPassMessage[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests languageTesting


#!${BASH}
managementFolderTests() {
  echo -e "Loading... Testing 'managementFolderTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="UnitManagementFolderTests"

createAndEraseFoldersTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local folder="${userFolder}/managementPanel"
  local theFolder=$(canonicalizeExistentPath ${folder})


  header --context "Fixture: ensure that the folder does not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local ensureTheFolderNotExist
  ensureTheFolderNotExist=$(erasePanel "${theFolder}")


  header --context "Testing 'management folders' " --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --section "Action 'proveValidationFolder' return error when creation folder is not possible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(proveValidationFolder "${theFolder}")
        local validation=$?

        weWishes --firstOperator "${validation[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'proveValidationFolder' return a negative message when creation folder is not possible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${messageReturnedByFunction[FolderNotExist]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'createPanel' return a 0" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(createPanel "${theFolder}")
        local capturedValidation=$?

        weWishes --firstOperator "${capturedValidation[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --subsection "... create a folder send a 0 message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'proveValidationFolder' return a positive message when folder exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(proveValidationFolder "${theFolder}")
        local validation=$?

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${messageReturnedByFunction[FolderExist]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'erasePanel' return a 0" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(erasePanel "${theFolder}")
        local capturedValidation=$?

        weWishes --firstOperator "${capturedValidation[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --subsection "... erase a folder send a 0 message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'proveValidationFolder' return a positive message when folder not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(proveValidationFolder "${theFolder}")
        local validation=$?

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${messageReturnedByFunction[FolderNotExist]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""




        header --section "Action 'stampValidations', if the folder not exist, break the runbook when condition 'folder exist' fail." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local messageCheckFolder
        local ensureTheFolderNotExist
        ensureTheFolderNotExist=$(erasePanel "${theFolder}")
        messageCheckFolder=$(checkIfFolderExist "${theFolder}")
        local validationCaptured=$?

        local validator=${validatedTheCodeWithTRUE}

        local capturedMessage=$(stampValidations "${validationCaptured}" "${validator}" ; echo "${theRunbookIsBrokenIs}")

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "=" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""




        header --section "Action 'stampValidations', if the folder exist, break the runbook when condition 'folder not exist' fail." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local messageCheckFolder
        local ensureTheFolderExist
        ensureTheFolderExist=$(createPanel "${theFolder}")
        messageCheckFolder=$(checkIfFolderExist "${theFolder}")
        local validationCaptured=$?

        local validator=${validatedTheCodeWithFALSE}

        local capturedMessage=$(stampValidations "${validationCaptured}" "${validator}" ; echo "${theRunbookIsBrokenIs}")

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "=" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""




  header --context "Fixture: ensure that the folder does not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local ensureTheFolderNotExist
  ensureTheFolderNotExist=$(erasePanel "${theFolder}")


  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests createAndEraseFoldersTesting



copyAndMoveFoldersTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local folder="${userFolder}/managementPanel"
  local theOriginFolder=$(canonicalizeExistentPath ${folder})
  local destinationFolder="${userFolder}/managementPanelCopy"
  local theDestinationFolder=$(canonicalizeExistentPath ${destinationFolder})


  header --context "Fixture: ensure that the folders does not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local ensureTheFolderNotExist
  ensureOriginFolderNotExist=$(erasePanel "${theOriginFolder}")
  ensureDestinationFolderNotExist=$(erasePanel "${theDestinationFolder}")
  ensureTheFolderExist=$(createPanel "${theOriginFolder}")


  header --context "Testing 'management folders' " --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --section "Action 'copyPanel' return a 0" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local capturedMessage
      capturedMessage=$(copyPanel "${theOriginFolder}" "${theDestinationFolder}")
      local capturedValidation=$?

      weWishes --firstOperator "${capturedValidation[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



      header --subsection "... copy folder send a 0 message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "Fixture: ensure that the folders does not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  ensureTheFolderNotExist=$(erasePanel "${theDestinationFolder}")


        header --section "Action 'movePanel' return a 0" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(movePanel "${theOriginFolder}" "${theDestinationFolder}")
        local capturedValidation=$?

        weWishes --firstOperator "${capturedValidation[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --subsection "... move folder send a 0 message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "Fixture: ensure that the folders does not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local ensureTheFolderNotExist
  ensureTheFolderNotExist=$(erasePanel "${theDestinationFolder}")


  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs


} ; launchGroupTests copyAndMoveFoldersTesting




createAndEraseBriefingTaskTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local theLocation="${userFolder}"
  local theFile="oneDocument.txt"
  local fileName="${theLocation}/${theFile}"


  header --context "Fixture: ensure that the file does not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local ensureTheFileNotExist=$(softDeleteBriefingTask "${fileName}")



  header --context "Testing 'management files' " --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --section "Action 'proveValidationFile' return error when creation file is not possible (the file aren't created)" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(proveValidationFile "${fileName}")
        local validation=$?

        weWishes --firstOperator "${validation[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'proveValidationFile' return a negative message when creation file is not possible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${messageReturnedByFunction[FileNotExist]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'createBriefingTask' return a 0" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(createBriefingTask "${fileName}")
        local capturedValidation=$?

        weWishes --firstOperator "${capturedValidation[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --subsection "... create a file send a 0 message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'proveValidationFile' return a positive message when file exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(proveValidationFile "${fileName}")
        local validation=$?

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${messageReturnedByFunction[FileExist]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'softDeleteBriefingTask' return a 0" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(softDeleteBriefingTask "${fileName}")
        local capturedValidation=$?

        weWishes --firstOperator "${capturedValidation[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --subsection "... erase a file send a 0 message" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --section "Action 'proveValidationFile' return a positive message when file not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local capturedMessage
        capturedMessage=$(proveValidationFile "${fileName}")
        local validation=$?

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "contain" --secondOperator "${messageReturnedByFunction[FileNotExist]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""




        header --section "Action 'stampValidations', if the file not exist, break the runbook when condition 'file exist' fail." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local messageCheckFolder
        local ensureTheFolderNotExist
        ensureTheFolderNotExist=$(softDeleteBriefingTask "${fileName}")
        messageCheckFolder=$(checkIfFileExist "${fileName}")
        local validationCaptured=$?

        local validator=${validatedTheCodeWithTRUE}

        local capturedMessage=$(stampValidations "${validationCaptured}" "${validator}" ; echo "${theRunbookIsBrokenIs}")

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "=" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""




        header --section "Action 'stampValidations', if the file exist, break the runbook when condition 'file not exist' fail." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local messageCheckFolder
        local ensureTheFolderExist
        ensureTheFolderExist=$(createBriefingTask "${fileName}")
        messageCheckFolder=$(checkIfFileExist "${fileName}")
        local validationCaptured=$?

        local validator=${validatedTheCodeWithFALSE}

        local capturedMessage=$(stampValidations "${validationCaptured}" "${validator}" ; echo "${theRunbookIsBrokenIs}")

        weWishes --firstOperator "${capturedMessage[@]}" --sentence "=" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""




  header --context "Fixture: ensure that the file does not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local ensureTheFileNotExist=$(softDeleteBriefingTask "${fileName}")


  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests createAndEraseBriefingTaskTesting



addRecipientToBriefingTaskTesting() {

  local theRunbookIsBrokenAreInactive=false
  local savedtheRunbookIsBrokenIs=$theRunbookIsBrokenIs
  theRunbookIsBrokenIs=$theRunbookIsBrokenAreInactive

  local theLocation="${userFolder}"
  local theFile="oneDocument.txt"
  local fileName="${theLocation}/${theFile}"
  local aLineOfText="This is a example='this is a text' "


  header --context "Fixture: ensure that the file does not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local ensureTheFileNotExist=$(softDeleteBriefingTask "${fileName}")


  header --context "Testing 'add line' to file " --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



          header --section "Action 'addRecipientToBriefingTask' " --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local capturedMessage=$(createBriefingTask "${fileName}")

          local haveContentBefore
          local addLine
          local haveContenAfter

          haveContentBefore=$(isTheContentInTheFile "${aLineOfText[@]}" "${fileName}" true)
          local validationContentBefore=$?
          addLine=$(addLineToFile "${aLineOfText[@]}" ${fileName})
          local validationContent=$?
          haveContenAfter=$(isTheContentInTheFile "${aLineOfText[@]}" "${fileName}" true)
          local validationContentAfter=$?

          weWishes --firstOperator "${haveContentBefore}" --sentence "=" --secondOperator false --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${validationContentBefore}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          weWishes --firstOperator "${addLine[@]}" --sentence "=" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${validationContent[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          weWishes --firstOperator "${haveContenAfter[@]}" --sentence "=" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
          weWishes --firstOperator "${validationContentAfter[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  header --context "Fixture: ensure that the file does not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local ensureTheFileNotExist=$(softDeleteBriefingTask "${fileName}")


  theRunbookIsBrokenIs=$savedtheRunbookIsBrokenIs

} ; launchGroupTests addRecipientToBriefingTaskTesting


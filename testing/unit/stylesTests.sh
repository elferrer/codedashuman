#!${BASH}
stylesTests() {
  echo -e "Loading... Testing 'stylesTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="UnitStylesTests"

stylesOnMessages() {
  local withoutText=""
  local readFalseMessage=$(saveOrSeeValidatedMessage "title" "${withoutText}" ${validatedTheCodeWithTRUE} ${systemView})
  local lenghtInOneEmptyTextString=${#readFalseMessage}

  header --context "The system view style change when change style" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      loadAddOn "${stylesPath}" "${consoleStylePath}" "bashColor"
      local boldTextOnBashColor=${boldTextView}
      loadAddOn "${stylesPath}" "${consoleStylePath}" "bashColorless"
      local boldTextOnBashColorless=${boldTextView}
      loadAddOn "${stylesPath}" "${consoleStylePath}" "bashUnformatted"
      local boldTextOnBashUnformatted=${boldTextView}
      loadAddOn "${stylesPath}" "${webStylePath}" "html"
      local boldTextOnHtml=${boldTextPrint}
      loadAddOn "${stylesPath}" "${consoleStylePath}" "${defaultConsoleStyle}"

      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#boldTextOnBashColor} --sentence "!=" --secondOperator ${#boldTextOnBashColorless} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#boldTextOnBashColorless} --sentence "!=" --secondOperator ${#boldTextOnBashUnformatted} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#boldTextOnBashColor} --sentence "!=" --secondOperator ${#boldTextOnBashUnformatted} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#boldTextOnBashColor} --sentence "!=" --secondOperator ${#boldTextOnHtml} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${boldTextOnBashColor} --sentence "!=" --secondOperator ${boldTextOnBashColorless} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${boldTextOnBashColorless} --sentence "!=" --secondOperator ${boldTextOnBashUnformatted} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${boldTextOnBashColor} --sentence "!=" --secondOperator ${boldTextOnBashUnformatted} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "lenghtOfString" --firstOperator ${boldTextOnBashColor} --sentence "!=" --secondOperator ${boldTextOnHtml} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests stylesOnMessages


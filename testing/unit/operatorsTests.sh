#!${BASH}
operatorsTests() {
  echo -e "Loading... Testing 'operatorsTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="UnitOperatorsTests"

operatorsTesting() {

  header --context "Testing operators functions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Test if return only alphanumeric characters" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local checkString=$(normalizeString "Ab-1")

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkString} --sentence "=" --secondOperator "Ab1" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Test if a variable is a number" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local checkNumber=$(isNumber "3")

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkNumber} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Test if in an indefined number of variables..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... all are numbers" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local checkNumbers=$(areAllVariablesNumbers "1" "2" "3")

        weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkNumbers} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... everithyng isn't numbers" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local checkNumbers=$(areAllVariablesNumbers "1" "text" "3")

        weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkNumbers} --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Test the operator for use with..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... numbers" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local checkOperator=$(convertOperatorsForUseWithNumbers ">")

        weWishes --messageInterpretation "theCheckIs" --firstOperator $checkOperator --sentence "=" --secondOperator "-gt" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... letters" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        checkOperator=$(convertOperatorsForUseWithLetters "-gt")

        weWishes --messageInterpretation "theCheckIs" --firstOperator $checkOperator --sentence "=" --secondOperator ">" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Test the result of an operation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local checkOperation=$(checkTheOperation "one" "=" "1")
      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkOperation} --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      checkOperation=$(checkTheOperation "1" "=" "1")
      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkOperation} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Fix bug: if lenght of numbers is different then it fail" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      checkOperation=$(checkTheOperation "100" ">" "98")
      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkOperation} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Test if the operator is a paradigm" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local checkParadigm=$(isParadigm "not")
      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkParadigm} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local checkParadigm=$(isParadigm "contain")
      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkParadigm} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local checkParadigm=$(isParadigm "not contain")
      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkParadigm} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local checkParadigm=$(isParadigm "are false")
      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkParadigm} --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "Test a paradigm" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local checkParadigm=$(checkTheParadigm "an number" "contain" "number")
      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkParadigm} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local checkParadigm=$(checkTheParadigm "an number" "not contain" "a n")
      weWishes --messageInterpretation "theCheckIs" --firstOperator ${checkParadigm} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests operatorsTesting


#!${BASH}
runbookSummaryTests() {
  echo -e "Loading... Testing 'runbookSummaryTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="UnitRunbookSummaryTests"

runbookSummaryTesting() {
  local getMessageFileExist
  local checkFileBeforeCreation
  local checkFileAfterCreation
  local checkFile

  header --context "The summary is saved to a file" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --section "... with name" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local fileName="testingSummaryOnFile"
      local fileNamePath="${fileName}.log.${typessetingWebStyle}"
      local validationSaveSummary

      getMessageFileExist=$(checkIfFileExist "${fileNamePath}")
      checkFileBeforeCreation=$?
      if [ "${checkFileBeforeCreation}" == ${validatedTheCodeWithTRUE} ]
        then
          softDeleteBriefingTask "${fileNamePath}"
      fi

      launchSaveSummary ${fileName}
      validationSaveSummary=$?

      getMessageFileExist=$(checkIfFileExist "${fileNamePath}")
      checkFileAfterCreation=$?

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkFileAfterCreation}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${validationSaveSummary}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      getMessageFileExist=$(checkIfFileExist "${fileNamePath}")
      checkFile=$?
      if [ "${checkFile}" = ${validatedTheCodeWithTRUE} ]
        then
          local result=$(softDeleteFile "${fileNamePath}")
      fi


      header --section "... with default name" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local fileName="${defaultSummaryFile}"
      local fileNamePath="${fileName}.log.${typessetingWebStyle}"

      getMessageFileExist=$(checkIfFileExist "${fileNamePath}")
      checkFile=$?
      if [ "${checkFile}" = ${validatedTheCodeWithTRUE} ]
        then
          local result=$(softDeleteFile "${fileNamePath}")
      fi

        getMessageFileExist=$(checkIfFileExist "${fileNamePath}")
        checkFileBeforeCreation=$?
        if [ "${checkFileBeforeCreation}" == ${validatedTheCodeWithTRUE} ]
          then
            softDeleteBriefingTask "${fileNamePath}"
        fi
        launchSaveSummary
        getMessageFileExist=$(checkIfFileExist "${fileNamePath}")
        checkFileAfterCreation=$?

        weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkFileAfterCreation}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


        header --subsection "Clean files created by test: remove '${fileNamePath}' file" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          getMessageFileExist=$(checkIfFileExist "${fileNamePath}")
          checkFile=$?
          if [ "${checkFile}" = ${validatedTheCodeWithTRUE} ]
            then
              local result=$(softDeleteFile "${fileNamePath}")
          fi
          getMessageFileExist=$(checkIfFileExist "${fileNamePath}")
          checkFile=$?

          weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkFile}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



        header --subsection "Clean files created by test: remove '${defaultSummaryFile}' file" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          getMessageFileExist=$(checkIfFileExist "${defaultSummaryFile}")
          checkFile=$?
          if [ "${checkFile}" = ${validatedTheCodeWithTRUE} ]
            then
              local result=$(softDeleteFile "${fileNamePath}")
          fi
          getMessageFileExist=$(checkIfFileExist "${defaultSummaryFile}")
          checkFile=$?

          weWishes --messageInterpretation "theCheckIs" --firstOperator "${checkFile}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  header --section "The score is viewed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local seeScore=$(launchSeeScore ${systemView})

    weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#seeScore} --sentence ">" --secondOperator "0" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


} ; launchGroupTests runbookSummaryTesting



launchByTagTesting() {

  local savedExceptionTest=${exceptTheGroupTests}
  local savedOnlyTest=${onlyTheGroupTests}

  header --section "The test is launched" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  saveConditionsToRunTests --onlyTheGroupTests "thisIsAFakeTest"
  launchThisGroupTests=$(isTheTestingGroupRunnable "thisIsAFakeTest")

  weWishes --messageInterpretation "theCheckIs" --firstOperator "${launchThisGroupTests[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  header --section "The excepted test isn't launched" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  saveConditionsToRunTests --exceptTheGroupTests "thisIsAFakeTest"
  launchThisGroupTests=$(isTheTestingGroupRunnable "thisIsAFakeTest")

  weWishes --messageInterpretation "theCheckIs" --firstOperator "${launchThisGroupTests[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  header --section "Only the test indicated is launched" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  saveConditionsToRunTests --onlyTheGroupTests "thisIsAnotherFakeTest"
  launchThisGroupTests=$(isTheTestingGroupRunnable "thisIsAFakeTest")

  weWishes --messageInterpretation "theCheckIs" --firstOperator "${launchThisGroupTests[@]}" --sentence "contain" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  exceptTheGroupTests=${savedExceptionTest}
  onlyTheGroupTests=${savedOnlyTest}

} ; launchGroupTests launchByTagTesting



conversionsTesting() {
  header --section "Conversion of code response to boolean ('true' or 'false')" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  theDataHasBeenCollected=$(convertCodeResponseToBoolean ${validatedTheCodeWithFALSE})
  weWishes --firstOperator "${theDataHasBeenCollected[@]}" --sentence "==" --secondOperator false --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  listDirectory=$(ls -la)
  returnOfList=$?
  theDataHasBeenCollected=$(convertCodeResponseToBoolean ${returnOfList})
  weWishes --firstOperator "${theDataHasBeenCollected[@]}" --sentence "==" --secondOperator true --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --section "Conversion of boolean to code response" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  theDataHasBeenCollected=$(convertBooleanToCodeResponse true )
  weWishes --firstOperator "${theDataHasBeenCollected[@]}" --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --section "Conversion of negated boolean to code response" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  theDataHasBeenCollected=$(convertNegatedBooleanToCodeResponse true )
  weWishes --firstOperator "${theDataHasBeenCollected[@]}" --sentence "==" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests conversionsTesting


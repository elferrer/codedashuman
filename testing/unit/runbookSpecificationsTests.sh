#!${BASH}
runbookTasksTests() {
  echo -e "Loading... Testing 'runbookTasksTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="UnitRunbookSpecificationsTests"

runbookTasksTesting() {

  fixtureCleanTask

  header --context "Tasks Book" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local briefAlias="styleUpdate"
    local briefTaskCompleted=true
    local briefTaskIsActive=true
    local briefTaskDependsOn=""
    local briefTaskParent=""

    header --section "Get..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... the title" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local briefSpecificationTitle="The P.O. has asked for changes"

        local aTitle=$(constructTheTask  --alias ${briefAlias} --title "${briefSpecificationTitle[@]}" ; echo "${titleOfTask[$briefAlias]}")

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${aTitle[@]}" --sentence "=" --secondOperator "${briefSpecificationTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --subsection "... explanation" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local briefSpecificationExplanation="The P.O. has asked for some stylistic changes"

        local explanation=$(constructTheTask --alias ${briefAlias} --explanation "${briefSpecificationExplanation[@]}" ; echo "${explanationOfTask[$briefAlias]}")

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${explanation[@]}" --sentence "=" --secondOperator "${briefSpecificationExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --subsection "... the tasks completed is true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local briefTaskIsCompleted=true

        local completed=$(constructTheTask --alias ${briefAlias} --completed "${briefTaskIsCompleted}" ; echo "${workCompletionOfTask[$briefAlias]}")

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${completed}" --sentence "=" --secondOperator "${briefTaskIsCompleted}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --subsection "... the tasks is active is true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local briefTaskIsActive=true

        local isActive=$(constructTheTask --alias ${briefAlias} --taskIsActive "${briefTaskIsActive}" ; echo "${taskIsActiveOfTask[$briefAlias]}")

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${isActive}" --sentence "=" --secondOperator "${briefTaskIsActive}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --subsection "... dependsOn is empty" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local briefTaskDependsOn=""

        local depends=$(constructTheTask --alias ${briefAlias} --dependsOn "${briefTaskDependsOn}" ; echo "${dependsOnOfTask[$briefAlias]}")

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${depends}" --sentence "=" --secondOperator "${briefTaskDependsOn}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


      header --subsection "... with all content" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local briefTaskCompleted=true
        local briefTaskIsActive=true
        local briefTaskDependsOn=""
        local briefTaskParent=""
        local briefTaskMomentum=""

        constructTheTask --alias "${briefAlias}" \
          --title "${briefSpecificationTitle[@]}" \
          --explanation "${briefSpecificationExplanation[@]}" \
          --completed "${briefTaskCompleted}" \
          --taskIsActive "${briefTaskIsActive}" \
          --dependsOn "${briefTaskDependsOn}" \
          --parent "${briefTaskParent}" \
          --labels "CAH" \
          --momentum "${briefTaskMomentum}"

        local aTitle="${titleOfTask[$briefAlias]}"
        local explanation="${explanationOfTask[$briefAlias]}"
        local completed="${workCompletionOfTask[$briefAlias]}"
        local isActive="${briefTaskIsActive[$briefAlias]}"
        local depends="${briefTaskDependsOn[$briefAlias]}"
        local parent="${briefTaskParent[$briefAlias]}"
        local momentum="${briefTaskMomentum[$briefAlias]}"

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${aTitle[@]}" --sentence "=" --secondOperator "${briefSpecificationTitle[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "theCheckIs" --firstOperator "${explanation[@]}" --sentence "=" --secondOperator "${briefSpecificationExplanation[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "theCheckIs" --firstOperator "${completed}" --sentence "=" --secondOperator "${briefTaskCompleted}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "theCheckIs" --firstOperator "${isActive}" --sentence "=" --secondOperator "${briefTaskIsActive}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "theCheckIs" --firstOperator "${depends}" --sentence "=" --secondOperator "${briefTaskDependsOn}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      weWishes --messageInterpretation "theCheckIs" --firstOperator "${parent}" --sentence "=" --secondOperator "${briefTaskParent}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  fixtureCleanTask


    header --section "It's saved the last task completed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local briefTaskCompleted=true

      local responseLastTask=$(constructTheTask --alias ${briefAlias} --completed "${briefTaskCompleted}" ; echo "${LastTaskApproved}")

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${responseLastTask}" --sentence "=" --secondOperator "${briefAlias}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    header --section "When the task fail, it's saved the last task completed" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local secondBriefAlias="styleUpdateFinished"
      local secondBriefTaskCompleted=false

      local responseLastTask=$(constructTheTask --alias ${secondBriefAlias} --completed "${secondBriefTaskCompleted}" ; echo "${LastTaskApproved}")

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${responseLastTask}" --sentence "=" --secondOperator "${briefAlias}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  fixtureCleanTask


    header --section "When fail the dependecy task, the task is not completed and is activated 'StopIfItFails', then the task fail" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local firstBriefAlias="styleUpdate"
      local firstBriefTaskCompleted=true
      local firstBriefTaskIsActive=true
      local firstBriefTaskStopIfIfFails=false
      local firstBriefTaskDependsOn=""
      local firstBriefTaskParent=""

      local secondBriefAlias="styleUpdateFinished"
      local secondBriefTaskCompleted=false
      local secondBriefTaskIsActive=true
      local secondBriefTaskStopIfIfFails=true
      local secondBriefTaskDependsOn="styleUpdate"
      local secondBriefTaskParent=""

      local thirdBriefAlias="styleUpdateChecked"
      local thirdBriefTaskCompleted=true
      local thirdBriefTaskIsActive=false
      local thirdBriefTaskStopIfIfFails=false
      local thirdBriefTaskDependsOn="styleUpdateFinished"
      local thirdBriefTaskParent=""

      local responseFirstTask=$(constructTheTask --alias ${firstBriefAlias} --completed "${firstBriefTaskCompleted}" --taskIsActive "${firstBriefTaskIsActive}" --stopIfItFails "${firstBriefTaskStopIfIfFails}" ; echo "${taskIsActiveOfTask[$firstBriefAlias]}")

      local responseSecondTask=$(constructTheTask --alias ${secondBriefAlias} --completed "${secondBriefTaskCompleted}" --taskIsActive "${secondBriefTaskIsActive}" --stopIfItFails "${secondBriefTaskStopIfIfFails}" ; echo "${taskIsActiveOfTask[$secondBriefAlias]}")

      local responseThirdTask=$(constructTheTask --alias ${thirdBriefAlias} --completed "${thirdBriefTaskCompleted}" --taskIsActive "${thirdBriefTaskIsActive}" --stopIfItFails "${thirdBriefTaskStopIfIfFails}" ; echo "${taskIsActiveOfTask[$thirdBriefAlias]}")

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${responseFirstTask}" --sentence "=" --secondOperator "${firstBriefTaskIsActive}" --alias ${firstBriefAlias} --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${responseSecondTask}" --sentence "!=" --secondOperator "${secondBriefTaskIsActive}" --alias ${secondBriefAlias} --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${responseThirdTask}" --sentence "=" --secondOperator "${thirdBriefTaskIsActive}" --alias ${thirdBriefAlias} --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests runbookTasksTesting

#!${BASH}
peopleTests() {
  echo -e "Loading... Testing 'peopleTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="UnitPeopleTests"

peopleTesting() {

  header --context "The current user is not the authorized user" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local authorizedUser="JackIsInAnotherWorldAndAliceIsNotPresent"

    local currentUserMessage
    local validationCurrentUser
    currentUserMessage=$(theCurrentUserIs)
    validationCurrentUser=$?

    weWishes --firstOperator "${authorizedUser[@]}" --sentence "!=" --secondOperator "${validationCurrentUser[@]}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "The current user is the authorized user" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local currentUserMessage
    local validationCurrentUser
    currentUserMessage=$(isThisTheCurrentUser $USER)
    validationCurrentUser=$?

    weWishes --firstOperator "${validationCurrentUser[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""




  header --context "The current user is within the authorized group" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local acceptedGroup="sudo"
    local groupMessage
    local validateGroupOfCurrentUser
    groupMessage=$(isTheUserInTheAcceptedGroup $acceptedGroup)
    validateGroupOfCurrentUser=$?

    weWishes --firstOperator "${validateGroupOfCurrentUser[@]}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests peopleTesting


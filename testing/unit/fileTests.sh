#!${BASH}
fileTests() {
  echo -e "Loading... Testing 'fileTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="UnitFileTests"

fileTesting() {
  local thisFile=$(realpath "${0}")
  header --context "Check 'currentDate' return a valid date" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local aDate=$(currentDate)
    local checkDate=$(date -d "${aDate}" ; echo $?)
    local result=$?

  weWishes --messageInterpretation "theCheckIs" --firstOperator "${result}" --sentence "=" --secondOperator "${validatedTheCodeWithTRUE}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "Check 'lastDateChangeInTheFile' return a valid date" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local aDate=$(lastDateChangeInTheFile "${thisFile}")
    local checkDate=$(date -d "${aDate}")
    local result=$?

  weWishes --messageInterpretation "theCheckIs" --firstOperator "${result}" --sentence "=" --secondOperator "${validatedTheCodeWithTRUE}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "Check 'waitForTheFileToBeCreated' checks whether the file has been created..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --section "... the file doesn't exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local file="./fileNotExist.never"
      local aFile=$(waitForTheFileToBeCreated "${file}" ; echo $?)

  weWishes --messageInterpretation "theCheckIs" --firstOperator "${aFile}" --sentence "=" --secondOperator "${validatedTheCodeWithFALSE}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "... the file exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local aFile=$(waitForTheFileToBeCreated "${thisFile}" ; echo $?)

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${aFile}" --sentence "=" --secondOperator "${validatedTheCodeWithTRUE}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests fileTesting


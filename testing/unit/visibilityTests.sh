#!${BASH}
visibilityTests() {
  echo -e "Loading... Testing 'visibilityTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="UnitVisibilityTests"

visibilityTesting() {

  header --context "Testing 'isVisible' function..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local tag="title"
    header --section "Testing '${tag}'..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        
      header --subsection "... is visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local backupshowTitleMessages=$showTitleMessages
        showTitleMessages=true

        local visible=$(isVisible "${tag}")
        showTitleMessages=$backupshowTitleMessages

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${visible} --sentence "=" --secondOperator "true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... isn't visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local backupshowTitleMessages=$showTitleMessages
        showTitleMessages=false

        local visible=$(isVisible "${tag}")
        showTitleMessages=$backupshowTitleMessages

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${visible} --sentence "=" --secondOperator "false" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    local tag="abstract"
    header --section "Testing '${tag}'..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        
      header --subsection "... is visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local backupshowAbstractMessages=$showAbstractMessages
        showAbstractMessages=true

        local visible=$(isVisible "${tag}")
        showAbstractMessages=$backupshowAbstractMessages

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${visible} --sentence "=" --secondOperator "true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... isn't visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local backupshowAbstractMessages=$showAbstractMessages
        showAbstractMessages=false

        local visible=$(isVisible "${tag}")
        showAbstractMessages=$backupshowAbstractMessages

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${visible} --sentence "=" --secondOperator "false" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""




    local tag="result"
    header --section "Testing '${tag}'..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        
      header --subsection "... is visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local backupshowResultMessages=$showResultMessages
        showResultMessages=true

        local visible=$(isVisible "${tag}")
        showResultMessages=$backupshowResultMessages

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${visible} --sentence "=" --secondOperator "true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... isn't visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local backupshowResultMessages=$showResultMessages
        showResultMessages=false

        local visible=$(isVisible "${tag}")
        showResultMessages=$backupshowResultMessages

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${visible} --sentence "=" --secondOperator "false" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    local tag="explanation"
    header --section "Testing '${tag}'..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        
      header --subsection "... is visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local backupshowExplanationMessages=$showExplanationMessages
        showExplanationMessages=true

        local visible=$(isVisible "${tag}")
        showExplanationMessages=$backupshowExplanationMessages

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${visible} --sentence "=" --secondOperator "true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... isn't visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local backupshowExplanationMessages=$showExplanationMessages
        showExplanationMessages=false

        local visible=$(isVisible "${tag}")
        showExplanationMessages=$backupshowExplanationMessages

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${visible} --sentence "=" --secondOperator "false" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



    local tag="specificationsSummary"
    header --section "Testing '${tag}'..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        
      header --subsection "... is visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local backupshowSpecificationsSummary=$showSpecificationsSummary
        showSpecificationsSummary=true

        local visible=$(isVisible "${tag}")
        showSpecificationsSummary=$backupshowSpecificationsSummary

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${visible} --sentence "=" --secondOperator "true" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      header --subsection "... isn't visible" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local backupshowSpecificationsSummary=$showSpecificationsSummary
        showSpecificationsSummary=false

        local visible=$(isVisible "${tag}")
        showSpecificationsSummary=$backupshowSpecificationsSummary

      weWishes --messageInterpretation "theCheckIs" --firstOperator ${visible} --sentence "=" --secondOperator "false" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests visibilityTesting


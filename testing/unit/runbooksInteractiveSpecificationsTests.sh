#!${BASH}
runbooksInteractiveTasksTests() {
  echo -e "Loading... Testing 'runbooksInteractiveTasksTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="UnitRunbookInteractiveTests"

runbooksInteractiveTaskTesting() {
  local briefAliasTomlCaption="styleUpdate"
  local briefTaskActivated=true


  header --context "The task..." --alias "${aliasTests}" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --section "... is active (work active is true)" --alias "${aliasTests}" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local anInteraction=$(constructTheTask --alias ${briefAliasTomlCaption} --momentum "${identifyTheTag}" --taskIsActive "${briefTaskActivated}" ; echo "${taskIsActiveOfTask[$briefAliasTomlCaption]}")

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${anInteraction}" --sentence "=" --secondOperator ${briefTaskActivated} --alias "${aliasTests}" --momentum "${identifyTheTag}" --labels "" --parent "" --explanation ""


    header --section "... using a configuration file, is completed (and work active is true)" --alias "${aliasTests}" --momentum "${identifyTheTag}" --parent "" --explanation ""

      fixtureCleanTask

      local file="${cahPath}/${testingPath}/${unloadedPath}/specifications.toml"
      local anInteraction=$(constructTheTask --toml "${file[@]}" ; echo "${taskIsActiveOfTask[$briefAliasTomlCaption]}")

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${anInteraction[@]}" --sentence "=" --secondOperator "${briefTaskActivated[@]}" --alias "${aliasTests}" --momentum "${identifyTheTag}" --labels "" --parent "" --explanation ""


    header --section "...is active (work active is true)..." --alias "${aliasTests}" --momentum "${identifyTheTag}" --parent "" --explanation ""

      fixtureCleanTask

      local briefAliasTomlCaption="styleUpdate"
      local briefTaskActivated=true

      local anInteraction=$(constructTheTask --alias ${briefAliasTomlCaption}  --momentum "${identifyTheTag}" --taskIsActive "${briefTaskActivated}" ; echo "${taskIsActiveOfTask[$briefAliasTomlCaption]}")

    weWishes --messageInterpretation "theCheckIs" --firstOperator "${anInteraction}" --sentence "=" --secondOperator ${briefTaskActivated} --alias "${aliasTests}" --momentum "${identifyTheTag}" --labels "" --parent "" --explanation ""


      header --subsection "... and, when completed is false..." --alias "${aliasTests}" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local briefTaskCompleted=false
        local aVerified=$(constructTheTask --alias ${briefAliasTomlCaption} --momentum "${identifyTheTag}" --completed "${briefTaskCompleted}" ; echo "${workCompletionOfTask[$briefAliasTomlCaption]}")

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${aVerified}" --sentence "=" --secondOperator "${briefTaskCompleted}" --alias "${aliasTests}" --momentum "${identifyTheTag}" --labels "" --parent "" --explanation ""


      header --subsection "... then, the Runbook is broken" --alias "${aliasTests}" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local briefTaskCompleted=false
      local briefTaskActivated=true
      local aRunbookIsBrokenIs=$(constructTheTask --alias ${briefAliasTomlCaption} --momentum "${identifyTheTag}" --completed "${briefTaskCompleted}" --taskIsActive "${briefTaskActivated}"  ; echo "${theRunbookIsBrokenIs}")

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${aRunbookIsBrokenIs}" --sentence "=" --secondOperator "true" --alias "${aliasTests}" --momentum "${identifyTheTag}" --labels "" --parent "" --explanation ""


      header --subsection "... and the task has actived 'StopIfItFails', then the Runbook is broken" --alias "${aliasTests}" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local briefTaskCompleted=false
      local briefTaskActivated=true
      local aRunbookIsBrokenIs=$(constructTheTask --alias ${briefAliasTomlCaption} --momentum "${identifyTheTag}" --completed "${briefTaskCompleted}" --taskIsActive "${briefTaskActivated}" --stopIfItFails "true" ; echo "${theRunbookIsBrokenIs}")

      weWishes --messageInterpretation "theCheckIs" --firstOperator "${aRunbookIsBrokenIs}" --sentence "=" --secondOperator "true" --alias "${aliasTests}" --momentum "${identifyTheTag}" --labels "" --parent "" --explanation ""

} ; launchGroupTests runbooksInteractiveTaskTesting


#!${BASH}
fixturesFittings() {
  echo -e "Loading... Testing 'fixturesFittings', version 0.1"
}


fixtureCleanTask() {
  initializeGlobalAlias "titleOfTask"
  initializeGlobalAlias "explanationOfTask"
  initializeGlobalAlias "workCompletionOfTask"
  initializeGlobalAlias "taskIsActiveOfTask"
  initializeGlobalAlias "stopIfItFails"
  initializeGlobalAlias "dependsOnOfTask"
  initializeGlobalAlias "typeOfDependencyOfTask"
  initializeGlobalAlias "ghostJobOfTask"
  initializeGlobalAlias "momentumOfTask"
  initializeGlobalAlias "labelsOfTask"
  initializeGlobalAlias "bulkDataOfTask"
}

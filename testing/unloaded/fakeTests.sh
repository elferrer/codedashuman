#!${BASH}
fakeTests() {
  echo -e "Loading... Testing 'fakeTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="FakeTests"

thisIsAFakeTest() {

    header --context "This is a fake test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --messageInterpretation "theCheckIs" --firstOperator "Fake test" --sentence "contain" --secondOperator "Fake test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
}

thisIsAnotherFakeTest() {

    header --context "This is another fake test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    weWishes --messageInterpretation "theCheckIs" --firstOperator "Fake test" --sentence "contain" --secondOperator "Fake test" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
}


#!${BASH}
helpTests() {
  echo -e "Loading... Testing 'helpTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="IntegrationHelpTests"

helpTesting() {

  local languages=("english" "spanish")
  for language in ${languages[@]}
    do
      header --context "Check 'Give me help' for '${language}' language " --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      loadAddOn "${languagesPath}" "${typeOfLanguagePath}" "${language}"

      for oneHelpToCheck in "${!helpDictionary[@]}"
        do
          header --subsection "Check 'basic' help message '${oneHelpToCheck}' exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local helpExist=$(giveMe ${oneHelpToCheck})

          weWishes --messageInterpretation "theCheckIs" --firstOperator ${#helpExist} --sentence ">" --secondOperator 0 --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


          header --subsection "Check 'extended' help message '${oneHelpToCheck}' exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local extendedHelp=$(giveMe ${oneHelpToCheck} "extended")

          weWishes --messageInterpretation "theCheckIs" --firstOperator ${#extendedHelp} --sentence ">" --secondOperator 0 --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


          header --subsection "Check 'example' help message '${oneHelpToCheck}' exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local exampleHelp=$(giveMe ${oneHelpToCheck} "example")

          weWishes --messageInterpretation "theCheckIs" --firstOperator ${#exampleHelp} --sentence ">" --secondOperator 0 --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


          header --subsection "Check 'options' help message '${oneHelpToCheck}' exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local optionsHelp=$(giveMe ${oneHelpToCheck} "options")

          weWishes --messageInterpretation "theCheckIs" --firstOperator ${#optionsHelp} --sentence ">" --secondOperator 0 --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


          header --subsection "Check 'see all' help message '${oneHelpToCheck}' exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

          local seeAllHelp=$(giveMe ${oneHelpToCheck} "see all")
          local basicCount=${#helpExist}
          local extendedCount=${#extendedHelp}
          local exampleCount=${#exampleHelp}
          local optionsCount=${#optionsHelp}
          local seeAll=$(( $basicCount + $extendedCount + $exampleCount + $optionsCount ))

          weWishes --messageInterpretation "theCheckIs" --firstOperator ${#seeAllHelp} --sentence ">" --secondOperator ${seeAll} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      done
  done

} ; launchGroupTests helpTesting


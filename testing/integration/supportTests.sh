#!${BASH}
supportTests() {
  echo -e "Loading... Testing 'supportTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="IntegrationSupportTests"

supportTestings() {

  header --context "Load a file with 'loadAddOn function' ... " --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local capturedReturn
  savedFolder=$(echo $PWD)

    header --section "... return a error code if not exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      loadAddOn "${testingPath}" "${unloadedPath}" "noTests"
      capturedReturn=$?

      weWishes --messageInterpretation "theCheckIs" --firstOperator $capturedReturn --sentence "!=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


    header --section "... return a pass code if exist" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      loadAddOn "${testingPath}" "${unloadedPath}" "fakeTests"
      capturedReturn=$?

      weWishes --messageInterpretation "theCheckIs" --firstOperator $capturedReturn --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests supportTestings


folderTestings() {

  header --context "Load a file with 'loadAddOn function' ... " --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  local capturedReturn
  savedFolder=$(echo $PWD)

  header --context "Go to another folder" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local initialFolder="${cahPath}/${testingPath}/${unloadedPath}"

    goToFolder $initialFolder
    capturedReturn=$?

    weWishes --messageInterpretation "theCheckIs" --firstOperator $capturedReturn --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    

    header --section "... with spaces" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local folderWithSpaces="another folder with spaces"
    if [ ! -d "$folderWithSpaces" ]
      then
        mkdir "$folderWithSpaces"
    fi
    goToFolder $folderWithSpaces
    capturedReturn=$?

    weWishes --messageInterpretation "theCheckIs" --firstOperator $capturedReturn --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    
    
    header --section "... with two dots" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    goToFolder ..
    capturedReturn=$?

    weWishes --messageInterpretation "theCheckIs" --firstOperator $capturedReturn --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
  
    header --subsection "... (cleaning)" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    rm -r "$folderWithSpaces"

    goToFolder $savedFolder


} ; launchGroupTests folderTestings


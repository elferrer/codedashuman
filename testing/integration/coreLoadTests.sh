#!${BASH}
coreLoadTests() {
  echo -e "Loading... Core 'coreLoadTests', version 0.1"
}

projectName=$(echo 'Coded As Human' | base64)
projectToken="${projectName}"
baseFilename=$(basename ${BASH_SOURCE:-$0} | cut -d'.' -f1 )
taskIdentifier=$(normalizeString "${baseFilename}")



identifyTheTag=$(date +%s%N)
aliasTests="IntegrationCoreLoadTests"

unusedFunction() {
  echo -e "This function is unnecessary"
}

coreLoadTesting() {
  header --context "Check the basic functions are loaded" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""


  local functionsToCheck=("initializeGlobalVariable" "validateResponse" "returnFilenameWithoutExtension" "returnFilenameWithoutPath"  "loadFeature" "loadAddOn")
  for oneFunctionToCheck in ${functionsToCheck[@]}
    do
      header --subsection "Check function '${oneFunctionToCheck}'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

      local checkFunction=$(declare -f ${oneFunctionToCheck} > /dev/null ; echo $?)
      local validateResult=$(validateResponse ${checkFunction})

      weWishes --firstOperator ${validateResult} --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
  done

  header --subsection "Check function 'unusedFunction' aren't loaded" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local anyFunctionAreLoaded=$(unusedFunction)
    weWishes --messageInterpretation "lenghtOfString" --firstOperator ${#anyFunctionAreLoaded} --sentence ">" --secondOperator 0 --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local checkFunction=$(declare -f "unusedFunction" > /dev/null ; echo $?)
    local validateResult=$(validateResponse ${checkFunction})
    weWishes --firstOperator ${validateResult} --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""



  header --context "Testing core CAH loads" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    header --section "Check 'core addons' are loaded files & functions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local coreFilesToTest=(${coreSupport})
    for oneCoreFile in ${coreFilesToTest[@]}
      do
        header --subsection "Check core '${oneCoreFile}'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local getMessage=$(checkIfConfigurationFileExist "${shell}" "${corePath}" "${oneCoreFile}")
        local checkCoreFile=$?
        weWishes --firstOperator "${checkCoreFile}" --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local checkCoreFile=$(checkIfFunctionIsLoaded "${oneCoreFile}")
        weWishes --firstOperator ${checkCoreFile} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    done

    header --section "Check 'configurations' are loaded files & functions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local thisLocation=$PWD

    goToFolder $cahPath/${configurationPath}
    local configurationFilesToTest=$(ls *${shell_extension})
    goToFolder $thisLocation

    for oneConfigurationFile in ${configurationFilesToTest[@]}
      do
        oneConfigurationFile=$(returnFilenameWithoutExtension ${oneConfigurationFile})

        header --subsection "Check configuration '${oneConfigurationFile}'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local getMessage=$(checkIfConfigurationFileExist "${shell}" "${corePath}" "${oneCoreFile}")
        local checkModuleFile=$?
        weWishes --firstOperator "${checkModuleFile}" --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local checkConfigurationFile=$(checkIfFunctionIsLoaded "${oneConfigurationFile}")
        weWishes --firstOperator ${checkConfigurationFile} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    done


    header --section "Check 'services' are loaded files & functions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local thisLocation=$PWD
    goToFolder $cahPath/${shell}/${servicesPath}
    local servicesFilesToTest=$(ls *${shell_extension})
    goToFolder $thisLocation

    for oneServiceFile in ${servicesFilesToTest[@]}
      do
        oneServiceFile=$(returnFilenameWithoutExtension ${oneServiceFile})

        header --subsection "Check service '${oneServiceFile}'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local getMessage=$(checkIfConfigurationFileExist "${shell}" "${servicesPath}" "${oneServiceFile}")
        local checkServiceFile=$?
        weWishes --firstOperator "${checkServiceFile}" --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local checkServiceFile=$(checkIfFunctionIsLoaded "${oneServiceFile}")
        weWishes --firstOperator ${checkServiceFile} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    done


    header --section "Check 'modules' are loaded files & functions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local thisLocation=$PWD
    goToFolder $cahPath/${shell}/${modulesPath}
    local moduleFilesToTest=$(ls *${shell_extension})
    goToFolder $thisLocation

    for oneModuleFile in ${moduleFilesToTest[@]}
      do
        oneModuleFile=$(returnFilenameWithoutExtension ${oneModuleFile})

        header --subsection "Check module '${oneModuleFile}'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local getMessage=$(checkIfConfigurationFileExist "${shell}" "${modulesPath}" "${oneModuleFile}")
        local checkModuleFile=$?
        weWishes --firstOperator "${checkModuleFile}" --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local checkModuleFile=$(checkIfFunctionIsLoaded "${oneModuleFile}")
        weWishes --firstOperator ${checkModuleFile} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    done


    header --section "Check 'actions' are loaded files & functions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local thisLocation=$PWD
    goToFolder $cahPath/${shell}/${actionsPath}
    local actionsFilesToTest=$(ls *${shell_extension})
    goToFolder $thisLocation

    for oneActionFile in ${actionsFilesToTest[@]}
      do
        oneActionFile=$(returnFilenameWithoutExtension ${oneActionFile})

        header --subsection "Check action '${oneActionFile}'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local getMessage=$(checkIfConfigurationFileExist "${shell}" "${actionsPath}" "${oneActionFile}")
        local checkActionFile=$?
        weWishes --firstOperator "${checkActionFile}" --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local checkActionFile=$(checkIfFunctionIsLoaded "${oneActionFile}")
        weWishes --firstOperator ${checkActionFile} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
      done


    header --section "Check 'languages' are loaded files & functions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local thisLocation=$PWD
    goToFolder "$cahPath/${languagesPath}/${typeOfLanguagePath}"
    local languageFilesToTest=$(ls *${shell_extension})
    goToFolder $thisLocation

    for oneLanguageFile in ${languageFilesToTest[@]}
      do
        oneLanguageFile=$(returnFilenameWithoutExtension ${oneLanguageFile})

        header --subsection "Check language '${oneLanguageFile}'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local getMessage=$(checkIfConfigurationFileExist "${languagesPath}" "${typeOfLanguagePath}" "${oneLanguageFile}")
        local checkLanguage=$?
        weWishes --firstOperator "${checkLanguage}" --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local checkLanguage=$(checkIfFunctionIsLoaded "${oneLanguageFile}")
        weWishes --firstOperator ${checkLanguage} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    done


    header --section "Check 'interfaces' are loaded files & functions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local thisLocation=$PWD
    goToFolder $cahPath/${shell}/${interfacesPath}
    local interfaceFilesToTest=$(ls *${shell_extension})
    goToFolder $thisLocation

    for oneInterfaceFile in ${interfaceFilesToTest[@]}
      do
        oneInterfaceFile=$(returnFilenameWithoutExtension ${oneInterfaceFile})

        header --subsection "Check interface '${oneInterfaceFile}'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local getMessage=$(checkIfConfigurationFileExist "${shell}" "${interfacesPath}" "${oneInterfaceFile}")
        local checkInterface=$?
        weWishes --firstOperator "${checkInterface}" --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local checkInterface=$(checkIfFunctionIsLoaded "${oneInterfaceFile}")
        weWishes --firstOperator ${checkInterface} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    done


    header --section "Check 'distributions' are loaded files & functions" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

    local thisLocation=$PWD
    goToFolder $cahPath/${shell}/${distributionsPath}
    local distributionsFilesToTest=$(ls *${shell_extension})
    goToFolder $thisLocation

    for oneDistributionFile in ${distributionsFilesToTest[@]}
      do
        oneDistributionFile=$(returnFilenameWithoutExtension ${oneDistributionFile})

        header --subsection "Check distribution '${oneDistributionFile}'" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

        local getMessage=$(checkIfConfigurationFileExist "${shell}" "${distributionsPath}" "${oneDistributionFile}")
        local checkDistribution=$?
        weWishes --firstOperator "${checkDistribution}" --sentence "==" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
        local checkDistribution=$(checkIfFunctionIsLoaded "${oneDistributionFile}")
        weWishes --firstOperator ${checkDistribution} --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    done

} ; launchGroupTests coreLoadTesting

checkValidationTesting() {
  header --context "Check validation..." --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

  header --section "... when it return a OK code" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    thisIsOK=$(validateResponse ${validatedTheCodeWithTRUE})
    thisIsKO=$(validateResponse ${validatedTheCodeWithFALSE})
    systemError=$(validateResponse "the return not have a code message")

    weWishes  --messageInterpretation "theCheckIs" --firstOperator "${thisIsOK}" --sentence "=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${thisIsKO}" --sentence "=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${thisIsOK}" --sentence "!=" --secondOperator "${thisIsKO}" --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${systemError}" --sentence "!=" --secondOperator ${validatedTheCodeWithTRUE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""
    weWishes --messageInterpretation "theCheckIs" --firstOperator "${systemError}" --sentence "!=" --secondOperator ${validatedTheCodeWithFALSE} --alias "${aliasTests}" --labels "CAH" --momentum "${identifyTheTag}" --parent "" --explanation ""

} ; launchGroupTests checkValidationTesting


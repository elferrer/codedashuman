#!${BASH}
default() {
  echo -e "Loading... Distribution 'Default', version 0.1"
}

# initializeGlobalVariable "distribution" "default"

updatePackagesWithDefault() {
  local validation

  local update=$(sudo pkcon -y update)
  validation=$?

  return $validation
}

downloadPackagesWithDefault() {
  local validation

  local update=$(sudo pkcon -y get-updates --plain)
  validation=$?
  if [ "${validation}" = "5" ]
    then
      local noUpdatesPending=${validatedTheCodeWithTRUE}
    else
      local noUpdatesPending=${validatedTheCodeWithFALSE}
  fi

  return $noUpdatesPending
}

refreshCacheWithDefault() {
  local validation

  local update=$(sudo pkcon -y refresh)
  validation=$?

  return $validation
}

catchPackageWithDefault() {
  local theProgram="${1}"
  local pathOfProgram
  local validation

  pathOfProgram=$(dpkg-query -f '${binary:Package}\n' -W | grep "${theProgram[@]}")
  validation=$?

  echo -e "${pathOfProgram}"
  return $validation
}

installPackageWithDefault() {
  local theProgram=${1}
  local installation
  local validation

  installation=$(sudo dpkg -i "${theProgram[@]}")
  validation=$?

  echo -e "${installation[@]}"
  return $validation
}

catchProgramWithDefault() {
  local theProgram="${1}"
  local pathOfProgram
  local validation

  pathOfProgram=$(which "${theProgram[@]}")
  validation=$?

  echo -e "${pathOfProgram}"
  return $validation
}

installProgramWithDefault() {
  local theProgram=${1}
  local installation
  local validation

  installation=$(sudo apt -y install "${theProgram[@]}")
  validation=$?

  echo -e "${installation[@]}"
  return $validation
}

removeProgramWithDefault() {
  local theProgram=${1}
  local uninstallation
  local validation

  uninstallation=$(sudo apt -y remove "${theProgram[@]}")
  validation=$?

  echo -e "${uninstallation[@]}"
  return $validation
}

catchGemWithDefault() {
  local theGem="${1}"
  local pathOfGem
  local validation

  pathOfGem=$(gem list "^${theGem[@]}$" -i)
  validation=$?

  echo -e "${pathOfGem}"
  return $validation
}

installGemWithDefault() {
  local theGem=${1}
  local installation
  local validation

  if [ -z "${GEM_HOME}" ]
    then
      echo "${gemHomeUnstablished}"
      installation=$(sudo gem install "${theGem[@]}")
      validation=$?
    else
      installation=$(gem install "${theGem[@]}")
      validation=$?
  fi


  echo -e "${installation[@]}"
  return $validation
}

removeGemWithDefault() {
  local theGem=${1}
  local uninstallation
  local validation

  if [ -z "${GEM_HOME}" ]
    then
      echo "${gemHomeUnstablished}"
      uninstallation=$(sudo gem uninstall "${theGem[@]}")
      validation=$?
    else
      uninstallation=$(gem uninstall "${theGem[@]}")
      validation=$?
  fi

  echo -e "${uninstallation[@]}"
  return $validation
}

canonicalizePathWithDefault() {
  local path=${1}
  local expandedPath
  local validation
  expandedPath=$(readlink --canonicalize ${path})
  validation=$?

  echo -e "${expandedPath}"
  return $validation
}

createFolderWithDefault() {
  local theFolder=${1}
  local validation
  local message

  message=$(mkdir -p "${theFolder}")
  validation=$?

  message=$(applySystemMessage ${validation} ${message})

  echo -e "${message[@]}"
  return $validation
}

removeFolderWithDefault() {
  local theFolder=${1}
  local validation
  local message

  message=$(rm -r "${theFolder}")
  validation=$?

  message=$(applySystemMessage ${validation} ${message})

  echo -e "${message[@]}"
  return $validation
}

copyFolderWithDefault() {
  local originFolder=${1}
  local destinationFolder=${2}
  local savedLocation=$PWD
  local validation
  local creation

  goToFolder ${originFolder}
  creation=$(tar cf - . | (goToFolder "${destinationFolder}" ; tar xvf - ) )
  validation=$?
  goToFolder $savedLocation

  creation=$(applySystemMessage ${validation} ${validation})

  echo -e "${creation[@]}"
  return $validation
}

createFileWithDefault() {
  local thePath=${1}
  local validation
  local creation
  local message

  message=$(touch "${thePath}")
  validation=$?

  creation=$(applySystemMessage ${validation} ${message})

  echo -e "${creation[@]}"
  return $validation
}

softDeleteFileWithDefault() {
  local thePath=${1}
  local validation
  local remove
  local message

  message=$(unlink "${thePath}")
  validation=$?

  remove=$(applySystemMessage ${validation} ${message})

  echo -e "${remove[@]}"
  return $validation
}

addLineToFileWithDefault() {
  local theLine="${1}"
  local thePath=${2}
  local validation
  local result
  local message

  creation=$(echo -e "${theLine[@]}" >> "${thePath}")
  validation=$?
  creation=$(applySystemMessage ${validation} ${creation})
  validation=$?
  message=$(applyBooleanMessage ${creation})

  echo -e "${message}"
  return $validation
}

isTheContentInTheFileWithDefault() {
  local theLine="${1}"
  local thePath="${2}"
  local atTheBeginningOfTheLine="${3}"
  local validation
  local check
  local message

  if [ "${atTheBeginningOfTheLine[@]}" != true ]
    then
      check=$(awk -v s="${theLine[@]}" 'index($0, s) != 1' "${thePath}")
    else
      check=$(awk -v s="${theLine[@]}" 'index($0, s) == 1' "${thePath}")
  fi
  message=$(applyBooleanMessage ${check})
  validation=$?

  echo -e "${message}"
  return $validation
}

countContentInTheFileWithDefault() {
  local theLine="${1}"
  local thePath=${2}
  local validation
  local creation
  local message

  message=$(grep -o -i "${theLine[@]}" "${thePath}"  | wc -l )
  validation=$?

  echo -e "${message}"
  return $validation
}

collectDateWithDefault() {
  local validation
  local collected

  collected=$(date -u)
  validation=$?

  echo -e "${collected}"
  return $validation
}

collectUserWithDefault() {
  local validation
  local collected

  collected="$USER"
  validation=$?

  echo -e "${collected}"
  return $validation
}

collectPathWithDefault() {
  local validation
  local collected

  collected="$PWD"
  validation=$?

  echo -e "${collected}"
  return $validation
}

whoIsItWithDefault() {
  local validation
  local whoIsIt

  whoIsIt=$(logname)
  validation=$?

  echo -e "${whoIsIt[@]}"
  return $validation
}

isUserInsideGroupsWithDefault() {
  local searchUser="${1}"
  local validation
  local isUserInsideGroups

  isUserInsideGroups=$(groups "${searchUser[@]}")
  validation=$?

  echo -e "${isUserInsideGroups[@]}"
  return $validation
}

catchDistroByIdWithDefault() {
  local momentum="${1}"
  local validation
  local catchDistro

  local id="^${momentum[@]}\\="
  catchDistro=$( cat /etc/*[_-][vr]el* | grep ${id[@]} | tr -d "${momentum[@]}=" | tr -d '"' )
  validation=$?

  echo -e "${catchDistro[@]}"
  return $validation
}

listAptEntriesWithDefault() {
  local list=$(find /etc/apt/ -name \*.list)
  for file in ${list[@]} ; do
    grep -Po "(?<=^deb\s).*?(?=#|$)" $file | while read entry ; do
      local host=$(echo $entry | returnTheNumberedPieceFromEndByCutting 3 "/")
      local user=$(echo $entry | returnTheNumberedPieceFromEndByCutting 4 "/")
      local ppa=$(echo $entry | returnTheNumberedPieceFromEndByCutting 5 "/")

      if [ "ppa.launchpad.net" = "$host" ]
      then
        echo -e "ppa:$user/$ppa"
      else
        echo -e "${entry}"
      fi
    done
  done
}

isTheContentInTheStringWithDefault() {
  local theContent="${1}"
  local theString="${2}"
  local validation
  local creation
  local message

  creation=$(echo "${theString[@]}" | grep -e "${theContent[@]}")
  validation=$?
  message=$(applyBooleanMessage ${creation})

  echo -e "${message}"
  return $validation
}

addRepoWithDefault() {
  local repository="${1}"
  local validation
  local addRepo

  addRepo=$(sudo add-apt-repository -y ${repository})
  validation=$?

  echo -e "${addRepo[@]}"
  return $validation
}

removeRepoWithDefault() {
  local repository="${1}"
  local validation
  local removeRepo

  removeRepo=$(sudo add-apt-repository -ry ${repository})
  validation=$?

  echo -e "${removeRepo[@]}"
  return $validation
}

installRecipeWithDefault() {
  local theRecipe=${1}
  local theAlias="${2}"
  local theMomentum="${3}"
  local theLabels=("${4}")
  local theBulkData=("${5}")
  local installation
  local validation

  installation=$(recipe${theRecipe}WithDefault "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}")
  validation=$?

  echo -e "${installation[@]}"
  return $validation
}

reciperubyWithDefault() {
  local theAlias="${1}"
  local theMomentum="${2}"
  local theLabels=("${3}")
  local theBulkData=("${4}")

  installProgramWith${distribution} "ruby-full"

  oldTempFolder=$tempFolder
  tempFolder="~"

  filename=~/.bashrc
  ensureBriefingTask --name "${filename}"  --alias "${theAlias}" --labels "${theLabels}" --momentum "${theMomentum}" --bulkData "${theBulkData[@]}"
  includeText='# Configure Ruby Gems to ~/gems'
  ensureRecipeIngredient  --name "${filename}" --line "${includeText[@]}" --alias "${theAlias}" --labels "${theLabels}" --momentum "${theMomentum}" --bulkData "${theBulkData[@]}"
  includeText='export GEM_HOME="$HOME/gems"'
  ensureRecipeIngredient  --name "${filename}" --line "${includeText[@]}" --alias "${theAlias}" --labels "${theLabels}" --momentum "${theMomentum}" --bulkData "${theBulkData[@]}"
  includeText='export PATH="$HOME/gems/bin:$PATH"'
  ensureRecipeIngredient  --name "${filename}" --line "${includeText[@]}"  --alias "${theAlias}" --labels "${theLabels}" --momentum "${theMomentum}" --bulkData "${theBulkData[@]}"

  . ${filename}
  echo "${GEM_HOME}"
  tempFolder=$oldTempFolder
}

getEpochDateWithDefault() {
  local theDate="${1}"
  local validation
  local resultOfCheck

  resultOfCheck=$(date +%s%N -d "${theDate[@]}")
  validation=$?

  echo -e "${resultOfCheck[@]}"
  return $validation
}

getCurrentDateWithDefault() {
  local validation
  local resultOfCheck

  resultOfCheck=$(date --rfc-3339='ns')
  validation=$?

  echo -e "${resultOfCheck[@]}"
  return $validation
}

getLastDateChangeInTheFileWithDefault() {
  local filename="${1}"
  local resultOfCheck
  local validation=${validatedTheCodeWithFALSE}

  local dateOfLastUpdate=$(stat -c '%z' "${filename}")
  local dateOfLastUpdateDate=$(echo "${dateOfLastUpdate}" | cut -d' ' -f1)
  local dateOfLastUpdateTime=$(echo "${dateOfLastUpdate}" | cut -d' ' -f2)
  local dateOfLastUpdateUTC=$(echo "${dateOfLastUpdate}" | cut -d' ' -f3)
  local dateOfLastUpdateUtcTime=$(echo "${dateOfLastUpdateTime}${dateOfLastUpdateUTC}")

  resultOfCheck=$(echo "${dateOfLastUpdateDate} ${dateOfLastUpdateUtcTime}" | sed 's/\(.*\)\([0-9][0-9]\)/\1:\2/')
  validation=$?

  echo -e "${resultOfCheck[@]}"
  return $validation
}

waitForTheFileWithDefault() {
  local file="${1}"
  local validation

  for waiting in {0..10}
    do 
      if [ ! -f "${file}" ]
        then
          sleep 1
        else
          break
      fi
  done 

  if [ ! -f "${file}" ]
    then
      validation=${validatedTheCodeWithFALSE}
    else
      validation=${validatedTheCodeWithTRUE}
  fi
  return "${validation}"
}

sendMessageToNetworkWithDefault() {
  local message="${1}"
  local sendMessageToHost="${2}"
  local sendMessageToPort="${3}"
  local waitForResponseMessage="${4}"
  local validation=${validatedTheCodeWithFALSE}

  printf "${message[@]}" | nc "${sendMessageToHost}" "${sendMessageToPort}" -q "${waitForResponseMessage}"
  validation=$?

  return $validation
}

getMessageFromNetworkWithDefault() {
  local getMessageFromHost="${1}"
  local getMessageFromPort="${2}"
  local validation=${validatedTheCodeWithFALSE}

  while true
    do
      nc "${getMessageFromHost}" "${getMessageFromPort}" -l
      validation=$?
  done

  return $validation
}

repeatMessageToNetworkWithDefault() {
  local getMessageFromHost="${1}"
  local getMessageFromPort="${2}"
  local sendMessageToHost="${3}"
  local sendMessageToPort="${4}"
  local waitForResponseMessage="${5}"
  local validation=${validatedTheCodeWithFALSE}

  nc "${getMessageFromHost}" "${getMessageFromPort}" -l | nc "${sendMessageToHost}" "${sendMessageToPort}" -q "${waitForResponseMessage}"
  validation=$?

  return $validation
}

liftContentFromDinamicVariableWithDefault() {
  for iterationInArguments in "$@"
    do
      local option=${1}
      shift
      local content=${1}
      case ${option} in
        "--newVariable" )
          local theNameForVariable="${content[@]}"
        ;;
        "--validation" )
          local validation="${content[@]}"
        ;;
        "--getContentFrom" )
          local actionMessage="${content[@]}"
        ;;
      esac
  done

  local theMessage

  case ${validation} in
    ${validatedTheCodeWithTRUE} ) typeOfResponse=${appellationForSUCCESS} ;;
    ${validatedTheCodeWithFALSE} ) typeOfResponse=${appellationForERROR} ;;
    * ) typeOfResponse="" ;;
  esac
  theMessage[0]=$(dynamicLoadFromVariable "${actionMessage}${typeOfResponse}[0]")
  theMessage[1]=$(dynamicLoadFromVariable "${actionMessage}${typeOfResponse}[1]")
  theMessage[2]=$(dynamicLoadFromVariable "${actionMessage}${typeOfResponse}[2]")
  theMessage[3]=$(dynamicLoadFromVariable "${actionMessage}${typeOfResponse}[3]")
  readarray -t $theNameForVariable <<< $(echo -e "${theMessage[0]}\n${theMessage[1]}\n${theMessage[2]}\n${theMessage[3]}")
}

goToFolderWithDefault() {
  local folder=$*

  cd "${folder[@]}"

  return $?
}

validateResponseWithDefault() {
  local expressionToCheck=${1}
  local validation
  case  ${expressionToCheck} in
    0 | ${validatedTheCodeWithTRUE} ) validation=${validatedTheCodeWithTRUE} ;;
    [1-90-9]* | ${validatedTheCodeWithFALSE} ) validation=${validatedTheCodeWithFALSE} ;;
    * ) echo -e "${validationHasFailed[@]}" ;;
  esac
  echo -e ${validation}
}

isNumberWithDefault() {
  local theString="${1}"
  local isNumber="x[0-9]*$"
  local result

  if [ $(expr "x$theString" : $isNumber) -gt 0 ]
    then
      result=${validatedTheCodeWithTRUE}
    else
      result=${validatedTheCodeWithFALSE}
  fi
  echo -e ${result}
}

normalizeStringWithDefault() {
  local theString="${1}"
  local result

  echo "$theString" | tr -cd '[:alnum:]'
}

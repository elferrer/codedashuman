#!${BASH}
debian() {
  echo -e "Loading... Distribution 'debian', version 0.1"
}


initializeGlobalVariable "distribution" "debian"

updatePackagesWithdebian() {
  updatePackagesWithDefault "$@"
  return $?
}

downloadPackagesWithdebian() {
  downloadPackagesWithDefault "$@"
  return $?
}

refreshCacheWithdebian() {
  refreshCacheWithDefault "$@"
  return $?
}

catchPackageWithdebian() {
  catchPackageWithDefault "$@"
  return $?
}

installPackageWithdebian() {
  installPackageWithDefault "$@"
  return $?
}

catchProgramWithdebian() {
  catchProgramWithDefault "$@"
  return $?
}

installProgramWithdebian() {
  installProgramWithDefault "$@"
  return $?
}

removeProgramWithdebian() {
  removeProgramWithDefault "$@"
  return $?
}

catchGemWithdebian() {
  catchGemWithDefault "$@"
  return $?
}

installGemWithdebian() {
  installGemWithDefault "$@"
  return $?
}

removeGemWithdebian() {
  removeGemWithDefault "$@"
  return $?
}

canonicalizePathWithdebian() {
  canonicalizePathWithDefault "$@"
  return $?
}

createFolderWithdebian() {
  createFolderWithDefault "$@"
  return $?
}

removeFolderWithdebian() {
  removeFolderWithDefault "$@"
  return $?
}

copyFolderWithdebian() {
  copyFolderWithDefault "$@"
  return $?
}

createFileWithdebian() {
  createFileWithDefault "$@"
  return $?
}

softDeleteFileWithdebian() {
  softDeleteFileWithDefault "$@"
  return $?
}

addLineToFileWithdebian() {
  addLineToFileWithDefault "$@"
  return $?
}

isTheContentInTheFileWithdebian() {
  isTheContentInTheFileWithDefault "$@"
  return $?
}

countContentInTheFileWithdebian() {
  countContentInTheFileWithDefault "$@"
  return $?
}

collectDateWithdebian() {
  collectDateWithDefault "$@"
  return $?
}

collectUserWithdebian() {
  collectUserWithDefault "$@"
  return $?
}

collectPathWithdebian() {
  collectPathWithDefault "$@"
  return $?
}

whoIsItWithdebian() {
  whoIsItWithDefault "$@"
  return $?
}

isUserInsideGroupsWithdebian() {
  isUserInsideGroupsWithDefault "$@"
  return $?
}

catchDistroByIdWithdebian() {
  catchDistroByIdWithDefault "$@"
  return $?
}

listAptEntriesWithdebian() {
  listAptEntriesWithDefault "$@"
  return $?
}

isTheContentInTheStringWithdebian() {
  isTheContentInTheStringWithDefault "$@"
  return $?
}

addRepoWithdebian() {
  addRepoWithDefault "$@"
  return $?
}

removeRepoWithdebian() {
  removeRepoWithDefault "$@"
  return $?
}

installRecipeWithdebian() {
  installRecipeWithDefault "$@"
  return $?
}

reciperubyWithdebian() {
  reciperubyWithDefault "$@"
  return $?
}

getEpochDateWithdebian() {
  getEpochDateWithDefault "$@"
  return $?
}

getCurrentDateWithdebian() {
  getCurrentDateWithDefault "$@"
  return $?
}

getLastDateChangeInTheFileWithdebian() {
  getLastDateChangeInTheFileWithDefault "$@"
  return $?
}

waitForTheFileWithdebian() {
  waitForTheFileWithDefault "$@"
  return $?
}

sendMessageToNetworkWithdebian() {
  sendMessageToNetworkWithDefault "$@"
  return $?
}

getMessageFromNetworkWithdebian() {
  getMessageFromNetworkWithDefault "$@"
  return $?
}

repeatMessageToNetworkWithdebian() {
  repeatMessageToNetworkWithDefault "$@"
  return $?
}

liftContentFromDinamicVariableWithdebian() {
  liftContentFromDinamicVariableWithDefault "$@"
  return $?
}

goToFolderWithdebian() {
  goToFolderWithDefault "$@"
  return $?
}

validateResponseWithdebian() {
  validateResponseWithDefault "$@"
  return $?
}

isNumberWithdebian() {
  isNumberWithDefault "$@"
  return $?
}

normalizeStringWithdebian() {
  normalizeStringWithDefault "$@"
  return $?
}

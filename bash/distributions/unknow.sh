#!${BASH}
unknow() {
  echo -e "Loading... Distribution 'UNKNOW', version 0.1"
}

# initializeGlobalVariable "distribution" "UNKNOW"

updatePackagesWithUNKNOW() {
  updatePackagesWithDefault "$@"
  return $?
}

downloadPackagesWithUNKNOW() {
  downloadPackagesWithDefault "$@"
  return $?
}

refreshCacheWithUNKNOW() {
  refreshCacheWithDefault "$@"
  return $?
}

catchPackageWithUNKNOW() {
  catchPackageWithDefault "$@"
  return $?
}

installPackageWithUNKNOW() {
  installPackageWithDefault "$@"
  return $?
}

catchProgramWithUNKNOW() {
  catchProgramWithDefault "$@"
  return $?
}

installProgramWithUNKNOW() {
  installProgramWithDefault "$@"
  return $?
}

removeProgramWithUNKNOW() {
  removeProgramWithDefault "$@"
  return $?
}

catchGemWithUNKNOW() {
  catchGemWithDefault "$@"
  return $?
}

installGemWithUNKNOW() {
  installGemWithDefault "$@"
  return $?
}

removeGemWithUNKNOW() {
  removeGemWithDefault "$@"
  return $?
}

canonicalizePathWithUNKNOW() {
  canonicalizePathWithDefault "$@"
  return $?
}

createFolderWithUNKNOW() {
  createFolderWithDefault "$@"
  return $?
}

removeFolderWithUNKNOW() {
  removeFolderWithDefault "$@"
  return $?
}

copyFolderWithUNKNOW() {
  copyFolderWithDefault "$@"
  return $?
}

createFileWithUNKNOW() {
  createFileWithDefault "$@"
  return $?
}

softDeleteFileWithUNKNOW() {
  softDeleteFileWithDefault "$@"
  return $?
}

addLineToFileWithUNKNOW() {
  addLineToFileWithDefault "$@"
  return $?
}

isTheContentInTheFileWithUNKNOW() {
  isTheContentInTheFileWithDefault "$@"
  return $?
}

countContentInTheFileWithUNKNOW() {
  countContentInTheFileWithDefault "$@"
  return $?
}

collectDateWithUNKNOW() {
  collectDateWithDefault "$@"
  return $?
}

collectUserWithUNKNOW() {
  collectUserWithDefault "$@"
  return $?
}

collectPathWithUNKNOW() {
  collectPathWithDefault "$@"
  return $?
}

whoIsItWithUNKNOW() {
  whoIsItWithDefault "$@"
  return $?
}

isUserInsideGroupsWithUNKNOW() {
  isUserInsideGroupsWithDefault "$@"
  return $?
}

catchDistroByIdWithUNKNOW() {
  catchDistroByIdWithDefault "$@"
  return $?
}

listAptEntriesWithUNKNOW() {
  listAptEntriesWithDefault "$@"
  return $?
}

isTheContentInTheStringWithUNKNOW() {
  isTheContentInTheStringWithDefault "$@"
  return $?
}

addRepoWithUNKNOW() {
  addRepoWithDefault "$@"
  return $?
}

removeRepoWithUNKNOW() {
  removeRepoWithDefault "$@"
  return $?
}

installRecipeWithUNKNOW() {
  installRecipeWithDefault "$@"
  return $?
}

reciperubyWithUNKNOW() {
  reciperubyWithDefault "$@"
  return $?
}

getEpochDateWithUNKNOW() {
  getEpochDateWithDefault "$@"
  return $?
}

getCurrentDateWithUNKNOW() {
  getCurrentDateWithDefault "$@"
  return $?
}

getLastDateChangeInTheFileWithUNKNOW() {
  getLastDateChangeInTheFileWithDefault "$@"
  return $?
}

waitForTheFileWithUNKNOW() {
  waitForTheFileWithDefault "$@"
  return $?
}

sendMessageToNetworkWithUNKNOW() {
  sendMessageToNetworkWithDefault "$@"
  return $?
}

getMessageFromNetworkWithUNKNOW() {
  getMessageFromNetworkWithDefault "$@"
  return $?
}

repeatMessageToNetworkWithUNKNOW() {
  repeatMessageToNetworkWithDefault "$@"
  return $?
}

liftContentFromDinamicVariableWithUNKNOW() {
  liftContentFromDinamicVariableWithDefault "$@"
  return $?
}

goToFolderWithUNKNOW() {
  goToFolderWithDefault "$@"
  return $?
}

validateResponseWithUNKNOW() {
  validateResponseWithDefault "$@"
  return $?
}

normalizeStringWithUNKNOW() {
  normalizeStringWithDefault "$@"
  return $?
}

isNumberWithUNKNOW() {
  isNumberWithDefault "$@"
  return $?
}

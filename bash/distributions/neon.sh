#!${BASH}
neon() {
  echo -e "Loading... Distribution 'neon', version 0.1"
}


initializeGlobalVariable "distribution" "neon"

updatePackagesWithneon() {
  updatePackagesWithubuntu "$@"
  return $?
}

downloadPackagesWithneon() {
  downloadPackagesWithubuntu "$@"
  return $?
}

refreshCacheWithneon() {
  refreshCacheWithubuntu "$@"
  return $?
}

catchPackageWithneon() {
  catchPackageWithubuntu "$@"
  return $?
}

installPackageWithneon() {
  installPackageWithubuntu "$@"
  return $?
}

catchProgramWithneon() {
  catchProgramWithubuntu "$@"
  return $?
}

installProgramWithneon() {
  installProgramWithubuntu "$@"
  return $?
}

removeProgramWithneon() {
  removeProgramWithubuntu "$@"
  return $?
}

catchGemWithneon() {
  catchGemWithubuntu "$@"
  return $?
}

installGemWithneon() {
  installGemWithubuntu "$@"
  return $?
}

removeGemWithneon() {
  removeGemWithubuntu "$@"
  return $?
}

canonicalizePathWithneon() {
  canonicalizePathWithubuntu "$@"
  return $?
}

createFolderWithneon() {
  createFolderWithubuntu "$@"
  return $?
}

removeFolderWithneon() {
  removeFolderWithubuntu "$@"
  return $?
}

copyFolderWithneon() {
  copyFolderWithubuntu "$@"
  return $?
}

createFileWithneon() {
  createFileWithubuntu "$@"
  return $?
}

softDeleteFileWithneon() {
  softDeleteFileWithubuntu "$@"
  return $?
}

addLineToFileWithneon() {
  addLineToFileWithubuntu "$@"
  return $?
}

isTheContentInTheFileWithneon() {
  isTheContentInTheFileWithubuntu "$@"
  return $?
}

countContentInTheFileWithneon() {
  countContentInTheFileWithubuntu "$@"
  return $?
}

collectDateWithneon() {
  collectDateWithubuntu "$@"
  return $?
}

collectUserWithneon() {
  collectUserWithubuntu "$@"
  return $?
}

collectPathWithneon() {
  collectPathWithubuntu "$@"
  return $?
}

whoIsItWithneon() {
  whoIsItWithubuntu "$@"
  return $?
}

isUserInsideGroupsWithneon() {
  isUserInsideGroupsWithubuntu "$@"
  return $?
}

catchDistroByIdWithneon() {
  catchDistroByIdWithubuntu "$@"
  return $?
}

listAptEntriesWithneon() {
  listAptEntriesWithubuntu "$@"
  return $?
}

isTheContentInTheStringWithneon() {
  isTheContentInTheStringWithubuntu "$@"
  return $?
}

addRepoWithneon() {
  addRepoWithubuntu "$@"
  return $?
}

removeRepoWithneon() {
  removeRepoWithubuntu "$@"
  return $?
}

installRecipeWithneon() {
  installRecipeWithubuntu "$@"
  return $?
}

reciperubyWithneon() {
  reciperubyWithubuntu "$@"
  return $?
}

getEpochDateWithneon() {
  getEpochDateWithubuntu "$@"
  return $?
}

getCurrentDateWithneon() {
  getCurrentDateWithubuntu "$@"
  return $?
}

getLastDateChangeInTheFileWithneon() {
  getLastDateChangeInTheFileWithubuntu "$@"
  return $?
}

waitForTheFileWithneon() {
  waitForTheFileWithubuntu "$@"
  return $?
}

sendMessageToNetworkWithneon() {
  sendMessageToNetworkWithubuntu "$@"
  return $?
}

getMessageFromNetworkWithneon() {
  getMessageFromNetworkWithubuntu "$@"
  return $?
}

repeatMessageToNetworkWithneon() {
  repeatMessageToNetworkWithubuntu "$@"
  return $?
}

liftContentFromDinamicVariableWithneon() {
  liftContentFromDinamicVariableWithubuntu "$@"
  return $?
}

goToFolderWithneon() {
  goToFolderWithubuntu "$@"
  return $?
}

validateResponseWithneon() {
  validateResponseWithubuntu "$@"
  return $?
}

isNumberWithneon() {
  isNumberWithubuntu "$@"
  return $?
}

normalizeStringWithneon() {
  normalizeStringWithubuntu "$@"
  return $?
}

#!${BASH}
ubuntu() {
  echo -e "Loading... Distribution 'ubuntu', version 0.1"
}


initializeGlobalVariable "distribution" "ubuntu"

updatePackagesWithubuntu() {
  updatePackagesWithdebian "$@"
  return $?
}

downloadPackagesWithubuntu() {
  downloadPackagesWithdebian "$@"
  return $?
}

refreshCacheWithubuntu() {
  refreshCacheWithdebian "$@"
  return $?
}

catchPackageWithubuntu() {
  catchPackageWithdebian "$@"
  return $?
}

installPackageWithubuntu() {
  installPackageWithdebian "$@"
  return $?
}

catchProgramWithubuntu() {
  catchProgramWithdebian "$@"
  return $?
}

installProgramWithubuntu() {
  installProgramWithdebian "$@"
  return $?
}

removeProgramWithubuntu() {
  removeProgramWithdebian "$@"
  return $?
}

catchGemWithubuntu() {
  catchGemWithdebian "$@"
  return $?
}

installGemWithubuntu() {
  installGemWithdebian "$@"
  return $?
}

removeGemWithubuntu() {
  removeGemWithdebian "$@"
  return $?
}

canonicalizePathWithubuntu() {
  canonicalizePathWithdebian "$@"
  return $?
}

createFolderWithubuntu() {
  createFolderWithdebian "$@"
  return $?
}

removeFolderWithubuntu() {
  removeFolderWithdebian "$@"
  return $?
}

copyFolderWithubuntu() {
  copyFolderWithdebian "$@"
  return $?
}

createFileWithubuntu() {
  createFileWithdebian "$@"
  return $?
}

softDeleteFileWithubuntu() {
  softDeleteFileWithdebian "$@"
  return $?
}

addLineToFileWithubuntu() {
  addLineToFileWithdebian "$@"
  return $?
}

isTheContentInTheFileWithubuntu() {
  isTheContentInTheFileWithdebian "$@"
  return $?
}

countContentInTheFileWithubuntu() {
  countContentInTheFileWithdebian "$@"
  return $?
}

collectDateWithubuntu() {
  collectDateWithdebian "$@"
  return $?
}

collectUserWithubuntu() {
  collectUserWithdebian "$@"
  return $?
}

collectPathWithubuntu() {
  collectPathWithdebian "$@"
  return $?
}

whoIsItWithubuntu() {
  whoIsItWithdebian "$@"
  return $?
}

isUserInsideGroupsWithubuntu() {
  isUserInsideGroupsWithdebian "$@"
  return $?
}

catchDistroByIdWithubuntu() {
  catchDistroByIdWithdebian "$@"
  return $?
}

listAptEntriesWithubuntu() {
  listAptEntriesWithdebian "$@"
  return $?
}

isTheContentInTheStringWithubuntu() {
  isTheContentInTheStringWithdebian "$@"
  return $?
}

addRepoWithubuntu() {
  addRepoWithdebian "$@"
  return $?
}

removeRepoWithubuntu() {
  removeRepoWithdebian "$@"
  return $?
}

installRecipeWithubuntu() {
  installRecipeWithdebian "$@"
  return $?
}

reciperubyWithubuntu() {
  reciperubyWithdebian "$@"
  return $?
}

getEpochDateWithubuntu() {
  getEpochDateWithdebian "$@"
  return $?
}

getCurrentDateWithubuntu() {
  getCurrentDateWithdebian "$@"
  return $?
}

getLastDateChangeInTheFileWithubuntu() {
  getLastDateChangeInTheFileWithdebian "$@"
  return $?
}

waitForTheFileWithubuntu() {
  waitForTheFileWithdebian "$@"
  return $?
}

sendMessageToNetworkWithubuntu() {
  sendMessageToNetworkWithdebian "$@"
  return $?
}

getMessageFromNetworkWithubuntu() {
  getMessageFromNetworkWithdebian "$@"
  return $?
}

repeatMessageToNetworkWithubuntu() {
  repeatMessageToNetworkWithdebian "$@"
  return $?
}

liftContentFromDinamicVariableWithubuntu() {
  liftContentFromDinamicVariableWithdebian "$@"
  return $?
}

goToFolderWithubuntu() {
  goToFolderWithdebian "$@"
  return $?
}

validateResponseWithubuntu() {
  validateResponseWithdebian "$@"
  return $?
}

isNumberWithubuntu() {
  isNumberWithdebian "$@"
  return $?
}

normalizeStringWithubuntu() {
  normalizeStringWithdebian "$@"
  return $?
}

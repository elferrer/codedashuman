#!${BASH}
moduleLanguages() {
  echo -e "Loading... Module 'moduleLanguages', version 0.1"
}


applyLanguage() {
  local language=false
  local initialInfo=false
  local endInfo=false
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  local sendMessageOfCode="${messageReturnedByFunction[ErrorInApplyLanguage]}"
  local messageReturnedBySystem
  local getMessageIfConfigurationFileExist=$(checkIfConfigurationFileExist "${languagesPath}" "${typeOfLanguagePath}" "${language}")
  local validation=$?

  if [ "$validation" = ${validatedTheCodeWithTRUE} ]
    then
      loadAddOn "${languagesPath}" "${typeOfLanguagePath}" "${language}"
      ErrorInApplyLanguage=${appellationForSUCCESS}
      messageReturnedBySystem=$(captureEchoOfFunction ${language})
    else
      validation=${validatedTheCodeWithFALSE}
      ErrorInApplyLanguage=${appellationForERROR}
      messageReturnedBySystem="${getMessageIfConfigurationFileExist[@]}"
      sendMessageOfCode=$(checkIfFunctionIsLoaded "${language}")
  fi

  local validation=$(checkIfFunctionIsLoaded "${language}")

  stampValidations "${validation}" ${validatedTheCodeWithTRUE}

  local desiredResponse="${language}"
  local obtainedResponse="${activeLanguage[@]}"

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${desiredResponse[@]}" "=" "${obtainedResponse[@]}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"
  return ${validation}
}

#!${BASH}
moduleRunbookWishes() {
  echo -e "Loading... Module 'moduleRunbookWishes', version 0.1"
}


weWishes() {
  local taggedMessages=(false)
  local firstOperator=(false)
  local sentence=false
  local secondOperator=(false)
  local humanReadableFirstOperator=false
  local humanReadableSecondOperator=false
  local initialInfo=false
  local endInfo=false
  local firstOperatorMessageToTell
  local secondOperatorMessageToTell
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  setGenericParameters "$@"
  setParametersWeWishes "$@"

  local messageOfCode=${messageReturnedByFunction[ErrorInTest]}
  local messageReturnedBySystem=false

  local validation=$(validateSentence "${firstOperator[@]}" "${sentence[@]}" "${secondOperator[@]}")
  local translatedSentence=$(translateSentence "${sentence[@]}")


  if [ "${humanReadableFirstOperator[@]}" != false ]
    then
      firstOperatorMessageToTell="${humanReadableFirstOperator[@]}"
    else
      firstOperatorMessageToTell="${firstOperator[@]}"
  fi

  if [ "${humanReadableSecondOperator[@]}" != false ]
    then
      secondOperatorMessageToTell="${humanReadableSecondOperator[@]}"
    else
      secondOperatorMessageToTell="${secondOperator[@]}"
  fi

  stampValidations "${validation}" ${validatedTheCodeWithTRUE}

  local nameOfParentFunction
  if [ "${taggedMessages[@]}" == false ]
    then
      nameOfParentFunction="theCheckIs"
    else
      nameOfParentFunction="${taggedMessages[@]}"
  fi

  tellTheStory "${nameOfParentFunction[@]}" "${validation}" "${firstOperatorMessageToTell[@]}" "${translatedSentence[@]}" "${secondOperatorMessageToTell[@]}" "${messageReturnedBySystem[@]}" "${messageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return ${validation}
}

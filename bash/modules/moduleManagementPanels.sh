#!${BASH}
moduleManagementPanels() {
  echo -e "Loading... Artifact 'moduleManagementPanels', version 0.1"
}


ensureThePanel() {
  local name=false
  local initialInfo=false
  local endInfo=false
  local validation
  local validationCreation
  local defaultMessage
  local messageReturnedOfOneFunction
  local codeMessage
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  messageCheckFolder=$(checkIfFolderExist "${name}")
  validationCheckFolder=$?
  if [ "${validationCheckFolder}" == ${validatedTheCodeWithFALSE} ]
    then
      defaultMessage=$(createPanel "${name}")
      validationCreation=$?
    else
      validationCreation=${validatedTheCodeWithTRUE}
  fi

  local validator=${validatedTheCodeWithTRUE}

  stampValidations "${validationCreation}" "${validator}"
  validation=$?

  messageReturnedBySystem="${name}"

  messageReturnedOfOneFunction=$(setErrorMessage "${name}" "ErrorInCreatePanel")
  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfCreation=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${name}" "=" "${resultOfCreation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

eraseTheManagementPanel() {
  local name=false
  local initialInfo=false
  local endInfo=false
  local getMessageErasePanel
  local validation
  local validationDeletion
  local codeMessage
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  getMessageErasePanel=$(erasePanel "${name}")
  validationDeletion=$?

  local validator=${validatedTheCodeWithFALSE}

  stampValidations "${validationDeletion}" "${validator}"
  local validated=$?

  validation=$(convertValidationWhenItIsCounteractiveAction "${validated}")

  messageReturnedBySystem="${name}"

  messageReturnedOfOneFunction=$(setErrorMessage "${name}" "ErrorInErasePanel")
  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfDeletion=$(convertCodeResponseToNegatedBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${name}" "=" "${resultOfDeletion}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

copyTheManagementPanel() {
  local origin=false
  local destinationFolder=false
  local initialInfo=false
  local endInfo=false
  local validation
  local defaultMessage
  local validationCopy
  local messageReturnedOfOneFunction
  local messageReturnedBySystem
  local codeMessage
  local sendMessageOfCode
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  defaultMessage=$(copyPanel "${origin}" "${destinationFolder}")
  validationCopy=$?

  local validator=${validatedTheCodeWithTRUE}

  stampValidations "${validationCopy}" "${validator}"
  validation=$?

  messageReturnedOfOneFunction=$(setErrorMessage "${origin}" "ErrorInCopyPanel")
  messageReturnedBySystem=$(setValidationMessage "${defaultMessage[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfCreation=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${origin}=${appellationForSUCCESS} & ${destinationFolder}=${appellationForSUCCESS}" "=" "${resultOfCreation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

moveThePanel() {
  local origin=false
  local destinationFolder=false
  local initialInfo=false
  local endInfo=false
  local validation
  local defaultMessage
  local validationMove
  local messageReturnedOfOneFunction
  local messageReturnedBySystem
  local codeMessage
  local sendMessageOfCode
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  defaultMessage=$(movePanel "${origin}" "${destinationFolder}")
  validationMove=$?

  local validator=${validatedTheCodeWithTRUE}

  stampValidations "${validationMove}" "${validator}"
  validation=$?

  messageReturnedOfOneFunction=$(setErrorMessage "${origin}" "ErrorInMovePanel")
  messageReturnedBySystem=$(setValidationMessage "${defaultMessage[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfCreation=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${origin}=${appellationForSUCCESS} & ${destinationFolder}=${appellationForSUCCESS}" "=" "${resultOfCreation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo[@]}" "${endInfo[@]}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

ensureTheBriefingTask() {
  local name=false
  local initialInfo=false
  local endInfo=false
  local defaultMessage
  local validation
  local validationCreation
  local messageReturnedOfOneFunction
  local messageReturnedBySystem
  local codeMessage
  local sendMessageOfCode
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  messageCheckFile=$(checkIfFileExist "${name}")
  validationCheckFile=$?
  if [ "${validationCheckFile}" == ${validatedTheCodeWithFALSE} ]
    then
      defaultMessage=$(createBriefingTask "${name}")
      validationCreation=$?
    else
      validationCreation=${validatedTheCodeWithTRUE}
  fi

  local validator=${validatedTheCodeWithTRUE}

  stampValidations "${validationCreation}" "${validator}"
  validation=$?

  messageReturnedOfOneFunction=$(setErrorMessage "${name}" "FileExist")
  messageReturnedBySystem=$(setValidationMessage "${defaultMessage[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfCreation=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${name}" "=" "${resultOfCreation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

eraseTheBriefingTask() {
  local name=false
  local initialInfo=false
  local endInfo=false
  local getMessageEraseBriefingTask
  local validationDeletion
  local validation
  local messageReturnedOfOneFunction
  local messageReturnedBySystem
  local codeMessage
  local sendMessageOfCode
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  getMessageEraseBriefingTask=$(softDeleteBriefingTask "${name}")
  local validationDeletion=$?

  local validator=${validatedTheCodeWithFALSE}

  stampValidations "${validationDeletion}" "${validator}"
  local validated=$?

  validation=$(convertValidationWhenItIsCounteractiveAction "${validated}")

  messageReturnedOfOneFunction=$(setErrorMessage "${name}" "ErrorInEraseFile")
  messageReturnedBySystem=$(setValidationMessage "${getMessageEraseBriefingTask[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfDeletion=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${name}" "=" "${resultOfDeletion}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

ensureTheRecipeIngredient() {
  local line=false
  local name=false
  local atTheBeginningOfTheLine=false
  local initialInfo=false
  local endInfo=false
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  local defaultMessage
  defaultMessage=$(addRecipeIngredientToFile "${line[@]}" "${name[@]}" "${atTheBeginningOfTheLine[@]}")
  local validationCreation=$?

  local validator=${validatedTheCodeWithTRUE}

  stampValidations "${validationCreation}" "${validator}"
  validation=$?

  messageReturnedBySystem="${line[@]}"

  messageReturnedOfOneFunction=$(setErrorMessage "${line[@]}" "ErrorInAddRecipeIngredientToFile")
  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfCreation=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${line[@]} ('${name[@]}')" "=" "${resultOfCreation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

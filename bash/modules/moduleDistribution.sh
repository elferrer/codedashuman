#!${BASH}
moduleDistribution() {
  echo -e "Loading... Module 'moduleDistribution', version 0.1"
}


useDistro() {
  local name=false
  local initialInfo=false
  local endInfo=false
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  local sendMessageOfCode=${messageReturnedByFunction[ErrorInDistribution]}
  local messageReturnedBySystem=${unexplainedError}
  local validation
  local getMessage

  getMessageIfConfigurationFileExist=$(checkIfConfigurationFileExist "${shell}" "${distributionsPath}" "${name}")
  validation=$?
  local getList=$(catchDistributionList)
  local catchDistroList=(${getList[@]})

  case "$validation" in
    ${validatedTheCodeWithTRUE} )
      messageReturnedBySystem=$(selectDistro "${name}")
      distribution=${name}
    ;;
    ${validatedTheCodeWithFALSE} )
      for aDistro in "${catchDistroList[@]}"
        do
        getMessageIfConfigurationFileExist=$(checkIfConfigurationFileExist "${shell}" "${distributionsPath}" "${aDistro}")
        validation=$?
        if [ "$validation" = ${validatedTheCodeWithTRUE} ]
          then
            messageReturnedBySystem=$(selectDistro "${aDistro}")
            distribution="${aDistro}"
            break
          else
            messageReturnedBySystem="${getMessageIfConfigurationFileExist[@]}"
            validation=${validatedTheCodeWithFALSE}
        fi
      done
    ;;
  esac

  stampValidations "${validation}" ${validatedTheCodeWithTRUE}

  local listOfDistros=${catchDistroList[@]}

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${listOfDistros[@]}" "contain" "${distribution}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"
  return ${validation}
}

ensureRepository() {
  local repositoryName=false
  local repositoryType=false
  local initialInfo=false
  local endInfo=false
  local validation
  local defaultMessage
  local validationInstallation
  local messageReturnedOfOneFunction
  local codeMessage
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  if [ "${repositoryName[@]}" == false ] || [ "${repositoryType[@]}" == false ]
    then
      defaultMessage=${messageReturnedByFunction[Malformed]}
      validationInstallation=${validatedTheCodeWithFALSE}
    else
      defaultMessage=$(addToolRepository "${repositoryType[@]}" "${repositoryName[@]}")
      validationInstallation=$?
  fi

  local validator=${validatedTheCodeWithTRUE}

  stampValidations "${validationInstallation}" ${validator}
  validation=$?

  messageReturnedOfOneFunction=$(setErrorMessage "${repositoryName[@]}" "ErrorInEnsureRepository")
  messageReturnedBySystem=$(setValidationMessage "${defaultMessage[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfCreation=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${repositoryName[@]}" "=" "${resultOfCreation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

eraseRepository() {
  local repositoryName=false
  local repositoryType=false
  local initialInfo=false
  local endInfo=false
  local validation
  local defaultMessage
  local validationInstallation
  local messageReturnedOfOneFunction
  local codeMessage
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  if [ "${repositoryName[@]}" == false ] || [ "${repositoryType[@]}" == false ]
    then
      defaultMessage=${messageReturnedByFunction[Malformed]}
      validationInstallation=${validatedTheCodeWithFALSE}
    else
      defaultMessage=$(removeToolRepository "${repositoryType[@]}" "${repositoryName[@]}")
      validationInstallation=$?
  fi

  local validator=${validatedTheCodeWithFALSE}

  stampValidations "${validationInstallation}" ${validator}
  validation=$?

  messageReturnedOfOneFunction=$(setErrorMessage "${repositoryName[@]}" "ErrorInEraseRepository")
  messageReturnedBySystem=$(setValidationMessage "${defaultMessage[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfCreation=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${repositoryName[@]}" "=" "${resultOfCreation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

refreshRepository() {
  local validation
  local initialInfo=false
  local endInfo=false
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  local result=$(refreshCacheRepository)
  validation=$?

  setGenericParameters "$@"
  setMainParameters "$@"

  local message="${messageReturnedByFunction[Repository]}"
  messageReturnedOfOneFunction=$(setErrorMessage "${message}" "ErrorInRefreshRepository")
  messageReturnedBySystem=$(setValidationMessage "${defaultMessage[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${message}" "=" "${validation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

updateAllPackages() {
  local validation
  local initialInfo=false
  local endInfo=false
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  downloadPackagesList
  noUpdatesPending=$?
  if [ "${noUpdatesPending}" == "${validatedTheCodeWithFALSE}" ]
    then
      local result=$(updateThePackages)
      validation=$?
    else
      validation="${noUpdatesPending}"
  fi 

  setGenericParameters "$@"
  setMainParameters "$@"

  local message="${messageReturnedByFunction[Repository]}"
  messageReturnedOfOneFunction=$(setErrorMessage "${message}" "ErrorInUpdateAllPackages")
  messageReturnedBySystem=$(setValidationMessage "${defaultMessage[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${message}" "=" "${validation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

#!${BASH}
moduleProgramFeatures() {
  echo -e "Loading... Module 'moduleProgramFeatures', version 0.1"
}


applyToolInstallation() {
  local tool=false
  local toolName=false
  local asName=false
  local initialInfo=false
  local endInfo=false
  local getMessageGem
  local validation
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  local validation
  local defaultMessage
  local validationInstallation
  local messageReturnedOfOneFunction
  local codeMessage

  if [ "${toolName[@]}" == false ]
    then
      defaultMessage=${messageReturnedByFunction[Malformed]}
      validationInstallation=${validatedTheCodeWithFALSE}
    else
      defaultMessage=$(installTheTool "${tool}" "${toolName[@]}" "${asName[@]}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}")
      validationInstallation=$?
  fi

  local validator=${validatedTheCodeWithTRUE}

  stampValidations "${validationInstallation}" ${validator}
  validation=$?

  messageReturnedOfOneFunction=$(setErrorMessage "${toolName[@]}" "ErrorInApplyToolInstallation")
  messageReturnedBySystem=$(setValidationMessage "${defaultMessage[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfCreation=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${toolName[@]}" "=" "${resultOfCreation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

applyToolRemove() {
  local tool=false
  local toolName=false
  local initialInfo=false
  local endInfo=false
  local getMessageGem
  local validation
  local checkedGemInstalled
  local validation
  local defaultMessage
  local validationUninstall
  local messageReturnedOfOneFunction
  local codeMessage
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  if [ "${toolName[@]}" == false ]
    then
      defaultMessage=${messageReturnedByFunction[Malformed]}
      validationUninstall=${validatedTheCodeWithTRUE}
    else
      defaultMessage=$(removeTheTool "${tool}" "${toolName[@]}")
      validationUninstall=$?
  fi

  local validator=${validatedTheCodeWithFALSE}

  stampValidations "${validationUninstall}" ${validator}
  validation=$?

  messageReturnedOfOneFunction=$(setErrorMessage "${toolName[@]}" "ErrorInApplyToolRemove")
  messageReturnedBySystem=$(setValidationMessage "${defaultMessage[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfCreation=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${toolName[@]}" "=" "${resultOfCreation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return ${validation}
}


cooking() {
  local tool=false
  local toolName=false
  local asName=false
  local initialInfo=false
  local endInfo=false
  local validation
  local defaultMessage
  local validationInstallation
  local messageReturnedOfOneFunction
  local codeMessage
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  if [ "${toolName[@]}" == false ]
    then
      defaultMessage=${messageReturnedByFunction[Malformed]}
      validationInstallation=${validatedTheCodeWithFALSE}
    else
      defaultMessage=$(cookingTheRecipe "${tool}" "${toolName[@]}" "${asName[@]}" "${theAlias}" "${theLabels}" "${theMomentum}" "${theBulkData[@]}")
      validationInstallation=$?
  fi

  local validator=${validatedTheCodeWithTRUE}

  stampValidations "${validationInstallation}" ${validator}
  validation=$?

  messageReturnedOfOneFunction=$(setErrorMessage "${toolName[@]}" "ErrorInApplyToolInstallation")
  messageReturnedBySystem=$(setValidationMessage "${defaultMessage[@]}" ${validation} "${messageReturnedOfOneFunction[@]}")

  codeMessage="${genericPassMessage[@]}"
  sendMessageOfCode=$(setValidationMessage "${codeMessage}" ${validation} "${messageReturnedOfOneFunction[@]}")

  local resultOfCreation=$(convertCodeResponseToBoolean ${validation})

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${toolName[@]}" "=" "${resultOfCreation}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return $validation
}

#!${BASH}
modulePeople() {
  echo -e "Loading... Module 'modulePeople', version 0.1"
}


usersInvolved() {
  local acceptedUser=false
  local acceptedGroup=false
  local initialInfo=false
  local endInfo=false
  local theAlias
  local theMomentum
  local theLabels
  local theBulkData

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      echo -e "${messageNotActionBecauseRunbookIsBroken}"
      return ${validatedTheCodeWithFALSE}
  fi

  setGenericParameters "$@"
  setMainParameters "$@"

  local userInvolved
  local sendMessageOfCode="${messageReturnedByFunction[ErrorInUsersInvolved]}"
  local messageReturnedBySystem

  local validateUserMessage
  local validateUser
  validateUserMessage=$(isThisTheCurrentUser "${acceptedUser}")
  validateUser=$?

  if [ "$validateUser" = ${validatedTheCodeWithTRUE} ]
    then
      messageReturnedBySystem=$(whoIsIt)
  fi

  local groupMessage
  local validateGroup
  groupMessage=$(isTheUserInTheAcceptedGroup "${acceptedGroup}")
  validateGroup=$?

  if [ "$validateGroup" = ${validatedTheCodeWithTRUE} ]
    then
      messageReturnedBySystem=$(whoIsIt)
  fi

  local validation
  if [ "${validateUser}" == ${validatedTheCodeWithTRUE} ] || [ "${validateGroup}" == ${validatedTheCodeWithTRUE} ]
    then
      validation=${validatedTheCodeWithTRUE}
    else
      validation=${validatedTheCodeWithFALSE}
      messageReturnedBySystem="${unexplainedError[@]}"
  fi

  stampValidations "${validation}" ${validatedTheCodeWithTRUE}

  local desiredResponse=${validation}
  local obtainedResponse=${validatedTheCodeWithTRUE}

  tellTheStory "${FUNCNAME[0]}" "${validation}" "${desiredResponse[@]}" "=" "${obtainedResponse[@]}" "${messageReturnedBySystem[@]}" "${sendMessageOfCode[@]}" "${initialInfo}" "${endInfo}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}"

  return ${validation}
}

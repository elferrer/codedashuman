#!${BASH}
moduleRunbooks() {
  echo -e "Loading... Module 'moduleRunbooks', version 0.1"
}


specificationsSummary() {
  local fileName=${1}
  local initialInfo="${2}"
  local endInfo="${3}"
  local capturedValidation=0
  local validationSaveSummary


  local stringifyContent="$(printf "%q" "${messageCounterSUCCESS}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"summary\": [\"success\": \"${stringifyContent[@]}\"]}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"
  
  local stringifyContent="$(printf "%q" "${messageCounterERRORS}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"summary\": [\"errors\": \"${stringifyContent[@]}\"]}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"
  
  launchSeeScore ${systemView} "${initialInfo[@]}" "${endInfo[@]}"
  launchSeeScore ${systemPrint} "${initialInfo[@]}" "${endInfo[@]}"
  launchSaveSummary ${fileName}
  validationSaveSummary=$?

  if [ ${messageCounterERRORS} -gt 0 ] || [ "${validationSaveSummary}" == ${validatedTheCodeWithFALSE} ]
    then
      capturedValidation=${validatedTheCodeWithFALSE}
    else
      capturedValidation=${validatedTheCodeWithTRUE}
  fi

  return $capturedValidation
}

constructTheTask() {
  local filename=false
  local configType
  local theAlias
  local theTitle
  local theExplanation
  local completed
  local taskIsActive
  local taskStopIfItFails=false
  local dependsOn
  local typeOfDependency
  local taskGhostJob
  local theParent
  local initialInfo=false
  local endInfo=false
  local theMomentum
  local taskLabels

  setGenericParameters "$@"
  setTaskParameters "$@"

  if [ "${filename}" == false ]
    then
      titleOfTask[$theAlias]="${theTitle[@]}"
      explanationOfTask[$theAlias]="${theExplanation[@]}"
      workCompletionOfTask[$theAlias]="${completed[@]}"
      taskIsActiveOfTask[$theAlias]="${taskIsActive[@]}"
      stopIfItFailsOfTask[$theAlias]="${taskStopIfItFails[@]}"
      dependsOnOfTask[$theAlias]="${dependsOn[@]}"
      typeOfDependencyOfTask[$theAlias]="${typeOfDependency[@]}"
      ghostJobOfTask[$theAlias]="${taskGhostJob[@]}"
      theParentOfTask[$theAlias]="${theParent[@]}"
      momentumOfTask[$theAlias]="${theMomentum[@]}"
      labelsOfTask[$theAlias]="${taskLabels[@]}"
      bulkDataOfTask[$theAlias]="${taskBulkData[@]}"

      sendTaskMessagesThroughTheNetwork

      surveyTheTask "${theAlias}" "${theParentOfTask[$theAlias]}" "${momentumOfTask[$theAlias]}" "${labelsOfTask[@]}" "${stopIfItFailsOfTask[$theAlias]}" "${bulkDataOfTask[@]}"
      local validation=$?

    else
      unset listOfSpecifications

      loadTasksConfigurationFile "${configType}" "${filename}"

      surveyAddedSpecificationsFromConfigurationFile
      local validation=$?
  fi

  return $validation
}

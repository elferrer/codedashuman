#!${BASH}
moduleRunbooksSummary() {
  echo -e "Loading... Module 'moduleRunbooksSummary', version 0.1"
}


seeSpecificationsSummary() {
  local save=false
  local initialInfo=false
  local endInfo=false

  setGenericParameters "$@"
  setMainParameters "$@"

  specificationsSummary "${save[@]}" "${initialInfo[@]}" "${endInfo[@]}"

  local validation=${validatedTheCodeWithTRUE}

  local continueRunbook=$(isRunnableAndActiveTheRunbook)
  if [ "${continueRunbook}" == false ]
    then
      validation=${validatedTheCodeWithFALSE}
  fi

  return ${validation}
}

initActionsNotRelevantToTheRunbook() {
  stopTheRunbook
}

returnControlToTheRunbook() {
  goTheRunbook
}

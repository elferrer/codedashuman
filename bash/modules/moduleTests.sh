#!${BASH}
moduleTests() {
  echo -e "Loading... Module 'moduleTests', version 0.1"
}


launchGroupTests() {
  local theGroupTests="${1}"

  launchThisGroupTests=$(isTheTestingGroupRunnable "${theGroupTests}")

  local continueRunbook=$(isRunnableAndActiveTheRunbook)

  if [ "${launchThisGroupTests}" = "${validatedTheCodeWithTRUE}" ] || [ "${considerLaunchAllTests}" = true ]
    then
      ${theGroupTests}
  fi
}

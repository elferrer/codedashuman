#!${BASH}
interfaceProgramFeatures() {
  echo -e "Loading... Artifact 'interfaceProgramFeatures', version 0.1"
}


ensureTool() {
  applyToolInstallation "$@"
  return $?
}

uninstallTool() {
  applyToolRemove "$@"
  return $?
}

cook() {
  cooking "$@"
  return $?
}

#!${BASH}
interfaceDistribution() {
  echo -e "Loading... Artifact 'interfaceDistribution', version 0.1"
}


setUpDistro() {
  useDistro "$@"
  return $?
}

ensureToolRepository() {
  ensureRepository "$@"
  return $?
}

eraseToolRepository() {
  eraseRepository "$@"
  return $?
}

refreshRepositories() {
  refreshRepository "$@"
  return $?
}

updateRepositoryPackages() {
  updateAllPackages "$@"
  return $?
}

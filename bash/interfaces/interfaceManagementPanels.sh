#!${BASH}
interfaceManagementPanels() {
  echo -e "Loading... Artifact 'interfaceManagementPanels', version 0.1"
}


ensureManagementPanel() {
  ensureThePanel "$@"
  return $?
}

eraseManagementPanel() {
  eraseTheManagementPanel "$@"
  return $?
}

copyManagementPanel() {
  copyTheManagementPanel "$@"
  return $?
}

moveManagementPanel() {
  moveThePanel "$@"
  return $?
}

ensureBriefingTask() {
  ensureTheBriefingTask "$@"
  return $?
}

eraseBriefingTask() {
  eraseTheBriefingTask "$@"
  return $?
}

ensureRecipeIngredient() {
  ensureTheRecipeIngredient "$@"
  return $?
}

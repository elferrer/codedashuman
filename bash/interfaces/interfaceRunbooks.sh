#!${BASH}
interfaceRunbooks() {
  echo -e "Loading... Artifact 'interfaceRunbooks', version 0.1"
}


stateOfAffairs() {
  constructTheTask "$@"
  return $?
}

weHope() {
  weWishes "$@"
  return $?
}

launchSpecificationsSummary() {
  seeSpecificationsSummary "$@"
  return $?
}

pauseRunbook() {
  initActionsNotRelevantToTheRunbook
}

unpauseRunbook() {
  returnControlToTheRunbook
}

returnRunbookStatusCode() {
  codeResult=$(convertNegatedBooleanToCodeResponse ${theRunbookIsBrokenIs})

  echo -e "${codeResult}"
  return ${codeResult}
}

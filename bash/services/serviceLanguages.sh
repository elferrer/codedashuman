#!${BASH}
serviceLanguages() {
  echo -e "Loading... Services 'serviceLanguages', version 0.1"
}


initializeDynamicVariable() {
  local theVariable="${1}"
  local theContent=${2}
  unset ${theVariable}
  if [ ${2} ]
    then
      applyContentToDynamicVariable "${theVariable}" "${theContent}"
  fi
}

applyContentToDynamicVariable() {
  local variableToInitialize=${1}
  local withContent=${2}
  read "$variableToInitialize" <<< "${withContent}"
}

checkIfFunctionIsLoaded() {
  declare -f "${1}" > /dev/null
  local validation=$(validateResponse $?)
  echo -e "${validation}"
}

saveTheArray() {
  local withName=${1}
  local withBeginMessage=${2}
  local withComparationMessage=${3}
  local withOperatorMessage=${4}
  local withEndMessage=${5}
  applyContentToDynamicVariable "$withName[0]" "${withBeginMessage}"
  applyContentToDynamicVariable "$withName[1]" "${withComparationMessage}"
  applyContentToDynamicVariable "$withName[2]" "${withOperatorMessage}"
  applyContentToDynamicVariable "$withName[3]" "${withEndMessage}"
}

dynamicLoadFromVariable() {
  local aliasOfMessage=("${1}")
  echo -e "${!aliasOfMessage}"
}

createTheArrayForMessages() {
  local typeOfResponse=${1}
  local aliasVariable="${2}"

  local withName="${aliasVariable[@]}${typeOfResponse}"

  case ${typeOfResponse} in
    ${appellationForSUCCESS} )
      local theBeginMessage="--withSuccessBeginMessage"
      local theComparationMessage="--withSuccessComparationMessage"
      local theOperatorMessage="--withSuccessOperatorMessage"
      local theEndMessage="--withSuccessEndMessage"
    ;;
    ${appellationForERROR} )
      local theBeginMessage="--withErrorBeginMessage"
      local theComparationMessage="--withErrorComparationMessage"
      local theOperatorMessage="--withErrorOperatorMessage"
      local theEndMessage="--withErrorEndMessage"
    ;;
  esac

  for iterationInArguments in "$@"
    do
      shift
      local theArgument=${2}
      shift
      local actionMessage=${2}

      case ${theArgument} in
        ${theBeginMessage} )
          withBeginMessage="${actionMessage[@]}"
        ;;
        ${theComparationMessage} )
          withComparationMessage="${actionMessage[@]}"
        ;;
        ${theOperatorMessage} )
          withOperatorMessage="${actionMessage[@]}"
        ;;
        ${theEndMessage} )
          withEndMessage="${actionMessage[@]}"
        ;;
      esac
  done

  initializeDynamicVariable "${withName}"
  saveTheArray "${withName}" "${withBeginMessage}" "${withComparationMessage}" "${withOperatorMessage}" "${withEndMessage}"
}

createTheArray() {
  local crossingArray=("$@")
  createTheArrayForMessages ${appellationForSUCCESS} "${crossingArray[@]}"
  createTheArrayForMessages ${appellationForERROR} "${crossingArray[@]}"
}

#!${BASH}
serviceDistribution() {
  echo -e "Loading... Service 'serviceDistribution', version 0.1"
}


selectDistro() {
  local distro=${1}
  loadAddOn "${shell}" "${distributionsPath}" "${distro}"
  local resultOfSelectDistro=$(captureEchoOfFunction "${distro}")
  echo -e "${resultOfSelectDistro}"
}

catchDistributionList() {
  local distroList=("")
  local catchDistroID=$( catchDistroById "ID" )
  local catchDistroIDLIKE=$( catchDistroById "ID_LIKE" )
  echo -e "${catchDistroID}"
  local distroList=($catchDistroIDLIKE)
  for distro in ${distroList[@]}
    do
      echo -e "${distro}"
  done
}

checkDistribution() {
  local distro="${1}"
  local distroList=("${2}")

  local validation=${validatedTheCodeWithFALSE}
  case "${distroList[@]}" in
    *${distro[@]}* )
      validation=${validatedTheCodeWithTRUE}
    ;;
  esac

  echo -e "${validation}"
}

checkPrimaryDistribution() {
  local distro="${1}"
  local distroList=("${2}")

  local validation=${validatedTheCodeWithFALSE}
  if [ ${distroList[0]} = ${distro} ]
    then
      validation=${validatedTheCodeWithTRUE}
  fi

  echo -e "${validation}"
}

addToolRepository() {
  local repositoryType="${1}"
  local repositoryName="${2}"
  local getMessageAddToolRepository
  local getMessageTool
  local validation

  getMessageTool=$(isTheRepositoryInTheList "${repositoryName}")
  validation=$?

  if [ "$validation" = ${validatedTheCodeWithFALSE} ]
    then
      getMessageAddToolRepository=$(addAptRepository "${repositoryType}" "${repositoryName}")
  fi

  getMessageTool=$(isTheRepositoryInTheList "${repositoryName}")
  validation=$?

  echo -e "${getMessageTool}"
  return ${validation}
}

removeToolRepository() {
  local repositoryType="${1}"
  local repositoryName="${2}"
  local getMessageRemoveToolRepository
  local getMessageTool
  local validation

  getMessageTool=$(isTheRepositoryInTheList "${repositoryName}")
  validation=$?

  if [ "$validation" = ${validatedTheCodeWithTRUE} ]
    then
      getMessageRemoveToolRepository=$(removeAptRepository "${repositoryType}" "${repositoryName}")
  fi

  getMessageTool=$(isTheRepositoryInTheList "${repositoryName}")
  validation=$?

  echo -e "${getMessageTool}"
  return ${validation}
}

refreshCacheRepository() {
  local validation

  local update=$(refreshCache)
  validation=$?

  return ${validation}
}

downloadPackagesList() {
  local validation

  local update=$(downloadPackages)
  validation=$?

  return ${validation}
}

updateThePackages() {
  local validation

  local update=$(updatePackages)
  validation=$?

  return ${validation}
}

#!${BASH}
serviceStyles() {
  echo -e "Loading... Services 'serviceStyles', version 0.1"
}


initTypoCaption() {
  local viewOrWebStyle=${1:-false}
  local setTag=${2}
  local typeStyle
  local validation

  case ${viewOrWebStyle} in
     ${systemView} ) destinationStyle=${typessetingConsoleStyle} ;;
     ${systemPrint} ) destinationStyle=${typessetingWebStyle} ;;
  esac

  if [ "${viewOrWebStyle}" == false ]
    then
      typeStyle="${messageReturnedByFunction[ErrorInInitTypoCaption]}"
      validation=${validatedTheCodeWithFALSE}
    else
      typeStyle=$(initTypoCaptionFor${viewOrWebStyle} ${setTag})
      validation=$?
  fi

  echo -e "${typeStyle}"
  return $validation
}

closeTypoCaption() {
  local viewOrWebStyle=${1:-false}
  local setTag=${2}
  local typeStyle
  local validation

  case ${viewOrWebStyle} in
     ${systemView} ) destinationStyle=${typessetingConsoleStyle} ;;
     ${systemPrint} ) destinationStyle=${typessetingWebStyle} ;;
    * ) ;;
  esac

  if [ "${viewOrWebStyle}" == false ]
    then
      typeStyle="${messageReturnedByFunction[ErrorInCloseTypoCaption]}"
      validation=${validatedTheCodeWithFALSE}
    else
      typeStyle=$(closeTypoCaptionFor${viewOrWebStyle} ${setTag})
      validation=$?
  fi

  echo -e "${typeStyle}"
  return $validation
}

initTypoCaptionForview() {
  local setTag=${1:-false}
  local typeStyle
  local validation

  if [ "${setTag}" == false ]
    then
      validation=${validatedTheCodeWithFALSE}
    else
      validation=${validatedTheCodeWithTRUE}
  fi

  case ${setTag} in
    "titleDescribe" ) typeStyle="${initTitleViewForDescribe}" ;;
    "titleContext" ) typeStyle="${initTitleViewForContext}" ;;
    "titleSection" ) typeStyle="${initTitleViewForSection}" ;;
    "titleSubsection" ) typeStyle="${initTitleViewForSubsection}" ;;
    "tagAbstract" ) typeStyle="${initTextViewForAbstract}" ;;
    "tagStatus" ) typeStyle="${initTextViewForResult}" ;;
    "tagExplanation" ) typeStyle="${initTextViewForExplanation}" ;;
    "tagParagraph" ) typeStyle="${initTextViewForParagraph}" ;;
    "tagInlineQuote" ) typeStyle="${initTextViewForInlineQuote}" ;;
    "tagBlockQuote" ) typeStyle="${initTextViewForBlockQuote}" ;;
    "tagPre" ) typeStyle="${initTextViewForPre}" ;;
    "tagCode" ) typeStyle="${initTextViewForCode}" ;;
    "tagSUCCESSmessage" ) typeStyle="${initViewForSUCCESSmessage}" ;;
    "tagERRORmessage" ) typeStyle="${initViewForERRORmessage}" ;;
    "tagNewLine" ) typeStyle="${initTextViewForNewLine}" ;;
    * ) typeStyle="" ;;
  esac

  echo -e "${typeStyle}"
  return $validation
}

closeTypoCaptionForview() {
  local setTag=${1:-false}
  local typeStyle
  local validation

  if [ "${setTag}" == false ]
    then
      validation=${validatedTheCodeWithFALSE}
    else
      validation=${validatedTheCodeWithTRUE}
  fi

  case ${setTag} in
    "titleDescribe" ) typeStyle="${closeTitleViewForDescribe}" ;;
    "titleContext" ) typeStyle="${closeTitleViewForContext}" ;;
    "titleSection" ) typeStyle="${closeTitleViewForSection}" ;;
    "titleSubsection" ) typeStyle="${closeTitleViewForSubsection}" ;;
    "tagAbstract" ) typeStyle="${closeTextViewForAbstract}" ;;
    "tagStatus" ) typeStyle="${closeTextViewForResult}" ;;
    "tagExplanation" ) typeStyle="${closeTextViewForExplanation}" ;;
    "tagParagraph" ) typeStyle="${closeTextViewForParagraph}" ;;
    "tagInlineQuote" ) typeStyle="${closeTextViewForInlineQuote}" ;;
    "tagBlockQuote" ) typeStyle="${closeTextViewForBlockQuote}" ;;
    "tagPre" ) typeStyle="${closeTextViewForPre}" ;;
    "tagCode" ) typeStyle="${closeTextViewForCode}" ;;
    "tagSUCCESSmessage" ) typeStyle="${closeViewForSUCCESSmessage}" ;;
    "tagERRORmessage" ) typeStyle="${closeViewForERRORmessage}" ;;
    "tagNewLine" ) typeStyle="${initTextViewForNewLine}" ;;
    * ) typeStyle="" ;;
  esac

  echo -e "${typeStyle}"
  return $validation
}

initTypoCaptionForprint() {
  local setTag=${1:-false}
  local typeStyle
  local validation

  if [ "${setTag}" == false ]
    then
      validation=${validatedTheCodeWithFALSE}
    else
      validation=${validatedTheCodeWithTRUE}
  fi

  case ${setTag} in
    "titleDescribe" ) typeStyle="${initTitlePrintForDescribe}" ;;
    "titleContext" ) typeStyle="${initTitlePrintForContext}" ;;
    "titleSection" ) typeStyle="${initTitlePrintForSection}" ;;
    "titleSubsection" ) typeStyle="${initTitlePrintForSubsection}" ;;
    "tagAbstract" ) typeStyle="${initTextPrintForAbstract}" ;;
    "tagStatus" ) typeStyle="${initTextPrintForResult}" ;;
    "tagExplanation" ) typeStyle="${initTextPrintForExplanation}" ;;
    "tagParagraph" ) typeStyle="${initTextPrintForParagraph}" ;;
    "tagInlineQuote" ) typeStyle="${initTextPrintForInlineQuote}" ;;
    "tagBlockQuote" ) typeStyle="${initTextPrintForBlockQuote}" ;;
    "tagPre" ) typeStyle="${initTextPrintForPre}" ;;
    "tagCode" ) typeStyle="${initTextPrintForCode}" ;;
    "tagSUCCESSmessage" ) typeStyle="${initPrintForSUCCESSmessage}" ;;
    "tagERRORmessage" ) typeStyle="${initPrintForERRORmessage}" ;;
    "tagNewLine" ) typeStyle="${initTextPrintForNewLine}" ;;
    * ) typeStyle="" ;;
  esac

  echo -e "${typeStyle}"
  return $validation
}

closeTypoCaptionForprint() {
  local setTag=${1:-false}
  local typeStyle
  local validation

  if [ "${setTag}" == false ]
    then
      validation=${validatedTheCodeWithFALSE}
    else
      validation=${validatedTheCodeWithTRUE}
  fi

  case ${setTag} in
    "titleDescribe" ) typeStyle="${closeTitlePrintForDescribe}" ;;
    "titleContext" ) typeStyle="${closeTitlePrintForContext}" ;;
    "titleSection" ) typeStyle="${closeTitlePrintForSection}" ;;
    "titleSubsection" ) typeStyle="${closeTitlePrintForSubsection}" ;;
    "tagAbstract" ) typeStyle="${closeTextPrintForAbstract}" ;;
    "tagStatus" ) typeStyle="${closeTextPrintForResult}" ;;
    "tagExplanation" ) typeStyle="${closeTextPrintForExplanation}" ;;
    "tagParagraph" ) typeStyle="${closeTextPrintForParagraph}" ;;
    "tagInlineQuote" ) typeStyle="${closeTextPrintForInlineQuote}" ;;
    "tagBlockQuote" ) typeStyle="${closeTextPrintForBlockQuote}" ;;
    "tagPre" ) typeStyle="${closeTextPrintForPre}" ;;
    "tagCode" ) typeStyle="${closeTextPrintForCode}" ;;
    "tagSUCCESSmessage" ) typeStyle="${closePrintForSUCCESSmessage}" ;;
    "tagERRORmessage" ) typeStyle="${closePrintForERRORmessage}" ;;
    "tagNewLine" ) typeStyle="${initTextPrintForNewLine}" ;;
    * ) typeStyle="" ;;
  esac

  echo -e "${typeStyle}"
  return $validation
}

constructTypoCaptionWithValidationMessage() {
  local typoCaption=${1}
  local viewOrWebStyle=${2:-false}
  local validation=${3}

  case ${validation} in
    ${validatedTheCodeWithTRUE} )
      emphassizedMessages=$(${typoCaption} ${viewOrWebStyle} "tagSUCCESSmessage")
    ;;
    ${validatedTheCodeWithFALSE} )
      emphassizedMessages=$(${typoCaption} ${viewOrWebStyle} "tagERRORmessage")
    ;;
  esac

  if [ "${viewOrWebStyle}" ==  false ]
    then
      validation=${validatedTheCodeWithFALSE}
  fi

  echo -e "${emphassizedMessages}"
  return $validation
}

initializeDocument() {
  local typessetingStyle=${1:-false}
  local stylePath=${2:-false}

  local validation
  if [ "${typessetingStyle}" ==  false ] || [ "${stylePath}" ==  false ]
    then
      validation=${validatedTheCodeWithFALSE}
    else
      validation=${validatedTheCodeWithTRUE}
      loadAddOn "${stylesPath}" "${stylePath}" "${typessetingStyle}"
  fi

  return $validation
}

initializeViewDocument() {
  initializeDocument ${typessetingConsoleStyle} ${consoleStylePath}
  validation=$?

  echo -e "${initDocumentConsoleStyle[@]}"
  return $validation
}

initializePrintDocument() {
  initializeDocument ${typessetingWebStyle} ${webStylePath}
  validation=$?

  return $validation
}

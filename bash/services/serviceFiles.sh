#!${BASH}
serviceFiles() {
  echo -e "Loading... Services 'serviceFiles', version 0.1"
}


compareFolder() {
  local originPath="${1}"
  local compareWithPath="${2}"

  local validation=${validatedTheCodeWithFALSE}
  local compareMessage="${messageReturnedByFunction[ErrorInCompareFolder]}"

  local originCanonicalizedPath=$(canonicalizeExistentPath "${originPath[@]}")
  local compareWithCanonicalizedPath=$(canonicalizeExistentPath "${compareWithPath[@]}")

  local compareFolder=$(checkTheParadigm "${compareWithCanonicalizedPath}" "contain" "${originCanonicalizedPath}")
  if [ "${compareFolder}" == ${validatedTheCodeWithTRUE} ]
    then
      compareMessage="${genericPassMessage[@]}"
      validation=${validatedTheCodeWithTRUE}
  fi

  echo -e "${compareMessage[@]}"
  return $validation
}


epochDate() {
  local theDate="${1}"
  local resultOfCheck
  local validation=${validatedTheCodeWithFALSE}

  resultOfCheck=$(getEpochDate "${theDate}")
  validation=$?

  echo -e "${resultOfCheck}"
  return $validation
}

currentDate() {
  local resultOfCheck
  local validation=${validatedTheCodeWithFALSE}

  resultOfCheck=$(getCurrentDate)
  validation=$?

  echo -e "${resultOfCheck}"
  return $validation
}

lastDateChangeInTheFile() {
  local filename="${1}"
  local resultOfCheck
  local validation=${validatedTheCodeWithFALSE}

  resultOfCheck=$(getLastDateChangeInTheFile "${filename}")
  validation=$?

  echo -e "${resultOfCheck}"
  return $validation
}

waitForTheFileToBeCreated() {
  local filename="${1}"
  local resultOfCheck
  local validation=${validatedTheCodeWithFALSE}

  resultOfCheck=$(waitForTheFile "${filename}")
  validation=$?

  echo -e "${resultOfCheck}"
  return $validation
}

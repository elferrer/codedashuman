#!${BASH}
serviceProveAndStamp() {
  echo -e "Loading... Module 'serviceProveAndStamp', version 0.1"
}


setGenericParameters() {
  for iterationInArguments in "$@"
    do
      local option=${1:-none}
      shift
      local content=${1:-none}
      case ${option} in
        "--alias" )
          theAlias="${content[@]}"
          addSpecificationToDictionary "$theAlias"
        ;;
        "--title" )
          theTitle="${content[@]}"
        ;;
        "--parent" )
          theParent="${content[@]}"
        ;;
        "--momentum" )
          theMomentum="${content[@]}"
        ;;
        "--explanation" )
          theExplanation=("${content[@]}")
        ;;
        "--labels" )
          theLabels="${content[@]}"
        ;;
        "--bulkData" )
          theBulkData="${content[@]}"
        ;;
        "--initialInfo" )
          initialInfo="${content[@]}"
        ;;
        "--endInfo" )
          endInfo="${content[@]}"
        ;;
    esac
  done
}

setMainParameters() {
  for iterationInArguments in "$@"
    do
      local option=${1}
      shift
      local content=${1}
      case ${option} in
        "--name" )
          name="${content[@]}"
        ;;
        "--asName" )
          asName="${content[@]}"
        ;;
        "--line" )
          line="${content[@]}"
        ;;
        "--origin" )
          origin="${content[@]}"
        ;;
        "--destination" )
          destinationFolder="${content[@]}"
        ;;
        "--repositoryName" )
          repositoryName="${content[@]}"
        ;;
        "--repositoryType" )
          repositoryType="${content[@]}"
        ;;
        "--language" )
          language="${content[@]}"
        ;;
        "--acceptedUser" )
          acceptedUser="${content[@]}"
        ;;
        "--acceptedGroup" )
          acceptedGroup="${content[@]}"
        ;;
        "--save" )
          save="${content[@]}"
        ;;
        "--gem" )
          toolName="${content[@]}"
          tool="Gem"
        ;;
        "--program" )
          toolName="${content[@]}"
          tool="Program"
        ;;
        "--recipe" )
          toolName="${content[@]}"
          tool="Recipe"
        ;;
        "--package" )
          toolName="${content[@]}"
          tool="Package"
        ;;
        "--atTheBeginningOfTheLine" )
          atTheBeginningOfTheLine="${content[@]}"
        ;;
      esac
  done
}

applySystemMessage() {
  local validation=${1}
  local message=${2:-false}

  if [ "${message}" == false ] ||  [ "${message}" == ${validatedTheCodeWithTRUE} ]
    then
      echo -e ${validation}
    else
      echo -e" ${message[@]}"
  fi
  return ${validation}
}

applyBooleanMessage() {
  local message=${1:-false}

  local booleanMessage=true
  local validation=${validatedTheCodeWithTRUE}
  if [ "${message}" == false ]
    then
      booleanMessage=false
      validation=${validatedTheCodeWithFALSE}
  fi
  echo -e ${booleanMessage}
  return ${validation}
}

setErrorMessage() {
  local folderName="${1}"
  local errorMessage="${2}"

  if  [ "${folderName}" == false ]
    then
      returnMessage="${messageReturnedByFunction[NotDefined]}"
    else
      returnMessage="${messageReturnedByFunction[${errorMessage}]}"
  fi

  echo -e "${returnMessage[@]}"
}

setValidationMessage() {
  local defaultMessage="${1}"
  local validation=${2}
  local errorMessage="${3}"

  if  [ "${validation}" == ${validatedTheCodeWithTRUE} ]
    then
      returnMessage="${defaultMessage[@]}"
    else
      returnMessage="${errorMessage[@]}"
  fi

  echo -e "${returnMessage[@]}"
}

proveValidationFolder() {
  local theFolder="${1}"
  local messageCheckFolder
  local validationCheckFolder
  local validation=${validatedTheCodeWithTRUE}

  messageCheckFolder=$(checkIfFolderExist "${theFolder}")
  validationCheckFolder=$?

  if [ "${validationCheckFolder}" == ${validatedTheCodeWithFALSE} ] ||  [ "${theFolder}" == false ]
    then
      validation=${validatedTheCodeWithFALSE}
  fi

  echo -e "${messageCheckFolder}"
  return ${validation}
}

proveValidationFile() {
  local theFile="${1}"
  local messageCheckFile
  local validationCheckFile
  local validation=${validatedTheCodeWithTRUE}

  messageCheckFile=$(checkIfFileExist "${theFile}")
  validationCheckFile=$?

  if [ "${validationCheckFile}" == ${validatedTheCodeWithFALSE} ] ||  [ "${theFile[@]}" == false ]
    then
      validation=${validatedTheCodeWithFALSE}
  fi

  echo -e "${messageCheckFile}"
  return ${validation}
}

stampValidations() {
  local validationCreation=${1}
  local validator=${2}

  local validation=${validatedTheCodeWithTRUE}

  if [ "${validationCreation}" != ${validator} ]
    then
      theRunbookIsBrokenIs=true
      validation=${validatedTheCodeWithFALSE}
  fi

  return ${validation}
}

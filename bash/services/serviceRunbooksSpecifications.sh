#!${BASH}
serviceRunbooksSpecifications() {
  echo -e "Loading... Services 'serviceRunbooksSpecifications', version 0.1"
}


loadTasksConfigurationFile() {
  local configType="${1}"
  local filename="${2}"
  local index
  local key
  local value

  local validation
  local getMessagePath
  getMessagePath=$(checkIfFileExist "${filename}")
  validation=$?

  if [ "${validation}" != "${validatedTheCodeWithTRUE}" ]
    then
      echo -e "${messageReturnedByFunction[FileNotExist]}"
      return ${validation}
  fi

  case $configType in
    "toml" )
      parseToml "${filename[@]}"
    ;;
  esac

  return ${validation}
}

setParametersWeWishes() {
  for iterationInArguments in "$@"
    do
      local option=${1:-none}
      shift
      local content=${1:-none}
      case ${option} in
        "--messageInterpretation" )
          taggedMessages="${content[@]}"
        ;;
        "--firstOperator" )
          firstOperator="${content[@]}"
        ;;
        "--sentence" )
          sentence="${content[@]}"
        ;;
        "--secondOperator" )
          secondOperator="${content[@]}"
        ;;
        "--humanReadableFirstOperator" )
          humanReadableFirstOperator="${content[@]}"
        ;;
        "--humanReadableSecondOperator" )
          humanReadableSecondOperator="${content[@]}"
        ;;
    esac
  done
}

#!${BASH}
serviceRunbooksTomlParser() {
  echo -e "Loading... Services 'serviceRunbooksTomlParser', version 0.1"
}


parseToml() {
  local filename="${1:-false}"
  local index
  local key
  local value

  if [ "${filename[@]}" == false ]
    then
      return ${validatedTheCodeWithFALSE}
  fi

  while IFS='=' read key value
    do
      case $key in
        "#" )
          unset key
        ;;
        \[*] )
          index=${key:1:-1}
          addSpecificationToDictionary "$index"
        ;;
        title )
          initializeGlobalAlias "titleOfTask[$index]" "$value"
          unset key
        ;;
        explanation )
          initializeGlobalAlias "explanationOfTask[$index]" "$value"
          unset key
        ;;
        completed )
          initializeGlobalAlias "workCompletionOfTask[$index]" "$value"
          unset key
        ;;
        taskIsActive )
          initializeGlobalAlias "taskIsActiveOfTask[$index]" "$value"
          unset key
        ;;
        stopIfItFails )
          initializeGlobalAlias "stopIfItFailsOfTask[$index]" "$value"
          unset key
        ;;
        dependsOn )
          initializeGlobalAlias "dependsOnOfTask[$index]" "$value"
          unset key
        ;;
        typeOfDependency )
          initializeGlobalAlias "typeOfDependencyOfTask[$index]" "$value"
          unset key
        ;;
        ghostJob )
          initializeGlobalAlias "ghostJobOfTask[$index]" "$value"
          unset key
        ;;
        parent )
          initializeGlobalAlias "theParentOfTask[$index]" "$value"
          unset key
        ;;
        momentum )
          initializeGlobalAlias "momentumOfTask[$index]" "$value"
          unset key
        ;;
        labels )
          initializeGlobalAlias "labelsOfTask[$index]" "$value"
          unset key
        ;;
        bulkData )
          initializeGlobalAlias "bulkDataOfTask[$index]" "$value"
          unset key
        ;;
      esac
  done < "${filename[@]}"

  return ${validatedTheCodeWithTRUE}
}

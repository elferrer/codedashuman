#!${BASH}
serviceHelp() {
  echo -e "Loading... Services 'serviceHelp', version 0.1"
}


addToSystemDictionaryMessages() {
  local word=${1}
  local errorExplanation=("${2}")

  messageReturnedByFunction[$word]="${errorExplanation[@]}"
}

addToHelpDictionary() {
  local word=${1}
  local answer=("${2}")

  helpDictionary[${word}]="${answer[@]}"
}

addToExtendedHelpDictionary() {
  local word=${1}
  local answer=("${2}")

  extendedHelpDictionary[${word}]="${answer[@]}"
}

addToExampleHelpDictionary() {
  local word=${1}
  local answer=("${2}")

  exampleHelpDictionary[${word}]="${answer[@]}"
}

addToOptionsHelpDictionary() {
  local word=${1}
  local answer=("${2}")

  optionsHelpDictionary[${word}]="${answer[@]}"
}

giveMe() {
  local giveMe=${1:-Help}
  local extendedInfo=${2:-false}
  local answer=("")

  if [ "${helpDictionary[$giveMe]}" == "Help" ]
    then
      answer="${acceptedWords[@]} ${!helpDictionary[@]} \n"
    else
      answer="${helpDictionary[$giveMe]} \n"
      case "${extendedInfo}" in
        "see all" )
          answer+="${extendedHelpDictionary[$giveMe]} \n"
          answer+="${exampleHelpDictionary[$giveMe]} \n"
          answer+="${optionsHelpDictionary[$giveMe]} \n"
        ;;
        "extended" )
          answer="${extendedHelpDictionary[$giveMe]} \n"
        ;;
        "example" )
          answer="${exampleHelpDictionary[$giveMe]} \n"
        ;;
        "options" )
          answer="${optionsHelpDictionary[$giveMe]} \n"
        ;;
      esac
  fi

  echo -e "${answer[@]}"
}

getStatusInfo() {
  local viewOrWebStyle=${1}
  local infoFrom="${2:-false}"
  local beginInfo
  local endInfo
  local validation
  local validationInitPre
  local validationInitCode
  local validationCloseCode
  local validationClosePre

  if [ "${infoFrom}" != false ]
    then
      beginInfo=$(initTypoCaption ${viewOrWebStyle} "tagPre")
      validationInitPre=$?
      beginInfo+=$(initTypoCaption ${viewOrWebStyle} "tagCode")
      validationInitCode=$?
      endInfo=$(closeTypoCaption ${viewOrWebStyle} "tagCode")
      validationCloseCode=$?
      endInfo+=$(closeTypoCaption ${viewOrWebStyle} "tagPre")
      validationClosePre=$?

      local statusInfo=("")
      IFS=";"
      read -a values <<< "${infoFrom[@]}"
      for value in ${values[@]}
        do
          case $value in
            "user" )
              statusInfo+=$(getInfoOf "User")
              statusInfo+="\n"
            ;;
            "path" )
              statusInfo+=$(getInfoOf "Path")
              statusInfo+="\n"
            ;;
            "date" )
              statusInfo+=$(getInfoOf "Date")
              statusInfo+="\n"
            ;;
            "userFolder" )
              statusInfo+="${userFolderMessage[@]}: ${userFolder[@]}\n"
            ;;
            "tempFolder" )
              statusInfo+="${tempFolderMessage[@]}: ${tempFolder[@]}\n"
            ;;
            "protectedFolder" )
              statusInfo+="${protectedFolderMessage[@]}: ${protectedFolder[@]}\n"
            ;;
          esac
      done
      echo -e "${beginInfo}${statusInfo[@]}${endInfo}"
    else
      echo -e ""
  fi

  validation=${validatedTheCodeWithTRUE}
  if [ "${validationInitPre}" == ${validatedTheCodeWithFALSE} ] || [ "${validationInitCode}" == ${validatedTheCodeWithFALSE} ] || [ "${validationClosePre}" == ${validatedTheCodeWithFALSE} ] || [ "${validationCloseCodePre}" == ${validatedTheCodeWithFALSE} ]
    then
      validation=${validatedTheCodeWithFALSE}
  fi

  return $validation
}

captureEchoOfFunction() {
  local function=${1}
  captureMessageOfFunction=$($function)
  echo -e "${captureMessageOfFunction[@]}"
}

#!${BASH}
serviceSortMessages() {
  echo -e "Loading... Services 'serviceSortMessages', version 0.1"
}


sendTitleOfMessages() {
  local validation=${1}

  local viewTitle=$(constructCurrentTitle ${validation} ${systemView})

  if [ "${viewedTheFirstTitleOcurrency}" == false ]
    then
      saveOrSeeValidatedMessage "title" "\n${viewTitle[@]}" ${validation} ${systemView}
    else
      echo -e ""
  fi
  viewedTheFirstTitleOcurrency=true

  local printTitle=$(constructCurrentTitle ${validation} ${systemPrint})
  saveOrSeeValidatedMessage "title" "${printTitle[@]}" ${validation} ${systemPrint}
}

sendAbstractOfMessages() {
  local taggedMessages=${1}
  local validation=${2}
  local firstOperatorInSentence=${3}
  local sentence=${4}
  local secondOperatorInSentence=${5}

  if [ "${viewedTheFirstAbstractOcurrency}" == false ]
    then
      local showLabel=false
    else
      local showLabel=true
  fi
  viewedTheFirstAbstractOcurrency=true

  local createSimpleResponse=$(composeAbstractMessage "${taggedMessages[@]}" ${validation} "${firstOperatorInSentence}" "${sentence[@]}" "${secondOperatorInSentence}" ${systemView})
  local viewMessage=$(constructionOfMessages "tagAbstract" "${createSimpleResponse[@]}" ${validation} ${systemView} ${showLabel})
  saveOrSeeValidatedMessage "abstract" "${viewMessage[@]}" ${validation} ${systemView}

  local showLabel=true
  local createSimpleResponse=$(composeAbstractMessage "${taggedMessages[@]}" ${validation} "${firstOperatorInSentence}" "${sentence[@]}" "${secondOperatorInSentence}" ${systemPrint})
  local printMessage=$(constructionOfMessages "tagAbstract" "${createSimpleResponse[@]}" ${validation} ${systemPrint} ${showLabel})
  saveOrSeeValidatedMessage "abstract" "${printMessage[@]}" ${validation} ${systemPrint}
}

sendResultOfMessages() {
  local validation=${1}
  local messageOfCode="${2}"
  local codeResponse

  local validatedResponse=$(validateResponse ${validation})
  case ${validatedResponse} in
    ${validatedTheCodeWithTRUE} )
      codeResponse=""
    ;;
    ${validatedTheCodeWithFALSE} )
      codeResponse="(${messageOfCode[@]})"
    ;;
  esac

  local createSimpleResponse="${validatedResponse} ${codeResponse[@]}"

  local showLabel=true
  local viewMessage=$(constructionOfMessages "tagStatus" "${createSimpleResponse[@]}" ${validation} ${systemView} ${showLabel})
  saveOrSeeValidatedMessage "result" "${viewMessage[@]}" ${validation} ${systemView}

  local showLabel=true
  local printMessage=$(constructionOfMessages "tagStatus" "${createSimpleResponse[@]}" ${validation} ${systemPrint} ${showLabel})
  saveOrSeeValidatedMessage "result" "${printMessage[@]}" ${validation} ${systemPrint}
}

sendExplanationOfMessages() {
  local taggedMessages="${1}"
  local messageReturnedBySystem=("${2}")
  local validation=${3}
  local initialInfo="${4}"
  local endInfo="${5}"

  local showLabel=true
  local createComplexResponse=$(composeExplanationMessage "${taggedMessages[@]}" "${messageReturnedBySystem[@]}" ${validation} ${systemView} "tagPre" ${initialInfo} ${endInfo})

  local viewMessage=$(constructionOfMessages "tagExplanation" "${createComplexResponse[@]}" ${validation} ${systemView} ${showLabel})
  saveOrSeeValidatedMessage "explanation" "${viewMessage[@]}" ${validation} ${systemView}

  local showLabel=true
  local createComplexResponse=$(composeExplanationMessage "${taggedMessages[@]}" "${messageReturnedBySystem[@]}" ${validation} ${systemPrint} "tagPre" ${initialInfo} ${endInfo})
  local printMessage=$(constructionOfMessages "tagExplanation" "${createComplexResponse[@]}" ${validation} ${systemPrint} ${showLabel})
  saveOrSeeValidatedMessage "explanation" "${printMessage[@]}" ${validation} ${systemPrint}
}

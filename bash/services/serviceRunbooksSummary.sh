#!${BASH}
serviceRunbooksSummary() {
  echo -e "Loading... Services 'serviceRunbooksSummary', version 0.1"
}


stopTheRunbook() {
  theRunbookPauseIs=true
}

goTheRunbook() {
  theRunbookPauseIs=false
}

isRunnableAndActiveTheRunbook() {
  local isRunnable

  if [ "${theRunbookIsBrokenIs}" == true ] && [ "${theRunbookPauseIs}" == false ]
    then
      isRunnable=false
    else
      isRunnable=true
  fi
  echo -e ${isRunnable}
}

launchSeeScore() {
  local viewOrWebStyle=${1}
  local initialInfo="${2}"
  local endInfo="${3}"

  if [ "${globalShow}" == true ] &&  [ ${showSpecificationsSummary} == true ]
    then
      local viewSummaryMessage=$(composeSummaryMessage "summaryTitle" "${messageCounterSUCCESS}" ${validatedTheCodeWithTRUE} ${viewOrWebStyle} "tagParagraph" "${initialInfo[@]}" "${endInfo[@]}")
      saveOrSeeValidatedMessage "specificationsSummary" "${viewSummaryMessage[@]}" ${validatedTheCodeWithTRUE} ${viewOrWebStyle}

      local viewSummaryMessage=$(composeSummaryMessage "summaryTitle"  "${messageCounterERRORS}" ${validatedTheCodeWithFALSE} ${viewOrWebStyle} "tagParagraph" "${initialInfo[@]}" "${endInfo[@]}")
      saveOrSeeValidatedMessage "specificationsSummary" "${viewSummaryMessage[@]}" ${validatedTheCodeWithFALSE} ${viewOrWebStyle}
  fi
}

launchSaveSummary() {
  local fileName=${1:-false}
  local validation

  if [ "$fileName" == false ]
    then
      fileName=$defaultSummaryFile
  fi

  initializePrintDocument
  validation=$?

  echo -e "${initDocumentWebStyle[@]}" >| "${fileName}.log.${typessetingWebStyle}"
  echo -e "${summaryMessages[@]}" >> "${fileName}.log.${typessetingWebStyle}"
  echo -e "${endDocumentWebStyle[@]}" >> "${fileName}.log.${typessetingWebStyle}"
  return $validation
}

cleanProjectResultsFromMemory() {
  unset summaryMessages
  unset titlesOfFailedTests
  messageCounterERRORS=0
  messageCounterSUCCESS=0
  theRunbookIsBrokenIs=false
  theRunbookPauseIs=false
  return ${validatedTheCodeWithTRUE}
}

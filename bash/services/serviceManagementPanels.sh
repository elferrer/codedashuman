#!${BASH}
serviceManagementPanels() {
  echo -e "Loading... Module 'serviceManagementPanels', version 0.1"
}


createPanel() {
  local folder="${1}"
  local validation
  local getMessageFolder
  local theFolder
  local validationProve

  getMessageFolder=$(proveValidationFolder "${folder}")
  validationProve=$?
  validation=${validatedTheCodeWithFALSE}
  if [ "${validationProve}" == ${validatedTheCodeWithFALSE} ] && [ "${folder}" != false ]
    then
      getMessageFolder=$(createFolder "${folder}")
      validation=$?
  fi

  echo -e "${getMessageFolder[@]}"
  return ${validation}
}

erasePanel() {
  local folder="${1}"
  local validation
  local theFolder
  local validationProve
  local resumOfRemoveFolder

  theFolder=$(canonicalizeExistentPath ${folder})
  resumOfRemoveFolder=$(proveValidationFolder "${theFolder}")
  validationProve=$?

  validation=${validatedTheCodeWithFALSE}
  if [ "${validationProve}" == ${validatedTheCodeWithTRUE} ]
    then
      resumOfRemoveFolder=$(removeFolder "${theFolder}")
      validation=$?
  fi

  echo -e "${resumOfRemoveFolder}"
  return ${validation}
}

copyPanel() {
  local originPath="${1}"
  local destinationPath="${2}"

  local theOriginFolder
  local getMessageOriginFolder
  local validationProveOrigin
  local theDestinationFolder
  local getMessageDestinationFolder
  local validationProveDestination
  local getMessageCopyFolder

  local validation=${validatedTheCodeWithFALSE}
  local originIsValid=${validatedTheCodeWithFALSE}
  local destinationIsValid=${validatedTheCodeWithFALSE}

  theOriginFolder=$(canonicalizeExistentPath ${originPath})
  getMessageOriginFolder=$(proveValidationFolder "${theOriginFolder}")
  validationProveOrigin=$?

  if [ "${validationProveOrigin}" == ${validatedTheCodeWithTRUE} ] && [ "${originPath}" != false ]
  then
    originIsValid=${validatedTheCodeWithTRUE}
  fi

  theDestinationFolder=$(canonicalizeExistentPath ${destinationPath})
  getMessageDestinationFolder=$(proveValidationFolder "${theDestinationFolder}")
  validationProveDestination=$?

  if [ "${validationProveDestination}" == ${validatedTheCodeWithFALSE} ] || [ "${destinationPath}" == false ]
    then
      destinationIsValid=${validatedTheCodeWithTRUE}
  fi

  if [ "${originIsValid}" == ${validatedTheCodeWithTRUE} ] && [ "${destinationIsValid}" == "${validatedTheCodeWithTRUE}" ]
    then
      getMessageCopyFolder=$(copyFolder "${theOriginFolder}" "${theDestinationFolder}")
      validation=$?
  fi

  echo -e "${getMessageCopyFolder[@]}"
  return ${validation}
}

movePanel() {
  local originPath="${1}"
  local destinationPath="${2}"

  local theOriginFolder
  local getMessageOriginFolder
  local validationProveOrigin
  local theDestinationFolder
  local getMessageDestinationFolder
  local validationProveDestination
  local getMessageCopyFolder
  local getMessageRemoveFolder
  local validationCopy
  local validationRemove
  local getMessage

  local validation=${validatedTheCodeWithFALSE}
  local originIsValid=${validatedTheCodeWithFALSE}
  local destinationIsValid=${validatedTheCodeWithFALSE}

  theOriginFolder=$(canonicalizeExistentPath ${originPath})
  getMessageOriginFolder=$(proveValidationFolder "${theOriginFolder}")
  validationProveOrigin=$?

  if [ "${validationProveOrigin}" == ${validatedTheCodeWithTRUE} ] && [ "${originPath}" != false ]
  then
    originIsValid=${validatedTheCodeWithTRUE}
  fi

  theDestinationFolder=$(canonicalizeExistentPath ${destinationPath})
  getMessageDestinationFolder=$(proveValidationFolder "${theDestinationFolder}")
  validationProveDestination=$?

  if [ "${validationProveDestination}" == ${validatedTheCodeWithFALSE} ] || [ "${destinationPath}" == false ]
    then
      destinationIsValid=${validatedTheCodeWithTRUE}
  fi

  if [ "${originIsValid}" == ${validatedTheCodeWithTRUE} ] && [ "${destinationIsValid}" == "${validatedTheCodeWithTRUE}" ]
    then
      getMessageCopyFolder=$(copyFolder "${theOriginFolder}" "${theDestinationFolder}")
      validationCopy=$?
      getMessageRemoveFolder=$(removeFolder "${theOriginFolder}")
      validationRemove=$?
  fi

  if  [ "${validationCopy}" == ${validatedTheCodeWithTRUE} ] && [ "${validationRemove}" == ${validatedTheCodeWithTRUE} ]
    then
      getMessage=${validatedTheCodeWithTRUE}
      validation=${validatedTheCodeWithTRUE}
    else
      getMessage=${validatedTheCodeWithFALSE}
      validation=${validatedTheCodeWithFALSE}
  fi

  echo -e "${getMessage}"
  return ${validation}
}

createBriefingTask() {
  local theFile="${1}"
  local resumOfCreationFile
  local validation

  local thePath=$(canonicalizeExistentPath ${theFile})
  resumOfCreationFile=$(proveValidationFile "${theFile}")
  validationProve=$?

  validation=${validatedTheCodeWithFALSE}
  if [ "${validationProve}" == ${validatedTheCodeWithFALSE} ] &&  [ "${theFile}" != false ]
    then
      resumOfCreationFile=$(createFile "${thePath}")
      validation=${validatedTheCodeWithTRUE}
  fi

  echo -e "${resumOfCreationFile[@]}"
  return ${validation}
}

softDeleteBriefingTask() {
  local theFile="${1}"
  local validation
  local validationProve
  local resumOfDeletionFile

  local thePath=$(canonicalizeExistentPath ${theFile})
  resumOfDeletionFile=$(proveValidationFile "${thePath}")
  validationProve=$?

  validation=${validatedTheCodeWithFALSE}
  if [ "${validationProve}" == ${validatedTheCodeWithTRUE} ]
    then
      resumOfDeletionFile=$(softDeleteFile "${thePath}")
      validation=$?
  fi

  echo -e "${resumOfDeletionFile}"
  return ${validation}
}

addRecipeIngredientToFile() {
  local theLine="${1}"
  local theFile="${2}"
  local atTheBeginningOfTheLine="${3}"
  local getMessageFileExist
  local validation
  local messageToSend

  local thePath=$(canonicalizeExistentPath ${theFile})
  getMessageFileExist=$(checkIfFileExist "${thePath}")
  validation=$?

  if [ "${validation}" == ${validatedTheCodeWithFALSE} ] ||  [ "${theFile}" == false ] || [ "${theLine}" == false ]
    then
      messageToSend=false
      validation=${validatedTheCodeWithFALSE}
    else
      validation=${validatedTheCodeWithTRUE}
  fi

  if [ "${validation}" == ${validatedTheCodeWithTRUE} ]
    then
      getMessageContentExist=$(isTheContentInTheFile "${theLine[@]}" ${thePath} "${atTheBeginningOfTheLine[@]}")
      if  [ "${getMessageContentExist}" == false ]
        then
          messageToSend=$(addLineToFile "${theLine[@]}" ${thePath})
          validation=$?
      fi
  fi

  echo -e "${messageToSend[@]}"
  return $validation
}

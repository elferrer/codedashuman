#!${BASH}
servicePeople() {
  echo -e "Loading... Service 'servicePeople', version 0.1"
}


theCurrentUserIs() {
  local whoIsIt=$(whoIsIt)
  local sessionUser
  local validation

  sessionUser=$(echo "${whoIsIt[@]}" | returnTheNumberedPieceFromEndByCutting 1 " ")
  validation=$?

  echo -e "${sessionUser[@]}"
  return $validation
}

isThisTheCurrentUser() {
  local acceptedUser="${1}"

  local sessionUserMessage
  sessionUserMessage=$(theCurrentUserIs)

  local validation=${validatedTheCodeWithFALSE}
  if [ "${acceptedUser[@]}" == "${sessionUserMessage[@]}" ]
    then
      validation=${validatedTheCodeWithTRUE}
  fi

  echo -e "${userNameMessageCaption[@]}: ${sessionUserMessage}"
  return $validation
}

isTheUserInTheAcceptedGroup() {
  local acceptedGroup="${1}"

  local sessionUserMessage
  sessionUserMessage=$(theCurrentUserIs)

  local userIsInGroups=$(isUserInsideGroups "${sessionUserMessage}")
  local userGroups=$(echo "${userIsInGroups[@]}" | rev | returnTheNumberedPieceFromEndByCutting 1 ":" | rev)

  local validation=$(checkTheParadigm "${userGroups[@]}" "contain" "${acceptedGroup[@]}")

  echo -e "${userGroupsMessage[@]}: ${userIsInGroups}"
  return $validation
}

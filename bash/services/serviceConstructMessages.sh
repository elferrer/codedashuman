#!${BASH}
serviceConstructMessages() {
  echo -e "Loading... Services 'serviceConstructMessages', version 0.1"
}


constructCurrentTitle() {
  local validation=${1}
  local viewOrWebStyle=${2}

  liftContentFromDinamicVariable --newVariable "theTagMessage" \
    --validation ${validation} \
    --getContentFrom "${tagAndTitle[0]}"

  local theLine="${theTagMessage[0]} ${tagAndTitle[1]}"

  local oneLineMessage=$(composeOneLineOfMessage "${tagAndTitle[0]}" "${theLine[@]}" "${viewOrWebStyle}")
  echo -e "${oneLineMessage[@]}"
}

constructionOfMessages() {
  local taggedMessages=${1}
  local theContent="${2}"
  local validation=${3}
  local viewOrWebStyle=${4}
  local showLabel=${5}

  liftContentFromDinamicVariable --newVariable "theTagMessage" \
    --validation ${validation} \
    --getContentFrom ${taggedMessages}

  local tagLabel=""
  if [ "${showLabel}" = true ]
    then
      tagLabel="${theTagMessage[0]} "
  fi
  local theLine="${tagLabel[@]}${theContent[@]}"

  local oneLineMessage=$(composeOneLineOfMessage "${taggedMessages}" "${theLine[@]}" "${viewOrWebStyle}")
  echo -e "${oneLineMessage[@]}"
}

composeSummaryMessage() {
  local taggedMessages="${1}"
  local theCounter="${2}"
  local validation=${3}
  local viewOrWebStyle=${4}
  local taggedLabel=${5}
  local initialInfo="${6}"
  local endInfo="${7}"

  local getInitialInfo=$(getStatusInfo ${viewOrWebStyle} "${initialInfo[@]}")

  local message=$(blockComplexText "${taggedMessages}" "${theCounter[@]}" ${validation} "${viewOrWebStyle}")

  local oneLineMessage=$(composeOneLineOfMessage "${taggedLabel}" "${message[@]}" "${viewOrWebStyle}")

  local getEndInfo=$(getStatusInfo ${viewOrWebStyle} "${endInfo[@]}")

  echo -e "${getInitialInfo[@]}"
  echo -e "${oneLineMessage[@]}"
  echo -e "${getEndInfo[@]}"

  if [ "${validation}" == ${validatedTheCodeWithFALSE} ]
    then
      echo -en "${titlesOfFailedTests[@]}"
      if [ "${theRunbookIsBrokenIs}" == true ]
        then
          echo -e "${messageRunbookIsBrokenIs[@]}"
        else
          echo -e "${messageRunbookIsOver[@]}"
      fi
  fi
}

composeAbstractMessage() {
  local taggedMessages=${1}
  local validation=${2}
  local firstOperatorInSentence="${3}"
  local sentence="${4}"
  local secondOperatorInSentence="${5}"
  local viewOrWebStyle=${6}

  liftContentFromDinamicVariable --newVariable "theTagMessage" \
    --validation ${validation} \
    --getContentFrom ${taggedMessages}

  local emphassizedTextForMessage=$(composeEmphassizedMessage "${theTagMessage[3]}" "${viewOrWebStyle}" ${validation})

  local baseMessage="${theTagMessage[0]} '${firstOperatorInSentence[@]}' (${theTagMessage[2]} '${sentence[@]}') ${theTagMessage[1]} '${secondOperatorInSentence[@]}', ${emphassizedTextForMessage[@]}"

  local oneLineMessage=$(composeOneLineOfMessage "${taggedMessages[@]}" "${baseMessage[@]}" "${viewOrWebStyle}")

  echo -e "${oneLineMessage[@]}"
}

composeExplanationMessage() {
  local taggedMessages="${1}"
  local theContent="${2}"
  local validation=${3}
  local viewOrWebStyle=${4}
  local taggedLabel="${5}"
  local initialInfo="${6}"
  local endInfo="${7}"

  local newLine
  newLine=$(initTypoCaption ${viewOrWebStyle} "tagNewLine")

  local getInitialInfo=$(getStatusInfo ${viewOrWebStyle} "${initialInfo[@]}")

  local message=$(blockComplexText "${taggedMessages}" "${theContent[@]}" ${validation} "${viewOrWebStyle}")

  local oneLineMessage=$(composeOneLineOfMessage "${taggedLabel}" "${message[@]}" "${viewOrWebStyle}")

  local getEndInfo=$(getStatusInfo ${viewOrWebStyle} "${endInfo[@]}")

  echo -e "${newLine}"
  echo -e "${getInitialInfo[@]}"
  echo -e "${oneLineMessage[@]}"
  echo -e "${getEndInfo[@]}"
  return $validation
}

composeEmphassizedMessage() {
  local baseMessage=("${1}")
  local viewOrWebStyle="${2}"
  local validation=${3}
  local validationInit
  local validationClose

  local useEmphassizedMessages
  useEmphassizedMessages=$(constructTypoCaptionWithValidationMessage "initTypoCaption" ${viewOrWebStyle} ${validation})
  validationInit=$?
  local resetEmphassizedMessages
  resetEmphassizedMessages=$(constructTypoCaptionWithValidationMessage "closeTypoCaption" ${viewOrWebStyle} ${validation})
  validationClose=$?

  if [ "${validationInit}" == ${validatedTheCodeWithFALSE} ] || [ "${validationClose}" == ${validatedTheCodeWithFALSE} ]
    then
      validation=${validatedTheCodeWithFALSE}
  fi

  echo -en "${useEmphassizedMessages}${baseMessage[@]}${resetEmphassizedMessages}"
  return $validation
}

composeOneLineOfMessage() {
  local taggedLabel="${1}"
  local baseMessage=("${2}")
  local viewOrWebStyle="${3}"

  local beginLine
  local validationBegin
  beginLine=$(initTypoCaption ${viewOrWebStyle} ${taggedLabel})
  validationBegin=$?

  local endLine
  local validationEnd
  endLine=$(closeTypoCaption ${viewOrWebStyle} ${taggedLabel})
  validationEnd=$?

  local validation=${validatedTheCodeWithTRUE}
  if [ "${validationBegin}" == ${validatedTheCodeWithFALSE} ] ||  [ "${validationEnd}" == ${validatedTheCodeWithFALSE} ]
    then
      validation=${validatedTheCodeWithFALSE}
  fi

  echo -e "${beginLine}${baseMessage[@]}${endLine}"
  return $validation
}

blockComplexText() {
  local taggedMessages="${1}"
  local theContent="${2}"
  local validation=${3}
  local viewOrWebStyle=${4}

  liftContentFromDinamicVariable --newVariable "theTagMessage" \
    --validation ${validation} \
    --getContentFrom ${taggedMessages}

  local emphassizedTextForMessage=$(composeEmphassizedMessage "${theContent[@]}" "${viewOrWebStyle}" ${validation})
  local newContent=$(composeOneLineOfMessage "tagCode" "${emphassizedTextForMessage[@]}" "${viewOrWebStyle}")
  local blockText=$(composeOneLineOfMessage "tagBlockQuote" "${newContent[@]}" "${viewOrWebStyle}")
  local message="${theTagMessage[0]} ${blockText[@]} ${theTagMessage[3]}"

  echo -e "${message[@]}"
}

#!${BASH}
serviceOperators() {
  echo -e "Loading... Services 'serviceOperators', version 0.1"
}


convertOperatorsForUseWithNumbers() {
  local operation=${1}
  local operator

  case ${operation} in
    "==" ) operator="-eq" ;;
    "=" ) operator="-eq" ;;
    ">" ) operator="-gt" ;;
    "<" ) operator="-lt" ;;
    * ) operator=${operation} ;;
  esac
  echo -e ${operator}
}

convertOperatorsForUseWithLetters() {
  local operation=${1}
  local operator
  case ${operation} in
    "-gt" ) operator=">" ;;
    "-lt" ) operator="<" ;;
    "-eq" ) operator="=" ;;
    "-ne" ) operator="!=" ;;
    * ) operator=${operation} ;;
  esac
  echo -e ${operator}
}

isParadigm() {
  local paradigm=${1}
  case $paradigm in
    "contain" |"contains" | "not" | "not contain" | "not contains" ) result=${validatedTheCodeWithTRUE} ;;
    * ) result=${validatedTheCodeWithFALSE} ;;
  esac
  echo -e "${result}"
}

checkTheParadigm() {
  local allTheContent="${1}"
  local comparator="${2}"
  local aPieceToVerify=${3}
  local result

  case "${comparator[@]}" in
    "contain" | "contains" )
      result=${validatedTheCodeWithFALSE}
      case "${allTheContent[@]}" in
        *${aPieceToVerify[@]}* )
          result=${validatedTheCodeWithTRUE}
        ;;
      esac
    ;;
    "not" | "not contain" | "not contains" )
      result=${validatedTheCodeWithTRUE}
      case "${allTheContent[@]}" in
        *${aPieceToVerify[@]}* )
          result=${validatedTheCodeWithFALSE}
        ;;
      esac
    ;;
  esac
  echo -e ${result}
}

translateSentence() {
  local comparator="${1}"
  local translation

  case "${comparator[@]}" in
    "contain" )
      translation="${paradigmContain[@]}"
    ;;
    "contains" )
      translation="${paradigmContains[@]}"
    ;;
    "not contain" )
      translation="${paradigmNotContain[@]}"
    ;;
    "not" | "not contains" )
      translation="${paradigmNotContains[@]}"
    ;;
    * )
     translation="${comparator[@]}"
    ;;
  esac
  echo -e "${translation[@]}"
}

areAllVariablesNumbers() {
  local iterated
  local checkNumber

  for iterated in "$@"
    do
      checkNumber=$(isNumber ${iterated})
      if [ "$checkNumber" == ${validatedTheCodeWithTRUE} ]
        then
          result=${validatedTheCodeWithTRUE}
        else
          result=${validatedTheCodeWithFALSE}
          break
      fi
  done
  echo -e ${result}
}

checkTheOperation() {
  local firstOperator="${1}"
  local theOperation=${2}
  local secondOperator="${3}"
  local result=""

  local convertedOperation=$(determineTypusOfSentence "${firstOperator[@]}" ${theOperation} "${secondOperator[@]}")

  if [ "${firstOperator[@]}" ${convertedOperation} "${secondOperator[@]}" ]
    then
      result=${validatedTheCodeWithTRUE}
    else
      result=${validatedTheCodeWithFALSE}
  fi

  echo -e "${result}"
}

determineTypusOfSentence() {
  local firstOperator="${1}"
  local sentence=${2}
  local secondOperator="${3}"

  local convertedSentence
  local checkNumber=$(areAllVariablesNumbers "${firstOperator[@]}" "${secondOperator[@]}")
  if [ "${checkNumber}" == ${validatedTheCodeWithTRUE} ]
    then
      convertedSentence=$(convertOperatorsForUseWithNumbers "${sentence[@]}" )
      echo -e ${convertedSentence}
    else
      convertedSentence=$(convertOperatorsForUseWithLetters "${sentence[@]}")
      echo -e "${convertedSentence[@]}"
  fi
}

validateSentence() {
  local firstOperator=("${1}")
  local sentence="${2}"
  local secondOperator=("${3}")

  local validation
  local theOperationContainAParadigm=$(isParadigm "${sentence[@]}")
  case ${theOperationContainAParadigm} in
    ${validatedTheCodeWithTRUE} )
      validation=$(checkTheParadigm "${firstOperator[@]}" "${sentence[@]}" "${secondOperator[@]}")
    ;;
    ${validatedTheCodeWithFALSE} )
      validation=$(checkTheOperation "${firstOperator[@]}" "${sentence[@]}" "${secondOperator[@]}")
    ;;
  esac

  echo -e ${validation}
}

isTheTestingGroupRunnable() {
  local theGroupTests="${1}"

  local isRunnable=${validatedTheCodeWithFALSE}

  if [ "${exceptTheGroupTests}" == "${theGroupTests}" ] || [ "${onlyTheGroupTests}" != "false" ]
    then
      isRunnable=${validatedTheCodeWithFALSE}
  fi

  if [ "${onlyTheGroupTests}" == "${theGroupTests}" ]
    then
      isRunnable=${validatedTheCodeWithTRUE}
  fi
  echo -e "${isRunnable}"
}

convertCodeResponseToBoolean() {
  local codeError=${1}
  local booleanResult=""
  if [ "${codeError[@]}" == ${validatedTheCodeWithTRUE} ]
    then
      booleanResult=true
    else
      booleanResult=false
  fi
  echo -e "${booleanResult}"
  return ${codeError}
}

convertCodeResponseToNegatedBoolean() {
  local codeError=${1}
  local booleanResult=""
  if [ "${codeError[@]}" == ${validatedTheCodeWithTRUE} ]
    then
      booleanResult=false
    else
      booleanResult=true
  fi
  echo -e "${booleanResult}"
  return ${codeError}
}

convertBooleanToCodeResponse() {
  local booleanMessage=${1}
  local codeResult=""
  if [ "${booleanMessage}" == true ]
    then
      codeResult=${validatedTheCodeWithTRUE}
    else
      codeResult=${validatedTheCodeWithFALSE}
  fi
  echo -e "${codeResult}"
  return ${codeResult}
}

convertNegatedBooleanToCodeResponse() {
  local booleanMessage=${1}
  local codeResult=""
  if [ "${booleanMessage}" != true ]
    then
      codeResult=${validatedTheCodeWithTRUE}
    else
      codeResult=${validatedTheCodeWithFALSE}
  fi
  echo -e "${codeResult}"
  return ${codeResult}
}

convertValidationWhenItIsCounteractiveAction() {
  local validator=${1}

  if [ "${validator}" == ${validatedTheCodeWithFALSE} ]
    then
      validation=${validatedTheCodeWithTRUE}
    else
      validation=${validatedTheCodeWithFALSE}
  fi

  echo -e "${validation}"
  return ${validation}
}

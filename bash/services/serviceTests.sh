#!${BASH}
serviceTests() {
  echo -e "Loading... Service 'serviceTests', version 0.1"
}


checkIfConfigurationFileExist() {
  local fromShell="${1}"
  local folder="${2}"
  local file="${3}.${shellExtension}"
  local codeMessage="${messageReturnedByFunction[checkIfConfigurationFileExist]}"
  local validation=${validatedTheCodeWithFALSE}
  local getMessagePath

  if [ "${fromShell}" != "" ]
    then
      fromPath="${cahPath}/${fromShell}"
    else
      fromPath="${cahPath}"
  fi 

  getMessagePath="${fromPath}/${folder}/${file}"

  theOrder=$?
  if [ -f "${getMessagePath}" ] || [ "${theOrder}"  != ${validatedTheCodeWithTRUE} ]
    then
      validation=${validatedTheCodeWithTRUE}
      codeMessage="${getMessagePath[@]}"
  fi
  echo -e "${codeMessage[@]}"
  return $validation
}

checkIfItIsInUserFolder() {
  local theFolder="${1}"
  local codeMessage="${messageReturnedByFunction[ErrorInValidationUserFolder]}"
  local validation=${validatedTheCodeWithFALSE}
  local getMessageUserFolder
  local validationUserFolder

  getMessageUserFolder=$(compareFolder "${theFolder[@]}" "${userFolder[@]}")
  validationUserFolder=$?

  if [ "${validationUserFolder}" == ${validatedTheCodeWithTRUE} ]
    then
      codeMessage=${messageReturnedByFunction[PathIsValid]}
      validation=${validatedTheCodeWithTRUE}
  fi

  echo -e "${codeMessage[@]}"
  return $validation
}

checkIfItIsInTempFolder() {
  local theFolder="${1}"
  local codeMessage="${messageReturnedByFunction[ErrorInValidationTempFolder]}"
  local validation=${validatedTheCodeWithFALSE}
  local getMessageTempFolder
  local validationTempFolder

  getMessageTempFolder=$(compareFolder "${theFolder[@]}" "${tempFolder[@]}")
  validationTempFolder=$?

  if [ "${validationTempFolder}" == ${validatedTheCodeWithTRUE} ]
    then
      codeMessage=${messageReturnedByFunction[PathIsValid]}
      validation=${validatedTheCodeWithTRUE}
  fi

  echo -e "${codeMessage[@]}"
  return $validation
}

checkIfItIsInProtectedFolder() {
  local theFolder="${1}"
  local codeMessage="${messageReturnedByFunction[PathIsValid]}"
  local validation=${validatedTheCodeWithFALSE}
  local getMessageProtectedFolder
  local validationProtectedFolder

  getMessageProtectedFolder=$(compareFolder "${theFolder[@]}" "${protectedFolder[@]}")
  validationProtectedFolder=$?

  if [ "${validationProtectedFolder}" == ${validatedTheCodeWithTRUE} ]
    then
      codeMessage=${messageReturnedByFunction[ErrorInValidationProtectedFolder]}
      validation=${validatedTheCodeWithTRUE}
  fi

  echo -e "${codeMessage[@]}"
  return $validation
}

checkIfPathIsValid() {
  local theFolder="${1}"
  local codeMessage
  local validation
  local getMessageUserFolder
  local getMessageTempFolder
  local getMessageProtectedFolder
  local validationUserFolder
  local validationTempFolder
  local validationProtectedFolder

  getMessageUserFolder=$(checkIfItIsInUserFolder "${theFolder[@]}")
  validationUserFolder=$?
  getMessageTempFolder=$(checkIfItIsInTempFolder "${theFolder[@]}")
  validationTempFolder=$?
  getMessageProtectedFolder=$(checkIfItIsInProtectedFolder "${theFolder[@]}")
  validationProtectedFolder=$?

  if [ "${validationProtectedFolder}" == ${validatedTheCodeWithTRUE} ]
  then
    echo -e "${getMessageProtectedFolder}"
    return ${validatedTheCodeWithFALSE}
  fi

  if [ "${validationUserFolder}" == ${validatedTheCodeWithTRUE} ]
    then
      echo -e "${getMessageUserFolder}"
      return ${validationUserFolder}
  fi

  if [ "${validationTempFolder}" == ${validatedTheCodeWithTRUE} ]
    then
      echo -e "${getMessageTempFolder}"
      return ${validationTempFolder}
  fi

  echo -e "${messageReturnedByFunction[ErrorInCheckIfPathIsValid]}"
  return ${validatedTheCodeWithFALSE}
}

checkFolderExistence() {
  local theFolder="${1}"
  local codeMessage="${messageReturnedByFunction[FolderNotExist]}"
  local validation=${validatedTheCodeWithFALSE}

  if [ -d "${theFolder}" ]
    then
      codeMessage="${messageReturnedByFunction[FolderExist]}"
      validation=${validatedTheCodeWithTRUE}
  fi

  echo -e "${codeMessage}"
  return $validation
}

checkIfFolderExist() {
  local theFolder="${1}"
  local codeMessage
  local validation
  local getMessageFolder

  codeMessage=$(checkFolderExistence "${theFolder}")
  validation=$?

  echo -e "${codeMessage[@]}"
  return ${validation}
}

checkFileExistence() {
  local thePath=${1}
  local codeMessage="${messageReturnedByFunction[FileNotExist]}"
  local validation=${validatedTheCodeWithFALSE}

  if [ -f "${thePath}" ]
    then
      codeMessage="${messageReturnedByFunction[FileExist]}"
      validation=${validatedTheCodeWithTRUE}
  fi

  echo -e "${codeMessage[@]}"
  return $validation
}

checkIfFileExist() {
  local thePath="${1}"
  local codeMessage
  local validation
  local getMessageFile

  codeMessage=$(checkFileExistence "${thePath[@]}")
  validation=$?

  echo -e "${codeMessage[@]}"
  return ${validation}
}


isTheToolInstalled() {
  local tool="${1}"
  local toolName="${2}"
  local installed
  local validation
  local check
  local foundTool
  local searchTool

  installed="${messageReturnedByFunction[ErrorInIsTheToolInstalled]}"
  validation=${validatedTheCodeWithFALSE}

  foundTool=$(catchTool "${tool}" "${toolName[@]}")
  validation=$?
  if [ "${foundTool}" != "" ] || [ "${validation}" = ${validatedTheCodeWithTRUE} ]
    then
      installed="${toolName[@]}"
      reverseValidation=${validatedTheCodeWithTRUE}
  fi

  echo -e "${installed[@]}"
  return ${validation}
}

isTheRepositoryInTheList() {
  local repository="${1}"
  local inTheList
  local validation
  local foundRepository

  inTheList="${messageReturnedByFunction[ErrorInEnsureRepository]}"
  validation=${validatedTheCodeWithFALSE}

  aptList=$(listAptEntries)
  foundRepository=$(isTheContentInTheString "${repository}" "${aptList[@]}")
  validation=$?
  if [ "${foundRepository}" == true ] || [ "${validation}" == ${validatedTheCodeWithTRUE} ]
    then
      inTheList=${foundRepository}
  fi

  echo -e "${inTheList[@]}"
  return ${validation}
}

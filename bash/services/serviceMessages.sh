#!${BASH}
serviceMessages() {
  echo -e "Loading... Services 'serviceMessages', version 0.1"
}



messageForNetwork() {
  local message="${1}"

  local messageLenght="${#message}"
  local messageConstruction="POST ${sentToPath} HTTP/1.1\nHost: ${sendToHost}\nContent Length: ${messageLenght}\nContent-Type: application/json\n\n${message}"
  echo "${messageConstruction[@]}"
}

sendMessagesThroughTheNetwork() {
  local message="${1}"
  local sendMessageToHost="${sendToHost}"
  local sendMessageToPort="${sendToPort}"
  local waitForResponseMessage="${waitForResponse}"
  local validation

  if [ "${sendMessagesToTheServer}" == "true" ] && [ "${sendToPort}" != "" ]
    then
      sendMessageToNetwork "${message[@]}\n" "${sendMessageToHost}" "${sendMessageToPort}" "${waitForResponseMessage}"
      validation=$?
  fi

  return $validation
}

getMessagesThroughTheNetwork() {
  local getMessageFromHost="${getFromHost}"
  local getMessageFromPort="${getFromPort}"

  if [ "${sendMessagesToTheServer}" == "true" ] && [ "${getFromPort}" != "" ]
    then
      while true
        do
          getMessageFromNetwork "${getMessageFromHost}" "${getMessageFromPort}"
    done
  fi
}

repeatMessagesThroughTheNetwork() {
  local getMessageFromHost="${getFromHost}"
  local getMessageFromPort="${getFromPort}"
  local sendMessageToHost="${sendToHost}"
  local sendMessageToPort="${sendToPort}"
  local waitForResponseMessage="${waitForResponse}"

  if [ "${sendMessagesToTheServer}" == "true" ] && [ "${getFromPort}" != "" ] && [ "${sendToPort}" != "" ]
    then
      repeatMessageToNetwork "${getMessageFromHost}" "${getMessageFromPort}" "${sendMessageToHost}" "${sendMessageToPort}" "${waitForResponseMessage}"
  fi
}

messageWithCountValidations() {
  local validation=${1}
  local currentTitle="${2}"
  case $validation in
    ${validatedTheCodeWithTRUE} )
      (( messageCounterSUCCESS += 1 ))
    ;;
    ${validatedTheCodeWithFALSE} )
      (( messageCounterERRORS += 1 ))
      titlesOfFailedTests+="[ ${currentTitle[@]} ]\n"
    ;;
  esac
}

setUpVisibility() {
  local nameOfParentFunction=${1}

  hideTheTitleMessage=false
  hideTheAbstractMessage=false
  hideTheResultMessage=false
  hideTheExplanationMessage=false

  case ${nameOfParentFunction} in
    "constructTheTask" )
      hideListedDivisions=("${hideDivisionsInSpecification[@]}")
    ;;
    "surveyTheTask" )
      hideListedDivisions=("${hideDivisionsInSpecification[@]}")
    ;;
    "usersInvolved" )
      hideListedDivisions=("${hideDivisionsInUsersInvolved[@]}")
    ;;
    "applyLanguage" )
      hideListedDivisions=("${hideDivisionsInSelectLanguage[@]}")
    ;;
    "applyToolInstallation" )
      hideListedDivisions=("${hideDivisionsInEnsureTool[@]}")
    ;;
    "applyToolRemove" )
      hideListedDivisions=("${hideDivisionsInUninstallTool[@]}")
    ;;
    "refreshRepository" )
      hideListedDivisions=("${hideDivisionsInRefreshRepository[@]}")
    ;;
    "updateAllPackages" )
      hideListedDivisions=("${hideDivisionsInUpdateAllPackages[@]}")
    ;;
    "ensureRepository" )
      hideListedDivisions=("${hideDivisionsInEnsureRepository[@]}")
    ;;
    "eraseRepository" )
      hideListedDivisions=("${hideDivisionsInEraseRepository[@]}")
    ;;
    "useDistro" )
      hideListedDivisions=("${hideDivisionsInSetUpDistro[@]}")
    ;;
    "ensureThePanel" )
      hideListedDivisions=("${hideDivisionsInEnsureManagementPanel[@]}")
    ;;
    "eraseTheManagementPanel" )
      hideListedDivisions=("${hideDivisionsInEraseManagementPanel[@]}")
    ;;
    "copyTheManagementPanel" )
      hideListedDivisions=("${hideDivisionsInCopyManagementPanel[@]}")
    ;;
    "moveThePanel" )
      hideListedDivisions=("${hideDivisionsInMoveManagementPanel[@]}")
    ;;
    "ensureTheBriefingTask" )
      hideListedDivisions=("${hideDivisionsInEnsureBriefingTask[@]}")
    ;;
    "eraseTheBriefingTask" )
      hideListedDivisions=("${hideDivisionsInEraseBriefingTask[@]}")
    ;;
    "ensureTheRecipeIngredient" )
      hideListedDivisions=("${hideDivisionsInEnsureRecipeIngredient[@]}")
    ;;
    * )
      hideListedDivisions=(false)
    ;;
  esac

  for argument in "${hideListedDivisions[@]}"
    do
      case $argument in
        "title" )
          hideTheTitleMessage=true
        ;;
        "abstract" )
          hideTheAbstractMessage=true
        ;;
        "result" )
          hideTheResultMessage=true
        ;;
        "explanation" )
          hideTheExplanationMessage=true
        ;;
      esac
  done
}

isVisible() {
  local taggedMessages="${1}"

  local visibility=false

  case ${taggedMessages} in
    "title" )
      if  [ "${showTitleMessages}" == true ] && [ "${globalShow}" == true ] && [ "${hideTheTitleMessage}" == false ]
        then
          visibility=true
      fi
    ;;
    "abstract" )
      if  [ "${showAbstractMessages}" == true ] && [ "${globalShow}" == true ] && [ "${hideTheAbstractMessage}" == false ]
        then
          visibility=true
      fi
    ;;
    "result" )
      if  [ "${showResultMessages}" == true ] && [ "${globalShow}" == true ] && [ "${hideTheResultMessage}" == false ]
        then
          visibility=true
      fi
    ;;
    "explanation" )
      if  [ "${showExplanationMessages}" == true ] && [ "${globalShow}" == true ] && [ "${hideTheExplanationMessage}" == false ]
        then
          visibility=true
      fi
    ;;
    "specificationsSummary" )
      if [ "${showSpecificationsSummary}" == true ] && [ "${globalShow}" == true ]
        then
          visibility=true
      fi
    ;;
  esac
  echo -e "${visibility}"
}

checkArrayOfParentFunction() {
  local validation

  liftContentFromDinamicVariable --newVariable "checkArrayOfParentFunction" \
    --validation ${validatedTheCodeWithTRUE} \
    --getContentFrom "${1}"

  if [ "${checkArrayOfParentFunction[0]}" == "" ]
    then
      validation=${validatedTheCodeWithFALSE}
    else
      validation=${validatedTheCodeWithTRUE}
  fi
  unset checkArrayOfParentFunction
  echo -e $validation
}

tellTheStory() {
  local nameOfParentFunction="${1}"
  local validation="${2}"
  local firstOperatorInSentence="${3}"
  local sentence="${4}"
  local secondOperatorInSentence="${5}"
  local messageReturnedBySystem=("${6}")
  local messageOfCode="${7}"
  local initialInfo="${8}"
  local endInfo="${9}"
  local theAlias="${10}"
  local theMomentum="${11}"
  local theLabels=("${12}")
  local theBulkData=("${13}")

  local currentTitleForErrorMessages=$(constructCurrentTitle ${validation} ${systemView})
  local checkArrayOfParentFunction=$(checkArrayOfParentFunction $nameOfParentFunction)

  local theTipusOfMessage
  if [ "${checkArrayOfParentFunction}" = ${validatedTheCodeWithFALSE} ]
    then
      theTipusOfMessage="theCheckIs"
      messageOfCode="${messageReturnedByFunction[ErrorInInterfaceFunctionWithoutMessage]}"
      messageReturnedBySystem=${messageOfCode[@]}
    else
      theTipusOfMessage=${nameOfParentFunction}
  fi

  if [ "${nameOfParentFunction}" == "surveyTheTask" ]
    then
      if [ "${theMomentum[@]}" != "" ]
        then
          local runbookValidation=$(convertCodeResponseToBoolean ${validation})

          local stringifyContent="$(printf "%q" "${runbookValidation}")"
          local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${theMomentum[@]}\": {\"runbook\": [\"validation\": \"${stringifyContent[@]}\"]}}}}}"
          sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"
      fi
    else
      if [ "${theMomentum[@]}" = "" ]
        then
          local currentDate=$(currentDate)
      local anotherMomentum="$(epochDate ${currentDate[@]})"
        else
          local anotherMomentum="${theMomentum[@]}"
      fi

      local stringifyContent="$(printf "%q" "${theLabels[@]}")"
      local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"test\": [\"labels\": \"${stringifyContent[@]}\"]}}}}}"
      sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"

      local stringifyContent="$(printf "%q" "${firstOperatorInSentence[@]}")"
      local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"test\": [\"firstOperator\": \"${stringifyContent[@]}\"]}}}}}"
      sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"

      local stringifyContent="$(printf "%q" "${sentence[@]}")"
      local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"test\": [\"sentence\": \"${stringifyContent[@]}\"]}}}}}"
      sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"

      local stringifyContent="$(printf "%q" "${secondOperatorInSentence[@]}")"
      local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"test\": [\"SecondOperator\": \"${stringifyContent[@]}\"]}}}}}"
      sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"

      local stringifyContent="$(printf "%q" "${validation}")"
      local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"test\": [\"validation\": \"${stringifyContent[@]}\"]}}}}}"
      sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"
  fi

  messageWithCountValidations $validation "${currentTitleForErrorMessages[@]}"
  sendMessages "${theTipusOfMessage}" "${validation}" "${firstOperatorInSentence[@]}" "${sentence[@]}" "${secondOperatorInSentence[@]}" "${messageReturnedBySystem[@]}" "${messageOfCode[@]}" "${initialInfo}" "${endInfo}"
}

sendMessages() {
  local nameOfParentFunction=${1}
  local validation=${2}
  local firstOperatorInSentence="${3}"
  local sentence="${4}"
  local secondOperatorInSentence="${5}"
  local messageReturnedBySystem=("${6}")
  local messageOfCode="${7}"
  local initialInfo="${8}"
  local endInfo="${9}"

  local taggedMessages="${nameOfParentFunction[@]}"

  setUpVisibility "${nameOfParentFunction}"

  sendAbstractOfMessages  "${taggedMessages[@]}" ${validation} "${firstOperatorInSentence}" "${sentence[@]}" "${secondOperatorInSentence}"
  sendResultOfMessages ${validation} "${messageOfCode[@]}"
  if [ "${messageReturnedBySystem[@]}" != false ]
    then
      sendExplanationOfMessages "${taggedMessages[@]}" "${messageReturnedBySystem[@]}" "${validation}" "${initialInfo}" "${endInfo}"
  fi

  setUpVisibility false
}

saveOrSeeValidatedMessage() {
  local taggedMessages=${1}
  local onceMessage=${2}
  local validation=${3}
  local viewOrWebStyle=${4}

  visibility=false

  if [ ${showExpectSUCCESS} == true ] && [ ${validation} == ${validatedTheCodeWithTRUE} ]
    then
      visibility=$(isVisible ${taggedMessages})
  fi
  if [ ${showExpectERROR} == true ] && [ ${validation} == ${validatedTheCodeWithFALSE} ]
    then
      visibility=$(isVisible ${taggedMessages})
  fi

  local isRunnable=$(isRunnableAndActiveTheRunbook)

  if  [ "${isRunnable}" == true ]
    then
      theOnceMessage="${onceMessage[@]}"
    else 
      theOnceMessage="${messageNotActionBecauseRunbookIsBroken[@]}"
      theOnceMessage="${onceMessage[@]}"
  fi 

  case ${viewOrWebStyle} in
    ${systemView} )
      case ${validation} in
        ${validatedTheCodeWithTRUE} )
          if [ ${visibility} == true ]
            then
              echo -e "${theOnceMessage[@]}"
          fi
        ;;
        ${validatedTheCodeWithFALSE} )
          if [ ${visibility} == true ]
            then
              echo -e "${theOnceMessage[@]}"
          fi
        ;;
      esac
    ;;
    ${systemPrint} )
      summaryMessages=("${summaryMessages[@]}${theOnceMessage[@]}\n")
    ;;
  esac
}

tellTheTitle() {
  local nameOfParentFunction=${1}
  local validation=${2}
  local theTag="${3}"
  local theTitle="${4}"
  local theAlias="${5}"
  local theMomentum="${6}"
  local theParent="${7}"
  local theExplanation=("${8}")
  local theLabels=("${9}")
  local theBulkData=("${10}")

  local theTitleTag="title${theTag}"

  tagAndTitle=(${theTitleTag} "${theTitle[@]}")
  viewedTheFirstTitleOcurrency=false
  viewedTheFirstAbstractOcurrency=false

  local stringifyContent="$(printf "%q" "${theTag[@]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${theMomentum[@]}\": {\"head\": [\"tag\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"

  local stringifyContent="$(printf "%q" "${theTitle[@]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${theMomentum[@]}\": {\"head\": [\"title\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"

  local stringifyContent="$(printf "%q" "${theMomentum[@]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${theMomentum[@]}\": {\"head\": [\"momentum\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"

  local stringifyContent="$(printf "%q" "${theParent[@]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${theMomentum[@]}\": {\"head\": [\"parent\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"

  local stringifyContent="$(printf "%q" "${theExplanation[@]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${theMomentum[@]}\": {\"head\": [\"explanation\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"

  local stringifyContent="$(printf "%q" "${theLabels[@]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${theMomentum[@]}\": {\"head\": [\"labels\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"

  sendTitleOfMessages ${validation}
}

setHeaderParameters() {
  for iterationInArguments in "$@"
    do
      local option=("${1}")
      shift
      local content=${1}
      case ${option} in
        "--describe" )
          theTitle="${content[@]}"
          theTag="Describe"
        ;;
        "--context" )
          theTitle="${content[@]}"
          theTag="Context"
        ;;
        "--section" )
          theTitle="${content[@]}"
          theTag="Section"
        ;;
        "--subsection" )
          theTitle="${content[@]}"
          theTag="Subsection"
        ;;
      esac
  done
}

header() {
  local theTitle
  local theTag
  local theParent
  local theAlias
  local theMomentum
  local theLabels
  local theExplanation
  local theBulkData

  setGenericParameters "$@"
  setHeaderParameters "$@"

  tellTheTitle "${FUNCNAME[0]}" "${validatedTheCodeWithTRUE}" "${theTag[@]}" "${theTitle[@]}" "${theAlias[@]}" "${theMomentum[@]}" "${theParent[@]}" "${theExplanation[@]}" "${theLabels[@]}" "${theBulkData[@]}"
}

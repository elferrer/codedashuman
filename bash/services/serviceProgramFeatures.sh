#!${BASH}
serviceProgramFeatures() {
  echo -e "Loading... Module 'serviceProgramFeatures', version 0.1"
}


installTheTool() {
  local tool="${1}"
  local toolName="${2}"
  local asName="${3}"
  local theAlias="${4}"
  local theMomentum="${5}"
  local theLabels=("${6}")
  local theBulkData=("${7}")
  local getMessageTool
  local validation

  getMessageTool=$(isTheToolInstalled "${tool}" "${toolName[@]}")
  validation=$?

  if [ "$validation" = ${validatedTheCodeWithFALSE} ]
    then

      if [ "${asName[@]}" = false ] || [ "${asName[@]}" = "" ]
        then
          installApplication="${toolName[@]}"
        else
          installApplication="${asName[@]}"
      fi

      getMessageTool=$(installTool "${tool}" "${installApplication[@]}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}")
  fi

  getMessageTool=$(isTheToolInstalled "${tool}" "${toolName[@]}")
  validation=$?

  echo -e "${getMessageTool}"
  return ${validation}
}


removeTheTool() {
  local tool="${1}"
  local toolName="${2}"
  local getMessageTool
  local validation

  getMessageTool=$(isTheToolInstalled "${tool}" "${toolName[@]}")
  validation=$?

  if [ "$validation" = ${validatedTheCodeWithTRUE} ]
    then
      getMessageRemoveTool=$(removeTool "${tool}" "${toolName[@]}")
      getMessageTool=$(isTheToolInstalled "${tool}" "${toolName[@]}")
      validation=$?
  fi

  echo -e "${getMessageTool}"
  return ${validation}
}

cookingTheRecipe() {
  local tool="${1}"
  local toolName="${2}"
  local asName="${3}"
  local theAlias="${4}"
  local theMomentum="${5}"
  local theLabels=("${6}")
  local theBulkData=("${7}")
  local getMessageTool
  local validation

  getMessageTool=$(bakeTheRecipe "${tool}" "${toolName[@]}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}")
  validation=$?

  echo -e "${getMessageTool}"
  return ${validation}
}

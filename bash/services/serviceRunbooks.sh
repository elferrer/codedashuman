#!${BASH}
serviceRunbooks() {
  echo -e "Loading... Services 'serviceRunbooks', version 0.1"
}


addSpecificationToDictionary() {
  local name=("${1}")
  case "${listOfSpecifications[@]}" in
    *${1}* ) ;;
    * ) listOfSpecifications+=("${name}") ;;
  esac
}

setLastTask() {
  local theAlias="${1}"
  LastTask="${theAlias}"
}

putLastTaskAsApprovedIfCompleted() {
  local completed="${1}"
  local theAlias="${2}"

  if [ "${completed}" == true ] && [ "${taskIsActiveOfTask[$theAlias]}" = true ]
    then
      LastTaskApproved="${theAlias}"
  fi
}

setTaskParameters() {
  for iterationInArguments in "$@"
    do
      local option=${1}
      shift
      local content=${1}
      case ${option} in
        "--toml" )
          filename="${content[@]}"
          configType="toml"
        ;;
        "--completed" )
          completed="${content[@]}"
        ;;
        "--taskIsActive" )
          taskIsActive="${content[@]}"
        ;;
        "--stopIfItFails" )
          taskStopIfItFails="${content[@]}"
        ;;
        "--dependsOn" )
          dependsOn="${content[@]}"
        ;;
        "--typeOfDependency" )
          typeOfDependency="${content[@]}"
        ;;
        "--ghostJob" )
          taskGhostJob="${content[@]}"
        ;;
      esac
  done
}

surveyTheTask() {
  local theAlias="${1:-false}"
  local theParent="${2}"
  local theMomentum="${3}"
  local theLabels=("${4}")
  local stopIfItFailsOfTask=("${5}")
  local theBulkData=("${6}")

  if [ "${workCompletionOfTask[$theAlias]}" == false ] && [ "${stopIfItFailsOfTask}" != false ]
    then
      theRunbookIsBrokenIs=true
      taskIsActiveOfTask[$theAlias]=false
  fi

  if [ "${theAlias}" != false ]
    then
      setLastTask "${theAlias}"
      putLastTaskAsApprovedIfCompleted "${workCompletionOfTask[$theAlias]}" "${theAlias}"
  fi

  local validation=$(convertNegatedBooleanToCodeResponse ${theRunbookIsBrokenIs})

  local response=$(convertCodeResponseToBoolean ${validation})
    local messageReturnedBySystem="${theTitle[@]}"
    if [ "${validation}" = "${validatedTheCodeWithTRUE}" ]
      then
        local sendMessageOfCode="${surveyTheTask[withSuccessEndMessage]}"
      else
        local sendMessageOfCode="${messageReturnedByFunction[ErrorInsurveyTheTask]}"
    fi


  messageWithCountValidations $validation "Alias: ${theAlias}"

  return $validation
}

surveyAddedSpecificationsFromConfigurationFile() {
  for content in "${listOfSpecifications[@]}"
    do
      if [ "${content}" == false ] || [ "${content}" == "" ]
        then
          theAlias=""
        else
          theAlias="${content[@]}"
      fi

      if [ "${content[@]}" != "" ]
        then
          sendTaskMessagesThroughTheNetwork
      fi

      surveyTheTask "${theAlias}" "${theParent[$theAlias]}" "${theMomentum[@]}" "${theLabels[@]}" "${stopIfItFailsOfTask[$theAlias]}" "${theBulkData[@]}"
      local validation=$?
  done

  return $validation
}

sendTaskMessagesThroughTheNetwork() {
  if [ "${theMomentum[$theAlias]}" = "" ]
    then
      local currentDate=$(currentDate)
      local anotherMomentum="$(epochDate ${currentDate[@]})"
    else
      local anotherMomentum="${theMomentum[$theAlias]}"
  fi

  local stringifyContent="$(printf "%q" "${theParentOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"parent\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  

  local stringifyContent="$(printf "%q" "${titleOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"title\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  

  local stringifyContent="$(printf "%q" "${explanationOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"explanation\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  

  local stringifyContent="$(printf "%q" "${workCompletionOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"completed\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  

  local stringifyContent="$(printf "%q" "${taskIsActiveOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"taskIsActive\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  

  local stringifyContent="$(printf "%q" "${stopIfItFailsOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"stopIfItFails\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  

  local stringifyContent="$(printf "%q" "${dependsOnOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"dependsOn\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  

  local stringifyContent="$(printf "%q" "${typeOfDependencyOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"typeOfDependency\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  

  local stringifyContent="$(printf "%q" "${ghostJobOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"ghostJob\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  

  local stringifyContent="$(printf "%q" "${labelsOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"labels\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  

  local stringifyContent="$(printf "%q" "${bulkDataOfTask[$theAlias]}")"
  local content="{\"${projectToken}\": {\"${taskIdentifier}\": {\"${theAlias}\": {\"${anotherMomentum[@]}\": {\"runbook\": [\"bulkData\": \"${stringifyContent[@]}\"]}}}}}"
  sendMessagesThroughTheNetwork "$(messageForNetwork "${content[@]}")"  
}

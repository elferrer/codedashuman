#!${BASH}
programFeatures() {
  echo -e "Loading... Actions 'programFeatures', version 0.1"
}


catchTool() {
  local tool="${1}"
  local toolName="${2}"
  local specificDistribution=${distribution}
  local validation
  local catch

  catch=$(catch${tool}With${specificDistribution} "${toolName[@]}" )
  validation=$?

  echo -e "${catch[@]}"
  return $validation
}

installTool() {
  local tool="${1}"
  local toolName="${2}"
  local theAlias="${3}"
  local theMomentum="${4}"
  local theLabels=("${5}")
  local theBulkData=("${6}")
  local validation
  local resultOfInstallation

  local specificDistribution=${distribution}
  resultOfInstallation=$(install${tool}With${specificDistribution} "${toolName[@]}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}")
  validation=$?

  echo -e "${resultOfInstallation[@]}"
  return $validation
}

removeTool() {
  local tool="${1}"
  local toolName="${2}"
  local validation
  local resultOfUninstallation

  local specificDistribution=${distribution}
  resultOfUninstallation=$(remove${tool}With${specificDistribution} "${toolName[@]}")
  validation=$?

  echo -e "${resultOfUninstallation[@]}"
  return $validation
}

bakeTheRecipe() {
  local tool="${1}"
  local toolName="${2}"
  local theAlias="${3}"
  local theMomentum="${4}"
  local theLabels=("${5}")
  local theBulkData=("${6}")
  local validation
  local resultOfInstallation

  local specificDistribution=${distribution}
  resultOfInstallation=$(install${tool}With${specificDistribution} "${toolName[@]}" "${theAlias[@]}" "${theMomentum[@]}" "${theLabels[@]}" "${theBulkData[@]}")
  validation=$?

  echo -e "${resultOfInstallation[@]}"
  return $validation
}

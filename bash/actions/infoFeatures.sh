#!${BASH}
infoFeatures() {
  echo -e "Loading... Actions 'infoFeatures', version 0.1"
}


getInfoOf() {
  local action=${1}
  local specificDistribution=${distribution}
  local validation
  local collector

  liftContentFromDinamicVariable --newVariable "tagMessage" \
    --validation ${validation} \
    --getContentFrom "info${action}"

  collector=$(collect${action}With${specificDistribution})
  validation=$?

  echo -e "${tagMessage[0]} ${collector[@]}"
  return $validation
}

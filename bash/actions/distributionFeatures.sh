#!${BASH}
distributionFeatures() {
  echo -e "Loading... Actions 'distributionFeatures', version 0.1"
}


catchDistroById() {
  local momentum="${1}"
  local validation
  local catchDistro

  local specificDistribution=${distribution}
  catchDistro=$(catchDistroByIdWith${specificDistribution} "${momentum[@]}")
  validation=$?

  echo -e "${catchDistro[@]}"
  return $validation
}

listAptEntries() {
  local aptEntries
  local specificDistribution=${distribution}
  aptEntries=$(listAptEntriesWith${specificDistribution})
  validation=$?

  echo -e "${aptEntries[@]}"
  return $validation
}

isTheContentInTheString() {
  local theContent="${1}"
  local theString="${2}"

  local specificDistribution=${distribution}
  aptEntries=$(isTheContentInTheStringWith${specificDistribution} "${theContent[@]}" "${theString[@]}")
  validation=$?

  echo -e "${aptEntries[@]}"
  return $validation
}

addAptRepository() {
  local typeRepository="${1}"
  local repository="${2}"
  local validation
  local resultOfAction

  local specificDistribution=${distribution}

  case "${typeRepository}" in
    "ppa" )
      resultOfAction=$(addRepoWith${specificDistribution} "ppa:${repository}")
      validation=$?
    ;;
    "deb" )
      resultOfAction=$(addRepoWith${specificDistribution} "deb ${repository}")
      validation=$?
    ;;
  esac

  echo -e "${resultOfAction[@]}"
  return $validation
}

removeAptRepository() {
  local typeRepository="${1}"
  local repository="${2}"
  local validation
  local resultOfAction

  local specificDistribution=${distribution}

  case "${typeRepository}" in
    "ppa" )
      resultOfAction=$(removeRepoWith${specificDistribution} "ppa:${repository}")
      validation=$?
    ;;
    "deb" )
      resultOfAction=$(removeRepoWith${specificDistribution} "deb ${repository}")
      validation=$?
    ;;
  esac

  echo -e "${resultOfAction[@]}"
  return $validation
}

refreshCache() {
  local validation

  local specificDistribution=${distribution}
  local update=$(refreshCacheWith${specificDistribution})
  validation=$?

  return $validation
}

downloadPackages() {
  local validation

  local specificDistribution=${distribution}
  local update=$(downloadPackagesWith${specificDistribution})
  validation=$?

  return $validation
}

updatePackages() {
  local validation

  local specificDistribution=${distribution}
  local update=$(updatePackagesWith${specificDistribution})
  validation=$?

  return $validation
}

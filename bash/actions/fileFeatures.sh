#!${BASH}
fileFeatures() {
  echo -e "Loading... Actions 'fileFeatures', version 0.1"
}


canonicalizeExistentPath() {
  local theFolder=${1}
  local validation
  local resultOfCanonicalize

  local specificDistribution=${distribution}
  resultOfCanonicalize=$(canonicalizePathWith${specificDistribution} ${theFolder})
  validation=$?

  echo -e "${resultOfCanonicalize[@]}"
  return $validation
}

createFolder() {
  local theFolder=${1}
  local validation
  local resultOfCreation

  local specificDistribution=${distribution}
  resultOfCreation=$(createFolderWith${specificDistribution} ${theFolder})
  validation=$?

  echo -e "${resultOfCreation[@]}"
  return $validation
}

removeFolder() {
  local theFolder=${1}
  local validation
  local resultOfRemoveFolder

  local specificDistribution=${distribution}
  resultOfRemoveFolder=$(removeFolderWith${specificDistribution} ${theFolder})
  validation=$?

  echo -e "${resultOfRemoveFolder[@]}"
  return $validation
}

copyFolder() {
  local originFolder="${1}/."
  local destinationFolder=${2}
  local validation
  local resultOfCopy

  local specificDistribution=${distribution}
  local createFolder=$(createFolderWith${specificDistribution} ${destinationFolder})
  resultOfCopy=$(copyFolderWith${specificDistribution} ${originFolder} ${destinationFolder})
  validation=$?

  echo -e "${resultOfCopy[@]}"
  return $validation
}

createFile() {
  local thePath=${1}
  local validation
  local resultOfCreation

  local specificDistribution=${distribution}
  resultOfCreation=$(createFileWith${specificDistribution} ${thePath})
  validation=$?

  echo -e "${resultOfCreation[@]}"
  return $validation
}

softDeleteFile() {
  local thePath=${1}
  local validation
  local resultOfDeletion

  local specificDistribution=${distribution}
  resultOfDeletion=$(softDeleteFileWith${specificDistribution} ${thePath})
  validation=$?

  echo -e "${resultOfDeletion[@]}"
  return $validation
}

addLineToFile() {
  local theLine=${1}
  local thePath=${2}
  local validation
  local resultOfAddition

  local specificDistribution=${distribution}
  resultOfAddition=$(addLineToFileWith${specificDistribution} "${theLine[@]}" ${thePath})
  validation=$?

  echo -e "${resultOfAddition}"
  return $validation
}

isTheContentInTheFile() {
  local theLine="${1}"
  local thePath=${2}
  local atTheBeginningOfTheLine="${3}"
  local validation
  local resultOfCheck

  local specificDistribution=${distribution}
  resultOfCheck=$(isTheContentInTheFileWith${specificDistribution} "${theLine[@]}" ${thePath} "${atTheBeginningOfTheLine[@]}")
  validation=$?

  echo -e "${resultOfCheck}"
  return $validation
}

countContentInTheFile() {
  local theLine="${1}"
  local thePath=${2}
  local validation
  local resultOfCheck

  local specificDistribution=${distribution}
  resultOfCheck=$(countContentInTheFileWith${specificDistribution} "${theLine[@]}" ${thePath})
  validation=$?

  echo -e "${resultOfCheck}"
  return $validation
}


getEpochDate() {
  local theDate="${1}"
  local validation
  local resultOfCheck

  local specificDistribution=${distribution}
  resultOfCheck=$(getEpochDateWith${specificDistribution} "${theDate[@]}")
  validation=$?

  echo -e "${resultOfCheck}"
  return $validation
}


getCurrentDate() {
  local validation
  local resultOfCheck

  local specificDistribution=${distribution}
  resultOfCheck=$(getCurrentDateWith${specificDistribution})
  validation=$?

  echo -e "${resultOfCheck}"
  return $validation
}

getLastDateChangeInTheFile() {
  local filename="${1}"
  local resultOfCheck
  local validation

  local specificDistribution=${distribution}
  resultOfCheck=$(getLastDateChangeInTheFileWith${specificDistribution}  "${filename}")
  validation=$?

  echo -e "${resultOfCheck}"
  return $validation
}

waitForTheFile() {
  local filename="${1}"
  local resultOfCheck
  local validation

  local specificDistribution=${distribution}
  resultOfCheck=$(waitForTheFileWith${specificDistribution}  "${filename}")
  validation=$?

  echo -e "${resultOfCheck}"
  return $validation
}

goToFolder() {
  local folder=$*

  local specificDistribution=${distribution}
  goToFolderWith${specificDistribution} "${folder[@]}"
  validation=$?

  return $validation
}

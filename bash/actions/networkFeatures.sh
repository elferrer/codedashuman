#!${BASH}
networkFeatures() {
  echo -e "Loading... Actions 'networkFeatures', version 0.1"
}


sendMessageToNetwork() {
  local message="${1}"
  local sendMessageToHost="${2}"
  local sendMessageToPort="${3}"
  local waitForResponseMessage="${4}"
  local validation

  local specificDistribution=${distribution}
  sendMessageToNetworkWith${specificDistribution} "${message}" "${sendMessageToHost}" "${sendMessageToPort}" "${waitForResponseMessage}"
  validation=$?

  return $validation
}

getMessageFromNetwork() {
  local getMessageFromHost="${1}"
  local getMessageFromPort="${2}"

  local specificDistribution=${distribution}
  getMessageFromNetworkWith${specificDistribution} "${getMessageFromHost}" "${getMessageFromPort}"
  validation=$?

  return $validation
}

repeatMessageToNetwork() {
  local getMessageFromHost="${1}"
  local getMessageFromPort="${2}"
  local sendMessageToHost="${3}"
  local sendMessageToPort="${4}"
  local waitForResponseMessage="${5}"

  local specificDistribution=${distribution}
  repeatMessageToNetworkWith${specificDistribution} "${getMessageFromHost}" "${getMessageFromPort}" "${sendMessageToHost}" "${sendMessageToPort}" "${waitForResponseMessage}"
  validation=$?

  return $validation
}

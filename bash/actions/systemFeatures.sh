#!${BASH}
systemFeatures() {
  echo -e "Loading... Actions 'systemFeatures', version 0.1"
}


whoIsIt() {
  local specificDistribution=${distribution}
  local validation
  local whoIsIt

  whoIsIt=$(whoIsItWith${specificDistribution})
  validation=$?

  echo -e "${whoIsIt[@]}"
  return $validation
}

isUserInsideGroups() {
  local searchUser="${1}"
  local validation
  local isUserInsideGroups

  local specificDistribution=${distribution}
  isUserInsideGroups=$(isUserInsideGroupsWith${specificDistribution} "${searchUser[@]}")
  validation=$?

  echo -e "${isUserInsideGroups[@]}"
  return $validation
}

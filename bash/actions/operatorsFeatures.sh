#!${BASH}
operatorsFeatures() {
  echo -e "Loading... Actions 'operatorsFeatures', version 0.1"
}


normalizeString() {
  local theString="${1}"

  local specificDistribution=${distribution}
  normalizeStringWith${specificDistribution} "${theString}"
  validation=$?

  return $validation
}


isNumber() {
  local theString="${1}"

  local specificDistribution=${distribution}
  isNumberWith${specificDistribution} "${theString}"
  validation=$?

  return $validation
}

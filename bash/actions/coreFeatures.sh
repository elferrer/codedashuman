#!${BASH}
coreFeatures() {
  echo -e "Loading... Actions 'coreFeatures', version 0.1"
}


liftContentFromDinamicVariable() {
  local specificDistribution=${distribution}
  liftContentFromDinamicVariableWith${specificDistribution} "$@"

  return $?
}




validateResponse() {
  local expressionToCheck=${1}

  local specificDistribution=${distribution}
  validateResponseWith${specificDistribution} "${expressionToCheck[@]}"
  validation=$?

  return $validation
}

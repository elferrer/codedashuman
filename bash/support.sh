#!${BASH}
support() {
  echo -e "Loading... Core 'support', version 0.1"
}


initializeGlobalVariable() {
  local theVariable="${1}"
  local theContent="${2}"
  unset ${theVariable}
  declare -g ${theVariable}="${theContent}"
}

initializeGlobalArray() {
  local theVariable="${1}"
  local theContent="${2}"
  unset ${theVariable}
  declare -ga ${theVariable}="${theContent}"
}

initializeGlobalAlias() {
  local theVariable="${1}"
  local theContent="${2}"
  unset ${theVariable}
  declare -gA ${theVariable}="${theContent}"
}

returnTheNumberedPieceFromEndByCutting() {
  local oneCut=${1}
  local splitter="${2}"
  local validation
  local cutting

  cutting=$(cut -d "${splitter[@]}" -f $oneCut)
  validation=$?

  echo -e "${cutting[@]}"
  return $validation
}

returnFilenameWithoutExtension() {
  local file="${1}"
  echo -e ${file} | returnTheNumberedPieceFromEndByCutting 1 '.'
}

returnFilenameWithoutPath() {
  local file="${1}"
  echo ${file} | rev | returnTheNumberedPieceFromEndByCutting 1 '/' | rev
}

initializeFilenameVariableFromPath() {
  local file=${1}
  getFilename=$(returnFilenameWithoutPath ${file})
  addOnName=$(returnFilenameWithoutExtension ${getFilename})
  initializeGlobalVariable "${addOnName}" "${addOnName}"
}

loadFeature() {
  local play=${1}
  local resultIs
  local thisLocation=$PWD
  cd "${cahPath}"

  if [ -f "${play}" ]
    then
      initializeFilenameVariableFromPath "${play}"
      . "${play}"
      resultIs=${validatedTheCodeWithTRUE}
    else
      resultIs=${validatedTheCodeWithFALSE}
  fi
  cd "${thisLocation}"
  return $resultIs
}

loadAddOn() {
  local fromShell="${1}"
  local artifacts="${2}"
  local addOn="${3}"
  local addOnFile="${3}.${shellExtension}"
  local theOrder
  local resultIs

  local thisLocation=$PWD

  if [ "${fromShell}" != "" ]
    then
      fromPath="${cahPath}/${fromShell}"
    else
      fromPath="${cahPath}"
  fi 

  if [ "${addOn}" = "" ]
    then
      local allAddOn=$(ls ${fromPath}/${artifacts}/*${shell_extension})
      cd "${fromPath}/${artifacts}"
      for file in ${allAddOn}
        do
          loadFeature "${file}"
          resultIs=$?
          if [ "${resultIs}" != "0" ]
            then
              return $resultIs
          fi
      done
    else
      theOrder="${fromPath}/${artifacts}/${addOnFile}"
      loadFeature "${theOrder}"
      resultIs=$?
  fi

  cd "${thisLocation}"
  return $resultIs
}

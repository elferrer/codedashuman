#!${BASH}
spanish() {
  echo -e "Loading... Language 'informal spanish', version 0.1"
}


initializeGlobalVariable "activeLanguage" "spanish"

createHelpDictionaryForGiveMeFunction() {
  acceptedWords=("Palabras aceptadas:")

  local Help="Help"
  addToHelpDictionary ${Help} "Para obtener esta información básica use \"giveMe Help\". Para obtener una información extendida use \"giveMe Help 'extended'\".
Ejemplo:
  giveMe List
  giveMe List 'extended'
  giveMe List 'example'
  giveMe List 'options'
  giveMe List 'see all'
  "
  addToExtendedHelpDictionary ${Help} "Para lanzar CAH puede hacerlo desde la consola desde el directorio padre con (versión BASH):
source 'layCAH.sh'

También puede crear un archivo con el siguiente contenido (recomendado):
#!\$BASH
source 'layCAH.sh'

La ayuda básica puede obtenerla mediante el comando:
giveMe

El uso a través de un script da la oportunidad de guardarlo como Runbook, aunque tiene la opción de lanzar los comandos directamente desde la consola (no aconsejado).

El flujo normal de un script CAH es:

1. Revisar la configuración de usuario para adaptar las rutas a su necesidades.
2. Crear el nombre del proyecto.
3. Seleccionar idioma.
4. Seleccionar distribución.
5. Crear acciones.
6. Finalizar con el resumen.
"
  addToOptionsHelpDictionary ${Help} " "
  addToExampleHelpDictionary ${Help} " "

  local selectLanguage="selectLanguage"
  addToHelpDictionary ${selectLanguage} "Le permite seleccionar el idioma que desee para visualizar los mensajes. Los idiomas disponibles están en la carpeta '${typeOfLanguagePath}'. "
  addToExtendedHelpDictionary ${selectLanguage} "En la visualización en pantalla se puede omitir información."
  addToOptionsHelpDictionary ${selectLanguage} "Opciones:
  --language = idioma que desea.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias =.
  --momentum=.
  --bulkData
  --labels"
  addToExampleHelpDictionary ${selectLanguage} "Ejemplo:
  selectLanguage --language spanish --initialInfo 'user;date' --endInfo 'date;path'"

  local launchSpecificationsSummary="launchSpecificationsSummary"
  addToHelpDictionary ${launchSpecificationsSummary} "Le muestra el resultado de los tests. "
  addToExtendedHelpDictionary ${launchSpecificationsSummary} "Puede especificar un nombre de archivo para guardar el log completo. Si no escribe un nombre de archivo se guardará como ${defaultSummaryFile[@]}."
  addToOptionsHelpDictionary ${launchSpecificationsSummary} "Opciones:
  --save = nombre del archivo para el log. Se guardará con la extensión añadiéndole el tipo de fichero (por ejemplo 'summary.log.html').
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas."
  addToExampleHelpDictionary ${launchSpecificationsSummary} "Ejemplo:
  launchSpecificationsSummary --save nameOfFile --initialInfo 'user;date' --endInfo 'date;path'"

  local ensureTool="ensureTool"
  addToHelpDictionary ${ensureTool} "Instala una utilidad."
  addToExtendedHelpDictionary ${ensureTool} "Comprueba que la utilidad deseada está instalada, en caso contrario la instala."
  addToOptionsHelpDictionary ${ensureTool} "Opciones:
  --program = nombre del programa, el programa está en localizable en los repositorios.
  --package = nombre del programa, el programa es un archivo local.
  --gem = nombre de la gema.
  --asName = nombre del paquete que incluye el programa (en caso de que el programa forme parte de otro paquete).
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias =.
  --momentum=.
  --bulkData
  --labels"
  addToExampleHelpDictionary ${ensureTool} "Ejemplo:
  ensureTool --gem theGem --initialInfo 'user;date' --endInfo 'date;path'
  ensureTool --program ifconfig --asName net-tools --initialInfo 'user;date' --endInfo 'date;path'"

  local uninstallTool="uninstallTool"
  addToHelpDictionary ${uninstallTool} "Desinstala una utilidad."
  addToExtendedHelpDictionary ${uninstallTool} "Comprueba que la utilidad deseada no está instalada, en caso contrario la desinstala."
  addToOptionsHelpDictionary ${uninstallTool} "Opciones:
  --program = nombre del programa, el programa está en localizable en los repositorios.
  --package = nombre del programa, el programa es un archivo local.
  --gem = nombre de la gema.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${uninstallTool} "Ejemplo:
  ensureTool --gem theGem --initialInfo 'user;date' --endInfo 'date;path'"

  local ensureToolRepository="ensureToolRepository"
  addToHelpDictionary ${ensureToolRepository} "Añade un repositorio."
  addToExtendedHelpDictionary ${ensureToolRepository} "Comprueba que el repositorio deseado está añadido, en caso contrario lo añade."
  addToOptionsHelpDictionary ${ensureToolRepository} "Opciones:
  --repositoryName = El repositorio deseado. Omitimos 'ppa:' si es un repositorio ppa y omitimos 'deb' si es un repositorio deb.
  --repositoryType = El tipo de repositorio, puede ser 'ppa' o 'deb'.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${ensureToolRepository} "Ejemplo:
  ensureToolRepository --repositoryName inkscape.dev/trunk --repositoryType ppa --initialInfo 'user;date' --endInfo 'date;path'"

  local eraseToolRepository="eraseToolRepository"
  addToHelpDictionary ${eraseToolRepository} "Quita un repositorio."
  addToExtendedHelpDictionary ${eraseToolRepository} "Comprueba que el repositorio deseado está añadido y lo borra."
  addToOptionsHelpDictionary ${eraseToolRepository} "Opciones:
  --repositoryName = El repositorio deseado. Omitimos 'ppa:' si es un repositorio ppa y omitimos 'deb' si es un repositorio deb.
  --repositoryType = El tipo de repositorio, puede ser 'ppa' o 'deb'.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${eraseToolRepository} "Ejemplo:
  eraseToolRepository --repositoryName inkscape.dev/trunk --repositoryType ppa --initialInfo 'user;date' --endInfo 'date;path'"

  local setUpDistro="setUpDistro"
  addToHelpDictionary ${setUpDistro} "Selecciona la distribución que desea utilizar."
  addToExtendedHelpDictionary ${setUpDistro} "Si la distribución indicada no existe buscará si es posible utilizar una versión compatible. Este comando es idempotente y solo devolverá error cuando no queden opciones viables."
  addToOptionsHelpDictionary ${setUpDistro} "Opciones:
  --name = nombre de la distribución.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${setUpDistro} "Ejemplo:
  setUpDistro --name theDistro --initialInfo 'user;date' --endInfo 'date;path'"

  local refreshRepositories="refreshRepositories"
  addToHelpDictionary ${refreshRepositories} "Actualiza los índices de la cache de los repositorios."
  addToExtendedHelpDictionary ${refreshRepositories} "Actualiza los índices de la cache de los repositorios. Este comando es idempotente y solo devolverá error cuando no queden opciones viables."
  addToOptionsHelpDictionary ${refreshRepositories} "Opciones:
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${refreshRepositories} "Ejemplo:
  refreshRepositories --initialInfo 'user;date' --endInfo 'date;path'"

  local updateRepositoryPackages="updateRepositoryPackages"
  addToHelpDictionary ${updateRepositoryPackages} "Actualiza los paquetes disponibles."
  addToExtendedHelpDictionary ${updateRepositoryPackages} "Actualiza los paquetes disponibles. Este comando es idempotente y solo devolverá error cuando no queden opciones viables."
  addToOptionsHelpDictionary ${updateRepositoryPackages} "Opciones:
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${updateRepositoryPackages} "Ejemplo:
  updateRepositoryPackages --initialInfo 'user;date' --endInfo 'date;path'"

  local ensureManagementPanel="ensureManagementPanel"
  addToHelpDictionary ${ensureManagementPanel} "Crea una carpeta si no existe. "
  addToExtendedHelpDictionary ${ensureManagementPanel} "Use comillas si es necesario."
  addToOptionsHelpDictionary ${ensureManagementPanel} "Opciones:
  --name = nombre de la carpeta.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${ensureManagementPanel} "Ejemplo:
  ensureManagementPanel --name folderName --initialInfo 'user;date' --endInfo 'date;path'"

  local eraseManagementPanel="eraseManagementPanel"
  addToHelpDictionary ${eraseManagementPanel} "Borra una carpeta. "
  addToExtendedHelpDictionary ${eraseManagementPanel} "Use comillas si es necesario."
  addToOptionsHelpDictionary ${eraseManagementPanel} "Opciones:
  --name = nombre de la carpeta.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${eraseManagementPanel} "Ejemplo:
  eraseManagementPanel --name folderName --initialInfo 'user;date' --endInfo 'date;path'"

  local copyManagementPanel="copyManagementPanel"
  addToHelpDictionary ${copyManagementPanel} "Copia una carpeta. "
  addToExtendedHelpDictionary ${copyManagementPanel} "Use comillas si es necesario."
  addToOptionsHelpDictionary ${copyManagementPanel} "Opciones:
  --origin = nombre de la carpeta.
  --destination = nombre de la carpeta.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${copyManagementPanel} "Ejemplo:
  copyManagementPanel --origin folderName --destination folderName --initialInfo 'user;date' --endInfo 'date;path'"

  local moveManagementPanel="moveManagementPanel"
  addToHelpDictionary ${moveManagementPanel} "Mueve una carpeta. "
  addToExtendedHelpDictionary ${moveManagementPanel} "Use comillas si es necesario."
  addToOptionsHelpDictionary ${moveManagementPanel} "Opciones:
  --origin = nombre de la carpeta.
  --destination = nombre de la carpeta.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${moveManagementPanel} "Ejemplo:
  moveManagementPanel --origin folderName --destination folderName --initialInfo 'user;date' --endInfo 'date;path'"

  local ensureBriefingTask="ensureBriefingTask"
  addToHelpDictionary ${ensureBriefingTask} "Crea un archivo. "
  addToExtendedHelpDictionary ${ensureBriefingTask} "Use comillas si es necesario."
  addToOptionsHelpDictionary ${ensureBriefingTask} "Opciones:
  --name = nombre del archivo.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${ensureBriefingTask} "Ejemplo:
  ensureBriefingTask --name filename --initialInfo 'user;date' --endInfo 'date;path'"

  local eraseBriefingTask="eraseBriefingTask"
  addToHelpDictionary ${eraseBriefingTask} "Borra un archivo. "
  addToExtendedHelpDictionary ${eraseBriefingTask} "Use comillas si es necesario."
  addToOptionsHelpDictionary ${eraseBriefingTask} "Opciones:
  --name = nombre del archivo.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${eraseBriefingTask} "Ejemplo:
  eraseBriefingTask --name filename --initialInfo 'user;date' --endInfo 'date;path'"

  local ensureRecipeIngredient="ensureRecipeIngredient"
  addToHelpDictionary ${ensureRecipeIngredient} "Añade una línea a un archivo, si el texto no existe en el archivo. "
  addToExtendedHelpDictionary ${ensureRecipeIngredient} "Use comillas si es necesario, puede utilizar espacios para 'theContent'."
  addToOptionsHelpDictionary ${ensureRecipeIngredient} "Opciones:
  --name = nombre del archivo.
  --line = indique el texto que incluir en el archivo.
  --atTheBeginningOfTheLine = true | false. Indique si desea que el texto a incluir esté al inicio de la línea de texto.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${ensureRecipeIngredient} "Ejemplo:
  ensureRecipeIngredient --name filename --line theContent --atTheBeginningOfTheLine 'true' --initialInfo 'user;date' --endInfo 'date;path'"

  local weHope="weHope"
  addToHelpDictionary ${weHope} "Herramienta para testear. Haga uso de 'giveMe options' para ver una descripción de las opciones aceptadas."
  addToExtendedHelpDictionary ${weHope} "Utilice 'weHope' para saber si se cumple una condición."
  addToOptionsHelpDictionary ${weHope} "Opciones:
  --messageInterpretation = nombre de la función o el mensaje del cual deseamos que muestre la leyenda.
  --firstOperator = el primer operando de la condición que desea comprobar.
  --sentence = la sentencia o paradigma que desea utilizar.
  --secondOperator = el segundo operando de la condición que desea comprobar.
  --humanReadableFirstOperator = el mensaje que desea que se muestre en la salida en sustitución del primer operando.
  --humanReadableSecondOperator = el mensaje que desea que se muestre en la salida en sustitución del segundo operando.
  --momentum=.
  --initialInfo = indique las opciones deseadas.
  --endInfo = indique las opciones deseadas."
  addToExampleHelpDictionary ${weHope} "Ejemplo:
  weHope --messageInterpretation theTipusOfMessage --firstOperator theFirstOperator --sentence aSentence --secondOperator theSecondOperator --humanReadableFirstOperator onePhrase --humanReadableSecondOperator onePhrase --momentum --initialInfo 'user;date' --endInfo 'date;path'"

  local options="options"
  addToHelpDictionary ${options} "Descripción de los diferentes valores que puede utilizar en cada opción."
  addToExtendedHelpDictionary ${options} "Puede ver los tests ('${testingPath}') como ejemplo de utilización."
  addToOptionsHelpDictionary ${options} "Opciones: "
  addToExampleHelpDictionary ${options} "Para las opciones que permiten varios valores no debe utilizar espacios; por ejemplo, para 'initialInfo'/'endInfo' utilice las comillas y separe los valores por punto y coma: 'user;date' . Puede ver los disponibles en el archivo '${shell}/${servicesPath}/serviceInfo.sh'
  Para los valores que sean frases debe entrecomillarlos.
  Las sentencias se tratan según sean comparadores aritméticos o paradigmas; los comparadores aritméticos podemos utilizarlos con comillas o sin ellas, los comparadores válidos son '==' / '=' / '-eq', '!=', '>' / '-gt', '<' / '-lt'; los paradigmas solo pueden utilizarse entrecomillados, los válidos son 'contain' / 'contains', 'not' / 'not contain' / 'not contains' ."

  local SetUp="SetUp"
  addToHelpDictionary ${SetUp} "Puede personalizar la configuración."
  addToExtendedHelpDictionary ${SetUp} "Ruta: '${shell}/${configurationPath}/userConfiguration.${shellExtension}'."
  addToOptionsHelpDictionary ${SetUp} "Opciones: "
  addToExampleHelpDictionary ${SetUp} "Ejemplo:
  nano ${shell}/${configurationPath}/userConfiguration.${shellExtension}"

  local SetUpConsoleStyles="SetUpConsoleStyles"
  addToHelpDictionary ${SetUpConsoleStyles} "Las opciones para la variable 'typessetingConsoleStyle' están en la carpeta '${shell}/${consoleStylePath}'."
  addToExtendedHelpDictionary ${SetUpConsoleStyles} "Opciones: puede utilizar 'bashColor', 'bashColorless' y 'bashUnformatted'."
  addToOptionsHelpDictionary ${SetUpConsoleStyles} "Opciones: "
  addToExampleHelpDictionary ${SetUpConsoleStyles} "Ejemplo: "

  local SetUpWebStyles="SetUpWebStyles"
  addToHelpDictionary ${SetUpWebStyles} "Las opciones para la variable 'typessetingWebStyle' están en la carpeta '${shell}/${webStylePath}'."
  addToExtendedHelpDictionary ${SetUpWebStyles} "Opciones: 'html', css interno por defecto (en blanco) o un link."
  addToOptionsHelpDictionary ${SetUpWebStyles} "Opciones: "
  addToExampleHelpDictionary ${SetUpWebStyles} "Ejemplo: "

  local SetUpFolders="SetUpFolders"
  addToHelpDictionary ${SetUpFolders} "Las opciones para configurar las carpetas están en la ruta '${shell}/${configurationPath}/userConfiguration.${shellExtension}'."
  addToExtendedHelpDictionary ${SetUpFolders} "Puede personalizar la carpeta de usuario: solo dentro de dicha carpeta podrá trabajar el proyecto.
  Puede personalizar la carpeta protegida: puede ser cualquier carpeta incluida en la carpeta de usuario, no se permitirá trabajar sobre dicha carpeta, de esta forma podemos bloquear carpetas de contraseñas.
  Puede personalizar la carpeta temporal: puede estar en cualquier localización que el sistema permita."
  addToOptionsHelpDictionary ${SetUpFolders} "Opciones: "
  addToExampleHelpDictionary ${SetUpFolders} "Ejemplo: "

  local SetUpViewOptions="SetUpViewOptions"
  addToHelpDictionary ${SetUpViewOptions} "Utiliza 'true' o 'false' para ver/ocultar los mensajes de 'Title', 'Abstract', 'Result', 'Explanation', 'Task Summary', 'Expected SUCCESS' (code 0), 'Expected ERROR' (code > 0). "
  addToExtendedHelpDictionary ${SetUpViewOptions} "Ruta: Están en la carpeta '${shell}/${configurationPath}/userConfiguration.${shellExtension}'. "
  addToOptionsHelpDictionary ${SetUpViewOptions} "Opciones: "
  addToExampleHelpDictionary ${SetUpViewOptions} "Ejemplo: "

  local CreateHelpDictionary="CreateHelpDictionary"
  addToHelpDictionary ${CreateHelpDictionary} "La función 'createHelpDictionaryForGiveMeFunction' contiene las traducciones de la ayuda. "
  addToExtendedHelpDictionary ${CreateHelpDictionary} "Para añadir una nueva palabra de ayuda solo es necesario añadirla al diccionario con el contenido deseado, así como también al resumen de la palabra 'Help'."
  addToOptionsHelpDictionary ${CreateHelpDictionary} "Opciones: "
  addToExampleHelpDictionary ${CreateHelpDictionary} "Ejemplo: "

  local CreateErrorDictionary="CreateErrorDictionary"
  addToHelpDictionary ${CreateErrorDictionary} "La función 'createDictionaryOfErrors' contiene las traducciones de los errores de la aplicación. "
  addToExtendedHelpDictionary ${CreateErrorDictionary} "Para añadir una nueva descripción de error deberá añadirla al diccionario, así como utilizarla en la función que desee que emita dicho error."
  addToOptionsHelpDictionary ${CreateErrorDictionary} "Opciones: "
  addToExampleHelpDictionary ${CreateErrorDictionary} "Ejemplo: "

  local HtmlQuotation="HtmlQuotation"
  addToHelpDictionary ${HtmlQuotation} "La función 'createHtmlQuotationForLanguage' contiene la configuración de las comillas para el idioma. "
  addToExtendedHelpDictionary ${HtmlQuotation} "La codificación establecida de las comillas es utilizada tanto para las vistas en pantalla como para la impresión."
  addToOptionsHelpDictionary ${HtmlQuotation} "Opciones: "
  addToExampleHelpDictionary ${HtmlQuotation} "Ejemplo: "

  local MessagesDictionary="MessagesDictionary"
  addToHelpDictionary ${MessagesDictionary} "Las funciones 'createBasicMessages', 'createInfoSystemMessages', 'createMessagesForTests', 'createMessagesForCleanSystem', 'createMessagesForTagsDescriptions' y 'createMessagesForUserActions' contienen las traducciones de los mensajes. "
  addToExtendedHelpDictionary ${MessagesDictionary} "Para una ayuda sobre cómo están construidos los arrays utilice 'giveMe MagicArrayCreation'."
  addToOptionsHelpDictionary ${MessagesDictionary} "Opciones: "
  addToExampleHelpDictionary ${MessagesDictionary} "Ejemplo: "

  local MagicArrayCreation="MagicArrayCreation"
  addToHelpDictionary ${MagicArrayCreation} "La creación y uso de arrays se ha normalizado para mantener una sistema homogéneo de mensajes. La creación se realiza mediante la función 'createTheArray' indicándole su nombre y una de las propiedades que contiene."
  addToExtendedHelpDictionary ${MagicArrayCreation} "Ejemplo: '--withSuccessBeginMessage' para indicar cuál es el sujeto de la frase). El array se crea tanto para dar un resultado positivo (Pass) como uno negativo (Error). No es necesario rellenar todas las propiedades. "
  addToOptionsHelpDictionary ${MagicArrayCreation} "Opciones: "
  addToExampleHelpDictionary ${MagicArrayCreation} "Ejemplo: "

  local stateOfAffairs="stateOfAffairs"
  addToHelpDictionary ${stateOfAffairs} "Comprueba si una tarea ha dado el resultado satisfactorio, en caso contrario el Runbook no continúa."
  addToExtendedHelpDictionary ${stateOfAffairs} "Puede escribir la configuración en un archivo Toml."
  addToOptionsHelpDictionary ${stateOfAffairs} "Opciones:
  --alias = especifiquelo entre corchetes.
  --title = añada un título descriptivo.
  --completed = indique si la tarea está completa con de 'true' o 'false'.
  --explanation = puede indicar un texto largo que sea completamente descriptivo, utilice comillas.
  --taskIsActive = indicar 'true' para indicar si la tarea está activa. Si indica 'false' esta tarea dejará de procesarse (no se verá en el runbook ni tampoco le afectará).
  --stopIfItFails
  --dependsOn = ***.
  --typeOfDependency
  --ghostJob
  --parent = ***.
  --momentum =.
  --toml = archivo en formato toml. Puede crear un archivo toml para escribir todas las tareas."
  addToExampleHelpDictionary ${stateOfAffairs} "Ejemplo:
  stateOfAffairs --alias Name --title Title --completed true --explanation SomeText --taskIsActive false --dependsOn *** --parent ***
  stateOfAffairs --toml file.toml"

  local launchTests="launchTests"
  addToHelpDictionary ${launchTests} "Lanza los test de construcción de CodedAsHuman."
  addToExtendedHelpDictionary ${launchTests} "Pueden lanzarse también desde la consola. Los tests de construcción se lanzan todos aunque rompan el runbook."
  addToOptionsHelpDictionary ${launchTests} "Opciones:
  --onlyTheGroupTests = nombre de la función que agrupa los tests.
  --exceptTheGroupTests = nombre de la función que agrupa los tests."
  addToExampleHelpDictionary ${launchTests} "Ejemplo:
  launchTests --onlyTheGroupTests functionName
  launchTests --exceptTheGroupTests functionName"

  local header="header"
  addToHelpDictionary ${header} "Indicamos el título del apartado."
  addToExtendedHelpDictionary ${header} "Podemos utilizar cuatro encabezados diferentes: 'describe', 'context', 'section' y 'subsection'."
  addToOptionsHelpDictionary ${header} "Opciones:
  --describe = es el título de mayor importancia.
  --context = es el segundo título en importancia.
  --section = es el tercer título en importancia.
  --subsection = es el cuarto título en importancia.
  --explanation
  --parent 
  --alias
  --bulkData
  --labels
  --momentum "
  addToExampleHelpDictionary ${header} "Ejemplo:
  header --describe title --explanation --parent --alias --labels --momentum "

  local pauseRunbook="pauseRunbook"
  addToHelpDictionary ${pauseRunbook} "Desactiva el control del runbook."
  addToExtendedHelpDictionary ${pauseRunbook} "Con el runbook desactivado todas las acciones continúan lanzándose aún habiendo errores."
  addToOptionsHelpDictionary ${pauseRunbook} "Opciones: no tiene."
  addToExampleHelpDictionary ${pauseRunbook} "Ejemplo:
  pauseRunbook"

  local unpauseRunbook="unpauseRunbook"
  addToHelpDictionary ${unpauseRunbook} "Activa el control del runbook."
  addToExtendedHelpDictionary ${unpauseRunbook} "Con el runbook activado las acciones no se lanzan cuando hay un error."
  addToOptionsHelpDictionary ${unpauseRunbook} "Opciones: no tiene."
  addToExampleHelpDictionary ${unpauseRunbook} "Ejemplo:
  unpauseRunbook"

  local List="List"
  addToHelpDictionary ${List} "Vocabulario aceptado (sensible a mayúsculas):
--Ayuda básica--
  [Help:]
   Help (una pequeña orientación)
   List (muestra este listado)
  [Configuración de usuario:]
   SetUp (muestra la ruta del archivo de configuración)
   SetUpConsoleStyles (muestra la ruta de los estilos de pantalla)
   SetUpWebStyles (muestra la ruta de los estilos para impresión)
   SetUpFolders (configura la ruta de las carpetas de usuario, protegida y temporal)
   SetUpViewOptions (configura la visibilidad de los mensajes)
  [Interface 'Languages' para el usuario:]
   selectLanguage (selecciona el idioma que desea para ver los mensajes)
  [Interface 'Runbooks' para el usuario:]
   header (indicamos el título)
   pauseRunbook (desactivamos el runbook)
   unpauseRunbook (activamos el runbook)
   launchSpecificationsSummary (le muestra el resultado de los tests)
  [Interface 'Program features' para el usuario:]
   setUpDistro (selecciona la distribución que utiliza)
   refreshRepositories (Actualiza los índices de la cache de los repositorios)
   updateRepositoryPackages (Actualiza los paquetes disponibles)
   ensureTool (comprueba que la utilidad deseada está instalada, en caso contrario la instala)
   uninstallTool (desinstala una utilidad)
   ensureToolRepository (comprueba que el repositorio deseado está añadido, en caso contrario lo añade)
   eraseToolRepository (comprueba que el repositorio deseado está añadido y lo borra)
  [Interface 'Management panels' para el usuario:]
   ensureManagementPanel (crea una carpeta si no existe)
   eraseManagementPanel (borra una carpeta)
   copyManagementPanel (copia una carpeta)
   moveManagementPanel (mueve una carpeta)
   ensureBriefingTask (crea un archivo)
   eraseBriefingTask (borra un archivo)
   ensureRecipeIngredient (añade una línea a un archivo, si el texto no existe en el archivo)
   "
   addToExtendedHelpDictionary ${List} "--Ayuda para usuarios avanzados--
  [Idiomas (sitos en la ruta: '${typeOfLanguagePath}):]
   HelpDictionary (traducciones de la ayuda)
   ErrorDictionary (traducciones de los errores de la aplicación)
   HtmlQuotation (configuración de las comillas para el idioma)
   MessagesDictionary (traducciones de los mensajes)
   MagicArrayCreation (explicación de cómo se construyen los arrays del diccionario de mensajes)

--Uso de tests--
  [Opciones disponibles:]
   options (muestra las opciones disponibles para aquellas funciones que las necesiten)
  [Herramienta de tests:]
   launchTests (lanza los test de construcción de CodedAsHuman)
   weHope (herramienta para testear)
   stateOfAffairs (comprueba una especificación)"
   addToOptionsHelpDictionary ${List} " "
   addToExampleHelpDictionary ${List} " "
}
createHelpDictionaryForGiveMeFunction


createDictionaryOfActuality() {
  addToSystemDictionaryMessages Malformed "La petición no está correctamente construída"
  addToSystemDictionaryMessages NotDefined "No ha suministrado el dato necesario"
  addToSystemDictionaryMessages FolderExist "La carpeta ya existe"
  addToSystemDictionaryMessages FolderNotExist "La carpeta no existe"
  addToSystemDictionaryMessages FileExist "El archivo ya existe"
  addToSystemDictionaryMessages FileNotExist "El archivo no existe"
  addToSystemDictionaryMessages PathIsValid "La carpeta cumple los requisitos"
  addToSystemDictionaryMessages Repository "Repositorio"
}
createDictionaryOfActuality


createDictionaryOfErrors() {
  addToSystemDictionaryMessages ErrorInTest "El test ha fallado"
  addToSystemDictionaryMessages ErrorInAddRecipeIngredientToFile "Error en AddRecipeIngredientToFile"
  addToSystemDictionaryMessages ErrorInApplyLanguage "Error en ApplyLanguage"
  addToSystemDictionaryMessages ErrorInApplyToolInstallation "Error en ApplyToolInstallation"
  addToSystemDictionaryMessages ErrorInApplyToolRemove "Error en ApplyToolRemove"
  addToSystemDictionaryMessages ErrorInCheckIfPathIsValid "Error en CheckIfPathIsValid"
  addToSystemDictionaryMessages ErrorInCloseTypoCaption "Error on CloseTypoCaption"
  addToSystemDictionaryMessages ErrorInCompareFolder "Error en CompareFolder"
  addToSystemDictionaryMessages ErrorInCopyPanel "Error en CopyPanel"
  addToSystemDictionaryMessages ErrorInconstructTheTask "Error en StateOfAffairs"
  addToSystemDictionaryMessages ErrorInsurveyTheTask "Error en StateOfAffairs"
  addToSystemDictionaryMessages ErrorInCreatePanel "Error en CreatePanel"
  addToSystemDictionaryMessages ErrorInDistribution "Error en Distribution"
  addToSystemDictionaryMessages ErrorInRefreshRepository "Error en RefreshRepository"
  addToSystemDictionaryMessages ErrorInUpdateAllPackages "Error en UpdateAllPackages"
  addToSystemDictionaryMessages ErrorInEnsureRepository "Error en EnsureRepository"
  addToSystemDictionaryMessages ErrorInEraseRepository "Error en EraseRepository"
  addToSystemDictionaryMessages ErrorInEraseFile "Error en EraseFile"
  addToSystemDictionaryMessages ErrorInErasePanel "Error en ErasePanel"
  addToSystemDictionaryMessages ErrorInInitTypoCaption "Error en InitTypoCaption"
  addToSystemDictionaryMessages ErrorInInitializeViewDocument "Error en InitializeViewDocument"
  addToSystemDictionaryMessages ErrorInInterfaceFunctionWithoutMessage "Error recogiendo el contenido de la variable dinámica, funciones: 'createDictionaryOfErrors', 'setUpVisibility', 'createMessagesForUserActions', el 'Test ERROR message' y las configuraciones."
  addToSystemDictionaryMessages ErrorInIsTheToolInstalled "Error en IsTheToolInstalled"
  addToSystemDictionaryMessages ErrorInMovePanel "Error en MovePanel"
  addToSystemDictionaryMessages ErrorInSoftDeleteBriefingTask "Error en SoftDeleteBriefingTask"
  addToSystemDictionaryMessages ErrorInTitleOfProject "Falta el título en 'ErrorInTitleOfProject'"
  addToSystemDictionaryMessages ErrorInUsersInvolved "Error en UsersInvolved"
  addToSystemDictionaryMessages ErrorInValidationProtectedFolder "Error en ValidationProtectedFolder"
  addToSystemDictionaryMessages ErrorInValidationTempFolder "Error en ValidationTempFolder"
  addToSystemDictionaryMessages ErrorInValidationUserFolder "Error en ValidationUserFolder"
}
createDictionaryOfErrors


createHtmlQuotationForLanguage() {
  initializeGlobalVariable "htmlLanguage" "es"
  initializeGlobalVariable "openMajorQuote" "«"
  initializeGlobalVariable "closeMajorQuote" "»"
  initializeGlobalVariable "openMinorQuote" "“"
  initializeGlobalVariable "closeMinorQuote" "”"
  initializeGlobalVariable "doubleApostrofeQuote" "\""
  initializeGlobalVariable "simpleApostrofeQuote" "'"
}
createHtmlQuotationForLanguage


createBasicMessages() {
  initializeGlobalVariable "CAHisLoaded"
    CAHisLoaded="CAH ya se esta ejecutando en el pid ${BASHPID}: omitiendo la nueva carga."

  initializeGlobalVariable "titleProjectMessageCaption"
    titleProjectMessageCaption="Título del proyecto"

  initializeGlobalVariable "nameMessageCaption"
    nameMessageCaption="Nombre"

  initializeGlobalVariable "userNameMessageCaption"
    userNameMessageCaption="Nombre del usuario"

  initializeGlobalVariable "userGroupsMessage"
    userGroupsMessage="El usuario está en los siguientes grupos"

  initializeGlobalVariable "validationHasFailed"
    validationHasFailed="el resultado devuelto por el sistema es incongruente!?"

  initializeGlobalVariable "messageNotActionBecauseRunbookIsBroken"
    messageNotActionBecauseRunbookIsBroken="No se ejecuta la acción debido a que el runbook está roto."

  initializeGlobalVariable "messageRunbookIsBrokenIs"
    messageRunbookIsBrokenIs="El runbook se ha roto."

  initializeGlobalVariable "messageRunbookIsOver"
    messageRunbookIsOver="El runbook ha finalizado (en verde)."

  initializeGlobalVariable "whatIsTheResult"
    whatIsTheResult="¿El resultado ha sido verdadero o falso (no hay una entrada predeterminada)?"

  initializeGlobalVariable "genericPassMessage"
    genericPassMessage="¡Todo está bien!"

  initializeGlobalVariable "unexplainedError"
    unexplainedError="¡Error sin explicación!"

  createTheArray "unexplained" \
    --withSuccessBeginMessage "${genericPassMessage[@]}" \
    --withErrorBeginMessage "${unexplainedError[@]}"

  initializeGlobalVariable "paradigmContain"
    paradigmContain="contiene"

  initializeGlobalVariable "paradigmContains"
    paradigmContains="contienen"

  initializeGlobalVariable "paradigmNotContain"
    paradigmNotContain="no contiene"

  initializeGlobalVariable "paradigmNotContains"
    paradigmNotContains="no contienen"

  initializeGlobalVariable "gemHomeUnstablished"
    paradigmNotContains="No se ha establecido la variable GEM_HOME, utilice la receta para instalar Ruby: 'cook --recipe ruby' "
}
createBasicMessages


createInfoSystemMessages() {
  initializeGlobalVariable "userFolderMessage"
    userFolderMessage="Carpeta de usuario"

  initializeGlobalVariable "tempFolderMessage"
    tempFolderMessage="Carpeta temporal"

  initializeGlobalVariable "protectedFolderMessage"
    protectedFolderMessage="Carpeta protegida"

  createTheArray "infoDate" \
    --withSuccessBeginMessage "Fecha UTC:" \
    --withErrorBeginMessage "Fecha UTC:"

  createTheArray "infoUser" \
    --withSuccessBeginMessage "Usuario:" \
    --withErrorBeginMessage "Usuario:"

  createTheArray "infoPath" \
    --withSuccessBeginMessage "Ruta:" \
    --withErrorBeginMessage "Ruta:"
}
createInfoSystemMessages


createMessagesForTests() {
  createTheArray "theCheckIs" \
    --withSuccessBeginMessage "Primer operando:" \
    --withSuccessComparationMessage "Segundo operando:" \
    --withSuccessOperatorMessage "Sentencia:" \
    --withSuccessEndMessage "se ha validado satisfactoriamente" \
    --withErrorBeginMessage "Primer operando:" \
    --withErrorComparationMessage "Segundo operando:" \
    --withErrorOperatorMessage "Sentencia:" \
    --withErrorEndMessage "es falso"

  createTheArray "theCheckIsNot" \
    --withSuccessBeginMessage "Primer operando:" \
    --withSuccessComparationMessage "Segundo operando:" \
    --withSuccessOperatorMessage "Sentencia:" \
    --withSuccessEndMessage "NO se ha validado satisfactoriamente" \
    --withErrorBeginMessage "Primer operando:" \
    --withErrorComparationMessage "Segundo operando:" \
    --withErrorOperatorMessage "Sentencia:" \
    --withErrorEndMessage "ha sido satisfactorio"

  createTheArray "lenghtOfString" \
    --withSuccessBeginMessage "La longitud del elemento es" \
    --withSuccessComparationMessage "y comparada con" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "ha dado un resultado positivo" \
    --withErrorBeginMessage "La longitud del elemento es" \
    --withErrorComparationMessage "y comparada con" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "ha dado un resultado fallido"
}
createMessagesForTests


createMessagesForTagsDescriptions() {
  createTheArray "titleDescribe" \
    --withSuccessBeginMessage "Descripción:" \
    --withErrorBeginMessage "Descripción:"

  createTheArray "titleContext" \
    --withSuccessBeginMessage "Contexto:" \
    --withErrorBeginMessage "Contexto:"

  createTheArray "titleSection" \
    --withSuccessBeginMessage "Sección:" \
    --withErrorBeginMessage "Sección:"

  createTheArray "titleSubsection" \
    --withSuccessBeginMessage "Subsección:" \
    --withErrorBeginMessage "Subsección:"

  createTheArray "tagTitleComments" \
    --withSuccessBeginMessage "Comentarios:" \
    --withErrorBeginMessage "Comentarios:"

  createTheArray "tagAbstract" \
    --withSuccessBeginMessage "Resumen:" \
    --withErrorBeginMessage "Resumen:"

  createTheArray "tagStatus" \
    --withSuccessBeginMessage "Estado:" \
    --withErrorBeginMessage "Estado:"

  createTheArray "tagExplanation" \
    --withSuccessBeginMessage "Explicación:" \
    --withErrorBeginMessage "Explicación:"

  createTheArray "summaryTitle" \
    --withSuccessBeginMessage "Resumen:" \
    --withSuccessEndMessage "están bien" \
    --withErrorBeginMessage "Resumen:" \
    --withErrorEndMessage "tienen error"
}
createMessagesForTagsDescriptions


createMessagesForUserActions() {
  createTheArray "constructTheTask" \
    --withSuccessBeginMessage "El estado del runbook" \
    --withSuccessComparationMessage "y comparado con" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "se ha establecido correctamente" \
    --withErrorBeginMessage "El estado del runbook" \
    --withErrorComparationMessage "y comparado con" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha establecido correctamente"

  createTheArray "surveyTheTask" \
    --withSuccessBeginMessage "El estado del runbook" \
    --withSuccessComparationMessage "y comparado con" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "se ha establecido correctamente" \
    --withErrorBeginMessage "El estado del runbook" \
    --withErrorComparationMessage "y comparado con" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha establecido correctamente"

  createTheArray "usersInvolved" \
    --withSuccessBeginMessage "El usuario/grupo seleccionado es válido" \
    --withSuccessComparationMessage "y comparado con" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "se ha establecido correctamente" \
    --withErrorBeginMessage "El usuario/grupo NO consta como aceptado" \
    --withErrorComparationMessage "y comparado con" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha establecido correctamente"

  createTheArray "applyLanguage" \
    --withSuccessBeginMessage "La selección del idioma" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que se ha aplicado" \
    --withErrorBeginMessage "La selección del idioma" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha aplicado"

  createTheArray "theLanguage" \
    --withSuccessBeginMessage "El idioma aplicado es" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que se ha aplicado" \
    --withErrorBeginMessage "No se ha podido aplicar el idioma" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha aplicado"

  createTheArray "useDistro" \
    --withSuccessBeginMessage "La selección de la distribución" \
    --withSuccessComparationMessage "y comparada con" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que se ha aplicado" \
    --withErrorBeginMessage "La selección de la distribución" \
    --withErrorComparationMessage "y comparada con" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha aplicado"

  createTheArray "distroNotExist" \
    --withSuccessBeginMessage "La distribución NO existe" \
    --withSuccessComparationMessage "y comparada con" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "advirtiendo que NO existe" \
    --withErrorBeginMessage "La distribución ya existe" \
    --withErrorComparationMessage "y comparada con" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "confirmando que existe"

  createTheArray "loadInterface" \
    --withSuccessBeginMessage "La carga de la interfaz" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que se ha aplicado" \
    --withErrorBeginMessage "La carga de la interfaz" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha aplicado"

  createTheArray "applyToolInstallation" \
    --withSuccessBeginMessage "La instalación de la herramienta" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que está instalado" \
    --withErrorBeginMessage "La instalación de la herramienta" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO está instalado"

  createTheArray "cooking" \
    --withSuccessBeginMessage "La ejecución de la receta" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que se ha realizado" \
    --withErrorBeginMessage "La ejecución de la receta" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha realizado"

  createTheArray "refreshRepository" \
    --withSuccessBeginMessage "Refrescar el repositorio" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que la caché está actualizada" \
    --withErrorBeginMessage "Refrescar el repositorio" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo la caché NO está actualizada"

  createTheArray "updateAllPackages" \
    --withSuccessBeginMessage "Actualizar el repositorio" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que está actualizada" \
    --withErrorBeginMessage "Actualizar el repositorio" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo NO está actualizada"

  createTheArray "ensureRepository" \
    --withSuccessBeginMessage "Añadir el repositorio" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que está instalado" \
    --withErrorBeginMessage "Añadir el repositorio" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO está instalado"

  createTheArray "eraseRepository" \
    --withSuccessBeginMessage "Quitar el repositorio" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que está instalado" \
    --withErrorBeginMessage "Quitar el repositorio" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO está instalado"

  createTheArray "toolExist" \
    --withSuccessBeginMessage "La herramienta ya existe" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que está instalado" \
    --withErrorBeginMessage "La herramienta NO existe" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO está instalado"

  createTheArray "applyToolRemove" \
    --withSuccessBeginMessage "La desinstalación de la herramienta" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "advirtiendo que NO está desinstalado" \
    --withErrorBeginMessage "La desinstalación de la herramienta" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "confirmando que está desinstalado"

  createTheArray "gemNotExist" \
    --withSuccessBeginMessage "La herramienta NO existe" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "advirtiendo que NO está instalado" \
    --withErrorBeginMessage "La herramienta ya existe" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "confirmando que está instalado"

  createTheArray "ensureThePanel" \
    --withSuccessBeginMessage "La creación de la carpeta de trabajo" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que existe" \
    --withErrorBeginMessage "La creación de la carpeta de trabajo" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO existe"

  createTheArray "panelExist" \
    --withSuccessBeginMessage "La carpeta de trabajo existe" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que existe" \
    --withErrorBeginMessage "La carpeta de trabajo NO existe" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO existe"

  createTheArray "panelNotExist" \
    --withSuccessBeginMessage "La carpeta de trabajo NO existe" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "advirtiendo que NO existe" \
    --withErrorBeginMessage "Contenido de la carpeta de trabajo" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "confirmando que existe"

  createTheArray "copyTheManagementPanel" \
    --withSuccessBeginMessage "El copiado de la carpeta de trabajo" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que se ha copiado" \
    --withErrorBeginMessage "El copiado de la carpeta de trabajo" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha copiado"

  createTheArray "moveThePanel" \
    --withSuccessBeginMessage "El traslado de la carpeta de trabajo" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que se ha movido" \
    --withErrorBeginMessage "El traslado de la carpeta de trabajo" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha movido"

  createTheArray "eraseTheManagementPanel" \
    --withSuccessBeginMessage "El borrado de la carpeta de trabajo" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que se ha eliminado" \
    --withErrorBeginMessage "El borrado de la carpeta de trabajo" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha eliminado"

  createTheArray "ensureTheBriefingTask" \
    --withSuccessBeginMessage "La creación de la hoja de ruta" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que existe" \
    --withErrorBeginMessage "La creación de la hoja de ruta" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO existe"

  createTheArray "briefingTaskExist" \
    --withSuccessBeginMessage "La hoja de ruta existe" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que existe" \
    --withErrorBeginMessage "La hoja de ruta NO existe" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO existe"

  createTheArray "briefingTaskNotExist" \
    --withSuccessBeginMessage "La hoja de ruta NO existe" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "advirtiendo que NO existe" \
    --withErrorBeginMessage "La hoja de ruta existe" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "confirmando que existe"

  createTheArray "eraseTheBriefingTask" \
    --withSuccessBeginMessage "El borrado de la hoja de ruta" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que se ha borrado" \
    --withErrorBeginMessage "El borrado de la hoja de ruta" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha borrado"

  createTheArray "ensureTheRecipeIngredient" \
    --withSuccessBeginMessage "El texto" \
    --withSuccessComparationMessage "ha devuelto el código" \
    --withSuccessOperatorMessage "siendo el operador" \
    --withSuccessEndMessage "confirmando que se ha añadido al archivo indicado" \
    --withErrorBeginMessage "El texto" \
    --withErrorComparationMessage "ha devuelto el código" \
    --withErrorOperatorMessage "siendo el operador" \
    --withErrorEndMessage "advirtiendo que NO se ha añadido al archivo indicado"
}
createMessagesForUserActions

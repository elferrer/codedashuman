#!${BASH}
english() {
  echo -e "Loading... Language 'informal english', version 0.1"
}


initializeGlobalVariable "activeLanguage" "english"

createHelpDictionaryForGiveMeFunction() {
  acceptedWords=("Accepted words:")

  local Help="Help"
  addToHelpDictionary ${Help} "To a basic info use \"giveMe Help\". To a extendend info use \"giveMe Help 'extended'\".
Example:
  giveMe List
  giveMe List 'extended'
  giveMe List 'example'
  giveMe List 'options'
  giveMe List 'see all'
  "
  addToExtendedHelpDictionary ${Help} "To launch CAH you can do it from the console from the parent directory with (BASH version):
source 'layCAH.sh'

You can also create a file with the following content (recommended):
#!\$BASH
source 'layCAH.sh'

Basic help can be obtained through the command:
giveMe

Using it through a script gives you the opportunity to save it as a Runbook, although you have the option to launch the commands directly from the console (not advised).

The normal flow of a CAH script is:

1. Review the user settings to adapt the routes to your needs.
2. Create the name of the project.
3. Select language.
4. Select distribution.
5. Create actions.
6. Finish with the summary."

  addToOptionsHelpDictionary ${Help} " "
  addToExampleHelpDictionary ${Help} " "

  local selectLanguage="selectLanguage"
  addToHelpDictionary ${selectLanguage} "Allows you to select the language you want to display the messages. The available languages are in the folder '${typeOfLanguagePath}'. "
  addToExtendedHelpDictionary ${selectLanguage} "Information can be omitted in the on-screen display."
  addToOptionsHelpDictionary ${selectLanguage} "Options:
  --language = desired language.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias =.
  --momentum=.
  --bulkData
  --labels"
  addToExampleHelpDictionary ${selectLanguage} "Example:
  selectLanguage --language english --initialInfo user --endInfo date"

  local launchSpecificationsSummary="launchSpecificationsSummary"
  addToHelpDictionary ${launchSpecificationsSummary} "It shows you the test result. "
  addToExtendedHelpDictionary ${launchSpecificationsSummary} "You can specify a file name to save the entire log. If you do not write a file name it will be saved as a ${defaultSummaryFile[@]}."
  addToOptionsHelpDictionary ${launchSpecificationsSummary} "Options:
  --save = File name for the log. It will be saved with the extension by adding the file type (for example 'summary.log.html').
  --initialInfo = desired options.
  --endInfo = desired options."
  addToExampleHelpDictionary ${launchSpecificationsSummary} "Example:
  launchSpecificationsSummary --save nameOfFile --initialInfo user --endInfo date"

  local ensureTool="ensureTool"
  addToHelpDictionary ${ensureTool} "Install a tool"
  addToExtendedHelpDictionary ${ensureTool} "Check that the desired tool is installed, otherwise install it."
  addToOptionsHelpDictionary ${ensureTool} "Options:
  --program = name of the program, the program is whitin the repositories.
  --package = name of the program, the program is a local file.
  --gem = name of gem.
  --asName = name of the package that includes the program (in case the program is part of another package).
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias =.
  --momentum=.
  --bulkData
  --labels"
  addToExampleHelpDictionary ${ensureTool} "Example:
  ensureTool --gem theGem --initialInfo user --endInfo date
  ensureTool --program ifconfig --asName net-tools"

  local uninstallTool="uninstallTool"
  addToHelpDictionary ${uninstallTool} "Uninstall a tool."
  addToExtendedHelpDictionary ${uninstallTool} "Check that the desired tool isn't installed, otherwise remove it."
  addToOptionsHelpDictionary ${uninstallTool} "Options:
  --program = name of the program, the program is whitin the repositories.
  --gem = name of gem.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${uninstallTool} "Example:
  uninstallTool --gem theGem --initialInfo user --endInfo date"

  local ensureToolRepository="ensureToolRepository"
  addToHelpDictionary ${ensureToolRepository} "Add a repository."
  addToExtendedHelpDictionary ${ensureToolRepository} "Check that the desired repository is added, otherwise add it."
  addToOptionsHelpDictionary ${ensureToolRepository} "Options:
  --repositoryName = The desired repository. We omit 'ppa:' if it is a ppa repository and we omit 'deb' if it is a deb repository.
  --repositoryType = The repository type can be 'ppa' or 'deb'.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${ensureToolRepository} "Example:
  ensureToolRepository --repositoryName inkscape.dev/trunk --repositoryType ppa --initialInfo user --endInfo date"

  local eraseToolRepository="eraseToolRepository"
  addToHelpDictionary ${eraseToolRepository} "Remove a repository."
  addToExtendedHelpDictionary ${eraseToolRepository} "Check that the desired repository is added and delete it."
  addToOptionsHelpDictionary ${eraseToolRepository} "Options:
  --repositoryName = The desired repository. We omit 'ppa:' if it is a ppa repository and we omit 'deb' if it is a deb repository.
  --repositoryType = The repository type can be 'ppa' or 'deb'.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${eraseToolRepository} "Example:
  eraseToolRepository --repositoryName inkscape.dev/trunk --repositoryType ppa --initialInfo user --endInfo date"

  local setUpDistro="setUpDistro"
  addToHelpDictionary ${setUpDistro} "Select the distribution you use."
  addToExtendedHelpDictionary ${setUpDistro} "If the indicated distribution does not exist, it will search if it is possible to use a compatible version. This command is idempotent and will only return an error when there are no viable options."
  addToOptionsHelpDictionary ${setUpDistro} "Options:
  --name = name of distribution.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${setUpDistro} "Example:
  setUpDistro --name theDistro --initialInfo user --endInfo date"

  local refreshRepositories="refreshRepositories"
  addToHelpDictionary ${refreshRepositories} "Update repository cache indexes."
  addToExtendedHelpDictionary ${refreshRepositories} "Update repository cache indexes. This command is idempotent and will only return an error when there are no viable options."
  addToOptionsHelpDictionary ${refreshRepositories} "Options:
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${refreshRepositories} "Ejemplo:
  refreshRepositories --initialInfo 'user;date' --endInfo 'date;path'"

  local updateRepositoryPackages="updateRepositoryPackages"
  addToHelpDictionary ${updateRepositoryPackages} "Upgrade available packages."
  addToExtendedHelpDictionary ${updateRepositoryPackages} "Upgrade available packages. This command is idempotent and will only return an error when there are no viable options."
  addToOptionsHelpDictionary ${updateRepositoryPackages} "Options:
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${updateRepositoryPackages} "Example:
  updateRepositoryPackages --initialInfo 'user;date' --endInfo 'date;path'"

  local ensureManagementPanel="ensureManagementPanel"
  addToHelpDictionary ${ensureManagementPanel} "Create a folder if it does not exist. "
  addToExtendedHelpDictionary ${ensureManagementPanel} "Use quotes if necessary."
  addToOptionsHelpDictionary ${ensureManagementPanel} "Options:
  --name = name of folder.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${ensureManagementPanel} "Example:
  ensureManagementPanel --name folderName --initialInfo user --endInfo date"

  local eraseManagementPanel="eraseManagementPanel"
  addToHelpDictionary ${eraseManagementPanel} "Delete a folder. "
  addToExtendedHelpDictionary ${eraseManagementPanel} "Use quotes if necessary."
  addToOptionsHelpDictionary ${eraseManagementPanel} "Options:
  --name = name of folder.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${eraseManagementPanel} "Example:
  eraseManagementPanel --name folderName --initialInfo user --endInfo date"

  local copyManagementPanel="copyManagementPanel"
  addToHelpDictionary ${copyManagementPanel} "Copy a folder. "
  addToExtendedHelpDictionary ${copyManagementPanel} "Use quotes if necessary."
  addToOptionsHelpDictionary ${copyManagementPanel} "Options:
  --origin = name of folder.
  --destination = name of folder.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${copyManagementPanel} "Example:
  copyManagementPanel --origin folderName --destination folderName --initialInfo user --endInfo date"

  local moveManagementPanel="moveManagementPanel"
  addToHelpDictionary ${moveManagementPanel} "Move a folder."
  addToExtendedHelpDictionary ${moveManagementPanel} "Use quotes if necessary."
  addToOptionsHelpDictionary ${moveManagementPanel} "Options:
  --origin = name of folder.
  --destination = name of folder.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${moveManagementPanel} "Example:
  moveManagementPanel --origin folderName --destination folderName --initialInfo user --endInfo date"

  local ensureBriefingTask="ensureBriefingTask"
  addToHelpDictionary ${ensureBriefingTask} "Create a file."
  addToExtendedHelpDictionary ${ensureBriefingTask} "Use quotes if necessary."
  addToOptionsHelpDictionary ${ensureBriefingTask} "Options:
  --name = name of file.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${ensureBriefingTask} "Example:
  ensureBriefingTask --name filename --initialInfo user --endInfo date"

  local eraseBriefingTask="eraseBriefingTask"
  addToHelpDictionary ${eraseBriefingTask} "Delete a file. Use 'eraseBriefingTask filename'."
  addToExtendedHelpDictionary ${eraseBriefingTask} "Use quotes if necessary."
  addToOptionsHelpDictionary ${eraseBriefingTask} "Options:
  --name = name of file.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${eraseBriefingTask} "Example:
  eraseBriefingTask --name theFile --initialInfo user --endInfo date"

  local ensureRecipeIngredient="ensureRecipeIngredient"
  addToHelpDictionary ${ensureRecipeIngredient} "Add a line to a file, if the text does not exist in the file. "
  addToExtendedHelpDictionary ${ensureRecipeIngredient} "Use quotes if necessary, you can use spaces for 'theContent'."
  addToOptionsHelpDictionary ${ensureRecipeIngredient} "Options:
  --name = name of file.
  --line = enter the text to include in the file.
  --atTheBeginningOfTheLine = true | false. Indicate whether you want the text to be included at the beginning of the text line.
  --initialInfo = desired options.
  --endInfo = desired options.
  --alias
  --momentum
  --bulkData
  --labels"
  addToExampleHelpDictionary ${ensureRecipeIngredient} "Example:
  ensureRecipeIngredient --name filename --line theContent --atTheBeginningOfTheLine 'true' --initialInfo user --endInfo date"

  local weHope="weHope"
  addToHelpDictionary ${weHope} "Test tool. Use 'giveMe options' to see a description of the accepted options."
  addToExtendedHelpDictionary ${weHope} "Use 'weHope' to find out if a condition is met."
  addToOptionsHelpDictionary ${weHope} "Options:
  --messageInterpretation = name of the function or the message we want the legend to display.
  --firstOperator = the first operand of the condition you want to check.
  --sentence = the sentence or paradigm you want to use.
  --secondOperator = the second operand of the condition you want to check.
  --humanReadableFirstOperator = the message you want to be displayed on the output replacing the first operand.
  --humanReadableSecondOperator = the message you want to be displayed on the output replacing the second operand.
  --momentum=.
  --initialInfo = indicate the desired options.
  --endInfo = indicate the desired options."
  addToExampleHelpDictionary ${weHope} "Example:
  weHope --messageInterpretation theTipusOfMessage --firstOperator theFirstOperator --sentence aSentence --secondOperator theSecondOperator --humanReadableFirstOperator onePhrase --humanReadableSecondOperator onePhrase --momentum --initialInfo user --endInfo date"

  local options="options"
  addToHelpDictionary ${options} "Description of the different values ​​you can use in each option."
  addToExtendedHelpDictionary ${options} "You can see the tests ('${testingPath}') as an example of use."
  addToOptionsHelpDictionary ${options} "Options: "
  addToExampleHelpDictionary ${options} "For options that allow multiple values ​​you should not use spaces; for example, for 'initialInfo' / 'endInfo' use the quotes and separate the values ​​by semicolons: 'user; date'. You can see the available in the file '${shell}/${servicesPath}/serviceInfo.sh'
For values ​​that are phrases you must quote them.
The sentences are treated according to whether they are arithmetic comparators or paradigms; the arithmetic comparators can be used with or without quotes, the valid comparators are '==' / '=' / '-eq', '! =', '>' / '-gt', '<' / '-lt '; the paradigms can only be used in quotes, the valid ones are 'contain' / 'contains', 'not' / 'not contain' / 'not contains' ."

  local SetUp="SetUp"
  addToHelpDictionary ${SetUp} "You can customize the settings."
  addToExtendedHelpDictionary ${SetUp} "Path: '${shell}/${configurationPath}/userConfiguration.${shellExtension}'."
  addToOptionsHelpDictionary ${SetUp} "Options: "
  addToExampleHelpDictionary ${SetUp} "Example:
  nano ${shell}/${configurationPath}/userConfiguration.${shellExtension}"

  local SetUpConsoleStyles="SetUpConsoleStyles"
  addToHelpDictionary ${SetUpConsoleStyles} "The options for variable 'typessetingConsoleStyle' they're in '${shell}/${consoleStylePath}' folder."
  addToExtendedHelpDictionary ${SetUpConsoleStyles} "Options: you can use 'bashColor', 'bashColorless' and 'bashUnformatted'."
  addToOptionsHelpDictionary ${SetUpConsoleStyles} "Options: "
  addToExampleHelpDictionary ${SetUpConsoleStyles} "Example: "

  local SetUpWebStyles="SetUpWebStyles"
  addToHelpDictionary ${SetUpWebStyles} "The options for variable 'typessetingWebStyle' they're in '${shell}/${webStylePath}' folder."
  addToExtendedHelpDictionary ${SetUpWebStyles} "Options: 'html', css empty by default or a link."
  addToOptionsHelpDictionary ${SetUpWebStyles} "Options: "
  addToExampleHelpDictionary ${SetUpWebStyles} "Example: "

  local SetUpFolders="SetUpFolders"
  addToHelpDictionary ${SetUpFolders} "The options to configure the folders are on '${shell}/${configurationPath}/userConfiguration.${shellExtension}' folder."
  addToExtendedHelpDictionary ${SetUpFolders} "You can customize the user box: only within that folder can the project work.
   You can customize the protected folder: it can be any folder included in the user folder, it will not be allowed to work on that folder, this way we can block password folders.
   You can customize the temporary folder: it can be in any location that the system allows."
   addToOptionsHelpDictionary ${SetUpFolders} "Options: "
   addToExampleHelpDictionary ${SetUpFolders} "Example: "

  local SetUpViewOptions="SetUpViewOptions"
  addToHelpDictionary ${SetUpViewOptions} "Use 'true' or 'false' to view/hidden message Title, Abstract, Result, Explanation, Task Summary, Expected SUCCESS (code 0), Expected ERROR (code > 0). "
  addToExtendedHelpDictionary ${SetUpViewOptions} "Path: They're on '${shell}/${configurationPath}/userConfiguration.${shellExtension}' folder."
  addToOptionsHelpDictionary ${SetUpViewOptions} "Options: "
  addToExampleHelpDictionary ${SetUpViewOptions} "Example: "

  local CreateHelpDictionary="CreateHelpDictionary"
  addToHelpDictionary ${CreateHelpDictionary} "The 'createHelpDictionaryForGiveMeFunction' function contains the help translations. "
  addToExtendedHelpDictionary ${CreateHelpDictionary} "To add a new help word you only need to add it to the dictionary with the desired content, as well as to the summary of the word 'Help'."
  addToOptionsHelpDictionary ${CreateHelpDictionary} "Options: "
  addToExampleHelpDictionary ${CreateHelpDictionary} "Example: "

  local CreateErrorDictionary="CreateErrorDictionary"
  addToHelpDictionary ${CreateErrorDictionary} "The 'createDictionaryOfErrors' function contains the translations of the application errors. "
  addToExtendedHelpDictionary ${CreateErrorDictionary} "To add a new error description you must add it to the dictionary, as well as use it in the function you want the error to issue."
  addToOptionsHelpDictionary ${CreateErrorDictionary} "Options: "
  addToExampleHelpDictionary ${CreateErrorDictionary} "Example: "

  local HtmlQuotation="HtmlQuotation"
  addToHelpDictionary ${HtmlQuotation} "The 'createHtmlQuotationForLanguage' function contains the quotation settings for the language. "
  addToExtendedHelpDictionary ${HtmlQuotation} "The set coding of quotes is used for both screen views and printing."
  addToOptionsHelpDictionary ${HtmlQuotation} "Options: "
  addToExampleHelpDictionary ${HtmlQuotation} "Example: "

  local MessagesDictionary="MessagesDictionary"
  addToHelpDictionary ${MessagesDictionary} "The functions 'createBasicMessages', 'createInfoSystemMessages', 'createMessagesForTests', 'createMessagesForCleanSystem', 'createMessagesForTagsDescriptions' and 'createMessagesForUserActions' contain the translations of the messages. "
  addToExtendedHelpDictionary ${MessagesDictionary} "For help on how arrays are built use 'giveMe MagicArrayCreation'."
  addToOptionsHelpDictionary ${MessagesDictionary} "Options: "
  addToExampleHelpDictionary ${MessagesDictionary} "Example: "

  local MagicArrayCreation="MagicArrayCreation"
  addToHelpDictionary ${MagicArrayCreation} "The creation and use of arrays has been standardized to maintain a homogeneous message system. The creation is done through the function 'createTheArray' indicating its name and one of the properties it contains."
  addToExtendedHelpDictionary ${MagicArrayCreation} "Example: '--withSuccessBeginMessage' to indicate the subject of the sentence). The array is created to give both a positive (Pass) and a negative (Error) result. It is not necessary to fill in all the properties. "
  addToOptionsHelpDictionary ${MagicArrayCreation} "Options: "
  addToExampleHelpDictionary ${MagicArrayCreation} "Example: "

  local stateOfAffairs="stateOfAffairs"
  addToHelpDictionary ${stateOfAffairs} "Check if a specification has given the satisfactory result, otherwise the Runbook does not continue."
  addToExtendedHelpDictionary ${stateOfAffairs} "You can write the configuration to a Toml file."
  addToOptionsHelpDictionary ${stateOfAffairs} "Options:
  --alias = specify your name.
  --title = add a descriptive title.
  --completed = ***.
  --explanation = you can enter a long text that is completely descriptive, use quotes.
  --taskIsActive = ***.
  --stopIfItFails 
  --dependsOn = ***.
  --typeOfDependency
  --ghostJob
  --parent = ***.
  --momentum =.
  --toml = file in toml format."
  addToExampleHelpDictionary ${stateOfAffairs} "Example:
  stateOfAffairs --alias Name --title Title --completed true --explanation SomeText --taskIsActive false --dependsOn --parent "

  local launchTests="launchTests"
  addToHelpDictionary ${launchTests} "Launches CodedAsHuman construction tests."
  addToExtendedHelpDictionary ${launchTests} "They can also be launched from the console. Construction tests are all released even if they break the runbook."
  addToOptionsHelpDictionary ${launchTests} "Options:
  --onlyTheGroupTests = name of the function that groups the tests.
  --exceptTheGroupTests = name of the function that groups the tests."
  addToExampleHelpDictionary ${launchTests} "Example:
  launchTests --onlyTheGroupTests functionName
  launchTests --exceptTheGroupTests functionName"

  local header="header"
  addToHelpDictionary ${header} "We indicate the title of the section."
  addToExtendedHelpDictionary ${header} "We can use four different headings: 'describe', 'context', 'section' and 'subsection'."
  addToOptionsHelpDictionary ${header} "Options:
  --describe = it is the most important title.
  --context = it is the second most important title.
  --section = it is the third title in importance.
  --subsection = it is the fourth title in importance.
  --explanation
  --parent 
  --alias
  --bulkData
  --labels
  --momentum "
  addToExampleHelpDictionary ${header} "Example:
  header --describe title --explanation --parent --alias --labels --momentum "

  local pauseRunbook="pauseRunbook"
  addToHelpDictionary ${pauseRunbook} "Disable runbook control."
  addToExtendedHelpDictionary ${pauseRunbook} "With the runbook deactivated, all actions continue to be launched even if there are errors."
  addToOptionsHelpDictionary ${pauseRunbook} "Options: you don't have."
  addToExampleHelpDictionary ${pauseRunbook} "Example:
  pauseRunbook"


  local unpauseRunbook="unpauseRunbook"
  addToHelpDictionary ${unpauseRunbook} "Disable runbook control."
  addToExtendedHelpDictionary ${unpauseRunbook} "With the runbook activated the actions are not launched when there is an error."
  addToOptionsHelpDictionary ${unpauseRunbook} "Options: you don't have."
  addToExampleHelpDictionary ${unpauseRunbook} "Example:
  unpauseRunbook"

  local List="List"
  addToHelpDictionary ${List} "Accepted vocabulary (case sensitive):
--Basic help--
  [Help:]
   Help (a basic introduction)
   List (this list)
  [User configuration:]
   SetUp (view path of configuration file)
   SetUpConsoleStyles (view path of view style configurations)
   SetUpWebStyles (view path of print style configurations)
   SetUpFolders (configure path of user, temp and protected folders)
   SetUpViewOptions (configure visibility of messages)
  [User interface of 'Languages':]
   selectLanguage (select the language you want to see the messages)
  [User interface of 'Runbooks':]
   header (we indicate the title)
   pauseRunbook (we deactivate the runbook)
   unpauseRunbook (we activate the runbook)
   launchSpecificationsSummary (it shows you the test result)
  [User interface of 'Program features':]
   setUpDistro (select the distribution you use)
   refreshRepositories (Update repository cache indexes)
   updateRepositoryPackages (Upgrade available packages)
   ensureTool (check that the desired tool is installed, otherwise install it)
   uninstallTool (uninstall a tool)
   ensureToolRepository (check that the desired repository is added, otherwise add it)
   eraseToolRepository (check that the desired repository is added and delete it)
  [User interface of 'Management panels':]
   ensureManagementPanel (create a folder if it does not exist)
   eraseManagementPanel (delete a folder)
   copyManagementPanel (copy a folder)
   moveManagementPanel (move a folder)
   ensureBriefingTask (create a file)
   eraseBriefingTask (delete a file)
   ensureRecipeIngredient (add a line to a file, if the text does not exist in the file)
   "
   addToExtendedHelpDictionary ${List} "--Help for advanced users--
  [Languages (placed in path: '${typeOfLanguagePath}):]
   HelpDictionary (help translations)
   ErrorDictionary (translations of application errors)
   HtmlQuotation (setting quotes for language)
   MessagesDictionary (message translations)
   MagicArrayCreation (explanation of how message dictionary arrays are built)

--Tests--
  [Available options:]
   options (shows the options available for those functions that need them)
  [Test tool:]
   launchTests (launches CodedAsHuman construction tests)
   weHope (testing tool)
   stateOfAffairs (check a specification)"
   addToOptionsHelpDictionary ${List} " "
   addToExampleHelpDictionary ${List} " "
}
createHelpDictionaryForGiveMeFunction


createDictionaryOfActuality() {
  addToSystemDictionaryMessages Malformed "The request is not correctly constructed"
  addToSystemDictionaryMessages NotDefined "You have not provided the necessary information"
  addToSystemDictionaryMessages FolderExist "Folder exist"
  addToSystemDictionaryMessages FolderNotExist "Folder not exist"
  addToSystemDictionaryMessages FileExist "File exist"
  addToSystemDictionaryMessages FileNotExist "File not exist"
  addToSystemDictionaryMessages PathIsValid "The folder meets the requirements"
  addToSystemDictionaryMessages Repository "Repository"
}
createDictionaryOfActuality


createDictionaryOfErrors() {
  addToSystemDictionaryMessages ErrorInTest "The tests has failed"
  addToSystemDictionaryMessages ErrorInAddRecipeIngredientToFile "Error on AddRecipeIngredientToFile"
  addToSystemDictionaryMessages ErrorInApplyLanguage "Error on ApplyLanguage"
  addToSystemDictionaryMessages ErrorInApplyToolInstallation "Error on ApplyToolInstallation"
  addToSystemDictionaryMessages ErrorInApplyToolRemove "Error on ApplyToolRemove"
  addToSystemDictionaryMessages ErrorInCheckIfPathIsValid "Error on CheckIfPathIsValid"
  addToSystemDictionaryMessages ErrorInCloseTypoCaption "Error on CloseTypoCaption"
  addToSystemDictionaryMessages ErrorInCompareFolder "Error on CompareFolder"
  addToSystemDictionaryMessages ErrorInCopyPanel "Error on CopyPanel"
  addToSystemDictionaryMessages ErrorInconstructTheTask "Error on StateOfAffairs"
  addToSystemDictionaryMessages ErrorInsurveyTheTask "Error on StateOfAffairs"
  addToSystemDictionaryMessages ErrorInCreatePanel "Error on CreatePanel"
  addToSystemDictionaryMessages ErrorInDistribution "Error on Distribution"
  addToSystemDictionaryMessages ErrorInRefreshRepository "Error on RefreshRepository"
  addToSystemDictionaryMessages ErrorInUpdateAllPackages "Error on UpdateAllPackages"
  addToSystemDictionaryMessages ErrorInEnsureRepository "Error on EnsureRepository"
  addToSystemDictionaryMessages ErrorInEraseRepository "Error on EraseRepository"
  addToSystemDictionaryMessages ErrorInEraseFile "Error on EraseFile"
  addToSystemDictionaryMessages ErrorInErasePanel "Error on ErasePanel"
  addToSystemDictionaryMessages ErrorInInitTypoCaption "Error on InitTypoCaption"
  addToSystemDictionaryMessages ErrorInInitializeViewDocument "Error en InitializeViewDocument"
  addToSystemDictionaryMessages ErrorInInterfaceFunctionWithoutMessage "Error picking up the content of the dynamic variable, functions: 'createDictionaryOfErrors', 'setUpVisibility', 'createMessagesForUserActions', the 'Test ERROR message' and the configurations."
  addToSystemDictionaryMessages ErrorInIsTheToolInstalled "Error on IsTheToolInstalled"
  addToSystemDictionaryMessages ErrorInMovePanel "Error on MovePanel"
  addToSystemDictionaryMessages ErrorInSoftDeleteBriefingTask "Error on SoftDeleteBriefingTask"
  addToSystemDictionaryMessages ErrorInTitleOfProject "Missing title in 'ErrorInTitleOfProject'"
  addToSystemDictionaryMessages ErrorInUsersInvolved "Error on UsersInvolved"
  addToSystemDictionaryMessages ErrorInValidationProtectedFolder "Error on ValidationProtectedFolder"
  addToSystemDictionaryMessages ErrorInValidationTempFolder "Error on ValidationTempFolder"
  addToSystemDictionaryMessages ErrorInValidationUserFolder "Error on ValidationUserFolder"
}
createDictionaryOfErrors


createHtmlQuotationForLanguage() {
  initializeGlobalVariable "htmlLanguage" "en"
  initializeGlobalVariable "openMajorQuote" "“"
  initializeGlobalVariable "closeMajorQuote" "”"
  initializeGlobalVariable "openMinorQuote" "‘"
  initializeGlobalVariable "closeMinorQuote" "’"
  initializeGlobalVariable "doubleApostrofeQuote" "\""
  initializeGlobalVariable "simpleApostrofeQuote" "'"
}
createHtmlQuotationForLanguage


createBasicMessages() {
  initializeGlobalVariable "CAHisLoaded"
    CAHisLoaded="CAH is already running on the pid ${BASHPID}: skipping the new load."

  initializeGlobalVariable "titleProjectMessageCaption"
    titleProjectMessageCaption="Project title"

  initializeGlobalVariable "nameMessageCaption"
    nameMessageCaption="Name"

  initializeGlobalVariable "userNameMessageCaption"
    userNameMessageCaption="User name"

  initializeGlobalVariable "userGroupsMessage"
    userGroupsMessage="The user is in the following groups"

  initializeGlobalVariable "validationHasFailed"
    validationHasFailed="the result returned by the system is incongruous!?"

  initializeGlobalVariable "messageNotActionBecauseRunbookIsBroken"
    messageNotActionBecauseRunbookIsBroken="No se ejecuta la acción debido a que el runbook está roto."

  initializeGlobalVariable "messageRunbookIsBrokenIs"
    messageRunbookIsBrokenIs="The runbook is broken."

  initializeGlobalVariable "messageRunbookIsOver"
    messageRunbookIsOver="The runbook is over (green)."

  initializeGlobalVariable "whatIsTheResult"
    whatIsTheResult="Has the result been true or false (there is no default entry)?"

  initializeGlobalVariable "genericPassMessage"
    genericPassMessage="All is Ok!"

  initializeGlobalVariable "unexplainedError"
    unexplainedError="Unexplained error!"

  createTheArray "unexplained" \
    --withSuccessBeginMessage "${genericPassMessage[@]}" \
    --withErrorBeginMessage "${unexplainedError[@]}"

  initializeGlobalVariable "paradigmContain"
    paradigmContain="contains"

  initializeGlobalVariable "paradigmContains"
    paradigmContains="contains"

  initializeGlobalVariable "paradigmNotContain"
    paradigmNotContain="does not contain"

  initializeGlobalVariable "paradigmNotContains"
    paradigmNotContains="does not contain"

  initializeGlobalVariable "gemHomeUnstablished"
    paradigmNotContains="GEM_HOME variable not set, use the recipe to install Ruby: 'cook --recipe ruby' "
}
createBasicMessages


createInfoSystemMessages() {
  initializeGlobalVariable "userFolderMessage"
    userFolderMessage="User folder"

  initializeGlobalVariable "tempFolderMessage"
    tempFolderMessage="Temp folder"

  initializeGlobalVariable "protectedFolderMessage"
    protectedFolderMessage="Protected folder"

  createTheArray "infoDate" \
    --withSuccessBeginMessage "UTC time:" \
    --withErrorBeginMessage "UTC time:"

  createTheArray "infoUser" \
    --withSuccessBeginMessage "User:" \
    --withErrorBeginMessage "User:"

  createTheArray "infoPath" \
    --withSuccessBeginMessage "Path:" \
    --withErrorBeginMessage "Path:"
}
createInfoSystemMessages


createMessagesForTests() {
  createTheArray "theCheckIs" \
    --withSuccessBeginMessage "First operand:" \
    --withSuccessComparationMessage "Second operand:" \
    --withSuccessOperatorMessage "Sentence:" \
    --withSuccessEndMessage "is true" \
    --withErrorBeginMessage "First operand:" \
    --withErrorComparationMessage "Second operand:" \
    --withErrorOperatorMessage "Sentence:" \
    --withErrorEndMessage "is false"

  createTheArray "theCheckIsNot" \
    --withSuccessBeginMessage "First operand:" \
    --withSuccessComparationMessage "Second operand:" \
    --withSuccessOperatorMessage "Sentence:" \
    --withSuccessEndMessage "it has NOT been successfully validated" \
    --withErrorBeginMessage "First operand:" \
    --withErrorComparationMessage "Second operand:" \
    --withErrorOperatorMessage "Sentence:" \
    --withErrorEndMessage "has been satisfactory"

  createTheArray "lenghtOfString" \
    --withSuccessBeginMessage "The lenght is" \
    --withSuccessComparationMessage "and compared from" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "gives a positive result" \
    --withErrorBeginMessage "The lenght is" \
    --withErrorComparationMessage "and compared from" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "gives a failed result"
}
createMessagesForTests


createMessagesForTagsDescriptions() {
  createTheArray "titleDescribe" \
    --withSuccessBeginMessage "Describe:" \
    --withErrorBeginMessage "Describe:"

  createTheArray "titleContext" \
    --withSuccessBeginMessage "Context:" \
    --withErrorBeginMessage "Context:"

  createTheArray "titleSection" \
    --withSuccessBeginMessage "Section:" \
    --withErrorBeginMessage "Section:"

  createTheArray "titleSubsection" \
    --withSuccessBeginMessage "Subsection:" \
    --withErrorBeginMessage "Subsection:"

  createTheArray "tagTitleComments" \
    --withSuccessBeginMessage "Comments:" \
    --withErrorBeginMessage "Comments:"

  createTheArray "tagAbstract" \
    --withSuccessBeginMessage "Abstract:" \
    --withErrorBeginMessage "Abstract:"

  createTheArray "tagStatus" \
    --withSuccessBeginMessage "Status:" \
    --withErrorBeginMessage "Status:"

  createTheArray "tagExplanation" \
    --withSuccessBeginMessage "Explanation:" \
    --withErrorBeginMessage "Explanation:"

  createTheArray "summaryTitle" \
    --withSuccessBeginMessage "Summary:" \
    --withSuccessEndMessage "passed" \
    --withErrorBeginMessage "Summary:" \
    --withErrorEndMessage "with error"
}
createMessagesForTagsDescriptions


createMessagesForUserActions() {
  createTheArray "constructTheTask" \
    --withSuccessBeginMessage "The status of the runbook" \
    --withSuccessComparationMessage "and compared from" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "it has been setted" \
    --withErrorBeginMessage "The status of the runbook" \
    --withErrorComparationMessage "and compared from" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been setted"

  createTheArray "surveyTheTask" \
    --withSuccessBeginMessage "The status of the runbook" \
    --withSuccessComparationMessage "and compared from" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "it has been setted" \
    --withErrorBeginMessage "The status of the runbook" \
    --withErrorComparationMessage "and compared from" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been setted"

  createTheArray "usersInvolved" \
    --withSuccessBeginMessage "The selected user/group is valid" \
    --withSuccessComparationMessage "and compared from" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "it has been setted" \
    --withErrorBeginMessage "The user/group is NOT recorded as accepted" \
    --withErrorComparationMessage "and compared from" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been setted"

  createTheArray "applyLanguage" \
    --withSuccessBeginMessage "Language selection" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "Language selection" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "theLanguage" \
    --withSuccessBeginMessage "The language applied is" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "The language isn't applied" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "useDistro" \
    --withSuccessBeginMessage "The selection of the distribution" \
    --withSuccessComparationMessage "and compared from" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "The selection of the distribution" \
    --withErrorComparationMessage "and compared from" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "distroNotExist" \
    --withSuccessBeginMessage "The distribution isn't" \
    --withErrorComparationMessage "and compared from" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied" \
    --withErrorBeginMessage "The distribution exist" \
    --withSuccessComparationMessage "and compared from" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied"

  createTheArray "loadInterface" \
    --withSuccessBeginMessage "Interface load" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "Interface load" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "applyToolInstallation" \
    --withSuccessBeginMessage "Tool installation" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "Tool installation" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "cooking" \
    --withSuccessBeginMessage "The receipt" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "The receipt" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "refreshRepository" \
    --withSuccessBeginMessage "Refresh repository" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "Refresh repository addition" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "updateAllPackages" \
    --withSuccessBeginMessage "Update repository" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "Update repository addition" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "ensureRepository" \
    --withSuccessBeginMessage "Repository addition" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "Repository addition" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "eraseRepository" \
    --withSuccessBeginMessage "Repository deletion" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "Repository deletion" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "toolExist" \
    --withSuccessBeginMessage "The tool exist" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been applied" \
    --withErrorBeginMessage "The tool isn't" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been applied"

  createTheArray "applyToolRemove" \
    --withSuccessBeginMessage "Tool uninstallation" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "warning that it is NOT uninstalled" \
    --withErrorBeginMessage "Tool uninstallation" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "confirming that it is uninstalled"

  createTheArray "gemNotExist" \
    --withSuccessBeginMessage "The tool isn't" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "warning that it has NOT been applied" \
    --withErrorBeginMessage "The tool exist" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "confirming that it has been applied"

  createTheArray "ensureThePanel" \
    --withSuccessBeginMessage "The creation of the management panel" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it exists" \
    --withErrorBeginMessage "The creation of the management panel" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it does NOT exist"

  createTheArray "panelExist" \
    --withSuccessBeginMessage "Content of management panel" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it exists" \
    --withErrorBeginMessage "The management panel isn't" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it does NOT exist"

  createTheArray "panelNotExist" \
    --withSuccessBeginMessage "The management panel isn't" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "warning that it does NOT exist" \
    --withErrorBeginMessage "Content of management panel" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "confirming that it exists"

  createTheArray "copyTheManagementPanel" \
    --withSuccessBeginMessage "The copy of the management panel" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been copied" \
    --withErrorBeginMessage "The copy of the management panel" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been copied"

  createTheArray "moveThePanel" \
    --withSuccessBeginMessage "The move of the management panel" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has moved" \
    --withErrorBeginMessage "The move of the management panel" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT moved"

  createTheArray "eraseTheManagementPanel" \
    --withSuccessBeginMessage "The deletion of the management panel" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been deleted" \
    --withErrorBeginMessage "The deletion of the management panel" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been removed"

  createTheArray "ensureTheBriefingTask" \
    --withSuccessBeginMessage "The creation of the road map" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it exists" \
    --withErrorBeginMessage "The road map" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it does NOT exist"

  createTheArray "briefingTaskExist" \
    --withSuccessBeginMessage "The road map exist" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it exists" \
    --withErrorBeginMessage "The road map isn't" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it does NOT exist"

  createTheArray "briefingTaskNotExist" \
    --withSuccessBeginMessage "The road map isn't" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "warning that it does NOT exist" \
    --withErrorBeginMessage "The road map exist" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "confirming that it exists"

  createTheArray "eraseTheBriefingTask" \
    --withSuccessBeginMessage "The deletion of the road map" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been deleted" \
    --withErrorBeginMessage "The road map" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been deleted"

  createTheArray "ensureTheRecipeIngredient" \
    --withSuccessBeginMessage "The text" \
    --withSuccessComparationMessage "has returned the code" \
    --withSuccessOperatorMessage "being the operator" \
    --withSuccessEndMessage "confirming that it has been added to the indicated file" \
    --withErrorBeginMessage "The text" \
    --withErrorComparationMessage "has returned the code" \
    --withErrorOperatorMessage "being the operator" \
    --withErrorEndMessage "warning that it has NOT been added to the indicated file"
}
createMessagesForUserActions

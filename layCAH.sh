#!${BASH}

shellPid=$$

unset testingMode 

case "${1}" in
  "--testing" )
    echo "Testing mode!"
    testingMode=true
  ;;
  * )
    case $shellPid in
      "${CAHPID}" )
        echo "${CAHisLoaded}"
        return 1 2>/dev/null
        exit 1
      ;;
      * )
        CAHPID=$shellPid
      ;;
    esac
  ;;
esac

unset shell
shell="bash"

unset shellExtension
shellExtension="sh"

unset cahPath
cahPath=$PWD

. "configuration/default.${shellExtension}"

unset coreSupport
coreSupport="support"
. "${shell}/${coreSupport}.${shellExtension}"


loadConfiguration() {
  initializeGlobalVariable "configurationPath" "configuration"
  loadAddOn "${configurationPath}" 
}
loadConfiguration


loadServices() {
  initializeGlobalVariable "servicesPath" "services"
  loadAddOn "${shell}" "${servicesPath}"
}
loadServices


loadLanguages() {
  initializeGlobalVariable "typeOfLanguagePath" "${defaultTypeOfLanguage}"
  loadAddOn "${languagesPath}" "${typeOfLanguagePath}"
}
loadLanguages
loadAddOn "${languagesPath}" "${typeOfLanguagePath}" "${defaultLanguage}"


loadStyles() {
  initializeGlobalVariable "consoleStylePath" "console"
  loadAddOn "${stylesPath}" "${consoleStylePath}" "${defaultConsoleStyle}"
  initializeGlobalVariable "webStylePath" "web"
  loadAddOn "${stylesPath}" "${webStylePath}" "${defaultWebStyle}"
}
loadStyles

loadModules() {
  initializeGlobalVariable "modulesPath" "modules"
  loadAddOn "${shell}" "${modulesPath}"
}
loadModules


prepareDistributions() {
  initializeGlobalVariable "distributionsPath" "distributions"
  loadAddOn "${shell}" "${distributionsPath}"
}
prepareDistributions
loadAddOn "${shell}" "${distributionsPath}" "${defaultDistribution}"


loadActions() {
  initializeGlobalVariable "actionsPath" "actions"
  loadAddOn "${shell}" "${actionsPath}"
}
loadActions


loadInterfaces() {
  initializeGlobalVariable "interfacesPath" "interfaces"
  loadAddOn "${shell}" "${interfacesPath}"
}
loadInterfaces


case "${1}" in
  "--testing" )
    . "${testingPath}/runTests.${shellExtension}"
  ;;
esac


canonicalizeUserPaths() {
  userFolder=$(canonicalizeExistentPath ${userFolder})
  if [ ! -d "${userFolder}" ]
    then
      echo "${messageReturnedByFunction[ErrorInValidationUserFolder]}"
  fi
  tempFolder=$(canonicalizeExistentPath ${tempFolder})
  if [ ! -d "${tempFolder}" ]
    then
      echo "${messageReturnedByFunction[ErrorInValidationTempFolder]}"
  fi
  protectedFolder=$(canonicalizeExistentPath ${protectedFolder})
  if [ ! -d "${protectedFolder}" ]
    then
      echo "${messageReturnedByFunction[ErrorInValidationProtectedFolder]}"
  fi
}
canonicalizeUserPaths


if [ "${showInitialHelp}" == true ]
  then
    giveMe
fi

if [ "${ensureUserFolders}" == true ]
  then
    ensureManagementPanel --name "${userFolder}" --alias "${aliasTests}" --bulkData "bulk data" --labels "CAH" --momentum "${identifyTheTag}"
    ensureManagementPanel --name "${tempFolder}" --alias "${aliasTests}" --bulkData "bulk data" --labels "CAH" --momentum "${identifyTheTag}"
fi

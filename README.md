# Coded As Human

CAH en este momento solo tiene contemplado utilizarse bajo la distribución linux Debian (y las derivadas Ubuntu y Neon).


## Ejecutar CAH

~~~
source "layCAH.sh"
~~~

A partir de este momento ya podemos utilizar todas sus funcionalidades.


## Ayuda

Para ver la ayuda escriba lo siguiente:

~~~
giveMe
~~~


## Trabajar con el repositorio

### Crear un nuevo tag

Una vez realizado el commit y el push correspondiente:

- actualizamos los tags,

~~~
git fetch --all --tags
~~~

- revisamos el listado de tags,

~~~
git tag
~~~

- realizamos un tag,

~~~
git tag -a v0.0 -m "my version 0.0"
~~~

- subimos el tag,

~~~
git push origin --tags
~~~


### Para lanzar los test:

Creamos un archivo con el siguiente contenido:

~~~
#!${BASH}
source "layCAH.sh" --testing

launchTests


header --describe "View the summary" --parent "filename" --alias "tests" --labels "CAH" --momentum "1234"
launchSpecificationsSummary --save "the_name_of_your_choice"

cleanProjectResultsFromMemory

return $(returnRunbookStatusCode)
~~~

Posteriormente lanzamos el script con el comando `source`.

Por ejemplo, si guardamos el script anterior como '__testing.cah', lanzamos:

~~~
source __testing.cah
~~~

#!${BASH}
html() {
  echo -e "Loading... Print styles 'html', version 0.1"
}

initializeGlobalVariable "newLinePrint" "</br>\n"

initializeGlobalVariable "boldTextPrint" "<b>"
initializeGlobalVariable "resetBoldTextPrint" "</b>"
initializeGlobalVariable "italicTextPrint" "<i>"
initializeGlobalVariable "resetItalicTextPrint" "</i>"
initializeGlobalVariable "underlinedTextPrint" "<u>"
initializeGlobalVariable "resetUnderlinedTextPrint" "</u>"
initializeGlobalVariable "boldItalicTextPrint" "<b><i>"
initializeGlobalVariable "resetBoldItalicTextPrint" "</i></b>"

initializeGlobalVariable "initPrintForSUCCESSmessage" "${boldTextPrint}"
initializeGlobalVariable "closePrintForSUCCESSmessage" "${resetBoldTextPrint}"
initializeGlobalVariable "initPrintForERRORmessage" "${underlinedTextPrint}"
initializeGlobalVariable "closePrintForERRORmessage" "${resetUnderlinedTextPrint}"

initializeGlobalVariable "initTitlePrintForDescribe" "<h1>"
initializeGlobalVariable "initTitlePrintForContext" "<h2>"
initializeGlobalVariable "initTitlePrintForSection" "<h3>"
initializeGlobalVariable "initTitlePrintForSubsection" "<h4>"
initializeGlobalVariable "initTextPrintForParagraph" "<p>"
initializeGlobalVariable "initTextPrintForInlineQuote" "<q>"
initializeGlobalVariable "initTextPrintForBlockQuote" "<blockquote>"
initializeGlobalVariable "initTextPrintForPre" "<pre>"
initializeGlobalVariable "initTextPrintForCode" "<code>"
initializeGlobalVariable "initTextPrintForAbstract" "${initTextPrintForParagraph}"
initializeGlobalVariable "initTextPrintForResult" "${initTextPrintForParagraph}"
initializeGlobalVariable "initTextPrintForExplanation" "${initTextPrintForParagraph}"

initializeGlobalVariable "closeTitlePrintForDescribe" "</h1>"
initializeGlobalVariable "closeTitlePrintForContext" "</h2>"
initializeGlobalVariable "closeTitlePrintForSection" "</h3>"
initializeGlobalVariable "closeTitlePrintForSubsection" "</h4>"
initializeGlobalVariable "closeTextPrintForParagraph" "</p>"
initializeGlobalVariable "closeTextPrintForInlineQuote" "</q>"
initializeGlobalVariable "closeTextPrintForBlockQuote" "</blockquote>"
initializeGlobalVariable "closeTextPrintForPre" "</pre>"
initializeGlobalVariable "closeTextPrintForCode" "</code>"
initializeGlobalVariable "closeTextPrintForAbstract" "${closeTextPrintForParagraph}"
initializeGlobalVariable "closeTextPrintForResult" "${closeTextPrintForParagraph}"
initializeGlobalVariable "closeTextPrintForExplanation" "${closeTextPrintForParagraph}"

initializeGlobalVariable "initPagePrint"
initPagePrint="<html lang=\"${htmlLanguage}\" xml:lang=\"${htmlLanguage}\" xmlns=\"http://www.w3.org/1999/xhtml\">
<meta charset=\"UTF-8\">"

initializeGlobalVariable "initHeaderPrint" "<head>"
initializeGlobalVariable "initBodyPrint" "<body>"
initializeGlobalVariable "initTitlePrint" "<title>"

initializeGlobalVariable "closePagePrint" "</html>"
initializeGlobalVariable "closeHeaderPrint" "</head>"
initializeGlobalVariable "closeBodyPrint" "</body>"
initializeGlobalVariable "closeTitlePrint" "</title>"

initializeGlobalVariable "initTextPrintForNewLine" "${newLinePrint}"

initializeGlobalVariable "initWebStyles" "<style>"
initializeGlobalVariable "closeWebStyles" "</style>"

initializeGlobalVariable "internalStyleSheet"
internalStyleSheet="<style>
h1 {font-size: 1.5rem;}
h2 {font-size: 1.4rem; margin: 0.5rem 0 0 1rem;}
h3 {font-size: 1.3rem; margin: 0.5rem 0 0 2rem;}
h4 {font-size: 1.2rem; margin: 0.5rem 0 0 3rem;}
p {font-size: 1rem; margin: 0.5rem 0 0 3rem;}
pre {font-size: 1.1rem; margin: 0.5rem 0 0 4rem; overflow-x: auto;	white-space: pre-wrap;	white-space: -moz-pre-wrap !important; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word; background: #fff; }
blockquote {quotes: '${openMajorQuote}' '${closeMajorQuote}' '${doubleApostrofeQuote}' '${doubleApostrofeQuote}';}
blockquotecode:before {content: '${openMajorQuote}'; content: open-quote;}
blockquotecode:after {content: ''; content: no-close-quote;}
blockquotecode:last-child:after {content: '${closeMajorQuote}'; content: close-quote;}
q {quotes: '${openMinorQuote}' '${closeMinorQuote}' '${simpleApostrofeQuote}' '${simpleApostrofeQuote}';}
</style>
\n"

initializeGlobalVariable "externalStyleSheet"
externalStyleSheet="
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<link rel=\"preload\" href=\"${defaultCss}\" as=\"style\">
<link rel=\"stylesheet\" href=\"${defaultCss}\" media=\"print\" onload=\"this.media='all'\">
"

initializeGlobalVariable "styleSheet"
if [ "${defaultCss}" != "" ]
  then
    styleSheet="${externalStyleSheet[@]}"
  else
    styleSheet="${internalStyleSheet[@]}"
fi

initializeGlobalVariable "initDocumentWebStyle"

initializeGlobalVariable "initDocumentWebStyle"
initDocumentWebStyle="${initPagePrint[@]}
${initHeaderPrint[@]}
${styleSheet[@]}
${initTitlePrint[@]}${theTitle[@]}${closeTitlePrint[@]}
${closeHeaderPrint[@]}
${initBodyPrint[@]}"

initializeGlobalVariable "endDocumentWebStyle"
endDocumentWebStyle="${closeBodyPrint}
${closePagePrint}"

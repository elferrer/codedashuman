#!${BASH}
bashColorless() {
  echo -e "Loading... View styles 'bashColorless', version 0.1"
}

initializeGlobalVariable "newLineView" "\n"

initializeGlobalVariable "redColor" "\033[31m"
initializeGlobalVariable "greenColor" "\033[32m"
initializeGlobalVariable "orangeColor" "\033[33m"
initializeGlobalVariable "blueColor" "\033[34m"
initializeGlobalVariable "resetColor" "\033[0m"

initializeGlobalVariable "boldTextView" "\033[1m"
initializeGlobalVariable "resetBoldTextView" "${resetColor}"
initializeGlobalVariable "italicTextView" "\033[3m"
initializeGlobalVariable "resetItalicTextView" "${resetColor}"
initializeGlobalVariable "underlinedTextView" "\033[4m"
initializeGlobalVariable "resetUnderlinedTextView" "${resetColor}"
initializeGlobalVariable "boldItalicTextView" "\033[4m"
initializeGlobalVariable "resetBoldItalicTextView" "${resetColor}"

initializeGlobalVariable "initViewForSUCCESSmessage" "${boldTextView}"
initializeGlobalVariable "closeViewForSUCCESSmessage" "${resetColor}"
initializeGlobalVariable "initViewForERRORmessage" "${underlinedTextView}"
initializeGlobalVariable "closeViewForERRORmessage" "${resetColor}"

initializeGlobalVariable "initTitleViewForDescribe" "\033[4m"
initializeGlobalVariable "initTitleViewForContext" "\033[4m"
initializeGlobalVariable "initTitleViewForSection" "\033[4m"
initializeGlobalVariable "initTitleViewForSubsection" "\033[4m"
initializeGlobalVariable "initTextViewForParagraph" ""
initializeGlobalVariable "initTextViewForBlockQuote" "${openMajorQuote}"
initializeGlobalVariable "initTextViewForInlineQuote" "${openMinorQuote}"
initializeGlobalVariable "initTextViewForPre" ""
initializeGlobalVariable "initTextViewForCode" ""
initializeGlobalVariable "initTextViewForAbstract" ""
initializeGlobalVariable "initTextViewForResult" ""
initializeGlobalVariable "initTextViewForExplanation" ""

initializeGlobalVariable "closeTitleViewForDescribe" "${resetColor}"
initializeGlobalVariable "closeTitleViewForContext" "${resetColor}"
initializeGlobalVariable "closeTitleViewForSection" "${resetColor}"
initializeGlobalVariable "closeTitleViewForSubsection" "${resetColor}"
initializeGlobalVariable "closeTextViewForParagraph" ""
initializeGlobalVariable "closeTextViewForBlockQuote" "${closeMajorQuote}"
initializeGlobalVariable "closeTextViewForInlineQuote" "${closeMinorQuote}"
initializeGlobalVariable "closeTextViewForPre" ""
initializeGlobalVariable "closeTextViewForCode" ""
initializeGlobalVariable "closeTextViewForAbstract" ""
initializeGlobalVariable "closeTextViewForResult" ""
initializeGlobalVariable "closeTextViewForExplanation" ""

initializeGlobalVariable "initPageView" ""
initializeGlobalVariable "initHeaderView" ""
initializeGlobalVariable "initBodyView" ""

initializeGlobalVariable "closePageView" ""
initializeGlobalVariable "closeHeaderView" ""
initializeGlobalVariable "closeHeaderView" ""

initializeGlobalVariable "initTextViewForNewLine" "${newLineView}"

initializeGlobalVariable "initConsoleStyles" ""
initializeGlobalVariable "closeConsoleStyles" ""

initializeGlobalVariable "styleSheet"

initializeGlobalVariable "initDocumentConsoleStyle" "${initPageView}${initHeaderView}${initConsoleStyles}${styleSheet[@]}${closeConsoleStyles}${closeHeaderView}${initBodyView}"

initializeGlobalVariable "endDocumentConsoleStyle" "${closeHeaderView}${closePageView}"

#!${BASH}
bashUnformatted() {
  echo -e "Loading... View styles 'bashUnformatted', version 0.1"
}

initializeGlobalVariable "newLineView" "\n"

initializeGlobalVariable "redColor" "\033[31m"
initializeGlobalVariable "greenColor" "\033[32m"
initializeGlobalVariable "orangeColor" "\033[33m"
initializeGlobalVariable "blueColor" "\033[34m"
initializeGlobalVariable "resetColor" "\033[0m"

initializeGlobalVariable "boldTextView" ""
initializeGlobalVariable "resetBoldTextView" ""
initializeGlobalVariable "italicTextView" ""
initializeGlobalVariable "resetItalicTextView" ""
initializeGlobalVariable "underlinedTextView" ""
initializeGlobalVariable "resetUnderlinedTextView" ""
initializeGlobalVariable "boldItalicTextView" ""
initializeGlobalVariable "resetBoldItalicTextView" ""

initializeGlobalVariable "initViewForSUCCESSmessage" ""
initializeGlobalVariable "closeViewForSUCCESSmessage" ""
initializeGlobalVariable "initViewForERRORmessage" ""
initializeGlobalVariable "closeViewForERRORmessage" ""

initializeGlobalVariable "initTitleViewForDescribe" ""
initializeGlobalVariable "initTitleViewForContext" ""
initializeGlobalVariable "initTitleViewForSection" ""
initializeGlobalVariable "initTitleViewForSubsection" ""
initializeGlobalVariable "initTextViewForParagraph" ""
initializeGlobalVariable "initTextViewForInlineQuote" ""
initializeGlobalVariable "initTextViewForBlockQuote" ""
initializeGlobalVariable "initTextViewForPre" ""
initializeGlobalVariable "initTextViewForCode" ""
initializeGlobalVariable "initTextViewForAbstract" ""
initializeGlobalVariable "initTextViewForResult" ""
initializeGlobalVariable "initTextViewForExplanation" ""

initializeGlobalVariable "closeTitleViewForDescribe" ""
initializeGlobalVariable "closeTitleViewForContext" ""
initializeGlobalVariable "closeTitleViewForSection" ""
initializeGlobalVariable "closeTitleViewForSubsection" ""
initializeGlobalVariable "closeTextViewForParagraph" ""
initializeGlobalVariable "closeTextViewForInlineQuote" ""
initializeGlobalVariable "closeTextViewForBlockQuote" ""
initializeGlobalVariable "closeTextViewForPre" ""
initializeGlobalVariable "closeTextViewForCode" ""
initializeGlobalVariable "closeTextViewForAbstract" ""
initializeGlobalVariable "closeTextViewForResult" ""
initializeGlobalVariable "closeTextViewForExplanation" ""

initializeGlobalVariable "initPageView" ""
initializeGlobalVariable "initHeaderView" ""
initializeGlobalVariable "initBodyView" ""

initializeGlobalVariable "closePageView" ""
initializeGlobalVariable "closeHeaderView" ""
initializeGlobalVariable "closeHeaderView" ""

initializeGlobalVariable "initTextViewForNewLine" "${newLineView}"

initializeGlobalVariable "initConsoleStyles" ""
initializeGlobalVariable "closeConsoleStyles" ""

initializeGlobalVariable "styleSheet"

initializeGlobalVariable "initDocumentConsoleStyle" "${initPageView}${initHeaderView}${initConsoleStyles}${styleSheet[@]}${closeConsoleStyles}${closeHeaderView}${initBodyView}"

initializeGlobalVariable "endDocumentConsoleStyle" "${closeHeaderView}${closePageView}"

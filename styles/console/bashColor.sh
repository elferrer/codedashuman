#!${BASH}
bashColor() {
  echo -e "Loading... View styles 'bashColor', version 0.1"
}

initializeGlobalVariable "newLineView" "\n"

initializeGlobalVariable "redColor" "\033[31m"
initializeGlobalVariable "greenColor" "\033[32m"
initializeGlobalVariable "orangeColor" "\033[33m"
initializeGlobalVariable "blueColor" "\033[34m"
initializeGlobalVariable "resetColor" "\033[0m"

initializeGlobalVariable "boldTextView" "${greenColor}"
initializeGlobalVariable "resetBoldTextView" "${resetColor}"
initializeGlobalVariable "italicTextView" "${blueColor}"
initializeGlobalVariable "resetItalicTextView" "${resetColor}"
initializeGlobalVariable "underlinedTextView" "${redColor}"
initializeGlobalVariable "resetUnderlinedTextView" "${resetColor}"
initializeGlobalVariable "boldItalicTextView" "${orangeColor}"
initializeGlobalVariable "resetBoldItalicTextView" "${resetColor}"

initializeGlobalVariable "initViewForSUCCESSmessage" "${boldTextView}"
initializeGlobalVariable "closeViewForSUCCESSmessage" "${resetColor}"
initializeGlobalVariable "initViewForERRORmessage" "${underlinedTextView}"
initializeGlobalVariable "closeViewForERRORmessage" "${resetColor}"

initializeGlobalVariable "initTitleViewForDescribe" "${blueColor}"
initializeGlobalVariable "initTitleViewForContext" "${blueColor}"
initializeGlobalVariable "initTitleViewForSection" "${blueColor}"
initializeGlobalVariable "initTitleViewForSubsection" "${blueColor}"
initializeGlobalVariable "initTextViewForParagraph" ""
initializeGlobalVariable "initTextViewForBlockQuote" "${openMajorQuote}"
initializeGlobalVariable "initTextViewForInlineQuote" "${openMinorQuote}"
initializeGlobalVariable "initTextViewForPre" ""
initializeGlobalVariable "initTextViewForCode" ""
initializeGlobalVariable "initTextViewForAbstract" ""
initializeGlobalVariable "initTextViewForResult" ""
initializeGlobalVariable "initTextViewForExplanation" ""

initializeGlobalVariable "closeTitleViewForDescribe" "${resetColor}"
initializeGlobalVariable "closeTitleViewForContext" "${resetColor}"
initializeGlobalVariable "closeTitleViewForSection" "${resetColor}"
initializeGlobalVariable "closeTitleViewForSubsection" "${resetColor}"
initializeGlobalVariable "closeTextViewForParagraph" ""
initializeGlobalVariable "closeTextViewForBlockQuote" "${closeMajorQuote}"
initializeGlobalVariable "closeTextViewForInlineQuote" "${closeMinorQuote}"
initializeGlobalVariable "closeTextViewForPre" ""
initializeGlobalVariable "closeTextViewForCode" ""
initializeGlobalVariable "closeTextViewForAbstract" ""
initializeGlobalVariable "closeTextViewForResult" ""
initializeGlobalVariable "closeTextViewForExplanation" ""

initializeGlobalVariable "initPageView" ""
initializeGlobalVariable "initHeaderView" ""
initializeGlobalVariable "initBodyView" ""

initializeGlobalVariable "closePageView" ""
initializeGlobalVariable "closeHeaderView" ""
initializeGlobalVariable "closeHeaderView" ""

initializeGlobalVariable "initTextViewForNewLine" "${newLineView}"

initializeGlobalVariable "initConsoleStyles" ""
initializeGlobalVariable "closeConsoleStyles" ""

initializeGlobalVariable "styleSheet"

initializeGlobalVariable "initDocumentConsoleStyle" "${initPageView}${initHeaderView}${initConsoleStyles}${styleSheet[@]}${closeConsoleStyles}${closeHeaderView}${initBodyView}"

initializeGlobalVariable "endDocumentConsoleStyle" "${closeHeaderView}${closePageView}"

#!${BASH}
cahConfiguration() {
  echo -e "Loading... Configuration 'cahConfiguration', version 0.1"
}


initializeGlobalVariable "validatedTheCodeWithTRUE" 0
initializeGlobalVariable "validatedTheCodeWithFALSE" 1
initializeGlobalVariable "appellationForSUCCESS" "SUCCESS"
initializeGlobalVariable "appellationForERROR" "ERROR"

initializeGlobalVariable "userFolder" "${PWD}"
initializeGlobalVariable "tempFolder" "/tmp"
initializeGlobalVariable "protectedFolder" "${HOME}/.ssh"

initializeGlobalVariable "ensureUserFolders" false
initializeGlobalVariable "showInitialHelp" false

initializeGlobalVariable "systemView" "view"
initializeGlobalVariable "systemPrint" "print"
initializeGlobalVariable "activeLanguage" false

initializeGlobalVariable "distribution"
initializeGlobalVariable "testingPath" "testing"
initializeGlobalVariable "languagesPath" "languages"

initializeGlobalVariable "stylesPath" "styles"
initializeGlobalVariable "typessetingConsoleStyle" "bashColor"
initializeGlobalVariable "typessetingWebStyle" "html"

initializeGlobalVariable "aliasTomlCaption" "alias"
initializeGlobalVariable "runbookBrokenTomlCaption" "runbookBroken"

initializeGlobalVariable "tagAndTitle"

initializeGlobalVariable "exceptTheGroupTests"
initializeGlobalVariable "onlyTheGroupTests"
initializeGlobalVariable "considerLaunchAllTests"
initializeGlobalVariable "acceptedWords"

initializeGlobalAlias "messageReturnedByFunction"
initializeGlobalAlias "helpDictionary"
initializeGlobalAlias "extendedHelpDictionary"
initializeGlobalAlias "exampleHelpDictionary"
initializeGlobalAlias "optionsHelpDictionary"

initializeGlobalVariable "viewedTheFirstTitleOcurrency" false
initializeGlobalVariable "viewedTheFirstAbstractOcurrency" false

initializeGlobalArray "summaryMessages"
initializeGlobalArray "titlesOfFailedTests"
initializeGlobalVariable "messageCounterERRORS" 0
initializeGlobalVariable "messageCounterSUCCESS" 0

initializeGlobalVariable "theRunbookIsBrokenIs" false
initializeGlobalVariable "theRunbookPauseIs" false

initializeGlobalVariable "hideTheTitleMessage"
initializeGlobalVariable "hideTheAbstractMessage"
initializeGlobalVariable "hideTheResultMessage"
initializeGlobalVariable "hideTheExplanationMessage"

initializeGlobalArray "hideDivisionsInSpecification"
initializeGlobalArray "hideDivisionsInUsersInvolved"
initializeGlobalArray "hideDivisionsInSelectLanguage"
initializeGlobalArray "hideDivisionsInEnsureTool"
initializeGlobalArray "hideDivisionsInUninstallTool"
initializeGlobalArray "hideDivisionsInRefreshRepository"
initializeGlobalArray "hideDivisionsInUpdateAllPackages"
initializeGlobalArray "hideDivisionsInEnsureRepository"
initializeGlobalArray "hideDivisionsInEraseRepository"
initializeGlobalArray "hideDivisionsInSetUpDistro"
initializeGlobalArray "hideDivisionsInEnsureManagementPanel"
initializeGlobalArray "hideDivisionsInEraseManagementPanel"
initializeGlobalArray "hideDivisionsInCopyManagementPanel"
initializeGlobalArray "hideDivisionsInMoveManagementPanel"
initializeGlobalArray "hideDivisionsInEnsureBriefingTask"
initializeGlobalArray "hideDivisionsInEraseBriefingTask"
initializeGlobalArray "hideDivisionsInEnsureRecipeIngredient"


initializeGlobalArray "listOfSpecifications"
initializeGlobalVariable "LastTask" false
initializeGlobalVariable "LastTaskApproved"

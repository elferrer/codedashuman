#!${BASH}
default() {
  echo -e "Loading... Configuration 'default', version 0.1"
}

unset globalShow
globalShow=true

unset defaultDistribution
unset defaultLanguage
unset defaultTypeOfLanguage
unset defaultConsoleStyle
unset defaultWebStyle
unset defaultSummaryFile

defaultDistribution="neon"
defaultLanguage="spanish"
defaultTypeOfLanguage="informal"
defaultConsoleStyle="bashColor"
defaultWebStyle="html"
defaultCss=""
defaultSummaryFile="summary"

unset sendMessagesToTheServer
unset waitForResponse
unset sendToPort
unset sendToHost
unset getFromPort
unset getFromHost
unset sentToPath

sendMessagesToTheServer=true
waitForResponse=0
sendToPort=1500
sendToHost='localhost'
getFromPort=1500
getFromHost='localhost'
sentToPath="/live/"

unset projectToken
unset taskIdentifier

projectToken="unspecifiedToken"
taskIdentifier="unidentifiedTask"

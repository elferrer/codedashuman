#!${BASH}
userConfiguration() {
  echo -e "Loading... Configuration 'userConfiguration', version 0.1"
}


showInitialHelp=false

typessetingConsoleStyle="bashColor"
typessetingWebStyle="html"

userFolder="${PWD}"
tempFolder="/tmp"
protectedFolder="${HOME}/.ssh"

initializeGlobalVariable "showTitleMessages" true
initializeGlobalVariable "showAbstractMessages" true
initializeGlobalVariable "showResultMessages" true
initializeGlobalVariable "showExplanationMessages" true
initializeGlobalVariable "showSpecificationsSummary" true
initializeGlobalVariable "showExpectSUCCESS" true
initializeGlobalVariable "showExpectERROR" true

hideTheTitleMessage=false
hideTheAbstractMessage=false
hideTheResultMessage=false
hideTheExplanationMessage=false

hideDivisionsInSetUpDistro=(false)
hideDivisionsInUsersInvolved=(false)
hideDivisionsInSpecification=(false)
hideDivisionsInSelectLanguage=("result" "abstract" "explanation")
hideDivisionsInEnsureTool=(false)
hideDivisionsInUninstallTool=(false)
hideDivisionsInRefreshRepository=(false)
hideDivisionsInUpdateAllPackages=(false)
hideDivisionsInEnsureRepository=(false)
hideDivisionsInEraseRepository=(false)
hideDivisionsInEnsureManagementPanel=(false)
hideDivisionsInEraseManagementPanel=(false)
hideDivisionsInCopyManagementPanel=(false)
hideDivisionsInMoveManagementPanel=(false)
hideDivisionsInEnsureBriefingTask=(false)
hideDivisionsInEraseBriefingTask=(false)
hideDivisionsInEnsureRecipeIngredient=(false)
